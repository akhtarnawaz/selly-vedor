<?php
return [
    'public_path' => 'public/',
    'title' => 'Best Online Grocery Shopping in Lahore, Pakistan | Selly.pk',
    'categories' => [
        'fruits',
        'vegetables',
    ],
    'sub_categories' => [
        'New Items' => 'latest_',
        'Popular Items' => 'special_',
        'Best Seller' => 'bestseller_',
    ],
    'urls'=>[
        'get-categories' => '/getCategories',
        'get-products' => '/getProducts'
    ],
    'unit_prefix' => 'Rs ',

];