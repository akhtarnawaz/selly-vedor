<?php

return [

    'models' => [


        /*
        /*When Making Relationships with Products , we need to know witch model to use
        /* to retieve results.
        */

        'products' => App\Models\Products::class,

        /*
        /*When Making Relationships of Products or images with Categories , we need to know witch model to use
        /* to retieve results.
        */

        'categories' => App\Models\Categories::class,


        /*
        /*When Making Relationships with Users , we need to know witch model to use
        /* to retieve results.
        */

        'users' => App\Users::class,


        /*
        /*When Making Relationships with Users , we need to know witch model to use
        /* to retieve results.
        */

        'CategoryImages' => App\Models\CategoryImages::class,


        /*
        /*When Making Relationships with FeaturedProducts , we need to know witch model to use
        /* to retieve results.
        */


        'FeaturedProducts' => App\Models\FeaturedProducts::class,

        /*
        /*When Making Relationships with FeaturedProducts , we need to know witch model to use
        /* to retieve results.
        */

        'ProductImages' => App\Models\ProductImages::class,

        /*
        /*When Making Relationships with FeaturedProducts , we need to know witch model to use
        /* to retieve results.
        */

        'Customers' => App\Models\Customers::class,

        /*
        /*When Making Relationships with FeaturedProducts , we need to know witch model to use
        /* to retieve results.
        */

        'Orders' => App\Models\Orders::class,


    ],

    'table_names' => [

        /*
         * The table that the application uses for users.
         */

        'users' => 'users',

        /*
         * The table that the application uses for users.
         */

        'products' => 'products',

        /*
         * The table that the application uses for categories.
         */

        'categories' => 'categories',

        /*
         * The table that the application uses for categories.
         */

        'CategoryImages' => 'category_images',

        /*
         * The table that the application uses for join of products and categories.
         */

        'ProductCategories' => 'category_products',

        /*
         * The table that the application uses for join of products and categories.
         */

        'FeaturedProducts' => 'featured_products',

        /*
         * The table that the application uses for join of products and categories.
         */

        'ProductImages' => 'product_images',

        /*
         * The table that the application uses for customers.
         */

        'customers' => 'customers',

        /*
         * The table that the application uses for orders.
         */

        'orders' => 'orders',
    ],

    'foreign_keys' => [

        /*
         * The name of the foreign key to the users table.
         */
        'users' => 'user_id',
    ],
];
