<?php

use Illuminate\Database\Seeder;

class ProducsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('products')->insert([
            'product_title' => str_random(10),
            'marketPrice' => rand(10,100),
        ]);
    }
}
