<?php
Route::get('/admin', 'admin\DashboardController@index')->name('home');
Route::get('/admin/dashboard', 'admin\DashboardController@index')->name('home');
//----------------------------- Categories Management ---------------------------------


Route::post('/admin/updateOrderLocation', 'admin\DashboardController@updateOrderLocation');

Route::post('/admin/store-management/updatlocation', 'admin\DashboardController@updatlocation');


Route::group(['middleware' => ['clearance:Categories']], function () {

    Route::get('/admin/categories-management/categories', 'admin\CategoriesController@listings');
    Route::get('/admin/categories-management/add-categories', 'admin\CategoriesController@index');
    Route::post('/admin/categories-management/addCategories', 'admin\CategoriesController@addCategories');
    Route::get('/admin/categories-management/update/{id}', 'admin\CategoriesController@updatelisting');
    Route::get('/admin/categories-management/statusupdate/{id}', 'admin\CategoriesController@updatestatus');
    Route::get('/admin/categories-management/updatestatusone/{id}', 'admin\CategoriesController@updatestatusone');
    Route::post('/admin/categories-management/updatecate/{id}', 'admin\CategoriesController@update');
    Route::post('/admin/categories-management/updCategories/{id}', 'admin\CategoriesController@updateCategory');
    Route::get('/admin/categories-management/delCategory/{id}', 'admin\CategoriesController@deleteCategories');
    Route::post('/admin/categories-management/delMultipleCategory', 'admin\CategoriesController@deleteMultipleCategories');

});
//-------------------------------------------------------------------------------------
//----------------------------- Products Management -----------------------------------
Route::group(['middleware' => ['clearance:Products']], function () {

    Route::get('/admin/products-management/products', 'admin\ProductsController@listings');
    Route::get('/admin/products-management/update-stock/{id}', 'admin\ProductsController@updatestock');
    Route::get('/admin/products-management/add-products', 'admin\ProductsController@index');
    Route::post('/admin/products-management/addProducts', 'admin\ProductsController@addProducts');
    Route::get('/admin/products-management/statusupdate/{id}', 'admin\ProductsController@updatestatus');
    Route::get('/admin/products-management/updatestatusone/{id}', 'admin\ProductsController@updatestatusone');
    Route::get('/admin/products-management/updateProducts/{id}', 'admin\ProductsController@listingProducts');
    //Route::post('/admin/products-management/updateproduct/{id}', 'admin\ProductsController@update');
    Route::post('/admin/products-management/updProducts/{id}', 'admin\ProductsController@updateProducts');
    Route::get('/admin/products-management/delProducts/{id}', 'admin\ProductsController@deleteProducts');
    Route::post('/admin/products-management/delmultipleProduct', 'admin\ProductsController@deletemultipleProducts');
    Route::get('/admin/products-management/product-update/cat', 'admin\ProductsController@setCategory');
    Route::match(['get', 'post'], '/admin/products-management/products/searchProducts', 'admin\ProductsController@searchProducts');

});
//----------------------------- Stock Management -----------------------------------
Route::group(['middleware' => ['clearance:stock']], function () {


    Route::get('/admin/stock-management/stock', 'admin\StockController@stockListings');
    Route::get('/admin/stock-management/add-purchase', 'admin\StockController@addPurchaseHistory');
    Route::post('/admin/stock-management/addPurchase', 'admin\StockController@addPurchaseEntry');
});

Route::get('/admin/stock-management/purchase-history/{name}', 'admin\StockController@purchaseHistory');

Route::post('/admin/stock-management/getProductInfo', 'admin\StockController@getProductInfo');
Route::post('/admin/stock-management/getProductUnitPrice', 'admin\StockController@getProductUnitPrice');
Route::post('/admin/stock-management/addStoreStock', 'admin\StockController@addStoreStock');

Route::group(['middleware' => ['clearance:Waste and Lose']], function () {


    Route::get('/admin/stock-management/waste-loss-stock', 'admin\StockController@addWasteLoss');
    Route::get('/admin/stock-management/wastByDate', 'admin\StockController@wastByDate');
    Route::post('/admin/stock-management/addWasteLossStock', 'admin\StockController@addWasteLossEntry');

    Route::get('/admin/stock-management/wasteHistory/{name}', 'admin\StockController@wasteHistory');


    Route::post('/admin/stock-management/StockExcel', 'admin\StockController@StockExcel');

    Route::post('/admin/stock-management/WasteExcel', 'admin\StockController@WasteExcel');

    Route::post('/admin/stock-management/stockReport', 'admin\StockController@stockReport');

});

//----------------------------- Marketing Management -----------------------------------

Route::group(['middleware' => ['clearance:New Launch/Premier']], function () {

    Route::get('/admin/marketing/new-launch-premier', 'admin\MarketingController@newLaunch');
    Route::get('/admin/marketing/add-slider', 'admin\MarketingController@addSlider');
    Route::post('/admin/marketing/addSlider', 'admin\MarketingController@addSliderEntry');
    Route::get('/admin/marketing/updateSlider/{id}', 'admin\MarketingController@updateSliderEntry');
    Route::post('/admin/marketing/updateSlider', 'admin\MarketingController@updateSlider');
    Route::get('/admin/marketing/deleteSlider/{id}', 'admin\MarketingController@deleteSliderEntry');
    Route::get('/admin/marketing/statusEnable/{id}', 'admin\MarketingController@statusEnable');
    Route::get('/admin/marketing/statusDisable/{id}', 'admin\MarketingController@statusDisable');
    Route::get('/admin/marketing/add-images', 'admin\MarketingController@addImages');
    Route::post('/admin/marketing/addImages', 'admin\MarketingController@addImagesEntry');
    Route::get('/admin/marketing/update-images/{id}', 'admin\MarketingController@updateImages');
    Route::post('/admin/marketing/update-images', 'admin\MarketingController@updateImagesPremier');


    Route::get('/admin/marketing/delete-images/{id}', 'admin\MarketingController@deleteImages');
    Route::get('/admin/marketing/statusEnableImage1/{id}', 'admin\MarketingController@statusEnableImage1');
    Route::get('/admin/marketing/statusDisableImage1/{id}', 'admin\MarketingController@statusDisableImage1');

    Route::get('/admin/marketing/statusEnableImage2/{id}', 'admin\MarketingController@statusEnableImage2');
    Route::get('/admin/marketing/statusDisableImage2/{id}', 'admin\MarketingController@statusDisableImage2');

    Route::get('/admin/marketing/statusEnableImage3/{id}', 'admin\MarketingController@statusEnableImage3');
    Route::get('/admin/marketing/statusDisableImage3/{id}', 'admin\MarketingController@statusDisableImage3');

    Route::get('/admin/marketing/statusEnableImage4/{id}', 'admin\MarketingController@statusEnableImage4');
    Route::get('/admin/marketing/statusDisableImage4/{id}', 'admin\MarketingController@statusDisableImage4');

    Route::post('/admin/marketing/addBanners', 'admin\MarketingController@addBannersEntry');
    Route::post('/admin/marketing/addBannersNew', 'admin\MarketingController@addBannersEntryNew');

});
Route::group(['middleware' => ['clearance:Category Banners']], function () {

    Route::get('/admin/marketing/category-banners', 'admin\MarketingController@categoryBanners');
    Route::get('/admin/marketing/add-banners', 'admin\MarketingController@addBanners');

    Route::get('/admin/marketing/DeleteBanners/{id}', 'admin\MarketingController@DeleteBanners');
    Route::get('/admin/marketing/UpdateBanners/{id}', 'admin\MarketingController@UpdateBanners');
    Route::post('/admin/marketing/UpdateBanners', 'admin\MarketingController@UpdateCategoryBanners');
    //-- status update--
    Route::get('/admin/marketing/horizentalStatusupdateEnable/{id}', 'admin\MarketingController@horizentalStatusupdateEnable');
    Route::get('/admin/marketing/horizentalStatusupdateDisable/{id}', 'admin\MarketingController@horizentalStatusupdateDisable');
    Route::get('/admin/marketing/verticleStatusupdateEnable/{id}', 'admin\MarketingController@verticleStatusupdateEnable');
    Route::get('/admin/marketing/verticleStatusupdateDisable/{id}', 'admin\MarketingController@verticleStatusupdateDisable');


});
//----------------------------- Users Management ----------------------------------------
//Route::post('/admin/users-management/pages', 'admin\UsersController@paginate');

//Route::match(['get', 'post'],'/admin/users-management/users', 'admin\UsersController@listings');

Route::group(['middleware' => ['clearance:User List']], function () {

    Route::get('/admin/users-management/users', 'admin\UsersController@listings');
    Route::get('/admin/users-management/add-users', 'admin\UsersController@index');
    Route::post('/admin/users-management/addUsers', 'admin\UsersController@addUsers');
    Route::get('/admin/users-management/updUsers/{id}', 'admin\UsersController@updateUser');
    Route::post('/admin/users-management/updateUsers/{id}', 'admin\UsersController@updateUsers');
    Route::get('/admin/users-management/delUsers/{id}', 'admin\UsersController@deleteUsers');
    Route::post('/admin/users-management/deleteMultipleUsers', 'admin\UsersController@deleteMultiple');

    Route::get('/admin/users-management/usersvalidation', 'admin\UsersController@usersvalidation');
    Route::get('/admin/users-management/usersnamevalidation', 'admin\UsersController@usersnamevalidation');
    Route::get('/admin/users-management/validateupdat', 'admin\UsersController@validateupdat');
    Route::get('/admin/users-management/validateupdatename', 'admin\UsersController@validateupdatename');

    Route::get('/admin/users-management/statusEnable/{id}', 'admin\UsersController@statusEnable');
    Route::get('/admin/users-management/statusDisable/{id}', 'admin\UsersController@statusDisable');

    //Route::get('/admin/users-management/searchkeyword', 'admin\UsersController@searchkeyword');
    //Route::get('/admin/users-management/statussearch', 'admin\UsersController@statussearch');

    Route::match(['get', 'post'], '/admin/users-management/users/searchUsers', 'admin\UsersController@searchUsers');

});

Route::get('/admin/users-management/resetpassword', 'admin\UsersController@resetpassword');
Route::post('/admin/users-management/updatepassword', 'admin\UsersController@updatepassword');

//----------------------------- Orders Management --------------------------------------
Route::group(['middleware' => ['clearance:Orders list']], function () {

    Route::get('/admin/orders-management/orders/{order}', 'admin\OrdersController@index');

    Route::match(['get', 'post'], '/admin/orders-management/order/searchAllOrders/{title}', 'admin\OrdersController@searchAllOrders');
//    Route::post('/admin/orders-management/order/searchAllOrders', 'admin\OrdersController@searchAllOrders');

    Route::get('/admin/orders-management/unpaid-orders/{order}', 'admin\OrdersController@index');
    Route::match(['get', 'post'], '/admin/orders-management/order/searchUnPaidOrders/{title}', 'admin\OrdersController@searchAllOrders');
//    Route::post('/admin/orders-management/order/searchUnPaidOrders', 'admin\OrdersController@searchUnpaidOrders');

    Route::get('/admin/orders-management/paid-orders/{order}', 'admin\OrdersController@index');

    Route::get('/admin/orders-management/users-orders/{id}', 'admin\OrdersController@userOrders');

    Route::post('/admin/orders-management/addDiscount', 'admin\OrdersController@addDiscount');


    Route::match(['get', 'post'], '/admin/orders-management/order/searchPaidOrders/{title}', 'admin\OrdersController@searchAllOrders');
//    Route::post('/admin/orders-management/order/searchPaidOrders', 'admin\OrdersController@searchPaidOrders');


    Route::group(['middleware' => ['clearance:OrderDelete']], function () {


        Route::get('/admin/orders-management/delete/{id}', 'admin\OrdersController@singledelete');
        Route::post('/admin/orders-management/deleteorders', 'admin\OrdersController@multipledelete');
    });

    Route::get('/admin/orders-management/OrdersDetail/{id}', 'admin\OrdersController@OrdersDetail');
    Route::post('/admin/orders-management/productdiscount', 'admin\OrdersController@productdiscount');
    Route::post('/admin/orders-management/SendOrdersEmail', 'admin\OrdersController@SendOrdersDetailEmail');
    Route::post('/admin/orders-management/setAddress', 'admin\OrdersController@setAddress');
    Route::post('/admin/orders-management/addNewOrders/{id}', 'admin\OrdersController@addNewOrders');
    //--staff note
    Route::post('/admin/orders-management/staffNote', 'admin\OrdersController@staffNote');

    Route::post('/admin/orders-management/order', 'admin\OrdersController@addCustomorder');
    Route::post('/admin/orders-management/order/addCommission', 'admin\OrdersController@addCommission');
    Route::post('/admin/orders-management/order/setPayStatus', 'admin\NewOrderController@setPaymentStatus');
    Route::post('/admin/orders-management/order/setFulfilStatus', 'admin\NewOrderController@fulfillmentStatusChangeRequest');
    Route::post('/admin/orders-management/order/setSubFulfilStatus', 'admin\NewOrderController@fulfillmentSubStatusChangeRequest');
    Route::post('/admin/orders-management/order/setProcessor', 'admin\OrdersController@setProcessedBy');
    Route::post('/admin/orders-management/order/setDeliverBoy', 'admin\OrdersController@setDeliveredBy');
    Route::post('/admin/orders-management/order/search-orders', 'admin\OrdersController@searchAllOrders');

    //-------- order delete----
    Route::get('/admin/orders-management/order/deleteOrders/{id}', 'admin\OrdersController@deleteOrders');

    Route::post('/admin/orders-management/deleteOrExportorders', 'admin\OrdersController@deleteOrExportorders');


});

Route::group(['middleware' => ['clearance:Order Statuses']], function () {

    Route::get('/admin/orders-management/order-statuses', 'admin\OrdersStatusesController@index');
    Route::get('/admin/orders-management/add-order-status', 'admin\OrdersStatusesController@add');
    Route::get('/admin/orders-management/update-order-status/{id}', 'admin\OrdersStatusesController@update');
    Route::get('/admin/orders-management/update-order-shipment-status/{id}', 'admin\OrdersStatusesController@shipmentupdate');
    Route::post('/admin/orders-management/addOrderStatus', 'admin\OrdersStatusesController@addStatus');
    Route::post('/admin/orders-management/updateOrderpaymentStatus/{id}', 'admin\OrdersStatusesController@updatePaymentStatus');
    Route::post('/admin/orders-management/updateOrdershipmentStatus/{id}', 'admin\OrdersStatusesController@updateOrdershipmentStatus');
    Route::get('/admin/orders-management/deletepayment/{id}', 'admin\OrdersStatusesController@deletepayment');
    Route::get('/admin/orders-management/deleteshipment/{id}', 'admin\OrdersStatusesController@deleteshipment');

});

//-------------- Order Taker -------------------------
Route::group(['middleware' => ['clearance:Take Order']], function () {

    Route::get('/admin/order-{form}', 'admin\OrdersTakerController@index');
    Route::post('/admin/orders-management/getProductInfo', 'admin\OrdersTakerController@getProductInfo');
    Route::post('/admin/orders-management/getunitInfo', 'admin\OrdersTakerController@getunitInfo');
    Route::post('/admin/orders-management/place-order', 'admin\NewOrderController@placeOrder');
    Route::post('/admin/orders-management/searchUser', 'admin\OrdersTakerController@searchCustomer');
    Route::get('/admin/orders-management/customerdetail', 'admin\OrdersTakerController@customerdetail');


    Route::post('/admin/orders-management/setNewUnit', 'admin\OrdersTakerController@setNewUnit');

    Route::get('/admin/orders-management/order-taker/cat', 'admin\OrdersController@setCategory');
    Route::get('/admin/orders-management/order-taker/product', 'admin\OrdersController@setproduct');


//----customer management----------
    Route::get('/admin/customer-management/customer', 'admin\OrdersController@customerDetail');
    Route::get('/admin/customer-management/deletecustomer/{id}', 'admin\OrdersController@Deletecustomer');
    Route::match(['post', 'get'], '/admin/customer-management/searchcustomer', 'admin\OrdersController@searchcustomer');


    Route::post('/admin/customer-management/DeleteMultiplecustomer', 'admin\OrdersController@DeleteMultiplecustomer');
});


//-------------- Reports management -------------------------


Route::group(['middleware' => ['clearance:Order Stats']], function () {
    Route::get('/admin/reports/orders-stats', 'admin\ReportsController@ordersStats');
    Route::get('/admin/reports/stock-report/{id}', 'admin\ReportsController@stockReport');
    Route::get('/admin/orders-management/getProductunitInfo', 'admin\ReportsController@getProductunitInfo');
    Route::get('/admin/reports/waste-report', 'admin\ReportsController@wasteReport');
    Route::get('/admin/reports/getwaste-report', 'admin\ReportsController@getwasteReport');
    Route::post('/admin/reports/getwaste-report', 'admin\ReportsController@getwasteReport');
    Route::get('/admin/reports/ordersource', 'admin\ReportsController@ordersource');
    Route::post('/admin/reports/ordersourceReportExcel', 'admin\ReportsController@ordersourceReportExcel');
    Route::post('/admin/reports/ordersource/Search', 'admin\ReportsController@ordersourceSearch');
});

Route::group(['middleware' => ['clearance:itemized-report']], function () {
    Route::get('/admin/reports/item-report', 'admin\ReportsController@itemReport');
    Route::get('/admin/reports/itemized-report', 'admin\ReportsController@itemizedReport');
    Route::get('/admin/reports/newitem-report', 'admin\ReportsController@newitemReport');
    Route::get('/admin/reports/newitemized-report', 'admin\ReportsController@newitemizedReport');
});
Route::group(['middleware' => ['clearance:permission_log_report']], function () {
    Route::get('/admin/reports/logReport', 'admin\NewReportsController@getLogReport');
    Route::post('/admin/reports/getLogReport', 'admin\NewReportsController@getLogData');
    Route::get('/admin/reports/stockReport', 'admin\NewReportsController@getStockReport');
    Route::post('/admin/reports/getStockReport', 'admin\NewReportsController@getStockData');


    Route::get('/admin/reports/categoryLogReport', 'admin\NewReportsController@categoryLogReport');
    Route::post('/admin/reports/getcategoryLogData', 'admin\NewReportsController@getcategoryLogData');
});

//---------------------- Reviews Management ---------------------------------

Route::group(['middleware' => ['clearance:Reviews']], function () {

    Route::get('/admin/reviews-management/reviews', 'admin\ReviewsController@index');
    Route::get('/admin/reviews-management/add-reviews', 'admin\ReviewsController@add');
    Route::post('/admin/reviews-management/addreviews', 'admin\ReviewsController@addReviews');

    Route::get('/admin/reviews-management/enableStatus/{id}', 'admin\ReviewsController@enableStatus');
    Route::get('/admin/reviews-management/disableStatus/{id}', 'admin\ReviewsController@disableStatus');
    Route::get('/admin/reviews-management/delete/{id}', 'admin\ReviewsController@deleteStatus');
    Route::post('/admin/reviews-management/deleteMutilpleReviews', 'admin\ReviewsController@deleteMutilpleReviews');
    Route::get('/admin/reviews-management/updreviewlisting/{id}', 'admin\ReviewsController@update');
    Route::post('/admin/reviews-management/updateReviews', 'admin\ReviewsController@updateReviews');
});
//
//Route::get('/admin/products-management/stock', 'admin\ProductsController@stock');
//Route::get('/admin/products-management/productunit', 'admin\ProductsController@productunit');
//Route::get('/admin/products-management/categoryValue', 'admin\ProductsController@categoryValue');
//Route::get('/admin/products-management/productorders', 'admin\ProductsController@productorders');
//Route::post('/admin/products-management/updatestock', 'admin\ProductsController@updatestocks');

//---------------------- Pages Contents ------------------------


Route::group(['middleware' => ['clearance:Pages']], function () {

    Route::get('/admin/pages/listings', 'admin\PagesController@listings');
    Route::get('/admin/pages/add', 'admin\PagesController@addPage');
    Route::post('/admin/pages/add', 'admin\PagesController@add');
    Route::get('/admin/pages/update/{id}', 'admin\PagesController@updatePage');
    Route::post('/admin/pages/update', 'admin\PagesController@update');
    Route::get('/admin/pages/enbStatus/{id}', 'admin\PagesController@updatestatustoenb');
    Route::get('/admin/pages/disabStatus/{id}', 'admin\PagesController@updatestatustodis');
    Route::get('/admin/pages/delPage/{id}', 'admin\PagesController@deletePages');
});
Route::group(['middleware' => ['clearance:Module Access Permission']], function () {
    Route::get('/admin/users-access-module/roles', 'admin\RoleController@roles');
    Route::post('/admin/users-access-module/addRoles', 'admin\RoleController@addRoles');

    Route::get('/admin/users-access-module/editRoles', 'admin\RoleController@editRoles');
    Route::post('/admin/users-access-module/updateRoles', 'admin\RoleController@updateRoles');
    Route::get('/admin/users-access-module/deleteRoles/{id}', 'admin\RoleController@deleteRoles');

    Route::get('/admin/users-access-module/permissions', 'admin\PermissionController@permissions');
    Route::post('/admin/users-access-module/addPermissions', 'admin\PermissionController@addPermissions');
    Route::get('/admin/users-access-module/editPermissions', 'admin\PermissionController@editPermissions');
    Route::post('/admin/users-access-module/updatePermissions', 'admin\PermissionController@updatePermissions');
    Route::get('/admin/users-access-module/deletePermissions/{id}', 'admin\PermissionController@deletePermissions');
    Route::get('/admin/users-access-module/getPermissions', 'admin\PermissionController@getPermissions');
    Route::get('/admin/users-access-module/getUserPermissions', 'admin\PermissionController@getUserPermissions');
    Route::post('/admin/users-access-module/assignPermissionsToRole', 'admin\PermissionController@assignPermissionsToRole');
    Route::post('/admin/users-access-module/assignPermissionsToUser', 'admin\PermissionController@assignPermissionsToUser');

    Route::get('/admin/users-access-module/assignUserRole', 'admin\UsersController@assignUserRole');
    Route::get('/admin/users-access-module/assignUserPermission', 'admin\UsersController@assignUserPermission');
    Route::post('/admin/users-access-module/loadUserRole', 'admin\UsersController@loadUserRole');
    Route::post('/admin/users-access-module/assignRoleToUser', 'admin\UsersController@assignRoleToUser');

});


Route::group(['middleware' => ['clearance:Store Location']], function () {
    Route::get('/admin/store-management/Store', 'admin\LocationController@index');
    Route::get('/admin/store-management/StoreLocation', 'admin\LocationController@StoreLocation');
    Route::post('/admin/store-management/addStoreLocation', 'admin\LocationController@addStoreLocation');

    Route::get('/admin/store-management/updatStoreLocation/{id}', 'admin\LocationController@updatStoreLocation');
    Route::post('/admin/store-management/updatStore', 'admin\LocationController@updatStore');

    Route::get('/admin/store-management/delete/{id}', 'admin\LocationController@delete');

});
    Route::group(['middleware' => ['clearance:Shops']], function () {

    Route::get('/admin/shops', 'admin\LocationController@shops');
    Route::get('/admin/shops/add', 'admin\LocationController@shopAddView');
    Route::post('/admin/shops/add', 'admin\LocationController@addShop');

    Route::get('/admin/shops/edit/{id}', 'admin\LocationController@editShop');
    Route::post('/admin/shops/updat', 'admin\LocationController@updatShop');

    Route::get('/admin/shops-management/delete/{id}', 'admin\LocationController@deleteShop');

});


Route::get('/admin/orders-management/csrSupervioserReport', 'admin\ReportsController@csrSupervioserReport');
Route::post('/admin/orders-management/csvSupervioserReport', 'admin\ReportsController@csvSupervioserReport');
Route::get('/admin/orders-management/bulckWasteReport', 'admin\ReportsController@bulckWasteReport');
Route::post('/admin/orders-management/WasteReportdata', 'admin\ReportsController@WasteReportdata');
Route::get('/admin/orders-management/totalSaleReport', 'admin\ReportsController@totalSaleReport');
Route::post('/admin/orders-management/totalSaleReportdata', 'admin\ReportsController@totalSaleReportdata');

Route::post('/admin/setting/siteOnOff', 'SettingsController@siteOnOff');

// check new order
Route::get('/checkneworder', 'admin\NewOrderController@checkneworder');
Route::get('/admin/schedule/order', 'admin\NewOrderController@scheduleorder');

Route::get('/admin/profile-management/profile', 'admin\UsersController@profile');

Route::post('/admin/profile-management/addprofile', 'admin\UsersController@addprofile');


?>