<?php
//Route::get('api/getCategories',function ()
//{
//    return Utility::getCategoriesApi();
//});

Route::get('api/getCategories',function ()
{
    return Utility::getCategoriesApi();
});

Route::get('api/getProductssApi',function ()
{
    return Utility::getProductssApi();
});

//
//Route::post('api/registerCustomer','Auth\RegisterController@registerCustomer')->name('registerCustomer');

Route::match(['get', 'post'],'api/registerCustomerr/{username}/{email}/{password}',function ($username,$email,$password)
{

    return Utility::getRegisterApi($username,$email,$password);
});

Route::match(['get', 'post'],'api/registerCustomerSocial/{username}/{email}/{password}',function ($username,$email,$password)
{

    return Utility::re($username,$email,$password);
});


Route::get('api/addtoorder/{user_id}' , function ($string){



    return Utility::putjsonOrder($string);

} );
//Route::get('api/addtoorder/{user_id}/{cart_total}/{cart_subtotal}/{storeLocation}' , function ($user_id,$cart_total,$cart_subtotal,$storeLocation){
//
//
//
//    return Utility::addinorder($user_id,$cart_total,$cart_subtotal,$storeLocation);
//
//} );

Route::get('api/addItemsOrder/{coustomer_number}/{product_id}/{product_qty}',function($order_id,$product_id,$product_qty){

    return Utility::apiaddinOrder($order_id,$product_id,$product_qty);
});

Route::get('api/login', function()
{
    // $html = view('auth/login')->render();
    $token = csrf_token();

    return Response::json(array('token' => $token)); //This only to verify if i am extracting the right token.
});

Route::get('api/loginCustomerr/{username}/{password}',function ($username,$password)
{
    return Utility::getLoginApi($username,$password);
});


Route::get('api/updateCustomerr/{id}/{username}/{phone}/{address}/{ordernumber}',function ($id,$name,$phone,$address,$orderNumber)
{
    return Utility::apiUpdateUser($id,$name,$phone,$address,$orderNumber);
});
Route::get('api/passwordReset/{email}',function ($email)
{
    return Utility::apiforgotPasword($email);
});


Route::match(['get','post'],'api/getLocationApi',function ()
{
    return Utility::getLocationApi();
});

Route::get('api/userProcessingOrder/{id}',function ($userId)
{
    return Utility::userProcessingOrder($userId);
});

Route::get('api/getCSRF', function()
{
    // $html = view('auth/login')->render();
    $token = csrf_token();

    return Response::json(array('token' => $token)); //This only to verify if i am extracting the right token.
});


Route::get('api/userOrderList/{id}',function ($userId)
{
    return Utility::userOrderList($userId);
});
Route::get('api/userOrderItemList/{id}',function ($oredrid)
{
    return Utility::userOrderItemList($oredrid);
});
Route::get('api/addtoorder2/{user_id}/{json_order}' , function ($user_id,$json_order){
    return Utility::putjsonOrder2($user_id,$json_order);

} );
Route::get('api/addtoorder3/{user_id}/{json_order}' , function ($user_id,$json_order){
    return Utility::putjsonOrder3($user_id,$json_order);

} );

// IOS appilaction Api

Route::get('api/getProductsIos',function ()
{
    return Utility::getProductsIos();
});