@extends('templates.layout')
@section('content')
    <div class="">
        <div class="row mt-15">
            <div class="helpful_links_leftside">
                <div class="col-md-3 mt-15 pl-0">
                    <div class="clearfix">
                        <ul>
                            <li>
                                <a href="{{url('about-us')}}">About Us</a>
                            </li>
                            <li>
                                <a href="{{url('privacy-policy')}}">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="{{url('return-policy')}}">Return Policy</a>
                            </li>
                            <li>
                                <a href="{{url('terms-conditions')}}">Terms & Conditions</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9-custom-width80">
                <div class="col-md-9 pr-0">
                    <div class="content-top-breadcum">
                        <div class="container" style="display: block">
                            <div class="row">
                                <div id="title-content">
                                    <h1 class="page-title">Return Policy
                                    </h1>
                                    <ul class="breadcrumb">
                                        <li><a href=""><i class="fa fa-home"></i></a></li>
                                        <li><a href="">Return Policy</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="return_policy_page clearfix">
                        {!! $ReturnPolicy->body !!}
                        {{--<p><a href="Selly.pk">Selly.pk</a> strives to deliver best quality products to its customers. Our return policy is simple and customer friendly.</p>--}}
                        {{--<p>Once the rider delivers to you the fruits or vegetables, you can check their quality and if you are not satisfied, you can immediately return them to the rider, NO QUESTIONS ASKED!</p>--}}
                        {{--<p>Furthermore, if you were satisfied with the quality of the produce at the time of receiving the product, but somehow you want to return it later, you can call us at 0304-111-7355 (Sell) or send an email to <a href="mailto:support@selly.pk">support@selly.pk</a>. A rider will pick up the fruits and vegetables from your house and return the money back to you. You do not need to pay any transportation charges or other fee.</p>--}}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
@stop