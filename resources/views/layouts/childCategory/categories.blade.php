@foreach($categories as $item)
    @php
    $title = $item->category_title;
         $image = $item->category_icon;
    @endphp
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 gallery_img_grids text-center">
        <a href="{{url('/'.strtolower($item->cleanURL))}}">
            <div class="box-border">
                <div class="image-grid">
                    <img src="{{Utility::getImage('category/'.$image)}}" title="{{$image}}"
                         alt="{{$image}}" class="img-responsive reg-image">
                    <div class="image-grid-effect">
                        <div class="wthree_text">
                            <h3>{{$title}}</h3>
                        </div>
                    </div>
                </div>
                <p>{{$title}}</p>
            </div>
        </a>
    </div>
@endforeach