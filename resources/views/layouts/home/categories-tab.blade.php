<div class="home-appliances clearfix">
    <div class="row category-block">
        <div class="text-center">



            @foreach($categories as $item)
                @php
                    $title = $item->category_title;
                    $image = $item->category_icon;

                @endphp


                <div class="col-md-3 col-sm-4 col-xs-12 gallery_img_grids mb-20">
                    <a href="{{url('/'.strtolower($item->cleanURL))}}">
                        <div class="box-border">
                            <div class="image-grid">
                                <img src="{{Utility::getImage('category/'.$image)}}" title="{{$title}}" alt="{{$title}}" class="img-responsive reg-image">
                            <!-- <div class="image-grid-effect">
                                        <div class="wthree_text">
                                            <h3>{{$title}}</h3>
                                        </div>
                                    </div> -->
                            </div>
                            <p>{{$title}}</p>
                        </div>
                    </a>
                </div>

            @endforeach



        </div>
    </div>
</div>