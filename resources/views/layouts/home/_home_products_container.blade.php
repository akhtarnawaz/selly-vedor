@php($count=0)
<div class="img-prd">
    <div class="hometab box">
        <div class="col-md-3 pr-0">
            <div class="banner-img">
                <div class="">
                    <img src="{{Utility::getImage('catalog/banner.png')}}" title="Aliquam Ac Neque"
                         alt="Aliquam Ac Neque"
                         class="img-responsive reg-image">
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="tab-head">
                <div class="hometab-heading box-heading">{{strtoupper($title)}}</div>
                {{--<div id="{{$title}}" class="htabs">--}}
                <div id="tab-{{$title}}" class="htabs tabs-catagory">
                    <ul class='etabs'>
                        @foreach(Config::get('constants.sub_categories') as $key=>$value)
                            @php($count++)
                            <li class='tab {{$count==1?'active':''}}'>
                                <a href="#tab-{{$value.$title}}" data-toggle="tab">{{$key}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>

            @php($count=0)
            <div class="tab-content">
                @foreach(Config::get('constants.sub_categories') as $key=>$value)
                    @php($count++)
                    <div id="tab-{{$value.$title}}">
                        <div class="box">
                            <div class="box-content">
                                <div class="customNavigation">
                                    <a class="fa prev"></a>
                                    <a class="fa next"></a>
                                </div>

                                <div class="owl-carousel custom_carousel box-product" id="products_{{$value.$title}}">

                                </div>
                            </div>

                        </div>
                        <span class="tablatest_default_width" style="display:none; visibility:hidden"></span>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $('#tab-{{$title}} a').tabs();

    {{--$( document ).ready(function() {--}}
        {{--url = '{{url(\Illuminate\Support\Facades\Config::get('constants.urls.get-products'))}}';--}}
        {{--$('products_{{$value.$title}}').html(getProducts(url));--}}
    {{--});--}}

</script>