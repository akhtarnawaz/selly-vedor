
<div class="img-prd">
    <div class="hometab box">

        <div class="col-md-12">
            <div class="tab-head fruits-section">

                <h1 class="hometab-heading box-heading">Premium Products</h1>


            </div>
            <div class="tab-content">
                <div id="latestItemGroceries" class="tab-pane fade in active mt-0">
                    <div class="box">
                        <div class="box-content">
                            <div class="customNavigation">
                                <a class="fa prev"></a>
                                <a class="fa next"></a>
                            </div>
                            <div class="owl-carousel custom_carousel box-product" id="carousel_premium" >

@include('layouts.home.products-block');
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <span class="tablatest_default_width" style="display:none; visibility:hidden"></span>
                </div>

            </div>
        </div>
    </div>
</div>