@php
    use App\Models\Products;
    use App\Models\Categories;
      use App\Models\Admin\Units;
 @endphp
{{--all product popular item --}}

<div class="img-prd">
    <div class="hometab box">

        <div class="col-md-12">
            <div class="tab-head fruits-section">

                <h1 class="hometab-heading box-heading">Premium Products</h1>


            </div>
            <div class="tab-content">
                <div id="latestItemGroceries" class="tab-pane fade in active mt-0">
                    <div class="box">
                        <div class="box-content">
                            <div class="customNavigation">
                                <a class="fa prev"></a>
                                <a class="fa next"></a>
                            </div>
                            <div class="owl-carousel custom_carousel box-product" id="carousel_premium" >
                                @php
                                     $products=Products::where('premiumProducts','=','1')->where('status','=','1')->where('quantity_in_stock','>',0)->orderBy('product_title','DESC')->get();

                                @endphp
                                @foreach($products as $prod)
                                    @php
                                        $categoryTitle = Categories::where('id',$prod->category_id)->first()->cleanURL;

                                    @endphp
                                    <div class="slider-item">
                                        <div class="product-block product-thumb {{$prod->quantity_in_stock<=0?'product-disable':''}} transition">
                                            <div class="product-block-inner">
                                                <div class="image ">
                                                    <a href="{{url('')}}/{{$categoryTitle.'/'.$prod->cleanURL}}.html">
                                                    <img src="{{Utility::getImage('products/')}}/{{$prod->product_image}}"
                                                         title="{{$prod->product_title}}" alt="{{$prod->product_title}}"
                                                         class="img-responsive reg-image"/>
                                                    <img class="img-responsive hover-image"
                                                         src="{{Utility::getImage('products/')}}/{{$prod->product_image}}"
                                                         title="{{$prod->product_title}}"
                                                         alt="{{$prod->product_title}}"/>
                                                    </a>
                                                    <div class="button-gr lists">
                                                        <div class="quickview-button">
                                                        </div>
                                                    </div>
                                                    <div class="rating">
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                    </div>
                                                </div>
                                                <div class="product-details">
                                                    <div class="caption">
                                                        <h4 class="name">
                                                            {{$prod->product_title}}
                                                        </h4>
                                                        <p class="price">
                                                            @php
                                                                $on_sale=$prod->on_sales;
                                                                $salePriceValue=$prod->salePriceValue;
                                                                $marketPrice=$prod->marketPrice;
                                                                $units=Units::join('products','products_unit.product_id','=','products.id')
                                                                ->where('products.id','=',$prod->id)->pluck('products_unit.unit')->first();
                                                            @endphp
                                                            @if(isset($on_sale) && $on_sale=="1")
                                                                <span class="price-new">Rs {{number_format($salePriceValue,2)}}
                                                                    /{{$units}}</span> <span
                                                                        class="price-old">Rs {{number_format($marketPrice,2)}}
                                                                    /{{$units}}</span>
                                                            @else
                                                                <span class="price-new">Rs {{number_format($salePriceValue,2)}}/{{$units}}</span>
                                                            @endif
                                                            <span class="price-tax">Ex Tax: $100.00</span>
                                                        </p>
                                                        <div class="button-group">
                                                            <button type="button" class="addtocart"
                                                                    {{$prod->quantity_in_stock<=0?'disabled':''}}
                                                                    onclick="cart.add('{{$prod->id}}',1);"><i
                                                                        class="fa fa-shopping-cart"></i><span
                                                                        class="hidden-xs hidden-sm hidden-md">Add to Cart</span>
                                                            </button>
                                                            <button class="wishlist" type="button" data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('48');"><i
                                                                        class="fa fa-heart"></i></button>
                                                            <button class="compare" type="button" data-toggle="tooltip"
                                                                    title="Add to Compare" onclick="compare.add('48');">
                                                                <i class="fa fa-exchange"></i></button>
                                                        </div>
                                                        {{--<p class="item-out-of-stock">{{$prod->quantity_in_stock<=0?'Out of Stock':''}}</p>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <span class="tablatest_default_width" style="display:none; visibility:hidden"></span>
                </div>

            </div>
        </div>
    </div>
</div>
 @php

    $categor=Categories::all();
   $cat= $categor->where('category_title','=','Vegetables')->pluck('id')->first();
$product=Products::where('status','=',1)->where('quantity_in_stock','>',0)->get();
     $units=Units::all();
$popularItems=Products::where('popular_items','=','1')->where('status','=',1)->where('quantity_in_stock','>',0)->where('category_id','=',$cat)->get();
$bestSellers=Products::where('best_sellers','=','1')->where('status','=',1)->where('quantity_in_stock','>',0)->where('category_id','=',$cat)->get();

$products=$product->where('category_id','=',$cat);

$categoryTitle = Categories::where('id',$cat)->first()->cleanURL;

@endphp
<div class="img-prd">
    <div class="hometab box">
        <div class="col-md-12">
            <div class="tab-head">
                <h2 class="hometab-heading box-heading">Vegetables</h2>
                <div id="tabs" class="htabs">
                    <ul class='etabs'>
                        <li class='tab'>
                            <a href="#tab-latest">New Items</a>
                        </li>
                        <li class='tab'>
                            <a href="#tab-special">Popular Items</a>
                        </li>
                        <li class='tab'>
                            <a href="#tab-bestseller">Best Seller</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="tab-content">
                <div id="tab-latest">
                    <div class="box">
                        <div class="box-content">
                            <div class="customNavigation">
                                <a class="fa prev"></a>
                                <a class="fa next"></a>
                            </div>
                            <div class="owl-carousel custom_carousel box-product" id="carousel_1">
                                @foreach($products as $prod)
                                    <div class="slider-item">
                                        <div class="product-block product-thumb {{$prod->quantity_in_stock<=0?'product-disable':''}} transition">
                                            <div class="product-block-inner">
                                                <div class="image {{$prod->quantity_in_stock<=0?'blur_item':''}}">
                                                    <a href="{{url('')}}/{{$categoryTitle.'/'.$prod->cleanURL}}.html">

                                                        <img src="{{Utility::getImage('products/')}}/{{$prod->product_image}}"
                                                             title="{{$prod->product_title}}" alt="{{$prod->product_title}}"
                                                             class="img-responsive reg-image"/>
                                                        <img class="img-responsive hover-image"
                                                             src="{{Utility::getImage('products/')}}/{{$prod->product_image}}"
                                                             title="{{$prod->product_title}}"
                                                             alt="{{$prod->product_title}}"/>
                                                    </a>
                                                    <div class="button-gr lists">
                                                        <div class="quickview-button"></div>
                                                    </div>
                                                    <div class="rating">
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                    </div>
                                                </div>
                                                <div class="product-details">
                                                    <div class="caption">
                                                        <h4 class="name"><a href="{{url('')}}/{{$categoryTitle.'/'.$prod->cleanURL}}.html"
                                                                            title="{{$prod->product_title}}">{{$prod->product_title}}</a>
                                                        </h4>
                                                        <p class="price">
                                                            @php
                                                                $on_sale=$prod->on_sales;
                                                                $salePriceValue=$prod->salePriceValue;
                                                                $marketPrice=$prod->marketPrice;
                                                              $unit=$units->where('product_id','=',$prod->id)->pluck('unit')->first();
                                                            @endphp
                                                            @if(isset($on_sale) && $on_sale=="1")
                                                                <span class="price-new">Rs {{number_format($salePriceValue,2)}}//{{$unit}}</span>
                                                                <span
                                                                        class="price-old">Rs {{number_format($marketPrice,2)}}</span>
                                                            @else
                                                                <span class="price-new">Rs {{number_format($salePriceValue,2)}}/{{$unit}}</span>
                                                            @endif
                                                            <span class="price-tax">Ex Tax: $100.00</span>
                                                        </p>
                                                        <div class="button-group">
                                                            <button type="button" class="addtocart"
                                                                    {{$prod->quantity_in_stock<=0?'disabled':''}}
                                                                    onclick="cart.add('{{$prod->id}}',1);"><i
                                                                        class="fa fa-shopping-cart"></i><span
                                                                        class="hidden-xs hidden-sm hidden-md">Add to Cart</span>
                                                            </button>
                                                            <button class="wishlist" type="button" data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('48');"><i
                                                                        class="fa fa-heart"></i></button>
                                                           <button class="compare" type="button" data-toggle="tooltip"
                                                                    title="Add to Compare" onclick="compare.add('48');">
                                                                <i class="fa fa-exchange"></i></button>
                                                        </div>
                                                        <p class="item-out-of-stock">{{$prod->quantity_in_stock<=0?'Out of Stock':''}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <span class="tablatest_default_width" style="display:none; visibility:hidden"></span>
                </div>
                <div id="tab-special">
                    <div class="box">
                        <div class="box-content">
                            <div class="customNavigation">

                            </div>
                            <div class="owl-carousel custom_carousel box-product" id="carousel_11">
                                @foreach($popularItems as $popular)
                                    @if($popular->status ==1 && $popular->quantity_in_stock >0)
                                    <div class="slider-item">
                                        <div class="product-block product-thumb {{$prod->quantity_in_stock<=0?'product-disable':''}} transition">
                                            <div class="product-block-inner">
                                                <div class="image {{$prod->quantity_in_stock<=0?'blur_item':''}}">
                                                    <a href="{{url('')}}/{{$popular->cleanURL}}.html">

                                                        <img src="{{Utility::getImage('products/')}}/{{$popular->product_image}}"
                                                             title="{{$popular->product_title}}" alt="{{$popular->product_title}}"
                                                             class="img-responsive reg-image"/>
                                                        <img class="img-responsive hover-image"
                                                             src="{{Utility::getImage('products/')}}/{{$popular->product_image}}"
                                                             title="{{$popular->product_title}}"
                                                             alt="{{$popular->product_title}}"/>
                                                    </a>
                                                    <div class="button-gr lists">
                                                        <div class="quickview-button"></div>
                                                    </div>
                                                    <div class="rating">
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                    </div>
                                                </div>
                                                <div class="product-details">
                                                    <div class="caption">
                                                        <h4 class="name"><a href="{{url('')}}/{{$categoryTitle.'/'.$prod->cleanURL}}.html"
                                                                            title="{{$prod->product_title}}">{{$popular->product_title}}</a>
                                                        </h4>
                                                        <p class="price">
                                                            @php
                                                                $on_sale=$popular->on_sales;
                                                                $salePriceValue=$popular->salePriceValue;
                                                                $marketPrice=$popular->marketPrice;
                                                              $unit=$units->where('product_id','=',$prod->id)->pluck('unit')->first();
                                                            @endphp
                                                            @if(isset($on_sale) && $on_sale=="1")
                                                                <span class="price-new">Rs {{number_format($salePriceValue,2)}}//{{$unit}}</span>
                                                                <span
                                                                        class="price-old">Rs {{number_format($marketPrice,2)}}/{{$unit}}</span>
                                                            @else

                                                                <span class="price-new">Rs {{number_format($salePriceValue,2)}}/{{$unit}}</span>
                                                            @endif
                                                            <span class="price-tax">Ex Tax: $100.00</span>
                                                        </p>
                                                        <div class="button-group">
                                                            <button type="button" class="addtocart"
                                                                    {{$prod->quantity_in_stock<=0?'disabled':''}}
                                                                    onclick="cart.add('{{$popular->id}}',1);"><i
                                                                        class="fa fa-shopping-cart"></i><span
                                                                        class="hidden-xs hidden-sm hidden-md">Add to Cart</span>
                                                            </button>
                                                            <button class="wishlist" type="button" data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('48');"><i
                                                                        class="fa fa-heart"></i></button>
                                                           <button class="compare" type="button" data-toggle="tooltip"
                                                                    title="Add to Compare" onclick="compare.add('48');">
                                                                <i class="fa fa-exchange"></i></button>
                                                        </div>
                                                        <p class="item-out-of-stock">{{$prod->quantity_in_stock<=0?'Out of Stock':''}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <span class="tablatest_default_width" style="display:none; visibility:hidden"></span>
                </div>
                <div id="tab-bestseller">
                    <div class="box">
                        <div class="box-content">
                            <div class="customNavigation">
                                <a class="fa prev"></a>
                                <a class="fa next"></a>
                            </div>
                            <div class="owl-carousel custom_carousel box-product" id="carousel_12">
                                @foreach($bestSellers as $bestSeller)
                                    @if($bestSeller->status ==1 && $bestSeller->quantity_in_stock >0)
                                    <div class="slider-item">
                                        <div class="product-block product-thumb {{$prod->quantity_in_stock<=0?'product-disable':''}} transition">
                                            <div class="product-block-inner">
                                                <div class="image {{$prod->quantity_in_stock<=0?'blur_item':''}}">
                                                    <a href="{{url('')}}/{{$bestSeller->cleanURL}}.html">

                                                        <img src="{{Utility::getImage('products/')}}/{{$bestSeller->product_image}}"
                                                             title="{{$bestSeller->product_title}}" alt="{{$bestSeller->product_title}}"
                                                             class="img-responsive reg-image"/>
                                                        <img class="img-responsive hover-image"
                                                             src="{{Utility::getImage('products/')}}/{{$bestSeller->product_image}}"
                                                             title="{{$bestSeller->product_title}}"
                                                             alt="{{$bestSeller->product_title}}"/>
                                                    </a>
                                                    <div class="button-gr lists">
                                                        <div class="quickview-button"></div>
                                                    </div>
                                                    <div class="rating">
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                    </div>
                                                </div>
                                                <div class="product-details">
                                                    <div class="caption">
                                                        <h4 class="name"><a href="{{url('')}}/{{$categoryTitle.'/'.$prod->cleanURL}}.html"
                                                                            title="{{$prod->product_title}}">{{$bestSeller->product_title}}</a>
                                                        </h4>
                                                        <p class="price">
                                                            @php
                                                                $on_sale=$bestSeller->on_sales;
                                                                $salePriceValue=$bestSeller->salePriceValue;
                                                                $marketPrice=$bestSeller->marketPrice;
                                                              $unit=$units->where('product_id','=',$prod->id)->pluck('unit')->first();
                                                            @endphp
                                                            @if(isset($on_sale) && $on_sale=="1")
                                                                <span class="price-new">Rs {{number_format($salePriceValue,2)}}/{{$unit}}</span>
                                                                <span
                                                                        class="price-old">Rs {{number_format($marketPrice,2)}}/{{$unit}}</span>
                                                            @else
                                                                <span class="price-new">Rs {{number_format($salePriceValue,2)}}/{{$unit}}</span>
                                                            @endif
                                                            <span class="price-tax">Ex Tax: $100.00</span>
                                                        </p>
                                                        <div class="button-group">
                                                            <button type="button" class="addtocart"
                                                                    {{$prod->quantity_in_stock<=0?'disabled':''}}
                                                                    onclick="cart.add('{{$bestSeller->id}}',1);"><i
                                                                        class="fa fa-shopping-cart"></i><span
                                                                        class="hidden-xs hidden-sm hidden-md">Add to Cart</span>
                                                            </button>
                                                            <button class="wishlist" type="button" data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('48');"><i
                                                                        class="fa fa-heart"></i></button>
                                                           <button class="compare" type="button" data-toggle="tooltip"
                                                                    title="Add to Compare" onclick="compare.add('48');">
                                                                <i class="fa fa-exchange"></i></button>
                                                        </div>
                                                        <p class="item-out-of-stock">{{$prod->quantity_in_stock<=0?'Out of Stock':''}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <span class="tablatest_default_width" style="display:none; visibility:hidden"></span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="img-prd">
    <div class="hometab box">

        <div class="col-md-12">
            <div class="tab-head fruits-section">
                <h2 class="hometab-heading box-heading">Fruits</h2>
                <div id="tabs1" class="htabs">
                    <ul class="nav etabs">
                        <li class="active"><a data-toggle="tab" href="#latestItem">New Items</a></li>
                        <li><a data-toggle="tab" href="#specialItem">Popular Items</a></li>
                        <li><a data-toggle="tab" href="#bestsellerItem">Best Seller</a></li>

                    </ul>
                </div>

            </div>
            <div class="tab-content">
                <div id="latestItem" class="tab-pane fade in active mt-0">
                    @php

                        $categor=Categories::all();
                       $cat= $categor->where('category_title','=','Fruits')->pluck('id')->first();
                    $product=Products::where('status','=',1)->where('quantity_in_stock','>',0)->get();
                    $products=$product->where('category_id','=',$cat);

                     $categoryTitle = Categories::where('id',$cat)->first()->cleanURL;
                    $units=Units::all();
                    $popularItems=Products::where('popular_items','=','1')->where('status','=',1)->where('quantity_in_stock','>',0)->where('category_id','=',$cat)->get();
                    $bestSellers=Products::where('best_sellers','=','1')->where('status','=',1)->where('quantity_in_stock','>',0)->where('category_id','=',$cat)->get();
                    @endphp
                    <div class="box">
                        <div class="box-content">
                            <div class="customNavigation">
                                <a class="fa prev"></a>
                                <a class="fa next"></a>
                            </div>
                            <div class="owl-carousel custom_carousel box-product" id="carousel_2">

                                @foreach($products as $prod)
                                    <div class="slider-item">
                                        <div class="product-block product-thumb {{$prod->quantity_in_stock<=0?'product-disable':''}} transition">
                                            <div class="product-block-inner">
                                                <div class="image {{$prod->quantity_in_stock<=0?'blur_item':''}}">

                                                    <a href="{{url('')}}/{{$categoryTitle.'/'.$prod->cleanURL}}.html">

                                                        <img src="{{Utility::getImage('products/')}}/{{$prod->product_image}}"
                                                             title="{{$prod->product_title}}" alt="{{$prod->product_title}}"
                                                             class="img-responsive reg-image"/>
                                                        <img class="img-responsive hover-image"
                                                             src="{{Utility::getImage('products/')}}/{{$prod->product_image}}"
                                                             title="{{$prod->product_title}}"
                                                             alt="{{$prod->product_title}}"/>
                                                    </a>
                                                    <div class="button-gr lists">
                                                        <div class="quickview-button">
                                                            {{--<a class="quickview-button" href="indexdd37.html?route=product/quick_view&amp;product_id=48">Quick View</a>--}}
                                                        </div>
                                                    </div>
                                                    <div class="rating">
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                    </div>
                                                </div>
                                                <div class="product-details">
                                                    <div class="caption">
                                                        <h4 class="name"><a href="{{url('')}}/{{$categoryTitle.'/'.$prod->cleanURL}}.html"
                                                                            title="{{$prod->product_title}}">{{$prod->product_title}}</a>
                                                        </h4>
                                                        <p class="price">
                                                            @php
                                                                $on_sale=$prod->on_sales;
                                                                $salePriceValue=$prod->salePriceValue;
                                                                $marketPrice=$prod->marketPrice;
                                                            $unit=$units->where('product_id','=',$prod->id)->pluck('unit')->first();
                                                            @endphp
                                                            @if(isset($on_sale) && $on_sale=="1")
                                                                <span class="price-new">Rs {{number_format($salePriceValue,2)}}
                                                                    /{{$unit}}</span> <span
                                                                        class="price-old">Rs {{number_format($marketPrice,2)}}
                                                                    /{{$unit}}</span>
                                                            @else
                                                                <span class="price-new">Rs {{number_format($salePriceValue,2)}}/{{$unit}}</span>
                                                            @endif
                                                            <span class="price-tax">Ex Tax: $100.00</span>
                                                        </p>
                                                        <div class="button-group">
                                                            <button type="button" class="addtocart"
                                                                    {{$prod->quantity_in_stock<=0?'disabled':''}}
                                                                    onclick="cart.add('{{$prod->id}}',1);"><i
                                                                        class="fa fa-shopping-cart"></i><span
                                                                        class="hidden-xs hidden-sm hidden-md">Add to Cart</span>
                                                            </button>
                                                            <button class="wishlist" type="button" data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('48');"><i
                                                                        class="fa fa-heart"></i></button>
                                                           <button class="compare" type="button" data-toggle="tooltip"
                                                                    title="Add to Compare" onclick="compare.add('48');">
                                                                <i class="fa fa-exchange"></i></button>
                                                        </div>
                                                        <p class="item-out-of-stock">{{$prod->quantity_in_stock<=0?'Out of Stock':''}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <span class="tablatest_default_width" style="display:none; visibility:hidden"></span>
                </div>
                <div id="specialItem" class="tab-pane fade mt-0">
                    <div class="box">
                        <div class="box-content">
                            <div class="customNavigation">

                            </div>
                            <div class="owl-carousel custom_carousel box-product" id="carousel_21">
                                @foreach($popularItems as $popular)
                                    @if($popular->status ==1 && $popular->quantity_in_stock >0)
                                    <div class="slider-item">
                                        <div class="product-block product-thumb {{$prod->quantity_in_stock<=0?'product-disable':''}} transition">
                                            <div class="product-block-inner">
                                                <div class="image {{$prod->quantity_in_stock<=0?'blur_item':''}}">
                                                    <a href="{{url('')}}/{{$popular->cleanURL}}.html">

                                                        <img src="{{Utility::getImage('products/')}}/{{$popular->product_image}}"
                                                             title="{{$popular->product_title}}" alt="{{$popular->product_title}}"
                                                             class="img-responsive reg-image"/>
                                                        <img class="img-responsive hover-image"
                                                             src="{{Utility::getImage('products/')}}/{{$popular->product_image}}"
                                                             title="{{$popular->product_title}}"
                                                             alt="{{$popular->product_title}}"/>
                                                    </a>
                                                    <div class="button-gr lists">
                                                        <div class="quickview-button"></div>
                                                    </div>
                                                    <div class="rating">
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                    </div>
                                                </div>
                                                <div class="product-details">
                                                    <div class="caption">
                                                        <h4 class="name"><a href="{{url('')}}/{{$categoryTitle.'/'.$prod->cleanURL}}.html"
                                                                            title="{{$prod->product_title}}">{{$popular->product_title}}</a>
                                                        </h4>
                                                        <p class="price">
                                                            @php
                                                                $on_sale=$popular->on_sales;
                                                                $salePriceValue=$popular->salePriceValue;
                                                                $marketPrice=$popular->marketPrice;
                                                             $unit=$units->where('product_id','=',$prod->id)->pluck('unit')->first();
                                                            @endphp
                                                            @if(isset($on_sale) && $on_sale=="1")
                                                                <span class="price-new">Rs {{number_format($salePriceValue,2)}}//{{$unit}}</span>
                                                                <span
                                                                        class="price-old">Rs {{number_format($marketPrice,2)}}//{{$unit}}</span>
                                                            @else
                                                                <span class="price-new">Rs {{number_format($salePriceValue,2)}}/{{$unit}}</span>
                                                            @endif
                                                            <span class="price-tax">Ex Tax: $100.00</span>
                                                        </p>
                                                        <div class="button-group">
                                                            <button type="button" class="addtocart"
                                                                    {{$prod->quantity_in_stock<=0?'disabled':''}}
                                                                    onclick="cart.add('{{$popular->id}}',1);"><i
                                                                        class="fa fa-shopping-cart"></i><span
                                                                        class="hidden-xs hidden-sm hidden-md">Add to Cart</span>
                                                            </button>
                                                            <button class="wishlist" type="button" data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('48');"><i
                                                                        class="fa fa-heart"></i></button>
                                                           <button class="compare" type="button" data-toggle="tooltip"
                                                                    title="Add to Compare" onclick="compare.add('48');">
                                                                <i class="fa fa-exchange"></i></button>
                                                        </div>
                                                        <p class="item-out-of-stock">{{$prod->quantity_in_stock<=0?'Out of Stock':''}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <span class="tablatest_default_width" style="display:none; visibility:hidden"></span>
                </div>
                <div id="bestsellerItem" class="tab-pane fade mt-0">
                    <div class="box">
                        <div class="box-content">
                            <div class="customNavigation">
                                <a class="fa prev"></a>
                                <a class="fa next"></a>
                            </div>
                            <div class="owl-carousel custom_carousel box-product" id="carousel_22">
                                @foreach($bestSellers as $bestSeller)

                                    @if($bestSeller->status ==1 && $bestSeller->quantity_in_stock >0)
                                    <div class="slider-item">
                                        <div class="product-block product-thumb {{$prod->quantity_in_stock<=0?'product-disable':''}} transition">
                                            <div class="product-block-inner">
                                                <div class="image {{$prod->quantity_in_stock<=0?'blur_item':''}}">
                                                    <a href="{{url('')}}/{{$bestSeller->cleanURL}}.html">

                                                        <img src="{{Utility::getImage('products/')}}/{{$bestSeller->product_image}}"
                                                             title="{{$bestSeller->product_title}}" alt="{{$bestSeller->product_title}}"
                                                             class="img-responsive reg-image"/>
                                                        <img class="img-responsive hover-image"
                                                             src="{{Utility::getImage('products/')}}/{{$bestSeller->product_image}}"
                                                             title="{{$bestSeller->product_title}}"
                                                             alt="{{$bestSeller->product_title}}"/>
                                                    </a>
                                                    <div class="button-gr lists">
                                                        <div class="quickview-button"></div>
                                                    </div>
                                                    <div class="rating">
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                    </div>
                                                </div>
                                                <div class="product-details">
                                                    <div class="caption">
                                                        <h4 class="name"><a href="{{url('')}}/{{$categoryTitle.'/'.$prod->cleanURL}}.html"
                                                                            title="{{$prod->product_title}}">{{$bestSeller->product_title}}</a>
                                                        </h4>
                                                        <p class="price">
                                                            @php
                                                                $on_sale=$bestSeller->on_sales;
                                                                $salePriceValue=$bestSeller->salePriceValue;
                                                                $marketPrice=$bestSeller->marketPrice;
                                                             $unit=$units->where('product_id','=',$prod->id)->pluck('unit')->first();
                                                            @endphp
                                                            @if(isset($on_sale) && $on_sale=="1")
                                                                <span class="price-new">Rs {{number_format($salePriceValue,2)}}/{{$unit}}</span>
                                                                <span
                                                                        class="price-old">Rs {{number_format($marketPrice,2)}}/{{$unit}}</span>
                                                            @else
                                                                <span class="price-new">Rs {{number_format($salePriceValue,2)}}/{{$unit}}</span>
                                                            @endif
                                                            <span class="price-tax">Ex Tax: $100.00</span>
                                                        </p>
                                                        <div class="button-group">
                                                            <button type="button" class="addtocart"
                                                                    {{$prod->quantity_in_stock<=0?'disabled':''}}
                                                                    onclick="cart.add('{{$bestSeller->id}}',1);"><i
                                                                        class="fa fa-shopping-cart"></i><span
                                                                        class="hidden-xs hidden-sm hidden-md">Add to Cart</span>
                                                            </button>
                                                            <button class="wishlist" type="button" data-toggle="tooltip"
                                                                    title="Add to Wish List"
                                                                    onclick="wishlist.add('48');"><i
                                                                        class="fa fa-heart"></i></button>
                                                           <button class="compare" type="button" data-toggle="tooltip"
                                                                    title="Add to Compare" onclick="compare.add('48');">
                                                                <i class="fa fa-exchange"></i></button>
                                                        </div>
                                                        <p class="item-out-of-stock">{{$prod->quantity_in_stock<=0?'Out of Stock':''}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                        @endforeach

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <span class="tablatest_default_width" style="display:none; visibility:hidden"></span>
                </div>
            </div>
        </div>
    </div>
</div>

{{--<div class="img-prd">--}}
    {{--<div class="hometab box">--}}

        {{--<div class="col-md-12">--}}
            {{--<div class="tab-head fruits-section">--}}
                {{--<h3 class="hometab-heading box-heading">Groceries</h3>--}}
                {{--<div id="tabs1" class="htabs">--}}
                    {{--<ul class="nav etabs">--}}
                        {{--<li class="active"><a data-toggle="tab" href="#latestItemGroceries">New Items</a></li>--}}
                        {{--<li><a data-toggle="tab" href="#specialItemGroceries">Popular Items</a></li>--}}
                        {{--<li><a data-toggle="tab" href="#bestsellerItemGroceries">Best Seller</a></li>--}}

                    {{--</ul>--}}
                {{--</div>--}}

            {{--</div>--}}
            {{--<div class="tab-content">--}}
                {{--<div id="latestItemGroceries" class="tab-pane fade in active mt-0">--}}
                    {{--@php--}}

                        {{--$categor=Categories::all();--}}
                       {{--$cat= $categor->where('category_title','=','Grocery')->pluck('id')->first();--}}
                    {{--$product=Products::where('status','=',1)->where('quantity_in_stock','>',0)->get();--}}
                    {{--$products=$product->where('category_id','=',$cat);--}}

                    {{--$categoryTitle = Categories::where('id',$cat)->first()->cleanURL;--}}
                    {{--$units=Units::all();--}}
                    {{--$popularItems=Products::where('popular_items','=','1')->where('status','=',1)->where('quantity_in_stock','>',0)->where('category_id','=',$cat)->get();--}}
                    {{--$bestSellers=Products::where('best_sellers','=','1')->where('status','=',1)->where('quantity_in_stock','>',0)->where('category_id','=',$cat)->get();--}}
                    {{--@endphp--}}
                    {{--<div class="box">--}}
                        {{--<div class="box-content">--}}
                            {{--<div class="customNavigation">--}}
                                {{--<a class="fa prev"></a>--}}
                                {{--<a class="fa next"></a>--}}
                            {{--</div>--}}
                            {{--<div class="owl-carousel custom_carousel box-product" id="carousel_3" >--}}

                                {{--@foreach($products as $prod)--}}
                                    {{--<div class="slider-item">--}}
                                        {{--<div class="product-block product-thumb {{$prod->quantity_in_stock<=0?'product-disable':''}} transition">--}}
                                            {{--<div class="product-block-inner">--}}
                                                {{--<div class="image {{$prod->quantity_in_stock<=0?'blur_item':''}}">--}}

                                                    {{--<a href="{{url('')}}/{{$categoryTitle.'/'.$prod->cleanURL}}.html">--}}

                                                        {{--<img src="{{Utility::getImage('products/')}}/{{$prod->product_image}}"--}}
                                                             {{--title="Aliquam Ac Neque" alt="Aliquam Ac Neque"--}}
                                                             {{--class="img-responsive reg-image"/>--}}
                                                        {{--<img class="img-responsive hover-image"--}}
                                                             {{--src="{{Utility::getImage('products/')}}/{{$prod->product_image}}"--}}
                                                             {{--title="{{$prod->product_title}}"--}}
                                                             {{--alt="{{$prod->product_title}}"/>--}}
                                                    {{--</a>--}}
                                                    {{--<div class="button-gr lists">--}}
                                                        {{--<div class="quickview-button">--}}
                                                            {{--<a class="quickview-button" href="indexdd37.html?route=product/quick_view&amp;product_id=48">Quick View</a>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="rating">--}}
                                                        {{--<span class="fa fa-stack"><i--}}
                                                                    {{--class="fa fa-star off fa-stack-2x"></i></span>--}}
                                                        {{--<span class="fa fa-stack"><i--}}
                                                                    {{--class="fa fa-star off fa-stack-2x"></i></span>--}}
                                                        {{--<span class="fa fa-stack"><i--}}
                                                                    {{--class="fa fa-star off fa-stack-2x"></i></span>--}}
                                                        {{--<span class="fa fa-stack"><i--}}
                                                                    {{--class="fa fa-star off fa-stack-2x"></i></span>--}}
                                                        {{--<span class="fa fa-stack"><i--}}
                                                                    {{--class="fa fa-star off fa-stack-2x"></i></span>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="product-details">--}}
                                                    {{--<div class="caption">--}}
                                                        {{--<h4 class="name"><a href="{{url('')}}/{{$categoryTitle.'/'.$prod->cleanURL}}.html"--}}
                                                                            {{--title="{{$prod->product_title}}">{{$prod->product_title}}</a>--}}
                                                        {{--</h4>--}}
                                                        {{--<p class="price">--}}
                                                            {{--@php--}}
                                                                {{--$on_sale=$prod->on_sales;--}}
                                                                {{--$salePriceValue=$prod->salePriceValue;--}}
                                                                {{--$marketPrice=$prod->marketPrice;--}}
                                                            {{--$unit=$units->where('product_id','=',$prod->id)->pluck('unit')->first();--}}
                                                            {{--@endphp--}}
                                                            {{--@if(isset($on_sale) && $on_sale=="1")--}}
                                                                {{--<span class="price-new">Rs {{number_format($salePriceValue,2)}}--}}
                                                                    {{--/{{$unit}}</span> <span--}}
                                                                        {{--class="price-old">Rs {{number_format($marketPrice,2)}}--}}
                                                                    {{--/{{$unit}}</span>--}}
                                                            {{--@else--}}
                                                                {{--<span class="price-new">Rs {{number_format($salePriceValue,2)}}/{{$unit}}</span>--}}
                                                            {{--@endif--}}
                                                            {{--<span class="price-tax">Ex Tax: $100.00</span>--}}
                                                        {{--</p>--}}
                                                        {{--<div class="button-group">--}}
                                                            {{--<button type="button" class="addtocart"--}}
                                                                    {{--{{$prod->quantity_in_stock<=0?'disabled':''}}--}}
                                                                    {{--onclick="cart.add('{{$prod->id}}',1);"><i--}}
                                                                        {{--class="fa fa-shopping-cart"></i><span--}}
                                                                        {{--class="hidden-xs hidden-sm hidden-md">Add to Cart</span>--}}
                                                            {{--</button>--}}
                                                            {{--<button class="wishlist" type="button" data-toggle="tooltip"--}}
                                                                    {{--title="Add to Wish List"--}}
                                                                    {{--onclick="wishlist.add('48');"><i--}}
                                                                        {{--class="fa fa-heart"></i></button>--}}
                                                           {{--<button class="compare" type="button" data-toggle="tooltip"--}}
                                                                    {{--title="Add to Compare" onclick="compare.add('48');">--}}
                                                                {{--<i class="fa fa-exchange"></i></button>--}}
                                                        {{--</div>--}}
                                                        {{--<p class="item-out-of-stock">{{$prod->quantity_in_stock<=0?'Out of Stock':''}}</p>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--@endforeach--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<span class="tablatest_default_width" style="display:none; visibility:hidden"></span>--}}
                {{--</div>--}}
                {{--<div id="specialItemGroceries" class="tab-pane fade mt-0">--}}
                    {{--<div class="box">--}}
                        {{--<div class="box-content">--}}
                            {{--<div class="customNavigation">--}}

                            {{--</div>--}}
                            {{--<div class="owl-carousel custom_carousel box-product" id="carousel_31">--}}
                                {{--@foreach($popularItems as $popular)--}}
                                    {{--<div class="slider-item">--}}
                                        {{--<div class="product-block product-thumb {{$prod->quantity_in_stock<=0?'product-disable':''}} transition">--}}
                                            {{--<div class="product-block-inner">--}}
                                                {{--<div class="image {{$prod->quantity_in_stock<=0?'blur_item':''}}">--}}
                                                    {{--<a href="{{url('')}}/{{$popular->cleanURL}}.html">--}}

                                                        {{--<img src="{{Utility::getImage('products/')}}/{{$popular->product_image}}"--}}
                                                             {{--title="Aliquam Ac Neque" alt="Aliquam Ac Neque"--}}
                                                             {{--class="img-responsive reg-image"/>--}}
                                                        {{--<img class="img-responsive hover-image"--}}
                                                             {{--src="{{Utility::getImage('products/')}}/{{$popular->product_image}}"--}}
                                                             {{--title="{{$prod->product_title}}"--}}
                                                             {{--alt="{{$prod->product_title}}"/>--}}
                                                    {{--</a>--}}
                                                    {{--<div class="button-gr lists">--}}
                                                        {{--<div class="quickview-button"></div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="rating">--}}
                                                        {{--<span class="fa fa-stack"><i--}}
                                                                    {{--class="fa fa-star off fa-stack-2x"></i></span>--}}
                                                        {{--<span class="fa fa-stack"><i--}}
                                                                    {{--class="fa fa-star off fa-stack-2x"></i></span>--}}
                                                        {{--<span class="fa fa-stack"><i--}}
                                                                    {{--class="fa fa-star off fa-stack-2x"></i></span>--}}
                                                        {{--<span class="fa fa-stack"><i--}}
                                                                    {{--class="fa fa-star off fa-stack-2x"></i></span>--}}
                                                        {{--<span class="fa fa-stack"><i--}}
                                                                    {{--class="fa fa-star off fa-stack-2x"></i></span>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="product-details">--}}
                                                    {{--<div class="caption">--}}
                                                        {{--<h4 class="name"><a href="{{url('')}}/{{$categoryTitle.'/'.$prod->cleanURL}}.html"--}}
                                                                            {{--title="{{$prod->product_title}}">{{$popular->product_title}}</a>--}}
                                                        {{--</h4>--}}
                                                        {{--<p class="price">--}}
                                                            {{--@php--}}
                                                                {{--$on_sale=$popular->on_sales;--}}
                                                                {{--$salePriceValue=$popular->salePriceValue;--}}
                                                                {{--$marketPrice=$popular->marketPrice;--}}
                                                            {{--@endphp--}}
                                                            {{--@if(isset($on_sale) && $on_sale=="1")--}}
                                                                {{--<span class="price-new">Rs {{number_format($salePriceValue,2)}}/{{$unit}}}</span>--}}
                                                                {{--<span--}}
                                                                        {{--class="price-old">Rs {{number_format($marketPrice,2)}}/{{$unit}}</span>--}}
                                                            {{--@else--}}
                                                                {{--<span class="price-new">Rs {{number_format($salePriceValue,2)}}/{{$unit}}</span>--}}
                                                            {{--@endif--}}
                                                            {{--<span class="price-tax">Ex Tax: $100.00</span>--}}
                                                        {{--</p>--}}
                                                        {{--<div class="button-group">--}}
                                                            {{--<button type="button" class="addtocart"--}}
                                                                    {{--{{$prod->quantity_in_stock<=0?'disabled':''}}--}}
                                                                    {{--onclick="cart.add('{{$popular->id}}',1);"><i--}}
                                                                        {{--class="fa fa-shopping-cart"></i><span--}}
                                                                        {{--class="hidden-xs hidden-sm hidden-md">Add to Cart</span>--}}
                                                            {{--</button>--}}
                                                            {{--<button class="wishlist" type="button" data-toggle="tooltip"--}}
                                                                    {{--title="Add to Wish List"--}}
                                                                    {{--onclick="wishlist.add('48');"><i--}}
                                                                        {{--class="fa fa-heart"></i></button>--}}
                                                           {{--<button class="compare" type="button" data-toggle="tooltip"--}}
                                                                    {{--title="Add to Compare" onclick="compare.add('48');">--}}
                                                                {{--<i class="fa fa-exchange"></i></button>--}}
                                                        {{--</div>--}}
                                                        {{--<p class="item-out-of-stock">{{$prod->quantity_in_stock<=0?'Out of Stock':''}}</p>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--@endforeach--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<span class="tablatest_default_width" style="display:none; visibility:hidden"></span>--}}
                {{--</div>--}}
                {{--<div id="bestsellerItemGroceries" class="tab-pane fade mt-0">--}}
                    {{--<div class="box">--}}
                        {{--<div class="box-content">--}}
                            {{--<div class="customNavigation">--}}
                                {{--<a class="fa prev"></a>--}}
                                {{--<a class="fa next"></a>--}}
                            {{--</div>--}}
                            {{--<div class="owl-carousel custom_carousel box-product" id="carousel_32">--}}
                                {{--@foreach($bestSellers as $bestSeller)--}}
                                    {{--<div class="slider-item">--}}
                                        {{--<div class="product-block product-thumb {{$prod->quantity_in_stock<=0?'product-disable':''}} transition">--}}
                                            {{--<div class="product-block-inner">--}}
                                                {{--<div class="image {{$prod->quantity_in_stock<=0?'blur_item':''}}">--}}
                                                    {{--<a href="{{url('')}}/{{$bestSeller->cleanURL}}.html">--}}

                                                        {{--<img src="{{Utility::getImage('products/')}}/{{$bestSeller->product_image}}"--}}
                                                             {{--title="Aliquam Ac Neque" alt="Aliquam Ac Neque"--}}
                                                             {{--class="img-responsive reg-image"/>--}}
                                                        {{--<img class="img-responsive hover-image"--}}
                                                             {{--src="{{Utility::getImage('products/')}}/{{$bestSeller->product_image}}"--}}
                                                             {{--title="{{$prod->product_title}}"--}}
                                                             {{--alt="{{$prod->product_title}}"/>--}}
                                                    {{--</a>--}}
                                                    {{--<div class="button-gr lists">--}}
                                                        {{--<div class="quickview-button"></div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="rating">--}}
                                                        {{--<span class="fa fa-stack"><i--}}
                                                                    {{--class="fa fa-star off fa-stack-2x"></i></span>--}}
                                                        {{--<span class="fa fa-stack"><i--}}
                                                                    {{--class="fa fa-star off fa-stack-2x"></i></span>--}}
                                                        {{--<span class="fa fa-stack"><i--}}
                                                                    {{--class="fa fa-star off fa-stack-2x"></i></span>--}}
                                                        {{--<span class="fa fa-stack"><i--}}
                                                                    {{--class="fa fa-star off fa-stack-2x"></i></span>--}}
                                                        {{--<span class="fa fa-stack"><i--}}
                                                                    {{--class="fa fa-star off fa-stack-2x"></i></span>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="product-details">--}}
                                                    {{--<div class="caption">--}}
                                                        {{--<h4 class="name"><a href="{{url('')}}/{{$categoryTitle.'/'.$prod->cleanURL}}.html"--}}
                                                                            {{--title="{{$prod->product_title}}">{{$bestSeller->product_title}}</a>--}}
                                                        {{--</h4>--}}
                                                        {{--<p class="price">--}}
                                                            {{--@php--}}
                                                                {{--$on_sale=$bestSeller->on_sales;--}}
                                                                {{--$salePriceValue=$bestSeller->salePriceValue;--}}
                                                                {{--$marketPrice=$bestSeller->marketPrice;--}}
                                                            {{--@endphp--}}
                                                            {{--@if(isset($on_sale) && $on_sale=="1")--}}
                                                                {{--<span class="price-new">Rs {{number_format($salePriceValue,2)}}/{{$unit}}</span>--}}
                                                                {{--<span--}}
                                                                        {{--class="price-old">Rs {{number_format($marketPrice,2)}}/{{$unit}}</span>--}}
                                                            {{--@else--}}
                                                                {{--<span class="price-new">Rs {{number_format($salePriceValue,2)}}/{{$unit}}</span>--}}
                                                            {{--@endif--}}
                                                            {{--<span class="price-tax">Ex Tax: $100.00</span>--}}
                                                        {{--</p>--}}
                                                        {{--<div class="button-group">--}}
                                                            {{--<button type="button" class="addtocart"--}}
                                                                    {{--{{$prod->quantity_in_stock<=0?'disabled':''}}--}}
                                                                    {{--onclick="cart.add('{{$bestSeller->id}}',1);"><i--}}
                                                                        {{--class="fa fa-shopping-cart"></i><span--}}
                                                                        {{--class="hidden-xs hidden-sm hidden-md">Add to Cart</span>--}}
                                                            {{--</button>--}}
                                                            {{--<button class="wishlist" type="button" data-toggle="tooltip"--}}
                                                                    {{--title="Add to Wish List"--}}
                                                                    {{--onclick="wishlist.add('48');"><i--}}
                                                                        {{--class="fa fa-heart"></i></button>--}}
                                                           {{--<button class="compare" type="button" data-toggle="tooltip"--}}
                                                                    {{--title="Add to Compare" onclick="compare.add('48');">--}}
                                                                {{--<i class="fa fa-exchange"></i></button>--}}
                                                        {{--</div>--}}
                                                        {{--<p class="item-out-of-stock">{{$prod->quantity_in_stock<=0?'Out of Stock':''}}</p>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--@endforeach--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<span class="tablatest_default_width" style="display:none; visibility:hidden"></span>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}





<div class="hometab box">
</div>


<script>
    $(document).ready(function(){
        $('#carousel_premium').owlCarousel({
            items: 5,
            singleItem: false,
            navigation: false,
            navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
            pagination: false,
            itemsDesktop : [1200,5],
            itemsDesktopSmall :	[979,3],
            itemsTablet :	[767,2],
            itemsMobile : [479,1]
        });

        $("#carousel_premium").prev(".customNavigation").children("a.next").click(function(){
            $("#carousel_premium").trigger('owl.next');
        })
        $("#carousel_premium").prev(".customNavigation").children("a.prev").click(function(){
            $("#carousel_premium").trigger('owl.prev');
        })
    });
</script>