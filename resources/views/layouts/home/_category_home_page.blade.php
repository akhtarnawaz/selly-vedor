{{--This Layout is not in Use--}}
@foreach($content as $item)
    @php($title = $item->category_title;
         $image = $item->category_icon;
    )
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 gallery_img_grids">
        <a href="{{url('/'.strtolower($item->cleanURL))}}">
            <div class="box-border">
                <div class="image-grid">
                    <img src="{{Utility::getImage('catalog/'.$image)}}" title="Aliquam Ac Neque"
                         alt="Aliquam Ac Neque" class="img-responsive reg-image">
                    <div class="image-grid-effect">
                        <div class="wthree_text">
                            <h3>{{$title}}</h3>
                        </div>
                    </div>
                </div>
                <p>{{$title}}</p>
            </div>
        </a>
    </div>
@endforeach