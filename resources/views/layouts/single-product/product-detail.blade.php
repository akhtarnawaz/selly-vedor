<style>
    /*sprite with stars*/
    #reviewStars-input input:checked ~ label, #reviewStars-input label, #reviewStars-input label:hover, #reviewStars-input label:hover ~ label {
        background: url({{url('public/images/catalog/rating.png')}}) no-repeat;
    }

    #reviewStars-input {

        /*fix floating problems*/
        overflow: hidden;
        *zoom: 1;
        /*end of fix floating problems*/

        position: relative;
        float: left;
    }

    #reviewStars-input input {
        filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
        opacity: 0;

        width: 43px;
        height: 40px;

        position: absolute;
        top: 0;
        z-index: 0;

    }

    #reviewStars-input input:checked ~ label {
        background-position: 0 -40px;
        height: 40px;
        width: 43px;
    }

    #reviewStars-input label {
        background-position: 0 0;
        height: 40px;
        width: 43px;
        float: right;
        cursor: pointer;
        margin-right: 10px;

        position: relative;
        z-index: 1;
    }

    #reviewStars-input label:hover, #reviewStars-input label:hover ~ label {
        background-position: 0 -40px;
        height: 40px;
        width: 43px;
    }

    #reviewStars-input #star-0 {
        left: 0px;
    }

    #reviewStars-input #star-1 {
        left: 53px;
    }

    #reviewStars-input #star-2 {
        left: 106px;
    }

    #reviewStars-input #star-3 {
        left: 159px;
    }

    #reviewStars-input #star-4 {
        left: 212px;
    }

    #reviewStars-input #star-5 {
        left: 265px;
    }

    .rating-50 {
        background-position: 0 0;
    }

    .rating-40 {
        background-position: -12px 0;
    }

    .rating-30 {
        background-position: -24px 0;
    }

    .rating-20 {
        background-position: -36px 0;
    }

    .rating-10 {
        background-position: -48px 0;
    }

    .rating-0 {
        background-position: -60px 0;
    }

    .checked {
        color: orange;

    }

</style>
@php
    use App\Models\Admin\Units;

        $product_id = $item->id;
        $product_title = $item->product_title;
        $product_description = $item->product_description;
        $health_benifits = $item->health_benifits;
        $product_detail = $item->product_detail;
        $available_for_sales = $item->available_for_sales;
        $product_description = $item->product_description;
        $meta_keyword = $item->meta_keywords;
        $meta_descriptions = $item->meta_descriptions;
        $image = $item->product_image;
        $title = $item->product_page_title;

        $units=Units::where('product_id','=',$product_id)->get();

@endphp
<div class="productpage_details" xmlns:fb="http://www.w3.org/1999/xhtml">
    <div class="col-sm-6 product-left">
        <div class="product-info">
            <div class="left product-image thumbnails">
                <!-- Megnor Cloud-Zoom Image Effect Start -->
                <div class="simpleLens-gallery-container" id="demo-1">
                    <div class="simpleLens-container">
                        <div class="simpleLens-big-image-container">
                            <a class="simpleLens-lens-image" data-lens-image="{{Utility::getImage('products/'.$image)}}">
                                <img src="{{Utility::getImage('products/'.$image)}}" class="simpleLens-big-image" alt="{{$image}}">
                            </a>
                        </div>
                    </div>
                </div>
                {{--<div class="simpleLens-gallery-container" id="demo-1">--}}
                {{--<div class="simpleLens-container">--}}
                {{--<div class="simpleLens-big-image-container">--}}
                {{--<a class="simpleLens-lens-image"--}}
                {{--href="{{Utility::getImage('products/'.$image)}}"><img class="simpleLens-big-image"--}}
                {{--src="{{Utility::getImage('products/'.$image)}}"--}}
                {{--data-lens-image="{{Utility::getImage('products/'.$image)}}"--}}
                {{--title="{{$product_title}}"--}}
                {{--alt="{{$product_title}}">--}}
                {{--</a>--}}
                {{--<a class="simpleLens-lens-image" data-lens-image="{{Utility::getImage('products/'.$image)}}">--}}
                {{--<img src="{{Utility::getImage('products/'.$image)}}" class="simpleLens-big-image">--}}
                {{--</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="image easyzoom" id="zoom-new-img">--}}
                {{--<a class="thumbnail elevatezoom-gallery"--}}
                {{--href="{{Utility::getImage('products/'.$image)}}"><img id="tmzoom"--}}
                {{--src="{{Utility::getImage('products/'.$image)}}"--}}
                {{--data-zoom-image="{{Utility::getImage('products/'.$image)}}"--}}
                {{--title="{{$product_title}}"--}}
                {{--alt="{{$product_title}}">--}}
                {{--</a>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>


    <div class="col-sm-6 product-right">
        <h1 class="product-title">{{$product_title}}</h1>
        {{--<p id="product-qty_{{$product_id}}" class="item-out-of-stock">{{$unit->product_quantity<=0?'Out of Stock':''}}</p>--}}
        <br>

        {{--<ul class="list-unstyled price mb-0">--}}

        {{--<li><select class="product-price" style="width: 90px" id="product-price" onchange="setprice()">--}}
        {{--@foreach($units as $unit)--}}

        {{--<option> {{$unit->product_price}}/{{$unit->unit}}--}}
        {{--</option>--}}
        {{--@endforeach--}}
        {{--</select>--}}

        {{--</li>--}}

        {{--<h3> Rs:<span class="product-price" id="set-product-price"></span></h3>--}}

        {{--</ul>--}}
        @php($productPriceUnit=Units::where('product_id','=',$product_id)->first())
        <h3> Rs:<div class="product-price" id="set-product-price">{{$productPriceUnit->product_price}}/{{$productPriceUnit->unit}}</div></h3>
        <input type="hidden" name="pricewithUnit" id="productPrice">
        <input type="hidden" value="{{$productPriceUnit->product_price}}/{{$productPriceUnit->unit}}" id="product-price_{{$product_id}}" >
        @foreach($units as $unit)

            <button type="button"  onclick="setunitPriceProduct('{{$unit->unit}}','{{$product_id}}')" >{{$unit->unit}}</button>

        @endforeach
        <div id="product">
            <div class="form-group qty">
                <label class="control-label" for="input-quantity">Qty</label>
                <input type="text" name="quantity" value="1" size="2" id="input-quantity" onkeyup="checkQuantity({{$product_id}})"
                       class="form-control">
                <div class="danger" hidden id="itemquantityalert" style="color: red">This Quantity is not available.
                    <br>Please select another quantity.
                </div>
                <input type="hidden" name="product_id" value="49">
                <!--<br />-->

                <button   onclick="cart.add('{{$product_id}}')" type="button" id="button-cart"
                         data-loading-text="Loading..."
                        class="btn btn-lg btn-block singleproductaddtocart" >Add to Cart
                </button>

                    <div class="btn-group prd_page">
                    <button type="button" data-toggle="tooltip" class="btn btn-default wishlist" title=""
                            onclick="wishlist.add({{$product_id}});" data-original-title="Add to Wish List">Add to Wish List
                    </button>
                    <button type="button" data-toggle="tooltip" class="btn btn-default compare" title=""
                    onclick="compare.add({{$product_id}});" data-original-title="Add to Compare">Add to Compare
                    </button>
                </div>
            </div>
        </div>
        <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style"
             data-url="index3d7a.html?route=product/product&amp;product_id=49"><a class="addthis_button_facebook_like"
                                                                                  fb:like:layout="button_count"></a> <a
                    class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a
                    class="addthis_counter addthis_pill_style"></a></div>
    {{--<script type="text/javascript" src="../../../../s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>--}}
    <!-- AddThis Button END -->
    </div>
</div>
<div class="col-sm-12" id="">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab-description" data-toggle="tab">Description</a></li>
        {{--<li><a href="#tab-selly" data-toggle="tab">Why Chose Selly</a></li>--}}
        <li><a href="#tab-review" data-toggle="tab">Reviews</a></li>
        <li><a href="#tab-healthBenefits" data-toggle="tab">Nutritional Value of {{$product_title}}</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active product-description" id="tab-description">
            {!! $product_description !!}
        </div>


        <div class="tab-pane" id="tab-review">
            <form class="form-horizontal" id="form-review">
                <div id="review"></div>
                <h3>Write a review</h3>
                <div class="form-group required">
                    <div class="col-sm-12">
                        <label class="control-label" for="input-name">Your Name</label>
                        <input type="text" name="name" value="" id="input-name" class="form-control">
                    </div>
                </div>
                <div class="form-group required">
                    <div class="col-sm-12">
                        <label class="control-label" for="input-review">Your Review</label>
                        <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                        <div class="help-block"><span class="text-danger">Note:</span> HTML is not translated!</div>
                    </div>
                </div>
                <input type="hidden" value="{{$product_id}}" name="productId" id="productId">
                <div class="form-group required">

                    <div id="reviewStars-input">
                        <input id="star-4" type="radio" name="reviewStars" value="5"/>
                        <label title="gorgeous" for="star-4"></label>

                        <input id="star-3" type="radio" name="reviewStars" value="4"/>
                        <label title="good" for="star-3"></label>

                        <input id="star-2" type="radio" name="reviewStars" value="3"/>
                        <label title="regular" for="star-2"></label>

                        <input id="star-1" type="radio" name="reviewStars" value="2"/>
                        <label title="poor" for="star-1"></label>

                        <input id="star-0" type="radio" name="reviewStars" value="1"/>
                        <label title="bad" for="star-0"></label>
                    </div>
                </div>
                <div class="buttons clearfix">
                    <div class="pull-right">
                        <button type="button" id="button-review" data-loading-text="Loading..." class="btn btn-primary">
                            Continue
                        </button>
                    </div>
                </div>
            </form>
            @php($totalReviews=$reviews->count())
            Total Reviews: {{$totalReviews}}
            @foreach($reviews as $review)
                <div class="productsReviews">

                    <h3>{{$review->reviewerName}}</h3>
                    @if($review->rating == '1')
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star "></span>
                        <span class="fa fa-star "></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                    @elseif($review->rating == '2')
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star "></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                    @elseif($review->rating == '3')
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                    @elseif($review->rating == '4')
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                    @elseif($review->rating == '5')
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                    @endif{{date('M d, Y', strtotime($review->additionDate))}}
                    <br>
                    {{ strip_tags($review->review) }}
                    <br>
                    <br>

                </div>
            @endforeach

            <p>{{ $reviews->links() }}</p>
        </div>
        <div class="tab-pane product-description" id="tab-healthBenefits">

            {!!   $health_benifits !!}
        </div>
    </div>
        @if($relatedProduct->count() > 0)
            <div class="img-prd">
                <div class="hometab box">

                    <div class="col-md-12">
                        <div class="tab-head fruits-section">

                            <div class="hometab-heading box-heading">Related Products</div>


                        </div>
                        <div class="tab-content">
                            <div id="latestItemGroceries" class="tab-pane fade in active mt-0">
                                <div class="box">
                                    <div class="box-content">
                                        <div class="customNavigation">
                                            <a class="fa prev"></a>
                                            <a class="fa next"></a>
                                        </div>
                                        <div class="owl-carousel custom_carousel box-product" id="carousel_6" >

                                            @foreach($relatedProduct as $prod)
                                                @php $unitCountProduct=\App\Models\Admin\Units::where('product_id','=',$prod->id)->count();
                                                $unitsQuantity=\App\Models\Admin\Units::where('product_id','=',$prod->id)->first();

                                            @endphp
                                            @if($unitsQuantity->product_quantity > 0)
                                                <div class="slider-item">

                                                    <div class="product-block product-thumb  transition">

                                                        <div class="product-block-inner">

                                                            <div class="Product-Box clearfix">
                                                                {{--<div class="ribbon ribbon-green">--}}
                                                                {{--<span class="save-text">Save</span>--}}
                                                                {{--<span class="disc-per">40%</span>--}}
                                                                {{--</div>--}}
                                                                {{--@if($prod->popular_items == 1)--}}
                                                                {{--<div class="corner-ribbon top-right sticky green">Popular Item</div>--}}
                                                                {{--@endif--}}
                                                                {{--<div class="corner-ribbon-special top-right sticky gray">Last Item</div>--}}
                                                                <div class="mt-15">

                                                                    <h4 class="name">
                                                                        {{$prod->product_title}}
                                                                    </h4>
                                                                    <p class="price">

                                                                        @php
                                                                            $on_sale=$prod->on_sales;
                                                                            $unit=\App\Models\Admin\Units::where('product_id','=',$prod->id)->first();
                                                                        @endphp

                                                                        @if(isset($on_sale) && $on_sale=="1")
                                                                            <span class="price-new">Rs {{number_format($unit->product_price,2)}} /{{$unit->unit}}
                                                        </span>
                                                                            <span class="price-old">Rs {{number_format($unit->product_price_discount,2)}} /{{$unit->unit}}
                                                        </span>
                                                                        @else
                                                                            <span class="price-new">Rs {{number_format($unit->product_price,2)}}/{{$unit->unit}}</span>
                                                                        @endif


                                                                        <input type="hidden" value="{{$unit->product_price}}/{{$prod->unit}}" id="product-price_{{$prod->id}}" >

                                                                        <span class="price-tax">Ex Tax: $100.00</span>
                                                                    </p>
                                                                </div>
                                                                <div class="button-group wishlist-new-btn">
                                                                    <a href="{{url('')}}/{{$categoryTitle.'/'.$prod->cleanURL}}.html">
                                                                        <button class="" type="button" data-toggle="tooltip" title="Quick view" data-placement="right" onclick="compare.add('48');">
                                                                            <i class="fa fa-search"></i>
                                                                        </button>
                                                                    </a>
                                                                    <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" data-placement="right" onclick="wishlist.add({{$prod->id}});">
                                                                        <i class="fa fa-heart"></i>
                                                                    </button>
                                                                    {{--<button class="compare" type="button" data-toggle="tooltip" title="Add to Compare" data-placement="right" onclick="compare.add('48');">--}}
                                                                    {{--<i class="fa fa-share-square-o"></i>--}}
                                                                    {{--</button>--}}

                                                                </div>
                                                                <div class="image ">
                                                                    <a href="{{url('')}}/{{$categoryTitle.'/'.$prod->cleanURL}}.html">
                                                                        <img src="{{Utility::getImage('products/')}}/{{$prod->product_image}}" title="{{$prod->product_title}}" alt="{{$prod->product_title}}" class="img-responsive "
                                                                        />
                                                                    </a>
                                                                    <div class="rating">
                                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="product-details">
                                                                    <div class="caption">
                                                                        <div class="button-group cart-btn-new">

                                                                            @if($unitCountProduct > 1)

                                                                                <a class="view-product-btn btn" href="{{url('')}}/{{$categoryTitle.'/'.$prod->cleanURL}}.html">
                                                                                    View Product
                                                                                </a>
                                                                            @else   <button type="button" class="addtocart"  onclick="cart.add('{{$prod->id}}',1);">
                                                                                Add to Cart
                                                                            </button>
                                                                            @endif
                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <span class="tablatest_default_width" style="display:none; visibility:hidden"></span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        @endif

        <input type="hidden" name="quantity" value="{{$units[0]['product_quantity']}}" size="2" id="itemquantity"
               class="form-control">
        <script>
            $(document).ready(function(){
                var itemquantity = $('#itemquantity').val();
                if(itemquantity  < 1){
                    $('#button-cart').hide();
                }
            });
        </script>
        <script>
            $(document).ready(function(){
                $('#carousel_6').owlCarousel({
                    items: 4,
                    singleItem: false,
                    navigation: false,
                    navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
                    pagination: false,
                    itemsDesktop : [1200,5],
                    itemsDesktopSmall :	[979,3],
                    itemsTablet :	[767,2],
                    itemsMobile : [479,1]
                });

                $("#carousel_6").prev(".customNavigation").children("a.next").click(function(){
                    $("#carousel_6").trigger('owl.next');
                })
                $("#carousel_6").prev(".customNavigation").children("a.prev").click(function(){
                    $("#carousel_6").trigger('owl.prev');
                })
            })
        </script>


</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.box-category').click();

    });
    $('.box-category').click(function () {
        var marginTop = $('.item').css('marginTop');
        if (marginTop == '0px') {
            marginTop = '150px';
        }
        else if (marginTop == '150px') {
            marginTop = '-0px';
        }
        $('.item').animate({marginTop: marginTop}, 1000);
    });

    function checkQuantity(product_id) {

        var quantity = $('#input-quantity').val();
        var itemquantity = $('#itemquantity').val();
        var priceUnit = $('#product-price_'+ product_id).val();
        var unitprice = priceUnit.split("/");
        var price= unitprice[0];
        var unit= unitprice[1];
        if (Number(quantity) > Number(itemquantity)) {
            $('#button-cart').hide();
            $('#itemquantityalert').show();

        }
        else {
            var  setprice = Number(quantity) * Number(price)

            $('#set-product-price').html(setprice+'/'+unit);
            $('#button-cart').show();
            $('#itemquantityalert').hide();
        }
    }

    function setprice() {
        var Itemprice = $('#product-price').val();
        var price = $('#set-product-price').html(Itemprice);

    }

    $(document).ready(function () {
        var Itemprice = $('#product-price').val();
        var price = $('#set-product-price').html(Itemprice);

        if ($(window).width() > 767) {
            $("#tmzoom").elevateZoom({

                gallery: 'additional-carousel',
                //inner zoom

                zoomType: "inner",
                cursor: "zoom-in"


            });
            var z_index = 0;

            $(document).on('click', '.thumbnail', function () {
                $('.thumbnails').magnificPopup('open', z_index);
                return false;
            });

            $('.additional-carousel a').click(function () {
                var smallImage = $(this).attr('data-image');
                var largeImage = $(this).attr('data-zoom-image');
                var ez = $('#tmzoom').data('elevateZoom');
                $('.thumbnail').attr('href', largeImage);
                ez.swaptheimage(smallImage, largeImage);
                z_index = $(this).index('.additional-carousel a');
                return false;
            });

        } else {
            $(document).on('click', '.thumbnail', function () {
                $('.thumbnails').magnificPopup('open', 0);
                return false;
            });
        }
    });
    $(document).ready(function () {
        $('.thumbnails').magnificPopup({
            delegate: 'a.elevatezoom-gallery',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-with-zoom',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function (item) {
                    return item.el.attr('title');
                }
            }
        });
    });

    $('#button-review').click(function () {
        cart.addReviews();

    });

    //-->
</script>
{{--<script>--}}
{{--$('#zoom-new-img').easyZoom({--}}
{{--width: 350,--}}
{{--position: 'right',--}}
{{--background: '#f7f7f7',--}}
{{--});--}}
{{--</script>--}}
<script>
    $(document).ready(function(){
        $('#demo-1 .simpleLens-thumbnails-container img').simpleGallery({
            loading_image: 'demo/images/loading.gif'
        });

        $('#demo-1 .simpleLens-big-image').simpleLens({
            loading_image: 'demo/images/loading.gif'
        });
    });
</script>

<script>
    function setunitPriceProduct(unit , productId ) {

        $.ajax({
            type: 'get',
            url: '{{url('/setprice')}}',
//            url: '/setprice?pid='+productId+'&unit='+unit,
            data : 'pid='+productId+'&unit='+unit,
            success: function(data)
            {

                $('#set-product-price').html(data['product_price']+'/'+data['unit']);
                $('#product-price_'+productId).val(data['product_price']+'/'+data['unit']);
             $('#itemquantity').val(data['product_quantity']);

                var itemquantity = $('#itemquantity').val();
                if( itemquantity > 0){
                    $('#button-cart').show();

                }
                else {
                    $('#button-cart').hide();

                }
            },
            error:function (error)
            {

            },
        });


    }
</script>
