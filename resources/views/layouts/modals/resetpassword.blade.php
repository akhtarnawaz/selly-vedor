

<div class="modal fade" id="resetpassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="login-form">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <form id="form_resetpassword"  class="form-horizontal" role="form" action="{{url('resetuserPasword')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div id="idFormGroup" class="form-group">

                    {{--<div class="or-seperator"><b>or</b></div>--}}
                    <h4 style="color: red;" class="reset_error_msg text-center"></h4>
                    <h4 style="color: green;" class="reset_success_msg text-center"></h4>

                    <p class="fname_error error text-center alert alert-danger hidden"></p>
                    <div class="form-group">
                        <div class="row"><span class="error-class" id="name"></span></div>
                        <label class="control-label col-sm-4" for="Name">Old Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" name="oldpassword" required id="oldpassword" >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row"><span class="error-class" id="name"></span></div>
                        <label class="control-label col-sm-4" for="Name">New Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" name="newpassword" required id="newpassword" >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row"><span class="error-class" id="name"></span></div>
                        <label class="control-label col-sm-4" for="Name">Confirm Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" name="confirmpassword" onkeyup="validatepasswor()" required id="confirmpassword" >
                        </div>
                    </div>
                    <span id='message'></span>
                    <p class="email_error error text-center alert alert-danger hidden"></p>
                    <div class="modal-footer" id="resetbutton">
                        <button type="submit" class="btn btn-success"  style="height: 35px;">
                            <span  class='glyphicon ' style="margin-bottom: 15px;"> Reset Password</span>
                        </button>


                    </div>


                </div>
            </form>

        </div>
    </div>
</div>


<script>

    $('#resetpassword').on('shown.bs.modal', function () {
        $('#resetpassword').trigger('focus')
    });

    function validatepasswor() {
        var password= $('#newpassword').val()
        var confirmpassword =$('#confirmpassword').val()
        if(password == confirmpassword){
            $('#message').html('Matching').css('color', 'green');

        } else
            $('#message').html('Not Matching').css('color', 'red');

    }

</script>