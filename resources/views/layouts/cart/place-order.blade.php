@extends('templates.layout')
@section('content')
    <style>
        .body{background:#fff !important;}
    </style>
    <div class="container checkout_payment place_order">
        <div class="row">
            <div class="content-top-breadcum">
                <div class="container" style="display: block">
                    <div class="row">
                        <div id="title-content">
                            <h1 class="page-title">Thank you for your order
                            </h1>
                            <ul class="breadcrumb">
                                <li><a href=""><i class="fa fa-home"></i></a></li>
                                <li><a href="">Shopping Cart</a></li>
                            </ul>


                        </div>


                    </div>
                </div>
            </div>
            <p>Congratulations! Your order has been placed.
                You will receive your order confirmation shortly.</p>
            <div class="col-md-12 p-0">

                <span class="preheader"></span>
                <table class="body">
                    <tr>
                        <td class="center" align="center" valign="top">
                            <center data-parsed="">
                                <style type="text/css" align="center" class="float-center">
                                    body,
                                    html,
                                    .body {
                                        background: #f3f3f3 !important;
                                        font-size: 14px;
                                    }
                                </style>
                                <table align="center" class="float-center" style="background: #fefefe;width: 580px;">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <table class="spacer">
                                                <tbody>
                                                <tr>
                                                    <td height="16px" style="font-size:16px;line-height:16px;">&#xA0;</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <table class="row">
                                                <tbody>
                                                <tr>
                                                    <th class="small-12 large-12 columns first last">
                                                        <table>
                                                            <tr>
                                                                <th>
                                                                    <a href="https://play.google.com/store/apps/details?id=com.p3care.sellypk" style="width: 150px;float: right;margin-bottom: 15px;">
                                                                        <img class="img-responsive" src="{{url('public/images/catalog/andriod-app.png')}}">
                                                                    </a>
                                                                    <img class="img-responsive" src="{{url('public/images/catalog/newsletter-bg.png')}}">
                                                                    <h1 style="background: #31c33c;font-size: 18px;padding: 10px 10px;color: #fff;margin-top: 10px; text-align: center;">Congratulation! Your order has been placed.</h1>
                                                                    <p style="font-size: 16px;font-weight: bold;color: #ec0101;text-align: center;">You will receive your order confirmation shortly</p>
                                                                    <p style="margin-top: 25px;">Hey
                                                                        <strong>{{$customer->username}},</strong>
                                                                    </p>
                                                                    <p style="font-size: 14px;margin-bottom: 20px !Important;">Great choice. Check below details of your order placed at <a href="{{url('/')}}">Selly.pk</a></p>

                                                                    <!-- <p style="font-size: 14px;">Your Selly.pk Team</p> -->
                                                                    <table style="border-bottom: 2px solid #57c298;margin-bottom: 10px;">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <table class="callout">
                                                                        <tr>
                                                                            <th>
                                                                                <table class="row">
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <th style="width: 70%;">
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <th>
                                                                                                        <p style="margin-top: 10px;">
                                                                                                            <strong>Delivery Address</strong>
                                                                                                        </p>
                                                                                                        <p style="font-size: 14px;">{{$customer->username}}</p>
                                                                                                        <p style="font-size: 14px;">@if($customer->phoneno == ''){{$customer->cell_no}}@else{{$customer->phoneno}}@endif</p>

                                                                                                        <p style="font-size: 14px;">{{$customer->address}}</p>
                                                                                                    </th>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </th>
                                                                                        <th style="width: 30%;">
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <th>
                                                                                                        <p style="color: #fff;background: #03ae50;width: 130px;padding: 10px;text-align: center;">
                                                                                                            <span style="font-size: 16px;font-weight: bold;">Payment Mode:</span>
                                                                                                            <br>
                                                                                                            <span style="margin-top: 10px;font-size: 14px;display: inline-block;">Cash on Delivery</span>
                                                                                                        </p>
                                                                                                    </th>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </th>
                                                                                    </tr>
                                                                                    <!-- <tr>
                                                                                      <th style="width: 100%;">
                                                                                          <p style="font-size: 16px;font-weight: bold;color: #207d44;">Order Details (Invoice #S01335 Date:06-04-2018 Time:10:03:03)</p>
                                                                                      </th>
                                                                                    </tr> -->
                                                                                    </tbody>
                                                                                </table>
                                                                                <table>
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <th>
                                                                                            <span class="date"> {{--Nov 21, 2017.--}}</span>
                                                                                            <span class="time"> {{--11:57 AM--}}</span>

                                                                                            <p style="font-size: 16px;font-weight: bold;color: #207d44;margin-top: 20px;">Order Details (Invoice #{{$order->orderNumber}} Date:{{date('M d, Y',strtotime($order->created_at))}}Time:{{date('g:i A',strtotime($order->created_at))}})</p>
                                                                                        </th>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </th>
                                                                            <th class="expander"></th>
                                                                        </tr>
                                                                    </table>
                                                                    <table style="border: 1px solid #5ceba8">
                                                                        <tr style="background: #93d250;">
                                                                            <th style="padding: 8px; border: 1px solid #5ceba8 ;font-size: 16px;font-weight: bold; text-align: center; font-size: 14px;">Sr</th>
                                                                            <th style="padding: 8px; border: 1px solid #5ceba8; font-size: 16px;font-weight: bold; font-size: 14px;">Product</th>
                                                                            <th style="padding: 8px; border: 1px solid #5ceba8;font-size: 16px;font-weight: bold;text-align: center; font-size: 14px;">Price</th>
                                                                            <th style="padding: 8px; border: 1px solid #5ceba8;font-size: 16px;font-weight: bold;text-align: center; font-size: 14px;">Qty</th>
                                                                            <th style="padding: 8px; border: 1px solid #5ceba8;font-size: 16px;font-weight: bold;text-align: center; font-size: 14px;">Total</th>
                                                                        </tr>
                                                                        @php($subTotal=0)
                                                                        @php
                                                                            use App\Models\Admin\Orders;
                                                                             $grandTotal=Orders::where('id',$order->id)->pluck('total')->first();
                                                                            $orderDetail= Orders::leftjoin('order_items','orders.id','=','order_items.order_id')
                                                                            ->leftJoin('products','order_items.object_id','=','products.id')
                                                                            ->where('orders.id','=',$order->id)->select('order_items.price as price',
                                                                            'order_items.total as total','order_items.quantity_unit_id as 	quantity_unit','order_items.item_quantity as item_quantity','order_items.id as id',
                                                                            'products.product_title as product_title','products.unit as unit')->get();

                                                                            $count=1;
                                                                        @endphp
                                                                        @foreach($orderDetail as $detail)
                                                                            <tr>
                                                                                <td style="padding: 8px; border: 1px solid #5ceba8;text-align: center;font-size: 14px;">{{$count}}</td>
                                                                                <td style="padding: 8px; border: 1px solid #5ceba8; font-size: 14px;">{{$detail->product_title}}</td>
                                                                                <td style="padding: 8px; border: 1px solid #5ceba8;text-align: center;font-size: 14px;">{{$detail->price}}</td>
                                                                                <td style="padding: 8px; border: 1px solid #5ceba8;text-align: center;font-size: 14px;">{{$detail->item_quantity}}</td>
                                                                                <td style="padding: 8px; border: 1px solid #5ceba8;text-align: center;font-size: 14px;">{{$detail->total}}</td>
                                                                            </tr>
                                                                            @php
                                                                                $count ++;
                                                                            @endphp
                                                                        @endforeach

                                                                        <tr>
                                                                            <td style="padding: 8px;"></td>
                                                                            <td style="padding: 8px;"></td>
                                                                            <td style="padding: 8px; border: 1px solid #5ceba8;text-align: center;font-size: 14px;" colspan="2">
                                                                                <b>Grand Total:</b>
                                                                            </td>
                                                                            <td style="padding: 8px; border: 1px solid #5ceba8;text-align: center;font-size: 14px;">
                                                                                <b>Rs {{$grandTotal}}</b></td>
                                                                        </tr>
                                                                    </table>

                                                                    <h4 style="text-align: center;color: #00af50;font-weight: 500;font-size: 20px;margin-top: 30px;margin-bottom: 25px;">THANK YOU FOR YOUR ORDER</h4>
                                                                    <table>
                                                                        <tbody style="background: #31c33c;">
                                                                        <tr>
                                                                            <th style="padding: 6px 0 0 0;">
                                                                                <p style="text-align: center;font-size: 14px;color: #fff;font-weight: 500;">405, Al Hafeez Shopping Mall, Main Boulevard Gulberg III. Lahore</p>
                                                                                <p style="text-align: center;font-size: 14px; color: #fff;font-weight: 500;">Phone : 0304 - 111 - 7355
                                                                                    <a href="https://selly.pk/" style="margin-left: 30px;color: #fff;font-weight: 500;">Selly.pk</a>
                                                                                </p>

                                                                                <ul style="text-align: center;padding: 0;list-style: none;margin: 0 0 5px 0">
                                                                                    <li>
                                                                                        <a style="color: #fff;margin: 0 10px;" href="https://www.facebook.com/www.Selly.pk/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                                                                        <a style="color: #fff;margin: 0 10px;"  href="https://twitter.com/SellyPk?ref_src=twsrc%5Etfw&ref_url=https%3A%2F%2Fselly.pk%2F" style="color: #fff;"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                                                                        <a style="color: #fff;margin: 0 10px;" href="https://plus.google.com/+SellyPk" style="color: #fff;"><i class="fa fa-google-plus" aria-hidden="true"></i>
                                                                                        </a>
                                                                                        <a style="color: #fff;margin: 0 10px;" href="https://www.instagram.com/selly.pk/" style="color: #fff;"><i class="fa fa-instagram" aria-hidden="true"></i>
                                                                                        </a>
                                                                                    </li>

                                                                                </ul>

                                                                            </th>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>


                                                                </th>
                                                            </tr>
                                                        </table>
                                                    </th>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                            </center>
                        </td>
                    </tr>
                </table>
                <!-- prevent Gmail on iOS font size manipulation -->
                <div style="display:none; white-space:nowrap; font:15px courier; line-height:0;"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
                <div class="clearfix"></div>
            </div>
            {{--<div class="checkout_form invoice_box clearfix">--}}

            {{--<h2 class="mb-10">Invoice # {{$order->orderNumber}}</h2>--}}
            {{--<p class="order-date mb-0">{{$order->created_at}}</p>--}}
            {{--<p class="grand-total">Grand total: <span class="rupees">{{$order->total}}</span></p>--}}

            {{--<div class="billing_address_box mt-20">--}}
            {{--<h3>Billing Address</h3>--}}
            {{--@php--}}
            {{--@endphp--}}
            {{--<span class="billing_firstName">{{$customer->username}}</span>--}}
            {{--<span class="billing_StreetNum">{{$customer->address}}</span>--}}
            {{--<span class="billind_PhoneNum">@if($customer->phoneno == ''){{$customer->cell_no}}@else{{$customer->phoneno}}@endif</span>--}}

            {{--</div>--}}

            {{--<div class="payment_method_box mt-20">--}}
            {{--<h3>Payment Method</h3>--}}
            {{--<span>Cash On Delivery</span>--}}

            {{--</div>--}}


            {{--</div>--}}




            {{--</div>--}}
            <div class="col-md-6 hidden">
                <div class="invoice_box_right clearfix">
                    <div class="">
                        <img src="{{Utility::getImage('catalog/selly.png')}}" title="Your Store" alt="Your Store" class="img-responsive">
                        <strong>Selly.pk</strong>
                        <p>
                            405, Al- Hafeez Shopping Mall, Main Boulevard Gulberg III.<br>  Lahore, Punjab, 54670<br>
                            Pakistan</p>
                        <p>Phone: 0304 - 111 - 7355</p>
                        <p class="url">
                            <a href="https://www.selly.pk">https://www.selly.pk</a>
                        </p>
                    </div>
                </div>

            </div>


        </div>
@stop