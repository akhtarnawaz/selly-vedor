@extends('templates.layout')
@section('content')
<div class="container checkout_payment place_order">
    <div class="row">
        <div class="content-top-breadcum">
            <div class="container" style="display: block">
                <div class="row">
                    <div id="title-content">
                        <h1 class="page-title">Thank you for your order
                        </h1>
                        <ul class="breadcrumb">
                            <li><a href=""><i class="fa fa-home"></i></a></li>
                            <li><a href="">Shopping Cart</a></li>
                        </ul>


                    </div>


                </div>
            </div>
        </div>
        <p>Congratulations! Your order has been placed.
            You will receive your order confirmation shortly.</p>
        <div class="col-md-12 p-0">
            {{--<button type="button" class="btn btn-default">Print Invoice</button>--}}
            <hr>
        </div>



        <div class="col-md-6 p-0">


            <div class="checkout_form invoice_box clearfix">

                <h2 class="mb-10">Invoice # {{$order->orderNumber}}</h2>
                <p class="order-date mb-0">{{$order->created_at}}</p>
                <p class="grand-total">Grand total: <span class="rupees">{{$order->total}}</span></p>

                <div class="billing_address_box mt-20">
                    <h3>Billing Address</h3>
                    @php
                    @endphp
                    <span class="billing_firstName">{{$customer->username}}</span>
                    <span class="billing_StreetNum">{{$customer->address}}</span>
                    <span class="billind_PhoneNum">@if($customer->phoneno == ''){{$customer->cell_no}}@else{{$customer->phoneno}}@endif</span>

                </div>

                <div class="payment_method_box mt-20">
                    <h3>Payment Method</h3>
                    <span>Cash On Delivery</span>

                </div>


            </div>




        </div>
        <div class="col-md-6 hidden-xs">
            <div class="invoice_box_right clearfix">
                <div class="">
                    <img src="{{Utility::getImage('catalog/selly.png')}}" title="Your Store" alt="Your Store" class="img-responsive">
                    <strong>Selly.pk</strong>
                    <p>
                        405, Al- Hafeez Shopping Mall, Main Boulevard Gulberg III.<br>  Lahore, Punjab, 54670<br>
                        Pakistan</p>
                    <p>Phone: 0304 - 111 - 7355</p>
                    {{--<p class="url">--}}
                        {{--<a href="https://www.selly.pk">https://www.selly.pk</a>--}}
                    {{--</p>--}}
                </div>
            </div>

        </div>

        <div class="clearfix"></div>
    </div>
</div>
    @stop