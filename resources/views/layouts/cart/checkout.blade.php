<div class="row address_form">
    <div id="content" class="col-md-8">
        <div class="content-top-breadcum">
            <div class="container" style="display: block;">
                <div class="row">
                    <div id="title-content">
                        <h1 class="page-title">Billing Form
                        </h1>
                        <ul class="breadcrumb" style="display: none;">
                            <li><a href=""><i class="fa fa-home"></i></a></li>
                            <li><a href="">Shopping Cart</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="checkout_form clearfix">
            <form action="" method="">
                {{--<h3></h3>--}}

                <div class="col-md-6">

                    <div class="customer-info">
                        <div class="form-group-custom mb-20">

                            <input type="email" id="email"
                                   onchange="validateEmail(this.value)"
                                   value="{{\Illuminate\Support\Facades\Auth::check()?\Illuminate\Support\Facades\Auth::user()->email:''}}
                                           " required/>
                            <label class="control-label" for="user-name">Email</label><i class="bar"></i>
                            <small>Your order details will be sent to your email address
                            </small>
                            <span id="emailerror" style="color: red" hidden>Please enter your email address</span><br>
                            <span id="emailvalid" style="color: red" hidden>email is not valid (example@example.com)</span>
                        </div>
                        <div class="form-group-custom" id="fullname">
                            <input type="text" id="full_name" name="full_name"
                                   onchange="validateName(this.value)"
                                   value="{{\Illuminate\Support\Facades\Auth::check()?\Illuminate\Support\Facades\Auth::user()->username:''}}"
                                   required/>
                            <label class="control-label" for="user-name">Full Name</label><i class="bar"></i>
                            <span id="nameerror" style="color: red" hidden>Please enter your name</span>
                        </div>

                        <span id="validatename" style="color: red" hidden>Name Formate is not valid</span>
                        <div class="form-group-custom">
                            <input type="text" id="address"
                                   value="{{\Illuminate\Support\Facades\Auth::check()?\Illuminate\Support\Facades\Auth::user()->address:''}}"
                                   required/>
                            <label class="control-label" for="user-name">Address</label><i class="bar"></i>
                            <span id="addresserror" style="color: red" hidden>Please enter your address</span>

                        </div>
                        <div class="form-group-custom" id="phone_number">
                            <input type="text" id="phone"
                                   onchange="validatePhone(this.value)"
                                   value="{{\Illuminate\Support\Facades\Auth::check()?\Illuminate\Support\Facades\Auth::user()->cell_no:''}}"
                                   required/>
                            <label class="control-label" for="user-name">Phone</label><i class="bar"></i>
                            <span id="phoneerror" style="color: red" hidden>Please enter phone number</span><br>
                            <span id="phonevalid" style="color: red" hidden>Must be a number</span>
                        </div>
                        @php use App\Models\Admin\Location; $location=Location::all(); @endphp
                        <div class="nearest-location-input form-group-custom">
                            <select class="form-control" name="storeLocation" id="storeLocation">
                                <option value="0">
                                    Please Select Your Nearest Store Location
                                </option>
                                @foreach($location as $store)
                                    <option value="{{$store->id}}">
                                        {{$store->store_name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                </div>
                {{--<div class="clearfix"></div>--}}
                <div class="col-md-6">
                    <div class="custom-datepicker">
                        <div class="form-group mb-0">
                            <h3>
                                <label class="custom-checkbox">
                                    Schedule your order for another Date
                                    <br>
                                    <span class="schedule-urdu-txt">
                                            اپنا آرڈر کسی اورتاریخ کے لئے بک کروائیں
                                        </span>
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>

                            </h3>
                            <input type="text" id="dep_date" name="dep_date" required="">
                        </div>
                    </div>
                </div>
                <div class="form-group-custom">
                    {{--<label class="control-label" for="user-name">Store Location</label><i ></i>--}}
                </div>


            </form>
        </div>

    </div>
    <div class="col-md-4 checkout_cart">
        <div>
            <ul>
                <li>
                    <a href="#" class="items_in_cart">{{$content['totalItems']}} item(s) in Cart</a>
                <!-- <span class="pull-right rupees">{{$content['total']}}</span> -->
                </li>
                <li class="mt-20">
                    <strong>Subtotal</strong>
                    <span class="pull-right">
                        <span></span>
                    </span>
                </li>
                <li>
                    <strong>Voucher/Discount</strong>
                    <span class="pull-right">
                        <span></span>
                    </span>
                </li>
                <li>
                    <strong>Shipping and Handling</strong>
                    <span class="pull-right">
                        <span></span>
                    </span>
                </li>
                <li>
                    <strong>Sales Tax</strong>
                    <span class="pull-right">
                        <span></span>
                    </span>
                </li>
                <li>
                    <hr>
                </li>
                {{--<li>--}}
                {{--<strong>Wallet Amount</strong>--}}
                {{--<span class="pull-right">--}}
                {{--<span class="">Rs1000</span>--}}
                {{--</span>--}}
                {{--</li>--}}
                <li class="total-price">
                    <strong>AMOUNT PAYABLE</strong>
                    <strong class="rupees pull-right">{{$content['total']}}</strong>
                </li>
            </ul>
            <div class="proceed_payment_btn checkOut-btn">
                <a class="btn btn-primary btn-block proceed_payment_btn" id="proceed_payment" onclick="checkvalidation()">Proceed Payment</a>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div style="display: none;" class="container checkout_payment">
    <div class="row">
        <div class="content-top-breadcum">
            <div class="container">
                <div class="row">
                    <div id="title-content">
                        <h1 class="page-title">
                        </h1>
                        <ul class="breadcrumb">
                            <li><a href=""><i class="fa fa-home"></i></a></li>
                            <li><a href="">Shopping Cart</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 p-0">


            <div class="checkout_form clearfix">

                <h3>Customer Note</h3>

                <div class="col-md-12">
                    <div class="billing_address_box mt-20">
                        <span class="billing_firstName"></span>
                        <span class="billing_StreetNum"></span>
                        <span class="billind_PhoneNum"></span>

                    </div>

                    <div class="form-group-custom">
                        <input type="text" id="ordernote" required/>
                        <label class="control-label" for="user-name">Customer note</label><i class="bar"></i>

                    </div>
                </div>

            </div>

        </div>
        <div class="col-md-4 checkout_cart">

            <div>
                <h3>Payment Method</h3>
                <a href="#">Cash on Deleivery</a>
                <br>
                <p class="mt-10">One of our Customer Care Representatives will shortly get in touch with you. </p>
                <br>

                <p><span class="red-txt">If you do not get a CALL in FIVE (5) MINUTES,</span> please CONTACT on this
                    number <a href="#">0304-111-7355</a> or send us an email at <a href="#">support@selly.pk</a></p>
            </div>
        </div>
        <div class="col-md-4 checkout_cart">
            <div>
                <ul>
                    <li>
                        <a href="#" class="items_in_cart">{{$content['totalItems']}} item(s) in Cart</a>
                        <span class="pull-right rupees">{{$content['total']}}</span>
                    </li>
                    <li>
                        <hr>
                    </li>
                    <li class="total-price">
                        <strong>TOTAL AMOUNT</strong>
                        <strong class="rupees pull-right">{{$content['total']}}</strong>
                    </li>
                </ul>
                <label class="custom-checkbox subscribe-text">Subscribe to our news list and be in touch with our latest offers.
                    <input type="checkbox">
                    <span class="checkmark"></span>
                </label>
                <!-- <label class="subscribe-sec">
                            <div class="subscribe-input">
                                <input type="checkbox">
                            </div>
                            <div class="subscribe-text"></div>
                        </label> -->
                <div class="proceed_payment_btn mt-20 checkOut-btn">
                    {{--<a class="btn btn-primary btn-block">Place Order</a>--}}
                    <a class="btn btn-primary btn-block proceed_payment_btn place_order ">Place Order</a>
                </div>

                <a href="https://www.selly.pk/terms-conditions" class="text-center display-ib mt-20 mb-0">By clicking the "Place order" button you confirm that you accept the Terms & Conditions
                </a>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<script>
    $('.place_order').click(function () {
        $('.address_form').css('display', 'none');
        $('.checkout_payment').css('display', 'block');

        cart.placeOrder();
    });
</script>
<script>
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var result= re.test(String(email).toLowerCase());
        if(result != true){
            $('#emailvalid').show()
            $('#proceed_payment').hide()
            $('#phone_number').hide()
            $('#fullname').hide()
        }
        else {
            $('#emailvalid').hide()
            $('#proceed_payment').show()
            $('#phone_number').show()
            $('#fullname').show()

        }
    }
    function validatePhone(number) {
        if (isNaN(number))
        {
            $('#phonevalid').show()

            $('#proceed_payment').hide()

        }
        else{
            $('#phonevalid').hide()
            $('#proceed_payment').show()

        }
    }
    function validateName(name) {
        var result =/^[A-Za-z ]+$/.test(name);
        if (result == true)
        {
            $('#validatename').hide();
            $('#phone_number').show();
            $('#proceed_payment').show();

        }
        else{
            $('#validatename').show();
            $('#proceed_payment').hide();
            $('#phone_number').hide();

        }
    }
    function checkvalidation() {

        // $('.address_form').css('display', 'none');
        // $('.checkout_payment').css('display', 'block');
        // $('.proceed_payment_btn').click(function () {
        if ($('#email').val()) {

            $('#emailerror').hide();
            if ($('#full_name').val()) {
                $('#nameerror').hide();

                if ($('#address').val()) {

                    $('#addresserror').hide();


                    if ($('#phone').val()) {

                        $('#phoneerror').hide();

                        $('.address_form').css('display', 'none');
                        $('.checkout_payment').css('display', 'block');

                    }

                    else {
                        $('#phoneerror').show();
                    }
                }
                else {
                    $('#addresserror').show();
                }
            }
            else {
                $('#nameerror').show();
            }
        } else {
            $('#emailerror').show();
        }


    }

</script>

<script>
    $(document).ready(function (e) {
        $("#dep_date").datetimepicker({
            timepicker: false,
            datepicker: true,
            inline: true,
            format: 'm/d/Y'
        });
        $("#dep_date_2").datetimepicker({
            timepicker: false,
            datepicker: true,
            inline: true,
            format: 'm/d/Y'
        });
    });
</script>


<style>
    .breadcrumb {
        display: none;
    }
</style>


<script>
    $(document).ready(function(){
        // $('#demo-1 .simpleLens-thumbnails-container img').simpleGallery({
        //     loading_image: 'demo/images/loading.gif'
        // });

        // $('#demo-1 .simpleLens-big-image').simpleLens({
        //     loading_image: 'demo/images/loading.gif'
        // });

        $('.custom-datepicker .xdsoft_datetimepicker.xdsoft_inline') .hide();
        $(".custom-datepicker input:checkbox").change(function() {
            if ($(this).is(":checked")) {
                $(".custom-datepicker .xdsoft_datetimepicker.xdsoft_inline").slideDown()
            } else {
                $(".custom-datepicker .xdsoft_datetimepicker.xdsoft_inline").slideUp ()
            }
        });

        $('.simpleLens-gallery-container') .hide();
        $(".special-packaging-box input:checkbox").change(function() {
            if ($(this).is(":checked")) {
                $(".simpleLens-gallery-container").slideDown()
            } else {
                $(".simpleLens-gallery-container").slideUp ()
            }
        });
    });
</script>