@extends('templates.layout')
@section('content')
    <div class="container">
        {!! $content !!}
    </div>
    @stop