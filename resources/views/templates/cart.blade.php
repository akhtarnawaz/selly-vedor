@extends('templates.layout')
@section('content')
    <div class="container">
        {!! $content or "No Data Found" !!}
    </div>
@stop