@extends('templates.layout')
@section('content')

        <div class="row">
            @include('layouts.single-product.left-pane')
            <div id="content" class="productpage col-sm-9">
                {!! $content or "No Data Found" !!}
            </div>

        </div>

@stop
