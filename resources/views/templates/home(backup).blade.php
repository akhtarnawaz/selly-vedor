@extends('templates.layout')
@section('content')
    <div class="row home_row">
        <div id="content" class="col-sm-12">
            @include('templates.imports.home.content-top')
            {!! $content or 'No Data Found'!!}
        </div>
    </div>

    <!--Newsletter Modal Start Here-->
    <div class="container">
        <div class="col-md-6 col-md-offset-3">
            <div class="modal-bgclr"></div>
            <div class="newletter-popup">
                <div id="boxes" class="newletter-container">
                    <div id="dialog" class="window">
                        <div id="popup2">
                            <span class="b-close"><span>X</span></span>
                        </div>
                        <div class="box">
                            <div class="newletter-title">
                                <h2>Delivering in Lahore, Currently</h2>
                            </div>
                            <div class="box-content newleter-content">
                                <div class="col-md-6 text-left newsletter-text">
                                    <ul>
                                        <li>Premium Quality </li>
                                        <li>Competitive Prices </li>
                                        <li>No Hidden Charges </li>
                                        <li>Free Shipping </li>
                                        <li>Cash on Delivery</li>
                                        <li>Money Back Guarantee</li>
                                        <li>Dedicated Customer Support </li>
                                        <li>Return anything you don’t like – No Questions will be asked </li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <img src="{{url('public/images/catalog/bike-img.png')}}" class="img-responsive">
                                </div>
                                <div class="clearfix"></div>

                                <div id="frm_subscribe">
                                    <div class="subscribe-bottom">
                                        <label for="newsletter_popup_dont_show_again">For more information, feedback & suggestion Dial <a href="tel:03041117355" class="contact-num" title="Call us 0304 111 7355">0304 - 111 - 7355</a>  </label>
                                    </div>
                                </div>
                                <!-- /#frm_subscribe -->
                            </div>
                            <!-- /.box-content -->
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    <!--Newsletter Modal End Here-->
@stop

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#carousel_1').owlCarousel({
                items: 5,
                singleItem: false,
                navigation: false,
                navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
                pagination: false,
                itemsDesktop : [1200,5],
                itemsDesktopSmall :	[979,3],
                itemsTablet :	[767,2],
                itemsMobile : [479,1]
            });

            $("#carousel_1").prev(".customNavigation").children("a.next").click(function(){
                $("#carousel_1").trigger('owl.next');
            })
            $("#carousel_1").prev(".customNavigation").children("a.prev").click(function(){
                $("#carousel_1").trigger('owl.prev');
            })
        });$(document).ready(function(){
            $('#carousel_11').owlCarousel({
                items: 5,
                singleItem: false,
                navigation: false,
                navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
                pagination: false,
                itemsDesktop : [1200,5],
                itemsDesktopSmall :	[979,3],
                itemsTablet :	[767,2],
                itemsMobile : [479,1]
            });

            $("#carousel_11").prev(".customNavigation").children("a.next").click(function(){
                $("#carousel_11").trigger('owl.next');
            })
            $("#carousel_11").prev(".customNavigation").children("a.prev").click(function(){
                $("#carousel_11").trigger('owl.prev');
            })
        });$(document).ready(function(){
            $('#carousel_12').owlCarousel({
                items: 5,
                singleItem: false,
                navigation: false,
                navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
                pagination: false,
                itemsDesktop : [1200,5],
                itemsDesktopSmall :	[979,3],
                itemsTablet :	[767,2],
                itemsMobile : [479,1]
            });

            $("#carousel_12").prev(".customNavigation").children("a.next").click(function(){
                $("#carousel_12").trigger('owl.next');
            })
            $("#carousel_12").prev(".customNavigation").children("a.prev").click(function(){
                $("#carousel_12").trigger('owl.prev');
            })
        });
        $(document).ready(function(){
            $('#carousel_2').owlCarousel({
                items: 5,
                singleItem: false,
                navigation: false,
                navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
                pagination: false,
                itemsDesktop : [1200,5],
                itemsDesktopSmall :	[979,3],
                itemsTablet :	[767,2],
                itemsMobile : [479,1]
            });

            $("#carousel_2").prev(".customNavigation").children("a.next").click(function(){
                $("#carousel_2").trigger('owl.next');
            })
            $("#carousel_2").prev(".customNavigation").children("a.prev").click(function(){
                $("#carousel_2").trigger('owl.prev');
            })
        });

        $(document).ready(function(){
            $('#carousel_21').owlCarousel({
                items: 5,
                singleItem: false,
                navigation: false,
                navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
                pagination: false,
                itemsDesktop : [1200,5],
                itemsDesktopSmall :	[979,3],
                itemsTablet :	[767,2],
                itemsMobile : [479,1]
            });

            $("#carousel_21").prev(".customNavigation").children("a.next").click(function(){
                $("#carousel_21").trigger('owl.next');
            })
            $("#carousel_21").prev(".customNavigation").children("a.prev").click(function(){
                $("#carousel_21").trigger('owl.prev');
            })
        });


        $(document).ready(function(){
            $('#carousel_22').owlCarousel({
                items: 5,
                singleItem: false,
                navigation: false,
                navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
                pagination: false,
                itemsDesktop : [1200,5],
                itemsDesktopSmall :	[979,3],
                itemsTablet :	[767,2],
                itemsMobile : [479,1]
            });

            $("#carousel_22").prev(".customNavigation").children("a.next").click(function(){
                $("#carousel_22").trigger('owl.next');
            })
            $("#carousel_22").prev(".customNavigation").children("a.prev").click(function(){
                $("#carousel_22").trigger('owl.prev');
            })
        });

        $(document).ready(function(){
            $('#carousel_3').owlCarousel({
                items: 5,
                singleItem: false,
                navigation: false,
                navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
                pagination: false,
                itemsDesktop : [1200,5],
                itemsDesktopSmall :	[979,3],
                itemsTablet :	[767,2],
                itemsMobile : [479,1]
            });

            $("#carousel_3").prev(".customNavigation").children("a.next").click(function(){
                $("#carousel_3").trigger('owl.next');
            })
            $("#carousel_3").prev(".customNavigation").children("a.prev").click(function(){
                $("#carousel_3").trigger('owl.prev');
            })
        });

        $(document).ready(function(){
            $('#carousel_31').owlCarousel({
                items: 5,
                singleItem: false,
                navigation: false,
                navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
                pagination: false,
                itemsDesktop : [1200,5],
                itemsDesktopSmall :	[979,3],
                itemsTablet :	[767,2],
                itemsMobile : [479,1]
            });

            $("#carousel_31").prev(".customNavigation").children("a.next").click(function(){
                $("#carousel_31").trigger('owl.next');
            })
            $("#carousel_31").prev(".customNavigation").children("a.prev").click(function(){
                $("#carousel_31").trigger('owl.prev');
            })
        });

        $(document).ready(function(){
            $('#carousel_32').owlCarousel({
                items: 5,
                singleItem: false,
                navigation: false,
                navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
                pagination: false,
                itemsDesktop : [1200,5],
                itemsDesktopSmall :	[979,3],
                itemsTablet :	[767,2],
                itemsMobile : [479,1]
            });

            $("#carousel_32").prev(".customNavigation").children("a.next").click(function(){
                $("#carousel_32").trigger('owl.next');
            })
            $("#carousel_32").prev(".customNavigation").children("a.prev").click(function(){
                $("#carousel_32").trigger('owl.prev');
            })
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.blogcarousel').owlCarousel({
                items: 5,
                singleItem: false,
                navigation: true,
                navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
                pagination: true,
                itemsDesktop : [1200,4],
                itemsDesktopSmall :	[979,3],
                itemsTablet :	[767,2],
                itemsMobile : [479,1]
            });

            $('.clientscarousel').owlCarousel({
                items: 6,
                singleItem: false,
                navigation: true,
                autoPlay: true,
                navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
                pagination: true,
                itemsDesktop : [1200,4],
                itemsDesktopSmall :	[979,3],
                itemsTablet :	[767,2],
                itemsMobile : [479,1]
            });
            //Launched featured Slider
            $('.launched-featured-carousel').owlCarousel({
                items: 1,
                singleItem: false,
                navigation: true,
                autoPlay: true,
                navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
                pagination: true,
                itemsDesktop : [1200,4],
                itemsDesktopSmall :	[979,3],
                itemsTablet :	[767,2],
                itemsMobile : [479,1]
            });

            //Newsletter Modal JS
            $('.b-close').click(function(){
                $('.modal-bgclr').hide();
                $('.newletter-popup').hide();
            });
            $('.modal-bgclr').click(function(){
                $('.modal-bgclr').hide();
                $('.newletter-popup').hide();
            });
            //Newsletter Modal JS

        });

    </script>
    <!-- ======= Quick view JS ========= -->
    <script>
        function quickbox(){
            if ($(window).width() > 767) {
                $('.quickview-button').magnificPopup({
                    type:'iframe',
                    delegate: 'a',
                    preloader: true,
                    tLoading: 'Loading image #%curr%...',
                });
            }
        }
        jQuery(document).ready(function() {quickbox();});
        jQuery(window).resize(function() {quickbox();});

    </script>
    <script type="text/javascript">
        $('#tabs a').tabs();
    </script>
    <!-- ======= Login modal JS ========= -->
    <script>
        function testAnim(x) {
            $('.modal .modal-dialog').attr('class', 'modal-dialog  ' + x + '  animated');
        };
        $('#myModal').on('show.bs.modal', function (e) {
            var anim = $('#entrance').val();
            testAnim("bounceIn");
        });
        $('#myModal').on('hide.bs.modal', function (e) {
            var anim = $('#exit').val();
            testAnim("flipOutX");
            $(window)
        });

    </script>
    <!-- ======= Registration modal JS ========= -->
    {{--<script>--}}
        {{--function testAnim(x) {--}}
            {{--$('.modal .modal-dialog').attr('class', 'modal-dialog  ' + x + '  animated');--}}
        {{--};--}}
        {{--$('#Sellyregistration').on('show.bs.modal', function (e) {--}}
            {{--var anim = $('#entrance').val();--}}
            {{--testAnim("bounceIn");--}}
        {{--});--}}
        {{--$('#Sellyregistration').on('hide.bs.modal', function (e) {--}}
            {{--var anim = $('#exit').val();--}}
            {{--testAnim("flipOutX");--}}
            {{--$(window)--}}
        {{--});--}}

    {{--</script>--}}
    <!-- ======== Forgot Password === -->



    <script>
        function testAnim(x) {

            $('.modal .modal-dialog').attr('class', 'modal-dialog  ' + x + '  animated');
        };


        $('#forgotModal').on('show.bs.modal', function (e) {
            $("#forgotModal").show();
            var anim = $('#entrance').val();
            testAnim("bounceIn");
        });
        $('#forgotModal').on('hide.bs.modal', function (e) {
            var anim = $('#exit').val();
            testAnim("flipOutX");
            $(window)
        });

    </script>
    <script>
        function testAnim(x) {
            $('.modal .modal-dialog').attr('class', 'modal-dialog  ' + x + '  animated');
        };
        $('#logout').on('show.bs.modal', function (e) {
            var anim = $('#entrance').val();
            testAnim("bounceIn");
        });
        $('#logout').on('hide.bs.modal', function (e) {
            var anim = $('#exit').val();
            testAnim("flipOutX");
            $(window)
        });


    </script>
    <script>
        function testAnim(x) {
            $('.modal .modal-dialog').attr('class', 'modal-dialog  ' + x + '  animated');
        };
        $('#did-you-know-txt').on('show.bs.modal', function (e) {
            var anim = $('#entrance').val();
            testAnim("bounceIn");
        });
        $('#did-you-know-txt').on('hide.bs.modal', function (e) {
            var anim = $('#exit').val();
            testAnim("flipOutX");
            $(window)
        });

    </script>

@stop

