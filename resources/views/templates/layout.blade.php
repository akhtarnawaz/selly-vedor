<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msvalidate.01" content="91DB5EAEE4317000FDAB523FF06DD43E" />
    <meta name="description" content="{{ $meta_descriptions or  'Fresh Online Grocery Shopping on Pakistan’s No.1 E-Store. Buy Fruits & Vegetables and Online Grocery in Lahore. Call Now 03041117355! '}}" />
    <meta name="keywords" content="{{$meta_keyword or 'Online Grocery Shopping, Fruits & Vegetables Online Market, Best Online Grocery Store, Grocery Shopping Lahore, Online Market Lahore .'}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">   <!-- CSRF Token -->
{{--    <title>{{$title or __('lang.site_title')}}</title>--}}
    <title>{{$title or 'Best Online Grocery Shopping in Lahore, Pakistan | Selly.pk'}}</title>
    <link href="{{asset(Config::get('constants.public_path').'images/icons/icon.png')}}" rel="icon" />
    <link rel="canonical" href="https://selly.pk/" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="google-site-verification" content="nf57kn3J7igPEUpFB5t3iIgrYzH6VTFPQf0eX6VkDII" />
    {{--<meta name="google-site-verification" content="google046d73b64dd0baca.html" />--}}
    <!-- Script -->

    <!-- Global site tag (gtag.js) - AdWords: 979845681 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-979845681"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-979845681');
    </script>

    <script>
        gtag('event', 'page_view', {
            'send_to': 'AW-979845681',
            'ecomm_pagetype': 'replace with value',
            'ecomm_prodid': 'replace with value',
            'ecomm_totalvalue': 'replace with value',
            'local_id': 'replace with value',
            'local_pagetype': 'replace with value',
            'local_totalvalue': 'replace with value',
            'user_id': 'replace with value'
        });
    </script>

@php
    $public_path=Config::get('constants.public_path');
@endphp
@include('templates.imports.head.fonts')
@include('templates.imports.head.style')
@include('templates.imports.head.scripts')
<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116695473-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-116695473-1');
    </script>

</head>
<body class="common-home layout-1">
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5b1905ca10b99c7b36d4b66b/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
<div id="preloader"> <div class="loader"></div> </div>
 <!-- Modal -->
  <div class="modal fade did-you-know-txt" id="did-you-know-txt" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Did You Know</h3>
        </div>
        <div class="modal-body">
          <p>Word Selly is from Middle English selli, sellich, which means rare, strange, wondrous, extraordinary, wonderful; having unusually good qualities, excellent, admirable; select, better, superior, choice”), equivalent to seld +‎ -ly. Cognate with Scots selly, silly (“approved, good, worthy”), Old Saxon seldlīk (“rare, wonderful”)</p>
        </div>
      </div>

    </div>
  </div>

<!--Sticky Button-->
      <div class="text-center">
            <button data-toggle="modal" data-target="#did-you-know-txt" class="toggle_btn absolute-position toggle-nav toggle-position">
                    <img width="20" src="{{Utility::getImage('/catalog/did-you-know.png')}}" alt="did you know">
                    <span>Did you know</span>
             </button>
      </div>

<!--sticky socail icons-->
<div class="sticky-container hidden-xs">
   <ul class="sticky">
      <li onclick="location = 'https://www.facebook.com/www.Selly.pk/'">
         <img width="32" height="32" title="" alt="facebook" src="{{Utility::getImage('/catalog/facebook.png')}}">
         <p>Facebook</p>
      </li>
      <li onclick="location = 'https://twitter.com/sellypk'">
         <img width="32" height="32" title="" alt="twitter" src="{{Utility::getImage('/catalog/twitter.png')}}">
         <p>Twitter</p>
      </li>
      <li onclick="location = 'https://plus.google.com/+SellyPk'">
         <img width="32" height="32" title="" alt="google" src="{{Utility::getImage('/catalog/google+.png')}}">
         <p>Google +</p>
      </li>
      <li onclick="location = 'https://www.instagram.com/selly.pk/'">
         <img width="32" height="32" title="" alt="instagram" src="{{Utility::getImage('/catalog/instagram.png')}}">
         <p>Instagram</p>
      </li>
   </ul>
</div>
<!--sticky socail icons-->
<!--Newsletter Modal Start Here-->
{{--<div class="model-wrapper">--}}
        {{--<div class="modal-bgclr"></div>--}}
        {{--<div class="newletter-popup">--}}
            {{--<div id="boxes" class="newletter-container">--}}
                {{--<div id="dialog" class="window">--}}
                    {{--<div id="popup2">--}}
                        {{--<span class="b-close"><span>X</span>--}}
                     {{--</span>--}}
                    {{--</div>--}}
                    {{--<div style="background:url('{{url('public')}}/images/catalog/selly-modal-banner.jpg') no-repeat center;background-size: cover;">--}}

                    {{--</div>--}}
                    {{--<img src="{{url('public/images/catalog/selly-modal-banner.jpg')}}" class="img-responsive onload-modal-img">--}}
                    {{--<a href="https://coronakoharanahai.selly.pk/" class="btn help-out-btn">HELP OUT NOW! <img src="{{url('public/images/catalog/arrow_right.png')}}" class="img-responsive"></a>--}}
                    {{--<div class="onload-modal-text">--}}
                    {{----}}
                    {{--For more information, feedback & suggestion Dial <a href="tel:03041117355" class="onload-modal-contact-num" title="Call us 0304 111 7355">0304 - 111 - 7355</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- /.box -->--}}
        {{--</div>--}}
{{--</div>--}}
<!--Newsletter Modal End Here-->

<!--Top Bar-->
{{--@include('templates.imports.shared.top-bar')--}}
{{--endtopbar--}}
{{--header--}}
{{--<div class="container">--}}
    {{--<div class="row">--}}
        {{--<div class="app-banner">--}}
            {{--<i class="fa fa-times close-app-banner-icon" onclick="hideFunction()"></i>--}}
            {{--<a href="selly-app.html">--}}
                {{--<img src="{{Utility::getImage('app-banner/selly-app.gif')}}" alt="selly app">--}}
            {{--</a>--}}
        {{--</div>--}}
    {{--</div>--}}

{{--</div>--}}
<div class="header-container">
    @include('templates.imports.shared.header')
    @include('templates.imports.shared.main-nav')
</div>


{{--endheader--}}
{{--BreadCrumbs--}}
{{--<div class="content-top-breadcum">--}}
{{--<div class="container">--}}
{{--<div class="row">--}}
{{--<div id="title-content"></div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--End BreadCrumb--}}

<div class="container">
    @yield('content')
</div>

<!-- Footer -->
@include('templates.imports.shared.footer')

<!--Modals-->
@include('layouts.modals.newsletter')
@include('layouts.modals.login')
@include('layouts.modals.forgotpassword')
@include('layouts.modals.resetpassword')
@include('layouts.modals.register')
<!-- Script -->
@include('templates.imports.shared.script')

@yield('script')


{{--endscript--}}
{{--endfooter--}}
<script>
    function hideFunction() {
        $('.app-banner').hide()
    }
</script>

</body>
</html>

