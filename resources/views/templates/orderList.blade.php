@extends('templates.layout')
@section('content')
    <div class="container">
        <div class="row home_row">
        <div id="content" class="col-sm-12">
            <div class="container">
                <div class="row home_row">
                    <h1 class="account-heading">My Account</h1>
                    <ul class="account-links">
                        <li>
                            <a href="#">New List Subscription</a>
                        </li>
                    </ul>

                    @foreach($orderlist as $order)
                    <div class="main-block">
                        <div class="account-block">

                            <div class="row">
                                <div class="col-md-9">
                                    <div class="plus_minus-btn">
                                        <a class="btn plus-minus" data-toggle="collapse" href="#collapseExample{{$order->id}}" onclick="gerOrderItems('{{$order->id}}')" aria-expanded="true" aria-controls="collapseExample{{$order->id}}"></a>
                                    </div>
                                    <div class="order-details">
                                        <div>
                                            <span class="order-number">{{$order->orderNumber}}</span>
                                            <span class="date">{{date('M d, Y',strtotime($order->created_at))}}</span>
                                            <span class="time">{{date('g:i A',strtotime($order->created_at))}}</span>
                                            <span class="contact-seller">
                           {{--<i class="fa fa-envelope"></i>--}}
                                                {{--<a href="#">Contact Seller</a>--}}
                           </span>
                                        </div>
                                        <div>
                                            <span>Total Amount :</span>
                                            <strong>{{$order->total}}Rs</strong>
                                            <span class="item_text">Items :{{$order->total}}</span>
                                            <b>19</b>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 order-status">
                                    <div>
                                        {{--<p class="mb-0">Deduct From Salary</p>--}}
                                        @php
                                        $orderStatusId=isset($order->shipping_status_id)?isset($order->shipping_status_id):'';
                                        @endphp
                                        <span>Fulfilment Status:</span><a href="#">{{$fulfilmentStatus->where('id','=',$order->shipping_status_id)->pluck('name')->first()}}</a>
                                    </div>
                                </div>
                                {{--<div class="col-md-1">--}}
                                    {{--<button class="btn re-order-btn">Re-Order</button>--}}
                                {{--</div>--}}
                            </div>

                        </div>

                        <div class="collapse" id="collapseExample{{$order->id}}">

                        </div>

                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
@section('script')

    <script>
        function gerOrderItems(orderId) {

            $.ajax({
                url: "{{ url('order/Items') }}",
                type: 'get',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data:{'orderId':orderId},
                success: function (data)
                {
$('#collapseExample'+orderId).html(data);
                },
            });
        }
    </script>

@stop