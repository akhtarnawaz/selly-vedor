<div class="box blogs hidden-xs">

    <div class="">
        <div class="box-head">
            <div class="box-heading">Available Brands</div>

        </div>
        <div class="box-content" style="margin-top: 0;">
            <div class="box-product  owl-carousel clientscarousel " id="blog-carousel">
                <div class="blog-item">
                    <div class="product-block">
                        <div class="blog-left">
                            <div class="blog-image clients-img">
                                <img src="{{Utility::getImage('catalog/client-daldalogo.png')}}" alt="Latest Blog" title="Latest Blog" class="img-thumbnail" />
                                <div class="post-image-hover"> </div>

                            </div>
                        </div>

                    </div>
                </div>
                <div class="blog-item">
                    <div class="product-block">
                        <div class="blog-left">
                            <div class="blog-image clients-img">
                                <img src="{{Utility::getImage('catalog/client-pepsilogo.png')}}" alt="Latest Blog" title="Latest Blog" class="img-thumbnail" />
                                <div class="post-image-hover"> </div>

                            </div>
                        </div>

                    </div>
                </div>
                <div class="blog-item">
                    <div class="product-block">
                        <div class="blog-left">
                            <div class="blog-image clients-img">
                                <img src="{{Utility::getImage('catalog/client-pamperlogo.png')}}" alt="Latest Blog" title="Latest Blog" class="img-thumbnail" />
                                <div class="post-image-hover"> </div>

                            </div>
                        </div>

                    </div>
                </div>
                <div class="blog-item">
                    <div class="product-block">
                        <div class="blog-left">
                            <div class="blog-image clients-img">
                                <img src="{{Utility::getImage('catalog/client-shanlogo.png')}}" alt="Latest Blog" title="Latest Blog" class="img-thumbnail" />
                                <div class="post-image-hover"> </div>

                            </div>
                        </div>

                    </div>
                </div>
                <div class="blog-item">
                    <div class="product-block">
                        <div class="blog-left">
                            <div class="blog-image clients-img">
                                <img src="{{Utility::getImage('catalog/client-p&glogo.png')}}" alt="Latest Blog" title="Latest Blog" class="img-thumbnail" />
                                <div class="post-image-hover"> </div>

                            </div>
                        </div>

                    </div>
                </div>
                <div class="blog-item">
                    <div class="product-block">
                        <div class="blog-left">
                            <div class="blog-image clients-img">
                                <img src="{{Utility::getImage('catalog/client-johnsons.png')}}" alt="Latest Blog" title="Latest Blog" class="img-thumbnail" />
                                <div class="post-image-hover"> </div>

                            </div>
                        </div>

                    </div>
                </div>
                <div class="blog-item">
                    <div class="product-block">
                        <div class="blog-left">
                            <div class="blog-image clients-img">
                                <img src="{{Utility::getImage('catalog/client-cocacolalogo.png')}}" alt="Latest Blog" title="Latest Blog" class="img-thumbnail" />
                                <div class="post-image-hover"> </div>

                            </div>
                        </div>

                    </div>
                </div>


            </div>
            <div class="buttons text-center">
                {{--<button type="button" onclick="';" class="btn btn-primary">See all Blogs</button>--}}
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.clientscarousel').owlCarousel({
            items: 6,
            singleItem: false,
            navigation: true,
            autoPlay: true,
            navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
            pagination: true,
            itemsDesktop: [1200, 4],
            itemsDesktopSmall: [979, 3],
            itemsTablet: [767, 2],
            itemsMobile: [479, 1]
        });
    });
</script>