{{--Header--}}
<div class="container">
    <div class="row">
        <div class="header-center">
            <div class="content_header_topleft">
                <div class="header-left">
                    <div class="col-sm-4 header-logo">
                        <div id="logo">
                            <a href="{{url('/')}}">
                                <img src="{{asset($public_path.'images/template/selly.png')}}"
                                     title="Selly" alt="Selly" class="img-responsive"/>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 header-search">
                    <div class="search"><i class="fa fa-search"></i></div>
                    <form action="{{url('search')}}" method="get">
                        <div id="search" class="input-group searchtoggle">
                        <!-- <div class="col-sm-3 sort">
                                <select name="category" class="form-control">
                                    <option value="">{{__('lang.all_categories')}}</option>
                                    @foreach($categories as $item)
                            <option value="{{$item->cleanURL}}">{{$item->category_title}} </option>
                                    @endforeach

                                </select>
                            </div> -->
                            <div id="searchbox">
                                <input type="text" name="search" value="" placeholder="{{__('lang.search_products')}}"
                                       class="form-control input-lg search-autocomplete"/>
                                <span class="input-group-btn">
                              <button type="submit" class="btn btn-default btn-lg"><span
                                          class="search fa fa-search"></span></button>
                              </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="head-right-bottom">
                <div class="headertopright">
                    <div class="text2">
                        <div class="cms-data-info">
                            <div class="text2-dec">{{__('lang.info_services')}}</div>
                            <a>
                                {{--<a href="index2724.html?route=information/contact">--}}
                                <span class="hidden-xs hidden-sm hidden-md">{{__('lang.info_services_num')}}</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

