<footer>
    <div id="footer" class="container">
        <div class="row">
            <div class="footer-blocks">
                <div class="col-sm-5 clearfix" id="footer_aboutus_block">

                    <ul>
                        <li class="tm-about-logo">
                            <a href="{{url('/')}}">
                                <img alt="Selly" src="{{asset($public_path.'images/template/selly.png')}}">
                            </a>
                        </li>
                        <li class="about">Selly is a symbol of swift delivery of Premium Quality Fresh Fruits and Vegetables city-wide. We are dedicated to meet the daily grocery needs of our valued customers. Our vision is to become the leading supplier of Fresh Fruits and Vegetables across the Country. </li>
                        <li class="follow">Follow Us </li>
                        <li class="facebook social">
                            <a href="https://www.facebook.com/www.Selly.pk/" target="_blank">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="twitter social">
                            <a href="https://twitter.com/SellyPk?ref_src=twsrc%5Etfw&ref_url=https%3A%2F%2Fselly.pk%2F" target="_blank">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="gplus social">
                            <a href="https://plus.google.com/+SellyPk" target="_blank">
                                <i class="fa fa-google-plus" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="linkedin social">
                            <a href="https://www.instagram.com/selly.pk/" target="_blank">
                                <i class="fa fa-instagram" aria-hidden="true"></i>

                            </a>
                        </li>
                    </ul>
                    <br>
                </div>

                <div id="info" class="col-sm-3">
                    <h5>Helpful Resourses</h5>
                    <ul class="list-unstyled">
                        <li><a href="{{url('about-us')}}">About Us</a></li>
                        <li><a href="{{url('privacy-policy')}}">Privacy Policy</a></li>
                        <li><a href="{{url('return-policy')}}">Return Policy</a></li>
                        <li><a href="{{url('terms-conditions')}}">Terms &amp; Conditions</a></li>
                    </ul>

                    <br>
                    <a href="https://play.google.com/store/apps/details?id=com.p3care.sellypk" target="_blank">
                        <img width="150" src="{{url('public/images/catalog/andriod-app.png')}}" alt="andriod-app"></a>

                </div>
                <div class="col-sm-4" id="footer_storeinformation_block">
                    <h5>contact us<a class="mobile_togglemenu">&nbsp;</a></h5>
                    <ul>
                        <li class="address">405, Al- Hafeez Shopping Mall,  </li>
                        <li>Main Boulevard Gulberg III. Lahore- 54670. Pakistan</li>
                        <li class="call-num">0304 - 111 - 7355 (Cell)</li>
                        <li class="phone">
                        042 - 3 577 4724, 26 - 27
                         </li>
                        <li class="email">
                            <span class="email">Email:</span>
                            <a href="mailto:support@selly.pk">support@selly.pk</a>
                            
                        </li>
                        <li class="payment">Payment Method</li>
                        <li class="cash-on-delivery">Cash on Delivery</li>
                       
                    </ul>
                </div>
            </div>
        </div>
        <!--<hr>-->
    </div>
    <div class="bottomfooter">
        <div class="container">
            <div class="row">
                <ul class="list-unstyled">
                    <li><a href="{{url('about-us')}}">About Us</a></li>
                    <li><a href="{{url('privacy-policy')}}">Privacy Policy</a></li>
                    <li><a href="{{url('return-policy')}}">Return Policy</a></li>
                    <li><a href="{{url('terms-conditions')}}">Terms &amp; Conditions</a></li>
                </ul>
                <p class="powered"> &copy;  2017 - 2018 <a href="https://www.selly.pk/">Selly.pk.</a> All rights reserved.  </p>
            </div>
        </div>
    </div>
</footer>
<script>
    $('#registrationPopup').on('shown.bs.modal', function () {
        $('#registrationPopup').trigger('focus')
    });

</script>




<!-- Registration modal Start Here -->
<div class="modal fade" id="registrationPopup" tabindex="-1" role="dialog" aria-labelledby="registrationLabel">
    <div class="modal-dialog" role="document">
        <div class="login-form">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="register_error">
                <form id="form_register" name="form_register" method="POST" action="{{ route('registerCustomer') }}">
                    {{ csrf_field() }}
                    <h3 class="text-center">New Account</h3>
                    <div class="text-center social-btn">
                        <a href="{{url('/redirect/facebook')}}" class="btn btn-primary btn-lg"><i class="fa fa-facebook"></i> Sign in with <b>Facebook</b></a>
                        {{--<a href="#" class="btn btn-info btn-lg"><i class="fa fa-twitter"></i> Sign in with <b>Twitter</b></a>--}}
                        <a href="{{url('/redirect/google')}}" class="btn btn-danger btn-lg"><i class="fa fa-google"></i> Sign in with <b>Google</b></a>
                    </div>
                    <div class="or-seperator"><b>or</b></div>
                    <h4 style="color: red;" class="register_error_msg text-center"></h4>
                    <div class="form-group">
                        <input type="text" value="" class="form-control input-lg" id="username" name="username" placeholder="Username" required>
                    </div>
                    <div class="form-group">
                        <input type="email" value="" class="form-control input-lg" id="registeremail" name="registeremail" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <input type="text" value="" class="form-control input-lg" id="address" name="address" placeholder="Address" required>
                    </div>
                    <div class="form-group">
                        <input type="text" value="" class="form-control input-lg" id="phone" name="phone" placeholder="Cell No." required>
                    </div>
                    <div class="form-group">
                        <input type="password" value="" class="form-control input-lg" id="password" name="password" placeholder="Password" required>
                    </div>
                    <div class="form-group">
                        <input type="password" value="" class="form-control input-lg" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password" required>
                    </div>
                    <div class="text-center mb-15">
                        <label><input type="checkbox">Sign up or Selly.pk news, sales and deals</label>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-lg login-btn">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    <!-- Registration modal End Here -->
{{--<!-- Login modal Start Here -->--}}
<div class="modal fade" id="loginPopup" tabindex="-1" role="dialog" aria-labelledby="loginPopupLabel">
    <div class="modal-dialog" role="document">
        <div class="login-form">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <form id="form_login" name="form_login" method="POST" action="{{ route('loginCustomer') }}">
                {{csrf_field()}}
                <h3 class="text-center">Sign in</h3>
                <div class="text-center social-btn">
                    {{--<a href="{{url('/redirect/facebook')}}" class="btn btn-primary btn-lg"><i class="fa fa-facebook"></i> Sign in with <b>Facebook</b></a>--}}
                    {{--<a href="#" class="btn btn-info btn-lg"><i class="fa fa-twitter"></i> Sign in with <b>Twitter</b></a>--}}
                    <a href="{{url('/redirect/google')}}" class="btn btn-danger btn-lg"><i class="fa fa-google"></i> Sign in with <b>Google</b></a
                </div>
                <div class="or-seperator"><b>or</b></div>
                <h4 style="color: red;" class="login_error_msg text-center"></h4>
                <div class="form-group">
                    <input type="text" class="form-control input-lg" id="login-username" name="username" placeholder="Username" required>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control input-lg" id="login-password" name="password" placeholder="Password" required>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-lg login-btn">Sign in</button>
                </div>
                <div class="text-center dont-have-account">
                    <a data-toggle="modal" data-target="#Sellyregistration" class="pull-left">Create New Account</a>
                    {{--<a type="button" data-toggle="modal" data-target="#Sellyregistration" class="pull-left">Create New Account</a>--}}
                    {{--<a href="#" class="pull-left">Create New Account</a>--}}
                    <a type="button" data-toggle="modal" data-target="#forgot" class="pull-right">Forgot Password</a>
                </div>
            </form>
        </div>
    </div>
</div>