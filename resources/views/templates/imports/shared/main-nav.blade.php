@php($logged_in = \Illuminate\Support\Facades\Auth::check())
<header>
    <!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1225273707576176');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1"
               src="https://www.facebook.com/tr?id=1225273707576176&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->
    <div class="container">
        <div class="row">
            <div class="header-main">
                <div class="header_left">
                    <div class="box-category">
                        <div class="box-heading border">Categories<i class="fa fa-angle-down" aria-hidden="true"></i>
                        </div>
                    </div>
                    <!--<div class="box-heading">Categories <i class="fa fa-angle-down" aria-hidden="true"></i></div>-->
                    <div class="box-content-category">
                        <ul id="nav-one" class="dropmenu">
                            @foreach($categories as $item)
                                <li class="top_level"><a href="{{url($item->cleanURL)}}">{{$item->category_title}}</a>
                            @endforeach

                        </ul>
                    </div>
                </div>
                <div class="header-right">
                    <div class="head-right-top">
                        <ul class="static_links">
                            <li class="head-links"><a href="{{url('/')}}">Home</a></li>
                            <li>
                                <a href="{{url('about-us')}}">About Us</a>
                            </li>
                            <li>
                                <a href="{{url('privacy-policy')}}">Privacy Policy</a>
                            </li>

                            <li class="head-links"><a href="{{url('blog')}}">Blog</a></li>

                            {{----}}
                            {{--<li class="head-links"><a href="">Brands</a></li>--}}
                            {{--<li class="head-links"><a href="#">Specials</a></li>--}}
                            {{--<li class="head-links"><a href="#">Blog</a></li>--}}
                            {{--<li class="head-links"><a href="#">Contact Us</a></li>--}}
                            {{--@foreach($categories as $item)--}}
                            {{--<li class="head-links"><a href="{{url($item->cleanURL)}}">{{$item->category_title}}</a>--}}
                            {{--</li>--}}
                            {{--@endforeach--}}
                        </ul>
                        <div class="header-topr">
                            <div class="head">
                                <div class="col-sm-3 header-cart">
                                    <div id="cart" class="btn-group btn-block">
                                        <button type="button" data-toggle="dropdown" data-loading-text="Loading..."
                                                class="btn btn-inverse btn-block btn-lg dropdown-toggle"><i
                                                    class="fa fa-shopping-basket"></i><span id="cart-total">0</span>
                                        </button>

                                        <h4>My Cart</h4>
                                        <ul class="dropdown-menu pull-right cart-menu cart-dropdown" id="cart-scroll">
                                            <li>
                                                <p class="text-center">Your shopping cart is empty!</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="dropdown myaccount"><a href="indexe223.html?route=account/account" title="My Account" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>
                                            {{--<span class="hidden-xs hidden-sm hidden-md">My Account</span>--}}
                                            <span class="hidden-xs hidden-sm hidden-md" style="color: #fff;">{{$logged_in?\Illuminate\Support\Facades\Auth::user()->username:'Login / Register'}}</span>
                                            <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                        <ul class="dropdown-menu dropdown-menu-right myaccount-menu" style="display: none;">
                                            @if(!$logged_in)
                                                <li><a href="#" data-toggle="modal" data-target="#loginPopup">Login</a></li>
                                                <li><a href="#" data-toggle="modal" data-target="#registrationPopup">Register</a>
                                                </li>
                                            @elseif($logged_in)
                                                <li><a href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                          style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>

                                                </li>
                                                <li>@php $id=\Illuminate\Support\Facades\Auth::user()->id; @endphp<a href="{{ url('order/list',[$id] )}}">Order List</a></li>
                                            @endif
                                            {{--<li><a href="#" data-toggle="modal" data-target="#loginPopup">Login</a></li>--}}
                                            {{--<li><a href="#" data-toggle="modal" data-target="#registrationPopup">Register</a>--}}
                                            {{--</li>--}}
                                        </ul>
                                    </div>
                                    <!--<i class="fa fa-shopping-basket" aria-hidden="true"></i>--></div>

                                <div class="display-ib andriod-app-img">
                                    <a href="selly-app.html" target="_blank">
                                        <img src="{{asset(Config::get('constants.public_path').'images/catalog/andriod-app.png')}}" alt="Andriod App"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-content">
        <nav class="nav-container" role="navigation">
            <div class="nav-inner container">
                <div class="row">
                    <div class="serach-inner">
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>
{{--<div id="login-model" class="modal fade order-datail-modal" role="dialog" data-backdrop="static">--}}
    {{--<div class="modal-dialog">--}}
        {{--<div class="modal-content">--}}
            {{--<div class="modal-header">--}}
                {{--<div class="cart-item-delete" id="cart-item-delete">--}}
                    {{--<i class="" aria-hidden="true" data-dismiss="modal">--}}
                        {{--<img src="{{asset($public_path.'images/template/cross-popup-img.png')}}">--}}
                    {{--</i>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 popup-login">--}}
                {{--<a href="{{url('/')}}">--}}
                    {{--<img src="{{asset($public_path.'images/template/selly.png')}}"--}}
                         {{--title="Selly" alt="Selly" class="img-responsive"/>--}}
                {{--</a>--}}
                {{--<div class="adv-text">--}}
                    {{--<strong>Advantages of our secure login</strong>--}}
                    {{--<ul>--}}
                        {{--<li><span> <i class="fa fa-car"></i></span>--}}
                            {{--<p>Easily Track Orders, Hassle free Returns</p></li>--}}
                        {{--<li><span><i class="fa fa-bell"></i></span>--}}
                            {{--<p>Get Relevant Alerts and Recommendation</p></li>--}}
                        {{--<li>--}}
                            {{--<span><i class="fa fa-star"></i></span>--}}
                            {{--<p> Wishlist, Reviews, Ratings and more.</p>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 popup-right popup-right-bg">--}}
                {{--<div class="modal-body">--}}
                    {{--<h2>Login with your account</h2>--}}

                    {{--<ul class="sign-btns fg-farosh list-unstyled">--}}
                        {{--<li>--}}
                            {{--<a data-toggle="modal" data-target="#signUp-password-model" class="google-sign signUp-btns fb-sign hover-farosh"><img src="{{asset($public_path.'images/template/login-wink.png')}}" alt="Farosh icon" />Continue with Selly</a>--}}
                        {{--</li>--}}
                        {{--<li class="sign-btns">--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

{{--Modal 2--}}
{{--<div id="signUp-password-model" class="modal fade order-datail-modal signUp-password-model" role="dialog" data-backdrop="static">--}}
    {{--<div class="modal-dialog">--}}
        {{--<div class="modal-content">--}}
            {{--<div class="modal-header">--}}
                {{--<div class="cart-item-delete" id="cart-item-delete">--}}
                    {{--<p>--}}
                        {{--<i class="" aria-hidden="true" data-dismiss="modal">--}}
                            {{--<img src="{{asset($public_path.'images/template/cross-popup-img.png')}}">--}}
                        {{--</i>--}}
                    {{--</p>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="modal-body padding-bottom-0 popup-right-bg">--}}
                {{--<div class="col-md-6 popup-login">--}}
                    {{--<a href="{{url('/')}}">--}}
                        {{--<img src="{{asset($public_path.'images/template/selly.png')}}"--}}
                             {{--title="Selly" alt="Selly" class="img-responsive"/>--}}
                    {{--</a>--}}
                    {{--<h2>Hello</h2>--}}
                    {{--<p>Please login with your details & enjoy great shopping experience.</p>--}}
                {{--</div>--}}
                {{--<div class="col-md-6  popup-right popup-right-bg margin-top-30 padding-right-50 padding-left-50">--}}
                    {{--<form id="singUp-password-form" action="https://farosh.pk/user/password" method="post">--}}
                        {{--<input type="hidden" name="" value="">--}}
                        {{--<div class="sign-btns" align="left">--}}
                            {{--<div class="form-group preview-password">--}}
                                {{--<label class="control-label" style="" for="full_name">Full Name <span>*</span></label>--}}
                                {{--<div class="custom-input text-left">--}}
                                    {{--<input type="text" class="form-control" id="full_name" name="full_name" placeholder="Write Your Full Name" required>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group preview-password margin-bottom-0">--}}
                                {{--<label class="control-label" for="reg_password">Password <span>*</span></label>--}}
                                {{--<div class="custom-input  text-left">--}}
                                    {{--<input type="password" class="form-control show-hide-password " id="reg_password" name="password" placeholder="Write Your Password" required>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group preview-password">--}}
                                {{--<label class="control-label" for="confirm_password">Confirm Password <span>*</span></label>--}}
                                {{--<div class="custom-input text-left form-reg-errors">--}}
                                    {{--<input type="password" class="form-control show-hide-password" id="confirm_password" placeholder="Confirm Your Password" name="password_confirmation">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group margin-0">--}}
                                {{--<div>--}}
                                    {{--<button type="submit" class="global-btn pull-left phone-continue" id="signUP-password-continue">Sign In</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                    {{--<div class="or-wrapper sec-strt">--}}
                    {{--</div>--}}
                    {{--<ul class="sign-btns">--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

<!-- Registration modal End Here -->



