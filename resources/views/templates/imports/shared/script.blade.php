{{--Register User Using Laravel AUth--}}
<script>

    var cart = {
        'placeOrder':function () {
            showLoader(true);
            var email = $('#email').val();
            var storeLocation = $('#storeLocation').val();
            var full_name = $('#full_name').val();
            var address = $('#address').val();
            var phone = $('#phone').val();
            var ordernote = $('#ordernote').val();
            var date = $('#dep_date').val();


            $.ajax({
                url: 'cart/placeOrder', //Cart\CartController@placeorder
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data : {email:email , full_name:full_name , address:address , phone:phone, ordernote:ordernote, storeLocation:storeLocation, scheduleDate:date},
                dataType: 'json',
                success:function (json) {
                    if('success' in json && json['success'])
                    {
                        showLoader(false);
                        notify('Order Placed Successfully...!!!');
                        setTimeout(function(){ window.history.replaceState(null, null, "{{url('/')}}/checkoutSuccess"); }, 1000);
                        setTimeout(function(){ window.location = "{{url('/')}}"; }, 2000);

                    }
                }
            });
        },
        'updateTotalItems':function (total) {
            if(total<0)
            {
                total = 0;
            }

        },
        'get':function () {
            $.ajax({
                url: '{{url('cart/getCart')}}', //Cart\CartController@makeCart
                type: 'get',
                dataType: 'json',
                success:function (json) {
                    if('success' in json && json['success'])
                    {

                        var total = json['total'];
                        $('#cart > button').html('<i class="fa fa-shopping-basket"></i><span id="cart-total">' + total + '</span>');

                        if(total>0)
                        {
                            cart.getDropDown(json['items']);
                        }
                        else
                        {
                            html = "<li>\n" +
                                "    <p class=\"text-center\">Your shopping cart is empty!</p>\n" +
                                "</li>";
                            $('.cart-dropdown').html(html)
                        }
                    }
                }
            });
        },
        'getTotalItemsAndPrice':function () {
            var values;
            $.ajax({
                url: '{{url('cart/getTotalItemsAndPrice')}}', //Cart\CartController@makeCart
                type: 'get',
                dataType: 'json',
                async:false,
                success:function (json) {
                    if('success' in json && json['success'])
                    {
                        values = json;
                    }
                }
            });
            return values;
        },
        'setDropDownHtml':function (html) {
            $('.cart-dropdown').html(html);
        },
        'addReviews':function(){

            var inputname=$('#input-name').val();
            var inputreview=$('#input-review').val();
            var productId=$('#productId').val();

            var rating= document.querySelector('input[name="reviewStars"]:checked').value;

            $.ajax({
                type:'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:'{{url('cart/addreviews')}}',
                data:{'productId':productId,'name':inputname,'review':inputreview,'rating':rating},
                success:function(data) {

                    showLoader(false);
                    notify('Review Added Successfully...!!!');

                    $('#input-name').val('');
                    $('#input-review').val('');
                },
                error:function(){

                },
            });

        },
        'getDropDown':function (items) {
            $.ajax({
                url: '{{url('cart/getCartDropDown')}}', //Cart\CartController@makeCartDropDown
                type: 'get',
                data: 'items=' + JSON.stringify(items),
                success: function(html) {
                    cart.setDropDownHtml(html);
                }
            });
        },
        'add': function(product_id) {
            var quantity=$('#input-quantity').val();
            var itemquantity=$('#itemquantity').val();
            var price=$('#product-price_'+product_id).val();
            showLoader(false);
//            notify('Sorry, we are not accepting any orders at the moment. The Store is under up-gradation!\n' +
//                'Please visit us soon. Inconvenience is heartily regretted.');
            $.ajax({
                url: '{{url('cart/add')}}',
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1)+ '&price='+ price,
                dataType: 'json',
                success: function(json) {

                    if (json['redirect']) {
                        location = json['redirect'];
                    }
                    if (json['success']) {
                        notify(json['success']);
                        cart.updateTotalItems(json['total']);
                        cart.get();
                        // $('.header-cart > ul').load('index.php?route=common/cart/info ul li');
                    }
                }
            });
        },
        'update': function(key, quantity,counter) {
          var price=  $('#ItemPriceUnit_'+counter).val();
          $.ajax({
                url: '{{url('cart/update')}}',
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1)+ '&price='+ price,
                dataType: 'json',
                async : false,
                success: function(json) {
                    if(json['success'])
                    {

                        notify('Cart is Updated');
                        cart.get();
                        // setTimeout(function(){// wait for 5 secs(2)
                        //     // document.getElementsByClassName(table-responsive).contentDocument.location.reload(true);
                        //     location.reload('.table-responsive'); // then reload the page.(3)
                        //
                        // }, 500);

                    }
                }
            });
        },
         'removeall': function() {

          $.ajax({
                url: '{{url('cart/deleteall')}}',
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                // data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1)+ '&price='+ price,
                dataType: 'json',
                async : false,
                success: function(json) {
                    if(json['success'])
                    {
                        $("#checkout").hide();
                        notify('Item(s) deleted from your cart');
                        setTimeout(function(){// wait for 5 secs(2)
                            window.location = "https://selly.pk";// then reload the page.(3)
                        }, 5000);

                    }
                }
            });
        },

        'remove': function(key,counter) {

            var price=  $('#ItemPriceUnit_'+counter).val();

            $.ajax({
                url: '{{url('cart/delete')}}',
                type: 'post',
                data: 'key=' + key + '&price=' + price,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                dataType: 'json',
                async : false,
                success: function(json) {
                    if(json['success'])
                    {

                        $("#checkout").hide();
                        notify('Item(s) deleted from your cart');
                        cart.get();

                    }
                }
            });

        }
    };

    var wishlist = {
        'add': function (product_id) {

            $.ajax({
                url: 'cart/addtowishlist', //add to wishlist
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {
                    product_id: product_id,

                },
                dataType: 'json',
                success: function (json) {
                    if ('success' in json && json['success']) {
                        // showLoader(false);
                        notify('Product Successfully added to wishlist...!!!');
                        // setTimeout(function () {
                        //     window.location = "CheckedOutSuccessfully-ThankYou";
                        // }, 2000);
                    }
                    if('usernotfound' in json && json['usernotfound']){
                        // showLoader(false);
                        notify('User not found Please login...!!!');

                    }
                    if('false' in json && json['false']){
                        // showLoader(false);
                        notify('Product is already added wishlist...!!!');

                    }
                }
            });
        },

    }
    var compare = {
        'add': function (product_id) {

            $.ajax({
                url: 'cart/addtocompare', //add to wishlist
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {
                    product_id: product_id,

                },
                dataType: 'json',
                success: function (json) {
                    if ('success' in json && json['success']) {
                        // showLoader(false);
                        notify('Product Successfully added to Compare List...!!!');
                        // setTimeout(function () {
                        //     window.location = "CheckedOutSuccessfully-ThankYou";
                        // }, 2000);
                    }
                    if('usernotfound' in json && json['usernotfound']){
                        // showLoader(false);
                        notify('User not found Please login...!!!');

                    }
                    if('false' in json && json['false']){
                        // showLoader(false);
                        notify('Product is already added Compare List...!!!');

                    }
                }
            });
        },
    }
        $("#form_register").on("submit", function (e) {
        $('.register_error_msg').html('');
        e.preventDefault();
        var results = '';
        $.ajax({
            url: 'registerCustomer', //Cart\CartController@placeorder
            type: 'post',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
                username: $("#username").val(),
                email: $("#registeremail").val(),
                password: $("#password").val(),
                cell_no: $("#phone").val(),
                address: $("#address").val(),
                password_confirmation: $("#password_confirmation").val()
            },
            dataType: 'json',
            success: function (json) {
                if('success' in json)
                {
                    location.reload();
                }
                else
                {
                    var errors = "";
                    $.each(json, function(index,value ) {
                        errors+=value+"<br>";
                    });
                    $('.register_error_msg').append(errors);
                }
            }
        });
    });


    $("#form_login").on("submit", function (e) {
        $('.login_error_msg').html('');
        e.preventDefault();
        var results = '';
        $.ajax({
            url: 'loginCustomer', //Cart\CartController@placeorder
            type: 'post',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
                username: $("#login-username").val(),
                password: $("#login-password").val()
            },
            dataType: 'json',
            success: function (json) {
                if('success' in json)
                {
                    location.reload();
                }
                else
                {
                    var errors = "";
                    $.each(json, function(index,value ) {
                        errors+=value+"<br>";
                    });
                    $('.login_error_msg').append(errors);
                }
            }
        });
    });

 $("#form_resetpassword").on("submit", function (e) {
        $('.reset_error_msg').html('');
        e.preventDefault();
        var results = '';
        $.ajax({
            url: 'resetuserPasword', //Cart\CartController@placeorder
            type: 'post',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
                oldpassword: $("#oldpassword").val(),
                newpassword: $("#newpassword").val(),
                confirmpassword: $("#confirmpassword").val()
            },
            dataType: 'json',
            success: function (json) {
                if('success' in json)
                {
                    var success = "";
                    $.each(json, function(index,value ) {
                        success+=value+"<br>";
                    });
                    $('.reset_success_msg').append(success);
                    location.reload();
                }
                else
                {
                    var errors = "";
                    $.each(json, function(index,value ) {
                        errors+=value+"<br>";
                    });
                    $('.reset_error_msg').append(errors);
                }
            }
        });
    });

</script>
<!-- ======= Quick view JS ========= -->
<script>
    function quickbox() {
        if ($(window).width() > 767) {
            $('.quickview-button').magnificPopup({
                type: 'iframe',
                delegate: 'a',
                preloader: true,
                tLoading: 'Loading image #%curr%...',
            });
        }
    }

    jQuery(document).ready(function () {
        quickbox();
    });
    jQuery(window).resize(function () {
        quickbox();
    });

    $(document).ready(function () {
        cart.get();
    });
</script>
{{--News Letter Modal JS--}}
<script>
    //Newsletter Modal JS
    $('.b-close').click(function () {
        $('.modal-bgclr').hide();
        $('.newletter-popup').hide();
    });
    $('.modal-bgclr').click(function () {
        $('.modal-bgclr').hide();
        $('.newletter-popup').hide();
    });
    //Newsletter Modal JS
</script>
<!-- ======= Login modal JS ========= -->
{{--<script>--}}
    {{--function testAnim(x) {--}}
        {{--$('.modal .modal-dialog').attr('class', 'modal-dialog  ' + x + '  animated');--}}
    {{--};--}}
    {{--$('#loginPopup').on('show.bs.modal', function (e) {--}}
        {{--var anim = $('#entrance').val();--}}
        {{--testAnim("bounceIn");--}}
    {{--});--}}
    {{--$('#loginPopup').on('hide.bs.modal', function (e) {--}}
        {{--var anim = $('#exit').val();--}}
        {{--testAnim("flipOutX");--}}
        {{--$(window)--}}
    {{--});--}}

{{--</script>--}}
<!-- ======= Registration modal JS ========= -->
<script>
    function testAnim(x) {
        $('.modal .modal-dialog').attr('class', 'modal-dialog  ' + x + '  animated');
    };
    $('#registration').on('show.bs.modal', function (e) {
        var anim = $('#entrance').val();
        testAnim("bounceIn");
    });
    $('#registration').on('hide.bs.modal', function (e) {
        var anim = $('#exit').val();
        testAnim("flipOutX");
        $(window)
    });

</script>
<script>
    function testAnim(x) {
        $('.modal .modal-dialog').attr('class', 'modal-dialog  ' + x + '  animated');
    };
    $('#Sellyregistration').on('show.bs.modal', function (e) {
        var anim = $('#entrance').val();
        testAnim("bounceIn");
    });
    $('#Sellyregistration').on('hide.bs.modal', function (e) {
        var anim = $('#exit').val();
        testAnim("flipOutX");
        $(window)
    });

</script>
<script>

    //Newsletter Modal JS
    $('.b-close').click(function(){
        $('.modal-bgclr').hide();
        $('.newletter-popup').hide();
    });
    $('.modal-bgclr').click(function(){
        $('.modal-bgclr').hide();
        $('.newletter-popup').hide();
    });
    //Newsletter Modal JS
</script>

