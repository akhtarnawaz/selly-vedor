{{--Top Bar--}}
<nav id="top">
    <div class="container">
        <div class="container-top">
            <div class="lang-curr">
                <div class="pull-left">
                    <form action="" method="post" enctype="multipart/form-data" id="form-currency">
                        <div class="btn-group">
                            <button class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                <span class="hidden-xs hidden-sm hidden-md">{{__('lang.home_top_bar_usd')}}</span> <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </button>
                            {{--<ul class="dropdown-menu currency-menu">--}}
                                {{--<li><button class="currency-select btn btn-link btn-block" type="button" name="EUR">€ Euro</button></li>--}}
                                {{--<li><button class="currency-select btn btn-link btn-block" type="button" name="GBP">£ Pound Sterling</button></li>--}}
                                {{--<li><button class="currency-select btn btn-link btn-block" type="button" name="USD">$ US Dollar</button></li>--}}
                            {{--</ul>--}}
                        </div>
                        <input type="hidden" name="code" value="" />
                        <input type="hidden" name="redirect" value="" />
                    </form>
                </div>
                <div class="pull-left">
                    <form action="" method="post" enctype="multipart/form-data" id="form-language">
                        <div class="btn-group">
                            <button class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                <img src="{{asset($public_path.'images/template/language/en-gb/en-gb.png')}}" alt="English" title="English">
                                <span class="hidden-xs hidden-sm hidden-md">{{__('lang.home_top_bar_english')}}</span>
                                {{--<i class="fa fa-angle-down" aria-hidden="true"></i>--}}
                            </button>
                            {{--<ul class="dropdown-menu language-menu">--}}
                                {{--<li><button class="btn btn-link btn-block language-select" type="button" name="ar"><img src="catalog/language/ar/ar.png" alt="Arabic" title="Arabic" /> Arabic</button></li>--}}
                                {{--<li><button class="btn btn-link btn-block language-select" type="button" name="en-gb"><img src="catalog/language/en-gb/en-gb.png" alt="English" title="English" /> English</button></li>--}}
                            {{--</ul>--}}
                        </div>
                        <input type="hidden" name="code" value="" />
                        <input type="hidden" name="redirect" value="" />
                    </form>
                </div>
            </div>
            <div class="nav pull-right">
                <div class="right-links">
                    <ul class="right-myaccount-menu">
                        <li><i class="fa fa-heart" aria-hidden="true"></i><a href="#" id="wishlist-total">{{__('lang.home_top_bar_wishlist')}} (0)</a></li>
                        <li><i class="fa fa-share" aria-hidden="true"></i><a href="#">{{__('lang.home_top_bar_checkout')}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>