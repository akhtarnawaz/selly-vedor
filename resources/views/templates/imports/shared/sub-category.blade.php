


    @foreach($childCategory as $category)
    <li  @if(count($category->childCategory) ) class="has-children" @endif>
        <a  @if(count($category->childCategory) )  href="#" @else href="{{url('/'.strtolower($category->cleanURL))}}" @endif >{{$category->category_title}}</a>

        <ul class="is-hidden">

            <li >
                @if(count($category->childCategory) )
                    @include('templates/imports/shared/sub-category',['childCategory' => $category->childCategory])
                @endif
            </li>
            {{--<li class="has-children">--}}
                {{--<a href="#0">Caps &amp; Hats</a>--}}

                {{--<ul class="is-hidden">--}}
                    {{--<li class="go-back"><a href="#0">Accessories</a></li>--}}
                    {{--<li class="see-all"><a href="#">All Caps &amp; Hats</a></li>--}}
                    {{--<li><a href="#">Beanies</a></li>--}}
                    {{--<li><a href="#">Caps</a></li>--}}
                    {{--<li><a href="#">Hats</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}

        </ul>
    </li>

            @endforeach
