<!-- CSS - Start -->
<link rel="stylesheet" type="text/css" href="{{asset($public_path.'css/template/OPC090216_3/stylesheet.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset($public_path.'css/template/OPC090216_3/megnor/carousel.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset($public_path.'css/template/OPC090216_3/megnor/custom.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset($public_path.'css/template/OPC090216_3/megnor/bootstrap.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset($public_path.'css/template/OPC090216_3/megnor/lightbox.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset($public_path.'css/template/OPC090216_3/megnor/animate.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset($public_path.'css/template/OPC090216_3/jquery.simpleGallery.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset($public_path.'css/template/OPC090216_3/jquery.simpleLens.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset($public_path.'css/template/OPC090216_3/megnor/search_suggestion.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset($public_path.'css/template/OPC090216_3/ken-burns.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset($public_path.'css/template/OPC090216_3/animate.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset($public_path.'css/template/OPC090216_3/jquery.datetimepicker.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset($public_path.'css/template/jquery/magnific-popup.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset($public_path.'css/template/jquery/jquery-ui.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset($public_path.'css/template/jquery/owl.carousel.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset($public_path.'css/template/jquery/owl.transitions.css')}}"/>
{{--<link rel="stylesheet" type="text/css" href="{{asset($public_path.'css/template/OPC090216_3/new-style.min.css')}}"/>--}}
<link rel="stylesheet" type="text/css" href="{{asset($public_path.'css/custom.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset($public_path.'css/responsive.css')}}"/>
<!-- CSS - End -->