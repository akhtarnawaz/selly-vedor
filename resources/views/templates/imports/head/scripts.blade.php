<!-- JavaScript - Start -->
<script type="text/javascript" src="{{asset($public_path.'javascript/template/jquery/jquery-2.1.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/jquery/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/megnor/custom.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/jquery/jquery.simpleLens.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/jquery/jquery.simpleGallery.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/megnor/jstree.min.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/megnor/carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/megnor/megnor.min.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/megnor/jquery.custom.min.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/megnor/jquery.formalize.min.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/lightbox/lightbox-2.6.min.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/megnor/tabs.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/megnor/jquery.elevatezoom.min.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/megnor/bootstrap-notify.min.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/megnor/jquery-migrate-1.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/megnor/jquery.easing.1.3.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/megnor/doubletaptogo.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/megnor/jquery.datetimepicker.full.min.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/magnific/jquery.magnific-popup.min.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/jquery/common.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/template/owl-carousel/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/progressive-image.js')}}"></script>
<script type="text/javascript" src="{{asset($public_path.'javascript/custom.js')}}"></script>

 <script src="{{asset($public_path.'catalog/view/javascript/custom.js')}}" type="text/javascript"></script>
<script src="{{asset($public_path.'catalog/view/javascript/search_suggestion/search_suggestion.js')}}" type="text/javascript"></script>
 <script src="{{asset($public_path.'catalog/view/javascript/jquery/ui/jquery-ui.js')}}" type="text/javascript"></script>
<!-- JavaScript - End -->