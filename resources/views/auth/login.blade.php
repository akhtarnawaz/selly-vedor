<!DOCTYPE html>
<html lang="en" class="fullscreen-bg">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link href="{{URL::asset('public/admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="{{URL::asset('public/admin/assets/css/style.css')}}" rel="stylesheet" type="text/css">
</head>
<body class="bgimg">
<!-- WRAPPER -->
<div id="wrapper">
    <div class="vertical-align-wrap">
        <div class="vertical-align-middle">
            <div class="auth-box ">
                <div class="left">
                    <div class="content">
                        <div class="header">
                            <div class="logo text-center">
                                <img src="{{URL::asset('public/admin/assets/img/login-icon.png')}}" alt="selly.pk">
                            </div>
                            <p class="lead">Login to your account</p>
                            @if (session('status'))
                                <div class="alert alert-danger">
                                    {{ session('status') }}
                                </div>
                            @endif
                        </div>
                        <form class="form-auth-small" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                                <label for="signin-email" class="control-label sr-only">User Name</label>
                                <div class="Position_Relative">
                                    <i class="fa fa-user-circle-o Position_Absolute"></i>
                                </div>
                                <input type="text" name="username" value="{{ old('username') }}" class="form-control" id="username" value="" placeholder="User Name" required autofocus>
                                @if ($errors->has('username'))
                                    <span class="help-block"><strong>{{ $errors->first('username') }}</strong></span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="signin-password" class="control-label sr-only">Password</label>
                                <div class="Position_Relative">
                                    <i class="fa fa-lock Position_Absolute"></i>
                                </div>
                                <input type="password" name="password" class="form-control" id="password" value="" placeholder="Password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                @endif
                            </div>
                            <!--<div class="form-group clearfix">
                               <label class="fancy-checkbox element-left custom-bgcolor-blue">
                               <input type="checkbox">
                               <span class="text-muted">Remember me</span>
                               </label>
                               <span class="helper-text element-right">Don't have an account? <a href="#">Register</a></span>
                            </div>-->
                            <button type="submit" class="btn btn-primary btn-lg btn-block login-btn">LOGIN</button>
                            <!--<div class="bottom">
                               <span class="helper-text"><i class="fa fa-lock"></i> <a href="#">Forgot password?</a></span>
                            </div>-->
                        </form>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>