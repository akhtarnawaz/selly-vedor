<tr>
    <th>OrderNumber</th>
    <th>Customer Name</th>
    <th>Customer Email</th>
    <th>Customer Address</th>
    <th>Customer Number</th>
    <th>Order Total</th>
    <th>Order Discount</th>
    <th>Order Discount Total</th>
    <th>shipping status</th>
    <th>payment status</th>
    <th>payment method</th>
    <th>orders date</th>
    <th>Staff note</th>
    <th>Process By</th>
    <th>Delivered By</th>
</tr>
<br>
@php
    $grandTatal=0;
@endphp

@foreach($data as $order)
    @foreach($order as $orderitem)
        <tr>
            <th>{{$orderitem->orderNumber}}</th>
            @if($orderitem->customer_id != null)
                <th>{{$orderitem->orderCustomer->username or ''}}</th>
                <th>{{$orderitem->orderCustomer->email or ''}}</th>
                <th>{{$orderitem->orderCustomer->address or ''}}</th>
                <th>{{$orderitem->orderCustomer->phoneno or ''}}</th>

            @else
                <th>{{$orderitem->orderUser->username or ''}}</th>
                <th>{{$orderitem->orderUser->email or ''}}</th>
                <th>{{$orderitem->orderUser->address or ''}}</th>
                <th>{{$orderitem->orderUser->cell_no or ''}}</th>
            @endif
            <th>{{$orderitem->total}}</th>
            <th>{{$orderitem->discount}}@if($orderitem->discount > 0)%@endif</th>
            <th>{{$orderitem->discountedAmount}}</th>
            <th>{{$orderitem->orderShippingStatus->name or ''}}</th>
            <th>{{$orderitem->orderPaymentStatus->name or ''}}</th>

            <th>COD</th>
            <th>{{$orderitem->created_at}}</th>
            <th>{{$orderitem->staff_note}}</th>


            <th>{{$orderitem->ordersprocess->username or ''}}</th>
            <th>{{$orderitem->orderDelieverdBy->username or ''}}</th>

        </tr>
    @endforeach
@endforeach
