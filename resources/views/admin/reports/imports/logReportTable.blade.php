<table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
        <th>Sr. #</th>
        <th>Order #</th>
        <th>Action Message</th>
        <th>Action By</th>
        <th>Action Time</th>
    </tr>
    </thead>
    <tbody>
    @php
        $counter = 1;
    @endphp
    @foreach($data as $item)
        @php
            $orderNumber = \App\Models\Orders::where('id',$item->order_id)->first()->orderNumber;
            if($item->user_id == null)
            {
                $userName = "";
            }
            else
            $userName = \App\Models\Admin\Users::where('id',$item->user_id)->first()->username;
        @endphp
        <tr>
            <td>{{$counter}}</td>
            <td>{{$orderNumber}}</td>
            <td>{{$item->message}}</td>
            <td>{{$userName}}</td>
            <td>{{date('h:i:s A  d-M-Y',strtotime($item->created_at))}}</td>
        </tr>
        @php
            $counter++;
        @endphp
    @endforeach

    </tbody>
</table>

<script>
    $(document).ready(function () {
        $('#example').DataTable({
            paging: false
        });

    });
</script>