<table id="stock-log-table" class="table table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
        <th>Sr. #</th>
        <th>Order #</th>
        <th>Product Name</th>
        <th>Action Message</th>
        <th>Action By</th>
        <th>Action Time</th>
    </tr>
    </thead>
    <tbody>
    @php
        $counter = 1;

    @endphp
    @foreach($data as $item)
        @php
        if($item->order_id!=null)
        {
            $order = \App\Models\Orders::where('id',$item->order_id)->first();
            if($order)
            {
                $orderNumber = $order->orderNumber;
            }
            else
            {
                $orderNumber = '';
            }
        }
        else
        {
            $orderNumber = '';
        }

        if($item->user_id == null)
        {
            $userName = "";
        }
        else
        $userName = \App\Models\Admin\Users::where('id',$item->user_id)->first()->username;
       if($item->product_id == null)
        {
            $productName = "";
        }
        else
        $productName = \App\Models\Admin\Products::where('id',$item->product_id)->first();
        @endphp
        <tr>
            <td>{{$counter}}</td>
            <td>{{$orderNumber}}</td>
            <td>@if($productName == null) {{$item->product_name}} @else{{$productName->product_title}}@endif</td>
            <td>{{$item->message}}</td>
            <td>{{$userName}}</td>
            <td>{{date('h:i:s A  d-M-Y',strtotime($item->created_at))}}</td>
        </tr>
        @php

             $counter++;
        @endphp
    @endforeach

    </tbody>
</table>

<script>
    $(document).ready(function () {
        $('#stock-log-table').DataTable({
            paging: false
        });

    });
</script>