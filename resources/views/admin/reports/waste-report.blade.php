@extends('admin.layouts.master')
@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main" class="stock-management-data">
            <div class="row">

            </div>
            <!-- /.col-lg-12 -->

            <div class="col-md-12">

                <div class="search-conditions-box clearfix">
                    <form  method="post" action="{{ url('/admin/reports/getwaste-report') }}">
                        <input type="hidden" name="value" id="value" value="csv">
                        {{csrf_field()}}
                    <div class="col-md-2 pl-0">
                        {{--<button class="border_blue search_btn border_radius3 mr-12 add-record btn add_inputbtn"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add Product</button>--}}
                        {{--</div>--}}
                        <select class="e1 form-control" name="products" id="product" onchange="getProductInfo(this.value)">
                            <option value="">Select Product</option>
                            @foreach($productsData as $product)
                                <option value="{{$product->id}}">{{$product->product_title}}</option>
                            @endforeach
                        </select>
                    </div>
                    {{--<div class="col-md-2 pl-0" id="addunitvalue">--}}

                    {{--</div>--}}
                    <ul>
                        <li >


                            <button type="submit" class="border_blue search_btn  btn add_inputbtn">CSV Report
                            </button>


                        </li>

                    <li class="col-md-2">

                            <i class="ti-calendar"></i>
                            <input name="ordersource_date" id="daterange"
                                   class="relative_position width_100 input_padding border_blue border_radius3"
                                   type="text" placeholder="Enter Date Range" >
                        </li>
                    </form>

&nbsp;&nbsp;&nbsp;                        <li >
                            <button type="button" class="border_blue search_btn  btn add_inputbtn"
                                    onclick="showProductWaste('waste')">Waste Report
                            </button>
                        </li>


                    </ul>

                </div>

                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table" id="tbl_products">
                            <thead>
                            {{--<th width="3%" class="right_border_dotted text-center">Sr</th>--}}
                            <tr>
                                <th>Sr</th>
                                <th>Product Title</th>
                                <th>Unit</th>
                                <th>Date</th>
                                <th>Opening</th>
                                <th>Waste</th>
                                <th>Waste Price</th>
                                {{--<th>Remaining Stock</th>--}}
                                <th>Waste Average</th>

                            </tr>

                            </thead>
                            <tbody id="tbl_products_body">

                            </tbody>
                        </table>

                        <input type="hidden" name="total_products" id="total_products" value="1">
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->

@endsection

@section('footer')
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>

    <script>


        $(document).ready(function () {

            $(function () {
                $('input[name="ordersource_date"]').daterangepicker({
                    autoUpdateInput: false,
                    locale: {
                        cancelLabel: 'Clear'
                    }
                });

                $('input[name="ordersource_date"]').on('apply.daterangepicker', function (ev, picker) {
                    $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
                });

                $('input[name="ordersource_date"]').on('cancel.daterangepicker', function (ev, picker) {
                    $(this).val('');
                });
                // $('input[name="order_date"]').daterangepicker();

            });
        })
    </script>

    <script type="text/javascript">

        function showProductWaste(value) {
            var product = $('#product').val();
            var daterange = $('#daterange').val();
            var units = $('#unit').val();


            $.ajax({

                type: 'get',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{ url('/admin/reports/getwaste-report') }}',
                data: 'pid=' + product + '&daterange=' + daterange + '&units=' + units+ '&value=' + value,
                success: function (data) {
                    $('#tbl_products_body').html(data);
                },
                error: function (error) {
//                    alert('error');
                }
            });
        }

    </script>
<script>
    function getProductInfo(val)
    {
        var productid = val;


        $.ajax({
            type: 'get',
            url: '{{url('/admin/orders-management/getProductunitInfo')}}',
            data: 'pid=' + productid,
            dataType: 'json',
            success: function(data)
            {
                if(data.status==="success")
                {

                    $('#addunitvalue').html(data.unitData);

                }
                else if(data.status==="error")
                {
                    //
                }
            },
            error:function (error)
            {
                $('#cost_price_'+divid).val('');
                $('#opening_stock_'+divid).val('');
                $('#order_quantity_'+divid).val('');
            }
        });
    }

</script>
@endsection
