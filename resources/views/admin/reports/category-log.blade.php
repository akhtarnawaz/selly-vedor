@extends('admin.layouts.master')
@section('content')
    <div class="main">
        @include('admin.layouts.header')
        <div id="main">
            <div class="row">
                <div class="col-md-9">
                    <div>
                        <h3 class="mt-35 font-normal mb-20 display-ib">Category Log</h3>
                        <br>
                        <form class="form-inline get_order_log" action="">
                            <div class="form-group mr-20">
                                <input type="text" class="form-control" id="categoryName" placeholder="Enter Category Name" required>
                            </div>
                            <button type="submit" class="btn btn-success">Get Log</button>
                        </form>
                    </div>
                    <div class="mt-35">
                        <div class="tab-content">
                            <div id="order-log" class="tab-pane fade in active">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="clearfix"></div>

    <script>
        $('.get_order_log').submit(function (e) {
            e.preventDefault();
            var categoryName = $('#categoryName').val();
            $.ajax({
                url: "{{ url('/admin/reports/getcategoryLogData') }}",
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {'categoryName': categoryName},
                success: function (data) {
                    $('#order-log').html(data);
                }
            });
        });
    </script>
@endsection