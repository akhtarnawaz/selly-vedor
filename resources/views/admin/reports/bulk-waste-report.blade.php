@extends('admin.layouts.master')
@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main" class="stock-management-data">
            <div class="row">

            </div>
            <!-- /.col-lg-12 -->
                <div class="col-md-12">

                    <div class="search-conditions-box clearfix">

                        <ul>


                            <li class="col-md-5">

                                <i class="ti-calendar"></i>
                                <input name="date" id="daterange"
                                       class="relative_position width_100 input_padding border_blue border_radius3"
                                       type="text" placeholder="Enter Date Range" >
                            </li>
                            <li class="col-md-4">
                                <button onclick="wasteReport()"  class="border_blue search_btn  btn add_inputbtn">Revenue Report
                                </button>
                            </li>


                        </ul>

                    </div>
<h1>Revenue Report</h1>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Purchase</th>
                            <th>Sale</th>
                            <th>Waste & Lose</th>
                            <th>Net Profit</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr id="waste-data">

                        </tr>
                        </tbody>
                    </table>


                    {{--</form>--}}
                </div>
                </div>
    </div>
    <!-- END MAIN CONTENT -->

@endsection

@section('footer')
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>
    <script>


        $(document).ready(function () {

            $(function () {
                $('input[name="date"]').daterangepicker({
                    autoUpdateInput: false,
                    locale: {
                        cancelLabel: 'Clear'
                    }
                });

                $('input[name="date"]').on('apply.daterangepicker', function (ev, picker) {
                    $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
                });

                $('input[name="date"]').on('cancel.daterangepicker', function (ev, picker) {
                    $(this).val('');
                });
                // $('input[name="order_date"]').daterangepicker();

            });
        })


        function wasteReport() {
            var daterange = $('#daterange').val();
            $.ajax({

                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{ url('/admin/orders-management/WasteReportdata') }}',
                data: 'daterange=' + daterange,
                success: function (data) {
                    $('#waste-data').html(data);
                },
                error: function (error) {
//                    alert('error');
                }
            });
        }

    </script>

@endsection
