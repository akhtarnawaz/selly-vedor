 @php
        use App\Models\Admin\Products;
        use App\Models\Admin\Orders;
        use App\Models\Admin\OrdersItems;
        use App\Models\Admin\InventoryWastage;
        use App\Models\Admin\InventoryWastageItems;
 @endphp
 @foreach($inventory as $prod)
        @php
            $title = Products::where('id',$prod['product_id'])->first()->product_title;
        @endphp
        @php
            $wasteItems = array();
            $quantity_waste=0;
            $sale = 0;
            $waste =InventoryWastage ::where('purchase_date', 'LIKE', '%' . date('Y-m-d',strtotime($prod[0])) . '%')->get();
            foreach ($waste as $item)
            {
                $wasteItems = InventoryWastageItems::where('inventory_id',$item->id)->where('product_id',$prod['product_id'])->first();
                if($wasteItems)
                {
                    $quantity_waste+=$wasteItems->quantity_waste;
                }
            }



            $itemDetail=OrdersItems::where('created_at', 'LIKE', '%' . date('Y-m-d',strtotime($prod[0])). '%')->where('object_id',$prod['product_id'])->select('item_quantity')->get();
            foreach ($itemDetail as $value)
                $sale += $value->item_quantity;

        @endphp

        <tr>
            <td>{{$title}}</td>
            <td>{{$prod['unit']}}</td>
            <td>{{date('Y-m-d',strtotime($prod[0]))}}</td>
            <td>{{$prod['opening_stock']}}</td>
            <td>{{(isset($prod['quantity'])?$prod['quantity']:'null')}}</td>
            <td>{{$prod['total_stock']}}</td>
            <td>{{$quantity_waste}}</td>
            <td>{{$sale}}</td>
            <td>{{$prod['total_stock']-($quantity_waste+$sale)}}</td>
    {{--        <td>{{isset($sale)?$sale:0}}</td>--}}
            
             <td>{{(isset($prod['sale_price'])?$prod['sale_price']:'null')}}</td>
        </tr>
    @endforeach


