@extends('admin.layouts.master')
@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main" class="stock-management-data">
            <div class="row">

            </div>
            <!-- /.col-lg-12 -->
            <form name="frmPurchase" id="frmPurchase" role="form" method="post" action="{{ url('/admin/orders-management/csvSupervioserReport') }}">
            {{--<input type="hidden" name="customer_id" id="customer_id" value="">--}}
            {{csrf_field()}}
            <div class="col-md-12">

                <div class="search-conditions-box clearfix">

                    <ul>


                        <li class="col-md-5">

                            <i class="ti-calendar"></i>
                            <input name="date" id="daterange"
                                   class="relative_position width_100 input_padding border_blue border_radius3"
                                   type="text" placeholder="Enter Date Range" >
                        </li>
                        <li class="col-md-4">
                            <button type="submit"  class="border_blue search_btn  btn add_inputbtn">Orders Report
                            </button>
                        </li>


                    </ul>

                </div>




                {{--</form>--}}
            </div>
            </form>
        </div>
    </div>
    <!-- END MAIN CONTENT -->

@endsection

@section('footer')
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>
    <script>


        $(document).ready(function () {

            $(function () {
                $('input[name="date"]').daterangepicker({
                    autoUpdateInput: false,
                    locale: {
                        cancelLabel: 'Clear'
                    }
                });

                $('input[name="date"]').on('apply.daterangepicker', function (ev, picker) {
                    $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
                });

                $('input[name="date"]').on('cancel.daterangepicker', function (ev, picker) {
                    $(this).val('');
                });
                // $('input[name="order_date"]').daterangepicker();

            });
        })
    </script>

@endsection
