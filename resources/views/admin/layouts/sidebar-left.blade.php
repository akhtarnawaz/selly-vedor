<div id="sidebar-nav" class="sidebar">
    <nav>

        <div class="brand">
            @can('Dashboard') <a href="{{ url('/admin/dashboard') }}">  @endcan <img src="{{ url('public/admin/assets/img/selly-logo.png')}}" alt="Selly.pk" class="img-responsive logo">
            </a> </div>

        <ul class="nav" id="sidebar-nav-menu">
            @can('Dashboard')
            <li class="panel mt-20"> <a href="{{ url('/admin/dashboard') }}"><i class="ti-dashboard"></i> <span class="title">Dashboard</span></a>
                <div id="forms" class="collapse ">
                    <ul class="submenu"></ul>
                </div>
            </li>
            @endcan
                @php
                    $order='orders';

                @endphp
                @can('Orders list')
            <li class="panel"> <a href="#dashboards" data-toggle="collapse" data-parent="#sidebar-nav-menu" class="active"><i class="ti-shopping-cart-full"></i>
                    <span class="title">Orders</span> <sup class="order-blink"></sup>   <i class="icon-submenu ti-angle-left"></i></a>
                <div id="dashboards" class="collapse">
                    <ul class="submenu">

                        @can('Orders list')
                        <li><a href="{{ url('/admin/orders-management/orders',[$order]) }}" class="active">Current Orders List <!--<span class="label label-info">UPD</span>--></a></li>
                        <li><a href="{{ url('/admin/schedule/order') }}" class="active">Schedule Orders List <!--<span class="label label-info">UPD</span>--></a></li>
                     @endcan
                        @can('Order Statuses')
                        <li><a href="{{ url('/admin/orders-management/order-statuses') }}">Order Statuses</a></li>
                        @endcan
                        {{--<li><a href="{{url('/admin/orders-management/order-taker')}}">Order Form</a></li>--}}
                        {{--<li><a href="#">Statistics <!--<span class="label label-info">UPD</span>--></a></li>
                        <li><a href="#">Vendor Statistics</a></li>
                        <li><a href="#">Payment Transactions</a></li>
                        <li><a href="#">Vendor Transactions</a></li>
                        <li><a href="#">Messages</a></li>--}}
                    </ul>
                </div>
            </li>
                @endcan
                @can('Take Order')

                <li class="panel"> <a href="{{url('/admin/order-form')}}"  class="active"><i class="fa fa-file-text-o"></i> <span class="title">Order Form</span></a>
                   </li>
                @endcan

            <li class="panel"> <a href="#subLayouts" data-toggle="collapse" data-parent="#sidebar-nav-menu" class="collapsed"><i class="fa fa-tags" aria-hidden="true"></i> <span class="title">Catalog</span> <i class="icon-submenu ti-angle-left"></i></a>
                <div id="subLayouts" class="collapse">
                    <ul class="submenu">
                      @can('Categories')
                        <li><a href="{{ url('/admin/categories-management/categories') }}">Categories</a></li>
                        @endcan
                      @can('Products')
                        <li><a href="{{ url('/admin/products-management/products') }}">Products</a></li>
                            @endcan
                      @can('Reviews')
                        <li><a href="{{ url('/admin/reviews-management/reviews') }}">Reviews</a></li>
                            @endcan
                    </ul>
                </div>
            </li>
            <li class="panel"> <a href="#subStock" data-toggle="collapse" data-parent="#sidebar-nav-menu" class="collapsed"><i class="fa fa-cubes" aria-hidden="true"></i> <span class="title">Manage Stock</span> <i class="icon-submenu ti-angle-left"></i></a>
                <div id="subStock" class="collapse">
                    <ul class="submenu">
                        @can('stock')
                        <li><a href="{{ url('/admin/stock-management/stock') }}">New Stock</a></li>
                        @endcan
                            @can('Waste and Lose')
                        <li>@php($name='newReport')<a href="{{ url('/admin/stock-management/wasteHistory',[$name]) }}">Waste and Loss</a></li>
                            @endcan
                                @can('Purchase History')
                                <li>@php($name='newReport')<a href="{{ url('/admin/stock-management/purchase-history',[$name]) }}">Purchase History</a></li>
                            @endcan
                    </ul>
                </div>
            </li>
            <li class="panel"> <a href="#subMarketing" data-toggle="collapse" data-parent="#sidebar-nav-menu" class="collapsed"><i class="fa  fa-paper-plane-o" aria-hidden="true" aria-hidden="true"></i> <span class="title">Marketing</span> <i class="icon-submenu ti-angle-left"></i></a>
                <div id="subMarketing" class="collapse">
                    <ul class="submenu">
                        @can('New Launch/Premier')
                        <li><a href="{{ url('/admin/marketing/new-launch-premier') }}">New Launch/ Premier</a></li>
                        @endcan
                            @can('Category Banners')
                            <li><a href="{{ url('/admin/marketing/category-banners') }}">Category Banners</a></li>
                            @endcan
                    </ul>
                </div>
            </li>
                @can('Store Location')
            <li class="panel">
                <a href="{{url('/admin/store-management/Store')}}" >
              <i class="ti-location-pin" aria-hidden="true"></i> <span class="title">Stores Location</span> <i class=""></i></a>
                <div id="substores" class="collapse">

                </div>
            </li>
                @endcan

                @can('Shops')
                <li class="panel">
                    <a href="{{url('/admin/shops')}}" >
                        <i class="ti-shopping-cart" aria-hidden="true"></i> <span class="title">Shops</span> <i class=""></i></a>
                    <div id="substores" class="collapse">

                    </div>
                </li>
                @endcan
            <li class="panel"> <a href="#subReports" data-toggle="collapse" data-parent="#sidebar-nav-menu" class="collapsed"><i class="fa fa-file" aria-hidden="true"></i> <span class="title">Reports</span> <i class="icon-submenu ti-angle-left"></i></a>
                <div id="subReports" class="collapse">
                    <ul class="submenu">
                        @can('Revenue-report')
                            <li><a href="{{ url('/admin/orders-management/bulckWasteReport') }}">Revenue Reports</a></li>
                        @endcan
                        @can('Sale-report')
                            <li><a href="{{ url('/admin/orders-management/totalSaleReport') }}">Sale Report</a></li>
                        @endcan
                        @can('Order Stats')
                        <li><a href="{{ url('/admin/reports/orders-stats') }}">Orders Stats</a></li>
                        @endcan
                        @can('itemized-report')
                        <li><a href="{{ url('/admin/reports/item-report') }}">Itemized Report</a></li>
                        <li><a href="{{ url('/admin/reports/newitem-report') }}">New Itemized Report</a></li>
                            @endcan
                      @can('waste-report')
                        <li><a href="{{ url('/admin/reports/waste-report') }}">Waste Report</a></li>
                            @endcan
                       @can('Order Stats')
                        <li><a href="{{ url('/admin/reports/ordersource') }}">Orders Source</a></li>
                            @endcan
                       @can('permission_log_report')
                        <li><a href="{{ url('/admin/reports/logReport') }}">Log Report</a></li>
                            @endcan
                        @can('permission_stock_report')
                        <li>
                            <a href="{{ url('/admin/reports/stockReport') }}">Stock Report</a></li>
                            <li><a href="{{ url('/admin/reports/categoryLogReport') }}">Category Report</a></li>
                            @endcan
                            @can('CSRSupervioserReport')
                            <li><a href="{{ url('/admin/orders-management/csrSupervioserReport') }}">CSR Report</a></li>
                            @endcan

                    </ul>
                </div>
            </li>
            <li class="panel"> <a href="#appViews" data-toggle="collapse" data-parent="#sidebar-nav-menu" class="collapsed"><i class="fa fa-users" aria-hidden="true"></i> <span class="title">Users</span> <i class="icon-submenu ti-angle-left"></i></a>
                <div id="appViews" class="collapse ">
                    <ul class="submenu">
                        <li><a href="{{ url('/admin/users-management/resetpassword') }}">Reset Password</a></li>
                        @can('User List')
                        <li><a href="{{ url('/admin/users-management/users') }}">User List</a></li>
                        @endcan
                        @can('Module Access Permission')
                        <li><a href="{{ url('/admin/users-access-module/roles') }}"> Module Access Permissions </a></li>
                        @endcan
                    </ul>
                </div>
            </li>

                @can('customer-detail')
            <li class="panel"> <a href="#customer" data-toggle="collapse" data-parent="#sidebar-nav-menu" class="collapsed"><i class="fa fa-users" aria-hidden="true"></i> <span class="title">Customer Detail</span> <i class="icon-submenu ti-angle-left"></i></a>
                <div id="customer" class="collapse ">
                    <ul class="submenu">
                        {{--<li><a href="#">Customer Wallet</a></li>--}}
                        <li><a href="{{url('/admin/customer-management/customer')}}">Customer Detail</a></li>
                    </ul>
                </div>
            </li>
                @endcan
                @can('Pages')
                <li class="panel"> <a href="#tables" data-toggle="collapse" data-parent="#sidebar-nav-menu" class="collapsed"><i class="fa fa-clipboard" aria-hidden="true"></i> <span class="title">Content</span> <i class="icon-submenu ti-angle-left"></i></a>
                <div id="tables" class="collapse ">
                    <ul class="submenu">
                        <li><a href="{{url('/admin/pages/listings')}}">Pages</a></li>
                    </ul>
                </div>
            </li>
                @endcan
        </ul>
    </nav>
</div>

<script>
    $( document ).ready(function() {
             setInterval(function(){

                 $('.order-blink').hide();
                     $.ajax({
                         url: "{{url('/checkneworder')}}",
                         type: 'get',
                         headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                         dataType: 'json',
                         success: function (data) {
                          $('.order-blink').show();
                         },
                         error: function (error) {
                             $('.order-blink').hide();
                         }
                     });

             }, 10000);

    })
</script>