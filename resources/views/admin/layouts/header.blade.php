<div class="main-content">
    <div class="content-heading clearfix">
        <div class="pull-left">
            <div class="heading-left">
                <button type="button" class="btn-toggle-minified" title="Toggle Minified Menu"><i class="ti-menu"></i></button>
                <div class="dropdown"> <a href="#" class="dropdown-toggle quick-menu display-ib ml-30 mt-7" data-toggle="dropdown"> <i class="ti-bell mt-10 font-size-20"></i></a>
                    <div class="dropdown-menu">
                        {{--<li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li class="divider"></li>
                        <li><a href="#">One more separated link</a> </li>--}}
                    </div>
                </div>
                {{--<div class="dropdown"> <a href="#" class="dropdown-toggle quick-menu display-ib ml-30 mt-7" data-toggle="dropdown"> <i class="ti-calendar mt-10 font-size-20"></i></a>
                    <div class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li class="divider"></li>
                        <li><a href="#">One more separated link</a> </li>
                    </div>
                </div>--}}
                <div class="dropdown"> <a href="#" class="dropdown-toggle quick-menu display-ib ml-30 mt-7 gray_text" data-toggle="dropdown">Quick Menu</a>
                    <div class="dropdown-menu">
                        {{--<li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li class="divider"></li>
                        <li><a href="#">One more separated link</a> </li>--}}
                    </div>
                </div>
            </div>
            <div class="input-group display-ib">
                <button type="button" class="btn btn-default dropdown-toggle ml-30 search_in_btn" data-toggle="dropdown" aria-expanded="true">Search in <span class="caret"></span></button>
                <ul class="dropdown-menu" role="menu">
                    {{--<li><a href="#" data-code="product" data-placeholder="Products - p: key">Products</a></li>
                    <li><a href="#" data-code="profile" data-placeholder="Users - u: key">Users</a></li>
                    <li><a href="#" data-code="order" data-placeholder="Orders - o: key">Orders</a></li>--}}
                </ul>
                <input type="text" class="form-control display-ib products_input" name="substring" value="" placeholder="Products">
            </div>
        </div>
        <div class="pull-right">
            <ul class="breadcrumb">
                @php
              $status=  \App\Models\WebSiteOnOff::where('id',1)->pluck('status')->first();
                @endphp
                <li><a href="{{ url('/') }}" class="gray_text" target="_blank"><label class="switch">
                           @can('site-on-off') <input type="checkbox" onclick="CloseSite()" id="siteClose" value="1" @if($status == 'On') checked @endif > @endcan
                            <span class="slider round"></span>
                        </label> View Storefornt</a></li>
                        <div class="dropdown display-ib login-dropdown"> <a href="#" class="dropdown-toggle quick-menu display-ib ml-30 mt-7" data-toggle="dropdown"> <i class="ti-user mt-10 font-size-20"></i></a>
                            <div class="dropdown-menu right0">
                                <li><a href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a></li>
                            </div>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<script>
    function CloseSite() {

        if(document.getElementById("siteClose").checked){
            var status= 'On';

        }
        else {
            var status='Off';
        }
        $.ajax({
            type: 'post',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{url('/admin/setting/siteOnOff')}}',
            data: {status: status},
            success: function (data) {


            }
        });

    }

</script>