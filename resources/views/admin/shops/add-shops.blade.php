



@extends('admin.layouts.master')

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">

<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
{{--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>--}}
@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')


        <form name="frmProducts" id="frmProducts" method="post"  action="{{ url('/admin/shops/add') }}" >
            {{ csrf_field() }}
            <div id="main">
                <!-- Add Product section Start-->
                <div class="add-product-sec">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <h3 class="mt-35 font-normal mb-20 display-ib">Add Shop</h3>
                            </div>
                            <div class="add-product-box">
                                <div class="form-horizontal">
                                    <fieldset>
                                        <!-- Product name Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="fstore_name">Name *</label>
                                            <div class="col-md-4 ">
                                                <input id="name" name="name" class="errors form-control input-md"  type="text" >

                                            </div>
                                        </div>

                                    {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('first_name') }}</span>--}}

                                    <!-- SKU Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="Store_Location">Shop owner</label>
                                            <div class="col-md-4">
                                                <select name="owner" class="form-control input-md">
                                                    <option value="">Select Owner</option>
                                                   @foreach($users as $user)
                                                    <option value="{{$user->id}}">{{$user->username}}</option>
                                                       @endforeach
                                                </select>
                                            </div>
                                        </div>



                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Add Product section End-->

            </div>
            <!-- END MAIN CONTENT -->
            <div class="add-productbtn-fixed">
                <input class="add_inputbtn" id="create_account" type="submit" name="" value="Save">
                {{--<a href="{{url('admin/users-management/users')}}" class="add_statusbtn" >Cancel </a>--}}

            </div>
            <!-- Add Product button fixed at bottom -->
        </form>
    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>
<script>$().bfhtimepicker('toggle')</script>
@endsection