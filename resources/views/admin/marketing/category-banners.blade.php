@extends('admin.layouts.master')

@section('content')
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main">
            <div class="row">
                <div class="col-md-9">
                    <div>
                        <h3 class="mt-35 font-normal mb-20 display-ib">Category Banners</h3>
                    </div>
                    <br>
                    <a href="{{url('/admin/marketing/add-banners')}}" class="btn add_statusbtn">Add Banner</a>
                    <br>
                    <br>

                    <form method="post" action="{{--{{url('/admin/categories-management/delMultipleCategory')}}--}}" onsubmit="return confirm('Are you sure to delete these records?');">
                        {{csrf_field()}}
                        <script type="text/javascript">
                            $(function() {
                                $("#hideaftermoment").delay(2000).fadeOut(0);
                            });
                        </script>
                        @if (session('status'))
                            <div id="hideaftermoment" class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <script type="text/javascript">
                            $(function() {
                                $("#testdiv").delay(2000).fadeOut(0);
                            });
                        </script>
                        @if($errors->any())
                            <h4 id="testdiv" class="alert alert-danger">{{$errors->first()}}</h4>
                        @endif

                        <table class="table order_table">
                            <thead>
                            <tr>
                                <th width="5%">Category</th>
                                <th width="5%">Horizontal Banner</th>
                                <th width="1%">status</th>
                                <th width="5%">Vertical Banner</th>
                                <th width="1%">Status</th>
                                <th width="5%">Vertical Banner Position</th>
                                <th width="5%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($categoryBanners as $categoryBannerInfo)
                                    <tr>
                                        <td class="right_border_dotted text-center">
                                            @php
                                                    $category_title=$categories->where('id','=',$categoryBannerInfo->category_id)->pluck('category_title')->first();
                                                    @endphp
                                            {{$category_title}}</td>
                                        <td class="right_border_dotted text-center"><img src="{{asset('/public/images/categoryBanner/')}}/{{$categoryBannerInfo->horizontal_banner}}" height="80" width="150"> </td>

                                        @if($categoryBannerInfo->horizontal_banner_status == '1')
                                            <td title="Enabled" class="right_border_dotted text-center">
                                                <a href="{{url('/admin/marketing/horizentalStatusupdateDisable',[$categoryBannerInfo->id])}}" name="enabled">
                                                    <i class="fa fa-power-off" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        @else
                                            <td title="Disabled" class="right_border_dotted text-center"><a href="{{url('/admin/marketing/horizentalStatusupdateEnable',[$categoryBannerInfo->id])}}" name="disable"><i class="fa fa-power-off" style="color:red" aria-hidden="true"></i></a></td>
                                        @endif

                                        <td class="right_border_dotted text-center"><img src="{{asset('/public/images/categoryBanner')}}/{{$categoryBannerInfo->vertical_banner}}" height="80" width="150"> </td>
                                        @if($categoryBannerInfo->vertical_banner_status == '1')
                                            <td title="Enabled" class="right_border_dotted text-center"><a href="{{url('/admin/marketing/verticleStatusupdateDisable',[$categoryBannerInfo->id])}}"><i class="fa fa-power-off" aria-hidden="true"></i></a></td>
                                        @else
                                            <td title="Disabled" class="right_border_dotted text-center"><a href="{{url('/admin/marketing/verticleStatusupdateEnable',[$categoryBannerInfo->id])}}"><i class="fa fa-power-off" style="color:red" aria-hidden="true"></i></a></td>
                                        @endif<td class="right_border_dotted text-center">{{$categoryBannerInfo->vertical_banner_position}}</td>

                                        <td id="delete-category"  class="left_border_dotted text-center">
                                            <a class="edit" href="{{url('/admin/marketing/UpdateBanners' ,[$categoryBannerInfo->id]) }}">
                                            <i class="fa fa-pencil"></i>
                                            </a><br>
                                            <a href="{{url('/admin/marketing/DeleteBanners' ,[$categoryBannerInfo->id]) }}" onclick="return confirm('Are You sure to delete this record?');">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="table_pager">
                            <div class="col-lg-6 col-md-6 text-left">

                            </div>
                            <div class="col-lg-6 col-md-6 text-right">
                                <div class="items-per-page-wrapper" style="float: right">

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </form>
                </div>
                @include('admin.layouts.sidebar-right')
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-progressbar/js/bootstrap-progressbar.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.time.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/flot.tooltip/jquery.flot.tooltip.js') }}"></script>
{{--    <script src="{{ URL::asset('public/admin/assets/vendor/x-editable/bootstrap3-editable/js/bootstrap-editable.min.js') }}"></script>--}}
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-sparkline/js/jquery.sparkline.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-tour/js/bootstrap-tour.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widget.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/data.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/scroll-parent.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/disable-selection.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widgets/mouse.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widgets/sortable.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/datatables/js-main/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/datatables/js-bootstrap/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-appear/jquery.appear.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jqvmap/jquery.vmap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jqvmap/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>

@endsection