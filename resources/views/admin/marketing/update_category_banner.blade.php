@extends('admin.layouts.master')

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<style>
    @if($errors->has('category_description'))
        #cke_1_contents{
        border: 1px solid red;
    }
    @endif
        @if($errors->has('meta_descriptions'))
           #cke_2_contents{
        border: 1px solid red;
    }
    @endif

</style>
@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->


        <form name="addBanner" id="addBanner" method="post" action="{{ url('/admin/marketing/UpdateBanners') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

                <input type="hidden" name="bannerid" value="{{$categorybanner->id}}" >

            <div id="main">
                <!-- Add Product section Start-->
                <div class="add-product-sec">
                    <div class="row">
                        <div class="col-md-9">
                            <div>
                                <h3 class="mt-35 font-normal mb-20 display-ib">update Banners</h3>
                            </div>
                            <div class="add-product-box">
                                <div class="form-horizontal">
                                    <fieldset>

                                    <!-- Product name Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="category_title">Select Category</label>
                                            <div class="col-md-4">
                                                <select class="form-control" name="category_id" id="category_id" style="width: 200px;">

                                                    @php $categoryTitle=$categories->where('id','=',$categorybanner->category_id)->first()
                                                    @endphp
                                                        {{--<option value="{{$categoryTitle->id}}">{{$categoryTitle->category_title}}</option>--}}
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}" @if($categoryTitle->category_title == $category->category_title) selected @endif>{{$category->category_title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="category_title">Horizontal Banner Image</label>
                                            <div class="col-md-2">
                                                <input id="horizontal_banner" type="file" name="horizontal_banner"  class="input-md"  >
                                                </div>
                                            <div class="col-md-offset-1 col-md-2">
                                                    <img src="{{asset('/public/images/categoryBanner/')}}/{{$categorybanner->horizontal_banner}}" height="50">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="status">Horizontal Banner Status</label>
                                            <div class="col-md-8">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" data-toggle="toggle" name="horizontal_banner_status" id="horizontal_banner_status" value="1"  @if($categorybanner->horizontal_banner_status == '1') checked @endif  >
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- SKU Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_name_fr">Vertical Banner Image</label>
                                            <div class="col-md-2">
                                                <input id="vertical_banner" type="file" name="vertical_banner"  class="input-md" >
                                            </div>
                                            <div class="col-md-offset-1 col-md-2">
                                                <img src="{{asset('/public/images/categoryBanner/')}}/{{$categorybanner->vertical_banner}}" height="50">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="status">Vertical Banner Status</label>
                                            <div class="col-md-8">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" data-toggle="toggle" name="vertical_banner_status" id="vertical_banner_status" value="1"  @if($categorybanner->vertical_banner_status == '1') checked @endif >
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Product name Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="vertical_banner_position">Vertical Banner Position *</label>
                                            <div class="col-md-4">
                                                <select class="form-control" name="vertical_banner_position" id="vertical_banner_position" style="width: 200px;">
{{--                                                        <option value="{{$categorybanner->vertical_banner_position}}" selected>{{$categorybanner->vertical_banner_position}}</option>--}}


                                                    <option value="Left" @if($categorybanner->vertical_banner_position == 'Left') selected @endif>Left</option>
                                                    <option value="Right"  @if($categorybanner->vertical_banner_position == 'Right' ) selected @endif>Right</option>
                                                </select>
                                            </div>
                                        </div>
                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('categoryTitle') }}</span>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Add Product section End-->
            </div>
            <!-- END MAIN CONTENT -->

            <!-- Add Product button fixed at bottom -->
            <div class="add-productbtn-fixed">
                <input class="btn add_inputbtn" type="submit" name="" value="Update Banner">
                <a href="{{url('admin/marketing/category-banners')}}" class="add_statusbtn" type="submit" name="" > Cancel</a>
            </div>
        </form>
    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>

@endsection