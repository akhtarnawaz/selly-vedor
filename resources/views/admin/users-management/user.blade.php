@extends('admin.layouts.master')

@section('content')
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    {{--<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">--}}

    {{--<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">--}}
    <style>
        .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #2caae1;;
            border-right: 16px solid white;;
            border-bottom: 16px solid #2caae1;;
            border-left: 16px solid white;    ;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
            position: fixed;
            display: none;
            width: 70px;
            height: 70px;
            top: 60%;
            left: 40%;
            right: 20%;
            bottom: 40%;
            cursor: pointer;

        }

        /* Safari */
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

    </style>
    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main">
            <div class="row" id="loading-image">
                <div class="col-md-12">
                    <div>
                        <h3 class="mt-35 font-normal mb-20 display-ib">Users</h3>
                    </div>
                    <form method="post" action="{{url('/admin/users-management/users/searchUsers')}}">
                        {{csrf_field()}}
                        <div class="search-conditions-box clearfix products_box">
                            <div class="col-md-6">
                                <input type="text" placeholder="Search Keywords" name="keyword" id="search-keywords" >
                            </div>
                            <div class="col-md-4">
                                <select name="status" id="status" >

                                <option value="">Any Status</option>
                                <option value="1">Enabled</option>
                                <option value="0">Disabled</option>

                                </select>
                            </div>

                            <div class="col-md-2">
                                <button type="submit" class="border_blue search_btn border_radius3 btn-block">Search</button>
                            </div>
                        </div>

                    {{--<div class="search-conditions-box clearfix products_box">--}}
                        {{--<div class="col-md-6">--}}
                            {{--<input type="text" name="keyword" placeholder="Search Keywords" id="search-keywords" >--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4">--}}
                            {{--<select name="status" id="status" >--}}

                                {{--<option value="">Any Status</option>--}}
                                {{--<option value="1">Enabled</option>--}}
                                {{--<option value="0">Disabled</option>--}}

                            {{--</select>--}}

                            {{--<button class="border_blue search_btn border_radius3">Search</button>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    </form>

                    <form   method="post" action="{{url('/admin/users-management/deleteMultipleUsers')}}" onsubmit="return confirm('Are you sure to delete these records?');">
                        {{csrf_field()}}
                        <a href="{{url('/admin/users-management/add-users')}}" class="btn add_statusbtn">Add User</a>

                        <input type="submit" name="submit" id="delete-order" class="btn add_inputbtn"     value="Delete Selected Value" >

                        <br>
                        <br>
                        <div class="table-responsive">
                            <table id="myTable" class="display table" width="100%" >
                                <script type="text/javascript">
                                    $(function() {
                                        $("#hideaftermoment").delay(2000).fadeOut(0);
                                    });
                                </script>
                                @if (session('status'))
                                    <div id="hideaftermoment" class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <script type="text/javascript">
                                    $(function() {
                                        $("#testdiv").delay(2000).fadeOut(0);
                                    });
                                </script>
                                @if($errors->any())
                                    <h4 id="testdiv" class="alert alert-danger">{{$errors->first()}}</h4>
                                @endif
                                <thead>
                                <tr>
                                    <script type="text/javascript">
                                        function selectAll(status){
                                            $('.multiplerow').each(function(){
                                                $(this).prop('checked',status);
                                            });

                                        }
                                    </script>

                                    <th>
                                        <input type="checkbox" onclick='selectAll(this.checked)' value="Select All"/>
                                    </th>
                                    <th>Username/ Login ID</th>
                                    <th>Email</th>
                                    <th>Name</th>
                                    <th>Access level</th>
                                    <th>Orders</th>
                                    <th>Created</th>
                                    <th>Last Login</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <div class="loader" id="loader" hidden></div>
                                <tbody id="tableBody">
                                @foreach($usersData as $userInfo)
                                    @php $curent_user_orders=$orders->where('user_id','=',$userInfo->id)->count(); @endphp
                                    <tr id="tableRow">
                                        <td class="right_border_dotted"><input type="checkbox" class="multiplerow" id="multiplerow" name="delete[]" value="{{$userInfo->id}}"></td>

                                        <td><span><a href="{{url('/admin/users-management/updUsers',[$userInfo->id])}}">{{$userInfo->username}}</a></span></td>
                                        <input type="hidden" value="{{$userInfo->id}}" id="order">
                                        <td><span>{{$userInfo->email}}</span></td>
                                        <td><span>{{$userInfo->first_name.' '.$userInfo->last_name}}</span></td>
                                        <td><span>{{$userInfo->admin}}</span></td>
                                        <td>
                                            {{$curent_user_orders}}

                                        </td>
                                        <td><span>{{$userInfo->created_at}}</span></td>
                                        <td><span>{{$userInfo->last_login}}</span></td>

                                        @if($userInfo->user_status=='1')
                                            <td title="Enabled" class="right_border_dotted text-center"><a href="{{url('/admin/users-management/statusDisable',[$userInfo->id])}}"><i class="fa fa-power-off" aria-hidden="true"></i></a></td>
                                        @else
                                            <td title="Disabled" class="right_border_dotted text-center"><a href="{{url('/admin/users-management/statusEnable',[$userInfo->id])}}"><i class="fa fa-power-off" style="color:red" aria-hidden="true"></i></a></td>
                                        @endif
                                        <td class="left_border_dotted text-center" onclick="return confirm('Are You sure to delete this record?');"><a href="{{url('/admin/users-management/delUsers', [$userInfo->id])}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="table_pager">
                            <div class="col-lg-4 col-md-4 text-left">


                            </div>
                            <div class="col-lg-4 col-md-offset-4 col-md-4 text-right">
                                <div class="items-per-page-wrapper" id="page-link" style="float: right">
                                    {{$usersData->links()}}
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
                @include('admin.layouts.sidebar-right')
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

@endsection

@section('footer')
    {{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>--}}

    {{--<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>--}}
    <script>
        $(document).ready(function(){
            $('#myTable').dataTable();
        });
    </script>

    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    {{--<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>--}}

{{--    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-progressbar/js/bootstrap-progressbar.min.js') }}"></script>--}}
{{--    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.js') }}"></script>--}}
{{--    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.resize.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.time.js') }}"></script>--}}
{{--    <script src="{{ URL::asset('public/admin/assets/vendor/flot.tooltip/jquery.flot.tooltip.js') }}"></script>--}}
{{--    <script src="{{ URL::asset('public/admin/assets/vendor/x-editable/bootstrap3-editable/js/bootstrap-editable.min.js') }}"></script>--}}
{{--    <script src="{{ URL::asset('public/admin/assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>--}}
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
{{--    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-sparkline/js/jquery.sparkline.min.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-tour/js/bootstrap-tour.min.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widget.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/data.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/scroll-parent.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/disable-selection.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widgets/mouse.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widgets/sortable.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/datatables/js-main/jquery.dataTables.min.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/datatables/js-bootstrap/dataTables.bootstrap.min.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jquery-appear/jquery.appear.min.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jqvmap/jquery.vmap.min.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jqvmap/maps/jquery.vmap.world.js') }}"></script>--}}
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>


    <script  type="text/javascript">


        {{--function searchUsers(){--}}
            {{--var status = $('#status').val();--}}
            {{--var keyword =  $('#search-keywords').val();--}}
            {{--$('#loader').show();--}}
            {{--$.ajax({--}}
                {{--type:'get',--}}
                {{--url:'{{url("admin/users-management/searchUsers")}}',--}}
                {{--data:{'keyword':keyword,'status':status},--}}
                {{--success:function(data) {--}}

                    {{--$(' tbody').html(data);--}}
                    {{--$('#page-link').hide();--}}
                    {{--$('#loader').hide()--}}

                {{--},--}}
                {{--error:function(){--}}
                    {{--$('#page-link').hide();--}}
                {{--},--}}
            {{--});--}}

        {{--}--}}

        function  confirmdelete(a){

            var txt=a;
            if (!confirm("Are You sure to delete this record?")) {
                return false;

            } else {
                window.location = "{{url('/admin/users-management/delUsers')}}/"+txt+"!";

            }

        }

    </script>


@endsection
