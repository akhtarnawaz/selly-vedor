@extends('admin.layouts.master')


<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

@section('content')


    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <form name="frmProducts" id="frmProducts" method="post"  action="{{ url('/admin/users-management/updatepassword') }}" >
            {{ csrf_field() }}

            <input type="hidden" name="id" id="hidden_id" value="{{$usersData->id}}">
            <div id="main">
                <!-- Add Product section Start-->
                <div class="add-product-sec">
                    <div class="row">
                        <div class="col-md-9">
                            <div>
                                <script type="text/javascript">
                                    $(function() {
                                        $("#hideaftermoment").delay(2000).fadeOut(0);
                                    });
                                </script>
                                @if (session('status'))
                                    <div id="hideaftermoment" class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <script type="text/javascript">
                                    $(function() {
                                        $("#testdiv").delay(2000).fadeOut(0);
                                    });
                                </script>
                                @if($errors->any())
                                    <h4 id="testdiv" class="alert alert-danger">{{$errors->first()}}</h4>
                                @endif
                                <thead>

                                <h3 class="mt-35 font-normal mb-20 display-ib">Update Profile</h3>
                            </div>
                            <div class="add-product-box">
                                <div class="form-horizontal">
                                    <fieldset>
                                        <!-- Product name Text input-->

                                        <div class="form-group{{ $errors->has('password') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label" for="password">Old Password *</label>
                                            <div class="col-md-4">
                                                <input type="password"  name="oldpassword" id="password" class="form-control input-md"  @if($errors->has('password'))style=" border-color:red ;" @endif>

                                            </div>

                                        </div>
                                        <div class="form-group{{ $errors->has('password') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label" for="password">Password *</label>
                                            <div class="col-md-4">
                                                <input type="password"  name="password" id="password" class="form-control input-md"  @if($errors->has('password'))style=" border-color:red ;" @endif>

                                            </div>

                                        </div>

                                        <div class="form-group{{ $errors->has('confirm_password') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label" for="confirm_password">Confirm password *</label>
                                            <div class="col-md-4">
                                                <input type="password"  name="confirm_password" id="confirm_password"  class="form-control input-md" @if($errors->has('confirm_password'))style=" border-color:red ;" @endif>

                                            </div>
                                        </div>
                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('confirm_password') }}</span>

                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
            <div class="add-productbtn-fixed">
                <input class="add_inputbtn" id="create_account" type="submit" name="" value="Reset Password">
            </div>
        </form>
    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>

@endsection