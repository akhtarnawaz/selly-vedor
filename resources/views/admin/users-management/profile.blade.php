@extends('admin.layouts.master')

<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
{{--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>--}}
@section('content')
    <script>
        function validateForm() {
            var x = document.forms["frmProducts"]["email"].value;
            var pasword = document.forms["frmProducts"]["password"].value;
            var atpos = x.indexOf("@");
            var dotpos = x.lastIndexOf(".");
            if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {

                $('#error_msg_validateemail').show();
                return false;
            }
            else{
                $('#error_msg_validateemail').hide();

            }

        }
    </script>
    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div class="container mt-20">
            <div class="steps-inner sec-strt">
                <ul class="steps-progress list-unstyled">
                    <li class="active">
                        Setup shop
                    </li>
                </ul>
            </div>
        </div>
        @if(Session::has('success'))
        <div class="alert alert-success" style="width: 600px; align-items: center">
            {{Session::get('success')}}

        </div>

        @endif
        <div class="shop-page-head sec-strt">
            <div class="container">
                <div class="page-head-inner sec-strt">
                    <h1 style="font-size: 20px; margin-bottom: 5px;">Name your shop</h1>
                    <p class="margin-bottom-12">Choose a memorable name that reflects your style.</p>
                </div>
            </div>
        </div>

        <div class="shop-formcontainer sec-strt mb-30">
            <div class="container">
                <div class="shop-form-inner sec-strt">
                    <form class="form-horizontal sec-strt" method="post" action="{{url('/admin/profile-management/addprofile')}}" id="shopMakeForm" enctype="multipart/form-data" novalidate="novalidate">
                       {{csrf_field()}}
                         <div class="form-group ">
                            <label class="control-label col-sm-2" for="shop_name">Shop Name *</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" maxlength="50" id="shop_name" placeholder="e.g: Power NoteBooks" name="shop_name" value="{{isset($user->shops->name)?$user->shops->name:''}}">
                            </div>
                        </div>
                        {{--<div class="form-group ">--}}
                            {{--<label class="control-label col-sm-2" for="tag_line">Tag Line </label>--}}
                            {{--<div class="col-sm-4 inpit-limit">--}}
                                {{--<input type="text" class="form-control" maxlength="40" id="tag_line" placeholder="e.g: Smartphones, Laptops, etc max-length 40 " name="tag_line" value="">--}}
                            {{--</div>--}}
                            {{--<div class="col-sm-6 col-xs-6">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="form-group ">
                            <label class="control-label col-sm-2" for="country">Country *</label>
                            <div class="col-sm-4">
                                <select class="form-control" name="country" id="country">
                                    <option value="Pakistan" selected="selected">Pakistan</option>
                                </select>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="control-label col-sm-2" for="city_id">City *</label>
                            <div class="col-sm-4">
                                <select class="form-control" name="city" id="city_id">
                                    <option value="8">Lahore</option>
                                 </select>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="control-label col-sm-2" for="address">Address *</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" maxlength="150" id="address" placeholder="e.g: 100-F Ali View Garden, Cantt Lahore" name="address" value="{{isset($user->address)?$user->address:''}}">
                            </div>
                            <div class="col-sm-6 col-xs-6">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="control-label col-sm-2" for="phone_no">Phone *</label>
                            <div class="col-sm-4">
                                <input type="number" class="form-control" maxlength="11" id="phone_no" placeholder="e.g: 03211234567" name="phone_no" value="{{isset($user->cell_no)?$user->cell_no:''}}">
                            </div>
                            <div class="col-sm-6 col-xs-6">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="control-label col-sm-2" for="cnic">CNIC *</label>
                            <div class="col-sm-4">
                                <input type="number" class="form-control" maxlength="13" id="cnic" placeholder="e.g: 3510190784563" name="cnic" value="{{isset($user->cnic)?$user->cnic:''}}">
                            </div>
                            <div class="col-sm-6 col-xs-6">
                            </div>
                        </div>
                        {{--<div class="form-group ">--}}
                            {{--<label class="control-label col-sm-2" for="category">Category *</label>--}}
                            {{--<div class="col-sm-4">--}}
                                {{--<select class="js-example-basic-multiple select2-hidden-accessible" multiple="" name="" id="category" tabindex="-1" aria-hidden="true">--}}
                                    {{--<option value="1">Phones &amp; Tablets</option>--}}
                                    {{--<option value="2">Computers &amp; Gaming</option>--}}
                                    {{--<option value="3">Men's Fashion</option>--}}
                                    {{--<option value="4">Women's Fashion</option>--}}
                                    {{--<option value="5">Beauty &amp; Health</option>--}}
                                    {{--<option value="6">Kitchen &amp; Appliances</option>--}}
                                    {{--<option value="8">TVS, Audio &amp; Cameras</option>--}}
                                    {{--<option value="9">Home &amp; Living</option>--}}
                                    {{--<option value="10">Sports &amp; Fitness</option>--}}
                                    {{--<option value="11">Toys &amp; Kids</option>--}}
                                    {{--<option value="12">Automobiles &amp; Toolkits</option>--}}
                                    {{--<option value="13">School &amp; Stationery</option>--}}
                                {{--</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1"><ul class="select2-selection__rendered"><li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="textbox" aria-autocomplete="list" placeholder="" style="width: 0.75em;"></li></ul></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>--}}
                            {{--</div>--}}
                            {{--<div class="col-sm-6 col-xs-6">--}}
                            {{--</div>--}}
                            {{--<div id="jqueryErrorofCategory"></div>--}}
                        {{--</div>--}}
                        <div class="form-group ">
                            <label class="control-label col-sm-2" for="shop_about">About Shop *</label>
                            <div class="col-sm-9">
                                <div action="#" method="POST" class='form-wysiwyg' >
                                    <textarea name="description" id="product_description" class='ckeditor span12' rows="5"  type="text"> {{ isset($user->shops->shop_detail)?$user->shops->shop_detail:'' }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="submit-holder sec-strt">
                            <div class="text-center">
                                <div class="submit-inner">
                                    <button class="global-btn btn-primary btn" type="submit">Save and Continue</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>

@endsection