@extends('admin.users-access-module.module-access-master')
@section('module_access_management_content')
    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main">
            <div>
                <h3 class="mt-35 font-normal mb-20 display-ib"> Module Access Permissions </h3>
                <div class="filters">

                    <ul>


                        <li><a href="{{url('/admin/users-access-module/roles')}}">Roles</a></li>
                        <li><a href="{{url('/admin/users-access-module/permissions')}}">Permissions</a></li>
                        <li> <a href="{{url('/admin/users-access-module/assignUserRole')}}">  Assign Role to User</a></li>

                        <li> <span>  Assign Permissions to User</span></li>

                    </ul>
                </div>
                <div class="row" id="loading-image">
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="box-content alerts col-md-12" id="message_alert" >

                            </div>
                        </div>

                        <div style="width: 400px">
                            <label for="user_select">Select User:</label>
                            <select onchange="loadPermission()" class="form-control" title="Users" name="user_select" id="user_select">
                                {{--<option selected value="" disabled="disabled">Select a Users</option>--}}

                                <option  value="">Select a User</option>

                                @foreach($data as $user)
                                    <option value="{{$user->id}}">{{$user->username}}</option>
                                @endforeach
                            </select>

                        </div>
                        <br>
                        <div id="roles_div">

                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>

    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>

    <script>
        function loadPermission() {
            var user = $('#user_select').find(":selected").val();

            $.ajax({
                type: 'get',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{url('/admin/users-access-module/getUserPermissions')}}',
                data: {user:user},
                success: function(data) {
                    if(data){
                        $('#roles_div').html(data);
                    }
                }
            });
        }

        function toggleUsersPermission(val,user,count) {

            if(document.getElementById("checkbox"+count).checked){
                var checkbox= 'checked';

            }
            else {
                var checkbox='unchecked';

            }
            $.ajax({
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{url('/admin/users-access-module/assignPermissionsToUser')}}',
                data: {user_id: user,checkbox: checkbox,permission_id: val},
                success: function (data) {


                }
            });

        }


    </script>
@endsection







