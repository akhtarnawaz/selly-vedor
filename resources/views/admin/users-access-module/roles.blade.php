@extends('admin.users-access-module.module-access-master')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">

{{--<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">--}}


@section('module_access_management_content')
    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main">
            <div>
                <h3 class="mt-35 font-normal mb-20 display-ib"> Module Access Permissions </h3>
                <div class="filters">
                    <ul>


                        <li><span>Roles</span></li>
                        <li><a href="{{url('/admin/users-access-module/permissions')}}">Permissions</a></li>
                        <li> <a href="{{url('/admin/users-access-module/assignUserRole')}}">  Assign Role to User</a></li>
                        <li> <a href="{{url('/admin/users-access-module/assignUserPermission')}}">  Assign Permissions to User</a></li>

                    </ul>
                </div>
            </div>
            <div class="row" id="loading-image">
    <div class="modal fade" id="roleUsersModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Roles User</h4>
                </div>
                <div class="modal-body">
                    <div id="div_roleUsers"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>





    <div class="modal fade" id="permissionModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Permissions</h4>
                </div>
                <div class="modal-body">
                    <div id="div_permissions"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <div class="col-lg-8">
        <div class="row">
            <div class="box-content alerts col-md-12" id="message_alert">

            </div>
        </div>
    </div>

    <div class="col-md-12 col-lg-12">
        <input type="button" id="createType" class="btn btn-info mb-20" data-toggle="modal" data-target="#addRoleModal" value="Create New Role"/>
        <table class="table" id="datatablerolepermisson">
            <thead>
            <tr>
                <th class="text-center">Sr.no</th>
                <th class="text-center">Name</th>
                <th class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody id="tableBody">
            @php
            $count =1;
            @endphp
@foreach($roles as $role)
                <tr class="item">
                    <td>{{$count}}</td>
                    <td id="role">{{$role->name}}</td>
                    <td>
                        <button class="edit-modal btn btn-info"
                                data-info="{{$role->id}},{{$role->name}}"   onclick="editRoleModal({{$role->id}})" >

                            <span class="glyphicon glyphicon-edit"></span> Edit
                        </button>
                        <button onclick="loadPermissions({{$role->id}})" class="btn btn-primary">
                            <span class="glyphicon glyphicon-edit"></span> Change Permissions
                        </button>

                        <a class="delete-modal btn btn-danger" onclick="return confirm('Are you Sure you want to delete  {{$role->name}} ?');" href="{{url('/admin/users-access-module/deleteRoles',[$role->id])}}">
                            <span class="glyphicon glyphicon-trash"></span> Delete
                        </a>
                        <button class="delete-modal btn btn-warning" onclick="loadRoleUsers({{$role->id}})" >
                            <span class="glyphicon glyphicon-user"></span>View Users
                        </button>

                    </td>
                </tr>
                @php
                    $count ++;
                @endphp
            @endforeach
            </tbody>
        </table>
    </div>



    </div>
    </div>
    </div>
    {{--add role modal--}}
    <div id="addRoleModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>

                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form" action="{{url('/admin/users-access-module/addRoles')}}" method="post">
                        {{csrf_field()}}
                        <div id="idFormGroup" class="form-group">
                            {{--<label class="control-label col-sm-2" for="Id">ID</label>--}}
                            <div class="col-sm-10">
                                <input type="hidden" class="form-control" id="addId" name="Id" >
                            </div>
                        </div>

                        <p class="fname_error error text-center alert alert-danger hidden"></p>
                        <div class="form-group">
                            <div class="row"><span class="error-class" id="name"></span></div>
                            <label class="control-label col-sm-2" for="Name">Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="Name" id="RoleName">
                            </div>
                        </div>

                        <p class="email_error error text-center alert alert-danger hidden"></p>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success "  style="height: 35px;">
                                <span id="footer_action_button" class='glyphicon glyphicon-check' style="margin-bottom: 15px;"> Create</span>
                            </button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal">
                                <span class='glyphicon glyphicon-remove'></span> Close
                            </button>

                        </div>
                    </form>
                    {{--<div class="deleteContent">--}}
                                  {{--Are you Sure you want to delete <span class="dname"></span> ? <span--}}
                                {{--class="hidden did"></span>--}}
                    {{--</div>--}}


                </div>
            </div>
        </div>
    </div>

    {{--edit role modal--}}
    <div id="editRoleModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>

                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form" action="{{url('/admin/users-access-module/updateRoles')}}" method="post">
                        {{csrf_field()}}
                        <div id="idFormGroup" class="form-group">
                            {{--<label class="control-label col-sm-2" for="Id">ID</label>--}}
                            <div class="col-sm-10">
                                <input type="hidden" class="form-control" id="EditId" name="Id" >
                            </div>
                        </div>

                        <p class="fname_error error text-center alert alert-danger hidden"></p>
                        <div class="form-group">
                            <div class="row"><span class="error-class" id="name"></span></div>
                            <label class="control-label col-sm-2" for="Name">Name:</label>
                            <div class="col-sm-10" >
                                <input type="text" class="form-control" name="Name" id="EditName">
                            </div>
                        </div>

                        <p class="email_error error text-center alert alert-danger hidden"></p>
                        <div class="modal-footer">
                            <button type="submit" class="btn  btn-success " data-update="modal" style="height: 35px;">
                                <span id="footer_action_button" class='glyphicon glyphicon-check' style="margin-bottom: 15px"> Update</span>
                            </button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal">
                                <span class='glyphicon glyphicon-remove'></span> Close
                            </button>

                        </div>
                    </form>
                    {{--<div class="deleteContent">--}}
                    {{--Are you Sure you want to delete <span class="dname"></span> ? <span--}}
                    {{--class="hidden did"></span>--}}
                    {{--</div>--}}


                </div>
            </div>
        </div>
    </div>
    <div id="RoleModalview" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>

                </div>
                <div class="modal-body" id="userDetail">

                        </div>


                </div>
            </div>
        </div>
    </div>

   <div id="LoadPermission" class="modal fade" role="dialog">



       <div class="modal-dialog">
           <!-- Modal content-->
           <div class="modal-content">
               <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                   <h4 class="modal-title"></h4>

               </div>
               <div class="modal-body" id="modal-body">




               </div>
           </div>
       </div>

    </div>



    <style>
        .modal-backdrop{position: inherit;}
        .modal-content{width: 100%;}
#LoadPermission{margin-top: auto;
}

    </style>
    <script>

        function editRoleModal(val) {
            $.ajax({
                type: 'get',
                url: '{{url('/admin/users-access-module/editRoles')}}',
                data: {id: val},
                success: function (data) {
                    $('#editRoleModal').modal('toggle');
                    $('#EditId').val(data['id']);
                    $('#EditName').val(data['name']);
                }
            });

        }

        function loadRoleUsers(val) {
            $('#RoleModalview').modal('show');
            $.ajax({
                type: 'get',
                url: '{{url('/admin/users-access-module/editRoles')}}',
                data: {id: val},
                success: function (data) {


                    $('#userDetail').html(data);
                }
            });

        }
        function loadPermissions(val) {
            $('#LoadPermission').modal('show');
            $.ajax({
                type: 'get',
                url: '{{url('/admin/users-access-module/getPermissions')}}',
                data: {role_id: val},
                success: function (data) {
                    $('#modal-body').html(data);

                }
            });

        }
        function togglePermission(val,role,count) {

            if(document.getElementById("checkbox"+count).checked){
               var checkbox= 'checked';

            }
            else {
                var checkbox='unchecked';

            }
            $.ajax({
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{url('/admin/users-access-module/assignPermissionsToRole')}}',
                data: {role_id: role,checkbox: checkbox,permission_id: val},
                success: function (data) {


                }
            });

        }

    </script>
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>

@endsection







