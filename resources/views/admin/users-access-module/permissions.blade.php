@extends('admin.users-access-module.module-access-master')

@section('module_access_management_content')
    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main">
            <div>
                <h3 class="mt-35 font-normal mb-20 display-ib"> Module Access Permissions </h3>
                <div class="filters">
                    <ul>


                        <li><a href="{{url('/admin/users-access-module/roles')}}">Roles</a></li>
                        <li><span>Permissions</span></li>
                        <li> <a href="{{url('/admin/users-access-module/assignUserRole')}}">  Assign Role to User</a></li>
                        <li> <a href="{{url('/admin/users-access-module/assignUserPermission')}}">  Assign Permissions to User</a></li>

                    </ul>
                </div>
            </div>
            <div class="row" id="loading-image">
    <div class="col-lg-12 col-md-12">
        <div class="row">
            <div class="box-content alerts col-md-12" id="message_alert" >

            </div>
        </div>

        <div class="">
            <input type="button" id="createType" class="btn btn-info mb-20" data-toggle="modal" data-target="#addPermission" value="Create New Permission"/>
            <table class="table" id="datatablerolepermisson">
                <thead>
                <tr>
                    <th class="text-center">Sr.no</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Title</th>
                    <th class="text-center">Description</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody id="tableBody">
                @php
                $count=1;
                        @endphp
                @foreach($permissionData as $permission)
                    <tr  class="item">
                        <td>{{$count}}</td>

                        <td>{{$permission->name}}</td>
                        <td>{{$permission->title}}</td>
                        <td>{{$permission->description}}</td>


                        <td>
                            <button class="edit-modal btn btn-info" onclick="editPermission({{$permission->id}})">
                                <span class="glyphicon glyphicon-edit"></span> Edit
                            </button>
                            <a href="{{url('/admin/users-access-module/deletePermissions',[$permission->id])}}" onclick="return confirm('Are you Sure you want to delete {{$permission->name}} ?');" class="delete-modal btn btn-danger"
                                    data-info="id name title,description">
                                <span class="glyphicon glyphicon-trash" ></span> Delete
                            </a>
                        </td>
                    </tr>
                    @php
                        $count ++;
                    @endphp
                @endforeach
                </tbody>
            </table>
        </div>
    </div>



            </div>
        </div>
    </div>
    <div id="addPermission" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>

                </div>
                <div class="modal-body">
                    <form method="post" action="{{url('/admin/users-access-module/addPermissions')}}"  class="form-horizontal" role="form">
                        {{csrf_field()}}
                        <p class="fname_error error text-center alert alert-danger hidden"></p>
                        <div class="form-group">
                            <div class="row"><span class="error-class" id="name"></span></div>
                            <label class="control-label col-sm-2" for="Name">Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="Name" id="Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row"><span class="error-class" id="title"></span></div>
                            <label class="control-label col-sm-2" for="Name">Title:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="Title" id="Title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="Name">Description:</label>
                            <div class="col-sm-10">
                                <textarea rows="7" class="form-control" name="Description" id="Description"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row"><span class="error-class" id="roles"></span></div>
                            <label class="control-label col-sm-2" for="role">Roles:</label>
                            <div class="col-sm-10">
                                <select id="role" name="role[]" class="form-control multiselect-ui" multiple data-placeholder="Select Roles" style="width: 100%;">
                                    @if(isset($roles) && count($roles)>0)
                                        @foreach($roles as $role)
                                            <option value="{{$role->id}}" >{{$role->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <p class="email_error error text-center alert alert-danger hidden"></p>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success "  style="height: 35px;">
                                <span id="footer_action_button" class='glyphicon glyphicon-check'  style="margin-bottom: 15px;"> Create</span>
                            </button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal">
                                <span class='glyphicon glyphicon-remove'></span> Close
                            </button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

    <div id="editPermission" class="modal fade" role="dialog">

    </div>
    <style>
        .modal-backdrop{position: inherit;}
        .modal-content{width: 100%;}

    </style>

    <script>
        function editPermission(val) {

            $.ajax({
                type: 'get',
                url: '{{url('/admin/users-access-module/editPermissions')}}',
                data: {id: val},
                success: function (data) {
                    console.log(data);
                    $('#editPermission').html(data);

                    $('#editPermission').modal('toggle');

                }
            });

        }
    </script>
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>

@endsection






