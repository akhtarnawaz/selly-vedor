@php
    $userRole=isset($user_role->role_id)?$user_role->role_id:'';
@endphp
@foreach($roles as $role)
    {{--@php(dd($role))--}}
    <label class="radio-inline">
        <input onchange="assignRoles({{$role->id}})" value="{{$role->name}}" type="radio" name="changeUserRole"
               @if($role->id==$userRole)
               checked
                @endif
        >{{ $role->name }}
    </label>
@endforeach
<br>
<input onchange="assignRoles()" value="{{$role->name}}" type="radio" name="changeUserRole" @if($role->id=='')
checked
        @endif>remove Role