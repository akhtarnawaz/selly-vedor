@extends('admin.layouts.master')
@php
    use App\Models\Admin\Products;
@endphp

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
@section('content')
    <style>


    </style>
    <script>
        function printFunction() {
            var contents = document.getElementById("main").innerHTML;
            var frame1 = document.createElement('iframe');
            frame1.name = "frame1";
            frame1.style.position = "absolute";
            frame1.style.top = "-1000000px";
            document.body.appendChild(frame1);
            var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
            frameDoc.document.open();
            frameDoc.document.write('<html><head><title></title>');
            frameDoc.document.write('</head><style>.table{width: 100% !important;}.quantityCheck{color: red !important;}.noprint{display: none !important;}.printslip{margin-top: -60px !important}.printslipname{margin-top: -60px !important}</style><body>');
            frameDoc.document.write(contents);
            frameDoc.document.write('</body></html>');
            frameDoc.document.close();
            setTimeout(function () {
                window.frames["frame1"].focus();
                window.frames["frame1"].print();
                document.body.removeChild(frame1);
            }, 500);
            return false;
        }
    </script>
    <div id="wrapper">

        <!-- MAIN -->
        <div class="main">
            <!-- MAIN CONTENT -->
            @include('admin.layouts.header')
            <div id="main">
                {{--{{$usersDetails}}--}}
                <div class="update-product-page">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-links-border">
                                <h2 class="noprint"> Order Details</h2>
                                <div class="product-tabs mt-35">
                                    <div class="pull-right print-invoice-btns">
                                        <button onclick="printFunction()" class="btn btn-default noprint">Print Packing
                                            Slip
                                        </button>
                                        <button onclick="printFunction()" class="btn btn-default noprint">Print
                                            Invoice
                                        </button>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 p-0">
                                        <div class="general-info-page">
                                            <div>
                                                <div class="order-info-title">
                                                    @php
                                                        $orderNumber=isset($usersDetails->orderNumber)?$usersDetails->orderNumber:'';
                                                        $ordernotes=isset($usersDetails->notes)?$usersDetails->notes:'';
                                                     $foc= \App\Models\Admin\OrderPaymentStatusTranslations::where('id',$usersDetails->payment_status_id)->pluck('name')->first();
                                                    @endphp
                                                    @if($foc == 'FOC')  <h1>FOC</h1>
                                                    @endif

                                                    <h2>Order No.{{$orderNumber}}</h2>
                                                    <div><img src="{{ url('public/admin/assets/img/selly-logo.png')}}"
                                                              style="height: 100px" alt="Selly.pk"
                                                              class="img-responsive logo">
                                                    </div>
                                                </div>
                                                {{--<a href="#" class="pull-right mt-20">Order History</a>--}}
                                            </div>
                                            <br>
                                            <div class="col-md-6 pl-0">
                                                <div class="placed-date">
                                                    Placed on <span class="date">{{date('M d, Y',strtotime($usersDetails['created_at']))}}
                                                        {{date('g:i A',strtotime($usersDetails['created_at']))}}</span>
                                                </div>
                                                <div class="placed-customer">
                                                    by
                                                    @php
                                                        $username=isset($usersDetails->username)?$usersDetails->username:'';@endphp


                                                    <span class="profile-login">
                                                    @php
                                                        $email=isset($usersDetails->email)?$usersDetails->email:'';@endphp
                                                        <a data-toggle="modal" data-target="#CustomerModel"
                                                           id="address">{{$email}}
                                                    </a>{{--<a><input  name="email" id="customerEmail" type="text" style="border: none" value="{{$email}}"></a>--}}
                                                </span>
                                                    {{--<span>(Customer Support)</span>--}}
                                                </div>
                                                <br>
                                                <div class="order-part payment noprint ">
                                                    <img src="{{url('public/admin/assets/img/order_payment_method_icon.png')}}"
                                                         class="img-responsive display_inlineblock">

                                                    <h2 class="heading">Payment Mode</h2>
                                                    <div class="box">
                                                        <h2>Cash on Delivery</h2>
                                                        <p class="transaction">
                                                            Transaction ID: <br>
                                                            002466-SETW
                                                        </p>
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <div class="order-part payment billing">

                                                    <div class="box printslipname">
                                                        <p class="address">
                                                            @php
                                                                $address=isset($usersDetails->address)?$usersDetails->address:'';
                                                                $id=isset($usersDetails->id)?$usersDetails->id:'';
                                                                $phoneno=isset($usersDetails->phoneno)?$usersDetails->phoneno:'';
                                                            @endphp
                                                            @if($foc == 'FOC')
                                                                <strong>Payment Method: </strong>FOC<br>
                                                            @else
                                                                <strong>Payment Mode: </strong>Cash on Delivery<br>
                                                            @endif
                                                            <strong>Name:</strong> <a data-toggle="modal"
                                                                                      data-target="#CustomerModel"
                                                                                      id="address">@if($usersDetails->orderUsername != '')
                                                                    {{$usersDetails->orderUsername}}
                                                                @else
                                                                    {{$username}}
                                                                @endif
                                                            </a><br>
                                                            <strong>Address:</strong> <a data-toggle="modal"
                                                                                         data-target="#CustomerModel"
                                                                                         id="address">@if($usersDetails->orderAddress != '')
                                                                    {{$usersDetails->orderAddress}}
                                                                @else
                                                                    {{$address}}
                                                                @endif</a><br>
                                                            {{--<strong>Address:</strong>     <button >Open Modal</button> <input type="text" id="address" value="{{$address}}" onkeyup="editAddress({{$userId}})" style="border: none"><br>--}}
                                                            <strong>Phone:</strong> <a type="button" data-toggle="modal"
                                                                                       data-target="#CustomerModel"
                                                                                       id="address">@if($usersDetails->orderPhoneno != '')
                                                                    {{$usersDetails->orderPhoneno}}
                                                                @else
                                                                    {{$phoneno}}
                                                                @endif</a>
                                                            {{--<strong>Phone:</strong>  <input type="text" id="phoneno" value="{{$phoneno}}" onkeyup="editAddress({{$userId}})" style="border: none">--}}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 pr-0 ">
                                                <div class="staff-note-box noprint">
                                                    <img src="{{url('public/admin/assets/img/user_male.svg')}}"
                                                         alt="shopping cart logo"
                                                         class="img-responsive display_inlineblock">
                                                    <span><b>Staff Note</b></span>
                                                    <p class="mt-10"><input type="text"
                                                                            class="form-control"
                                                                            onkeyup="addStafNote({{$usersDetails->id}})"
                                                                            placeholder="Staff Note"

                                                                            value="{{$usersDetails->staff_note}}"
                                                                            id="staffNote"></p>
                                                </div>
                                                <div class="staff-note-box noprint">
                                                    <img src="{{url('public/admin/assets/img/user_male.svg')}}"
                                                         alt="shopping cart logo"
                                                         class="img-responsive display_inlineblock">
                                                    <span><b>User Note</b></span>
                                                    <p class="mt-10">{{$usersDetails->notes}}</p>
                                                </div>
                                                <br>
                                                <br>
                                                <div class="order-part actions">
                                                    <div class="statuses noprint">
                                                        <?php
                                                        $hide = [];
                                                        ?>
                                                        @php
                                                            $id=isset($usersDetails->id)?$usersDetails->id:'';
                                                            $shipping_status_id=isset($usersDetails->shipping_status_id)?$usersDetails->shipping_status_id:'';
                                                        @endphp
                                                        @cannot('edit_orders_master')
                                                            <?php


                                                            if ($shipping_status_id == 1) {
                                                                $hide = [3, 4, 5, 6, 7];
                                                            }

                                                            if ($shipping_status_id == 3) {
                                                                $hide = [2];
                                                            }

                                                            if ($shipping_status_id == 4) {
                                                                $hide = [2, 3, 5, 6, 7];
                                                            }

                                                            if ($shipping_status_id == 5) {
                                                                $hide = [2, 3, 4, 6, 7];
                                                            }
                                                            if ($shipping_status_id == 6) {
                                                                $hide = [2, 3, 4, 5, 7];
                                                            }
                                                            if ($shipping_status_id == 7) {
                                                                $hide = [2, 3, 4, 6, 5];
                                                            }

                                                            if ($shipping_status_id > 1) {
                                                                array_push($hide, 1);
                                                            }

                                                            ?>
                                                        @endcannot

                                                        <div class="shipping-status">
                                                            <label>Fulfilment status</label>
                                                            <select name="fulfil_status" class="form-control"
                                                                    @cannot('order_shipping_status')
                                                                    disabled
                                                                    @endcannot
                                                                    id="fulfil_status_{{$id}}"
                                                                    onchange="updateFullfilmentStatus(this.value,'{{$id}}')">
                                                                <option value="" disabled>---Select One---</option>
                                                                @foreach ($fulfilmentStatus as $fStatus)
                                                                    @if(!in_array($fStatus['id'],$hide))
                                                                        <option value="{{$fStatus['id']}}"
                                                                                @if($shipping_status_id == $fStatus['id']) selected @endif>{{$fStatus['name']}}</option>
                                                                        {{$id}}
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                            <p id="shipping_sub_status_message_{{$id}}"
                                                               class="text-danger mt-7">{{isset($usersDetails->order_sub_shipment_status)?$usersDetails->order_sub_shipment_status:''}}</p>
                                                        </div>

                                                        @php
                                                            $payment_status_id=isset($usersDetails->payment_status_id)?$usersDetails->payment_status_id:'';
                                                        @endphp
                                                        <?php
                                                        $hide = [];
                                                        ?>
                                                        @cannot('edit_orders_master')
                                                            <?php


                                                            if ($payment_status_id == 4) {
                                                                $hide = [5, 7, 10, 11];
                                                            }

                                                            if ($payment_status_id == 5) {
                                                                $hide = [4, 7, 10, 11];
                                                            }
                                                            if ($payment_status_id == 7) {
                                                                $hide = [5, 4, 10, 11];
                                                            }
                                                            if ($payment_status_id == 10) {
                                                                $hide = [5, 7, 4, 11];
                                                            }
                                                            if ($payment_status_id == 11) {
                                                                $hide = [5, 7, 4, 10];
                                                            }

                                                            if ($payment_status_id > 1) {
                                                                array_push($hide, 1);
                                                            }

                                                            ?>
                                                        @endcannot

                                                        <div class="payment-status ">
                                                            <label>Payment status</label>

                                                            <select class="form-control " name="payment_status"
                                                                    @cannot('order_payment_status')
                                                                    disabled
                                                                    @endcannot
                                                                    id="payment_status"
                                                                    onchange="updatePaymentStatus(this.value,'{{$id}}')">
                                                                <option value="" selected>---Select One---</option>
                                                                @foreach ($orderPaymentStatusesValues as $pStatus)
                                                                    @if(!in_array($pStatus['id'],$hide))
                                                                        <option value="{{$pStatus['id']}}"
                                                                                @if($payment_status_id == $pStatus['id']) selected @endif>{{$pStatus['name']}}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12">
                                                <div class="table-responsive order_detail_table printslip">
                                                    <h3>Item Details</h3>
                                                    {{--{{$usersDetails}}--}}
                                                    <form method="post"
                                                          action="{{url('/admin/orders-management/addNewOrders',[$orderNumber])}}">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="ordernumber"
                                                               value="{{$usersDetails->orderNumber}}">
                                                        <table class="table width_auto" id="tbl_table">
                                                            <thead>
                                                            <tr>
                                                                <th style="text-align: left;width: 50px;">Sr.</th>
                                                                <th style="text-align: left;width: 300px;">Product(s)</th>
                                                                <th style="text-align: left;width: 300px;">Shop</th>
                                                                <th style="text-align: left;width: 100px;">Price</th>
                                                                <th style="text-align: left;width: 100px;">Qty</th>
                                                                <th style="text-align: left;width: 100px;">Sub Total</th>
                                                                @can('Discount')
                                                                    @if(!$usersDetails->discount)
                                                                        <th style=";width: 80px;"  class="noprint">Discount</th>
                                                                        <th style="width: 80px;" class="text-right noprint" >Discount Total</th>
                                                                    @endif
                                                                @endcan
                                                                <th style="text-align: left;width: 100px;"></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="tbl_body">
                                                            @php
                                                                $count=1;

                                                            @endphp
                                                            @if($orderDetail != '')
                                                                @foreach($orderDetail as $detail)
                                                                    @if($detail->object_id != '')
                                                                        @php
                                                                            $productQuantity=\App\Models\Admin\Products::where('id',$detail->object_id)->select('quantity_in_stock')->first()->quantity_in_stock;
                                                                            $productUnits = \App\Models\Admin\Units::where('unit',$detail->quantity_unit)->where('product_id',$detail->object_id)->first()
                                                                        @endphp
                                                                        <tr style="    font-size: 14px;">
                                                                            <td class="item-name" style="text-align: left">

                                                                                {{$count}}
                                                                            </td>
                                                                            <td class="item-name" style="text-align: left">

                                                                                {{$detail->product_title}}
                                                                            </td>
                                                                            <td class="item-name" style="text-align: left">

                                                                                {{\App\Models\Shops::where('id','=',$detail->shop_id)->pluck('name')->first()}}
                                                                            </td>
                                                                            <td class="price" style="text-align: left">{{$detail->price}}</td>
                                                                            @if(isset($detail->quantity_unit)?isset($detail->quantity_unit):'')
                                                                                @php
                                                                                    $unit = $detail->quantity_unit;
                                                                                @endphp
                                                                            @else
                                                                                @php
                                                                                    $unit = $detail->unit;

                                                                                @endphp
                                                                            @endif
                                                                            <td class="qty" style="text-align: left">
                                                                                <input @if($shipping_status_id !=2 && $shipping_status_id !=1 )disabled @endif
                                                                                style="border: none ;@if($shipping_status_id ==1)
                                                                                @if($productUnits->product_quantity < $detail->item_quantity ) color: red;@endif
                                                                                @endif"
                                                                                       id="UpdateQuantity{{$detail->id}}"
                                                                                       onchange="setProductquantity('{{$detail->id}}')"
                                                                                       value="{{$detail->item_quantity.'/'.$unit}}">
                                                                            </td>
                                                                            <td class="total" style="text-align: left"
                                                                                id="toltalAmount">{{$usersDetails->is_foc=="1"?0.00:$detail->total}}</td>
                                                                            @can('Discount')
                                                                                @if(!$usersDetails->discount)
                                                                                    <td class="@if($detail->discount=='') noprint @endif" >
                                                                                        @can('Discount')
                                                                                            <input type="text" placeholder="%"  class="noprint" style="border: none"  id="discount{{$detail->id}}" value="{{$detail->discount}}" onchange="addDiscountAmount('{{$detail->id}}','{{$detail->order_id}}' )">
                                                                                    </td>
                                                                                    @endcan

                                                                                    <td class="total text-right @if($detail->discount=='') noprint @endif"
                                                                                        id="doscountAmount_{{$detail->id}}">
                                                                                        {{$detail->discountedSubtotal}}
                                                                                    </td>
                                                                                @endif
                                                                            @endcan

                                                                            <td class="total text-right " style="text-align: left" id="delete"><a
                                                                                        onclick="return confirm('Are you sure to delete these records?');"
                                                                                        href="{{url('/admin/orders-management/order/deleteOrders',[$detail->id])}}"><i
                                                                                            class="fa fa-trash-o"></i></a></td>


                                                                        </tr>

                                                                        @php
                                                                            $count ++;
                                                                        @endphp
                                                                    @else
                                                                        <tr>   <h1 style="color: red">Empty Order</h1></tr>
                                                                    @endif
                                                                @endforeach
                                                            @else

                                                            @endif
                                                            <tr style="border: none">
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                @if($usersDetails->discountedAmount > 0 )
                                                                    <td><strong>Discount Amount</strong></td>
                                                                    <td style="float: right" class="SubTotal" id="SubTotal"
                                                                    >{{$usersDetails->discountedAmount}}</td>
                                                                @else<td><strong >Grand Total</strong></td>
                                                                <td style="text-align: left" class="SubTotal" id="SubTotal"
                                                                ><strong>{{$usersDetails->total}}</strong></td>
                                                                @endif
                                                                <td></td>
                                                                @can('Discount')
                                                                    @if(!$usersDetails->discount)
                                                                        <td></td>

                                                                        <td></td>
                                                                    @endif
                                                                @endcan
                                                            </tr>

                                                            <button id="addProducts" class="btn btn-primary add-record noprint {{$shipping_status_id==2?'':'hidden'}}"><i
                                                                        class="glyphicon glyphicon-plus"></i>&nbsp;Add Product
                                                            </button>

                                                            </tbody>
                                                        </table>

                                                        <h1 class="quantityCheck" style="text-align: center; font-size:18px;"> براہ مہربانی ادائیگی سے پہلے معیار اور مقدار کی تسلی کر لیں
                                                        </h1>

                                                        <input type="hidden" name="orderNumber"
                                                               value="{{$orderNumber}}">
                                                        <input type="submit" value="save"
                                                               class="btn btn-primary noprint {{$shipping_status_id==2?'':'hidden'}}" id="saveOrder" name="submitbutton">
                                                        <input type="submit" class="btn btn-primary noprint"
                                                               value="Send Email" name="submitbutton">
                                                    </form>
                                                    <form method="post"
                                                          action="{{url('/admin/orders-management/SendOrdersEmail')}}">
                                                        {{csrf_field()}}
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <!-- Trigger the modal with a button -->


                <div class="modal fade" id="CustomerModel" role="dialog">
                    <div class="modal-dialog" style="width: 90%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body">
                                <h2>Customer Information</h2>
                                <form method="post" action="{{ url('/admin/orders-management/setAddress') }}"
                                      class="form-horizontal" role="form">
                                    {{csrf_field()}}
                                    <p class="fname_error error text-center alert alert-danger hidden"></p>
                                    <div class="form-group">
                                        <div class="row"><span class="error-class" id="name"></span></div>
                                        <label class="control-label col-sm-2" for="Name">Name:</label>
                                        <div class="col-sm-10">
                                            <input type="hidden" class="form-control" name="customerId" id="EditId"
                                                   value="{{$userId}}">
                                            <input type="hidden" class="form-control" name="orderId" id="EditId"
                                                   value="{{$usersDetails->id}}">
                                            <input type="text" class="form-control" name="name" id="name"
                                                   @if($usersDetails->orderUsername != '')value="{{$usersDetails->orderUsername}}"
                                                   @else
                                                   value="{{$username}}"
                                                    @endif>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row"><span class="error-class" id="name"></span></div>
                                        <label class="control-label col-sm-2" for="Name">Email:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="customerEmail" id="email"
                                                   value="{{$email}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row"><span class="error-class" id="title"></span></div>
                                        <label class="control-label col-sm-2" for="Name">Address:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="address" id="address"
                                                   @if($usersDetails->orderAddress != '')  value="{{$usersDetails->orderAddress}}"
                                                   @else
                                                   value="{{$address}}"
                                                    @endif>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row"><span class="error-class" id="title"></span></div>
                                        <label class="control-label col-sm-2" for="Name">Title:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="phoneno" id="phoneno"
                                                   @if($usersDetails->orderPhoneno != '') value="{{$usersDetails->orderPhoneno}}"
                                                   @else
                                                   value="{{$phoneno}}"
                                                    @endif>
                                        </div>
                                    </div>


                                    <p class="email_error error text-center alert alert-danger hidden"></p>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-success " style="height: 35px;">
                                            <span id="footer_action_button" class="glyphicon glyphicon-check"
                                                  style="margin-bottom: 15px;">Save Change</span>
                                        </button>
                                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                                            <span class="glyphicon glyphicon-remove"></span> Close
                                        </button>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>


                </div>

            </div>
            <div class="clearfix"></div>

        </div>
        <!-- END MAIN -->
        <div class="clearfix"></div>
    </div>

@endsection

@section('footer')


    <script>

        function getProductInfo(val, divid) {
            var productid = val;

            $.ajax({
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{url('/admin/orders-management/getProductInfo')}}',
                data: 'pid=' + productid + '&divid=' + divid,
                dataType: 'json',
                success: function (data) {
                    if (data.status === "success") {
//                        $('#opening_stock_' + divid).val(data.openingStock);
//                        $('#cost_price_' + divid).val(data.market_price);

                        $('#unit' + divid).html(data.unitData);

                    }
                    else if (data.status === "error") {
                        //
                    }
                },
                error: function (error) {
                    //
                }
            });
        }



        $(document).ready(function () {

            jQuery(document).delegate('button.add-record', 'click', function (e) {

                e.preventDefault();

                var content = jQuery('#sample_table tr'),
                    size = jQuery('#tbl_table >tbody >tr').length + 1,
                    element = null,
                    element = content.clone();

                $('#total_products').val(size);


                var i = size;

                //$('#tbl_products_body').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="name[]" placeholder="Enter your Name" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
                $('#tbl_body').append('<tr id="rec-' + i + '">\n' +
                    '                <td>' + i + '</td><td>\n' +
                    '                    <select style="width:120px;" required class="e1 form-control" name="products[]" id="product_' + i + '" onchange="getProductInfo(this.value,' + i + ');">\n' +
                    '                        <option value="">Select Product</option>\n' +
                    '                        @foreach($productsData as $product)\n' +
                    '                            <option value="{{$product->id}}">{{$product->product_title}}</option>\n' +
                    '                        @endforeach\n' +
                    '                    </select>\n' +
                    '                </td>\n' +
                    '                <td class="" id="unit' + i + '">\n' +
                    '                </td>\n' +
                    '                <td><input readonly style="background: #ddd;" type="text" name="costPrices[]" id="cost_price_' + i + '"></td>\n' +
                    '                <td><input readonly style="background: #ddd;" type="text" name="openingStocks[]" id="opening_stock_' + i + '"></td>\n' +
                    '                <td><input placeholder="Add New Quantity" required type="text" name="orderQuantity[]" id="order_quantity_' + i + '" onchange="updateSalePrice(this.value,' + i + ');" class="quantity"></td>\n' +
                    '                <td><input readonly style="background: #ddd;" type="text" name="totalPrices[]" id="total_price_' + i + '" class="price"></td>\n' +
                    '                <td width="50" class="left_border_dotted text-center"><a class="btn btn-xs delete-record" data-id="' + i + '"><i class="glyphicon glyphicon-trash"></i></a></td>\n' +
                    '            </tr>');
            });


            jQuery(document).delegate('a.delete-record', 'click', function (e) {
                e.preventDefault();
                var didConfirm = confirm("Are you sure You want to delete");
                if (didConfirm == true) {
                    var id = jQuery(this).attr('data-id');
                    var targetDiv = jQuery(this).attr('targetDiv');
                    jQuery('#rec-' + id).remove();

                    size = jQuery('#tbl_products >tbody >tr').length;
                    $('#total_products').val(size);

                    //regnerate index number on table
                    $('#tbl_products_body tr').each(function (index) {
                        $(this).find('span.sn').html(index + 1);
                    });

                    return true;
                }
                else {
                    return false;
                }
            });
        });

        function updateSalePrice(val, divid) {

            var price = $('#cost_price_' + divid).val();

            var total = price * val;
            $('#total_price_' + divid).val(total);
            var openingStock=$('#opening_stock_'+divid).val()
            var quantity=$('#order_quantity_'+divid).val()

            if(parseInt(openingStock) < parseInt(quantity) ){
                alert('Please select available quantity');
                $('#saveOrder').hide()
                $('#addProducts').hide()

            }
            else {
                $('#saveOrder').show()
                $('#addProducts').show()

            }

        }

        //    $(document).ready(function (){
        //
        //        var toltalAmount=$('#toltalAmount').val();
        //        $('#SubTotal').val(toltalAmount);
        //    })

        $(function () {
            function tally(selector) {
                $(selector).each(function () {
                    var total = 0,
                        column = $(this).siblings(selector).andSelf().index(this);
                    $(this).parents().prevUntil(':has(' + selector + ')').each(function () {
                        total += parseFloat($('td.total:eq(' + column + ')', this).html()) || 0;
                    })
                    $(this).html(total);
                });
            }

//        tally('td.toltalAmount');
//            tally('td.SubTotal');
        });

        function addStafNote(id) {

            var stafNote = $('#staffNote').val();

            $.ajax({
                url: "{{ url('/admin/orders-management/staffNote') }}",
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {'id': id, 'stafNote': stafNote,},
                success: function (data) {
                    if (data != "Error") {
                        /*var message_contaner = $('#direct-chat-messages_' + id);
                        message_contaner.append(data);*/
                    }
                },
            });
        }
        {{--function setunitPrice(divid, product) {--}}
        {{--var unit = $('#unit_' + divid).val();--}}


        {{--$.ajax({--}}

        {{--type: 'post',--}}
        {{--headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},--}}
        {{--url: '{{url('/admin/orders-management/getunitInfo')}}',--}}
        {{--data: 'pid=' + product + '&unit=' + unit,--}}
        {{--success: function (data) {--}}


        {{--$('#cost_price_'+divid).val(data.product_price);--}}
        {{--$('#opening_stock_'+divid).val(data.product_quantity);--}}
        {{--},--}}
        {{--error: function (error) {--}}
        {{--alert('error');--}}
        {{--}--}}
        {{--});--}}
        {{--}--}}

        function setunitPrice(divid, product) {
            var unit = $('#unit_' + divid).val();


            $.ajax({

                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{url('/admin/orders-management/getunitInfo')}}',
                data: 'pid=' + product + '&unit=' + unit,
                success: function (data) {


                    $('#cost_price_'+divid).val(data.product_price);
                    $('#opening_stock_'+divid).val(data.product_quantity);
                },
                error: function (error) {
                    alert('error');
                }
            });
        }
    </script>
    <script>

        function setProductquantity(orderitemId) {
            var newquantity = $('#UpdateQuantity' + orderitemId).val();
            $.ajax({

                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{url('/admin/orders-management/setNewUnit')}}',
                data: 'orderitemId=' + orderitemId + '&newquantity=' + newquantity,
                success: function (data) {

                    $('#toltalAmount' + orderitemId).html(data.total);
                    location.reload();
                },
                error: function (error) {
                    alert('Please Select available quantity');
                    location.reload();
                }
            });
        }
        function addDiscountAmount(order_item,orderId) {
            var discount=$('#discount'+order_item).val();


            $.ajax({
                url: "{{url('/admin/orders-management/productdiscount')}}", //Cart\CartController@placeorder
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {discount: discount, order_item: order_item,orderId: orderId},
                success: function (data,total) {
                    location.reload();
//                   $('#doscountAmount_'+order_item).html(data[0]['discountedSubtotal']);
//                    $('#discount'+order_item).val(data[0]['discount']);
//                    $('#SubTotal').val(total);
                }
            });
        }
    </script>
    <style>
        #main {
            padding: 10px 21px 35px 10px;
        }
    </style>
@endsection
