@extends('admin.layouts.master')

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">

@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <form name="frmProducts" id="frmProducts" method="post" action="{{ url('/admin/orders-management/updateOrderpaymentStatus',[$orderpaymentstatuses->id])}}">
            {{ csrf_field() }}
            <div id="main">
                <!-- Add Product section Start-->
                <div class="add-product-sec">
                    <div class="row">
                        <div class="col-md-9">
                            <div>
                                <h3 class="mt-35 font-normal mb-20 display-ib">Add Order Status</h3>
                            </div>
                            <div class="add-product-box">
                                <div class="form-horizontal">
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_name_fr">Status Type *</label>
                                            <div class="col-md-4">
                                                <select class="form-control" id="type" name="type" required>
                                                     <option selected value="P">Payment</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="category_title">Status Title *</label>
                                            <div class="col-md-4">
                                                <input id="name" name="name" value="{{$orderpaymentstatuses->name}}" class="form-control input-md" required type="text">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Add Product section End-->
            </div>
            <!-- END MAIN CONTENT -->

            <!-- Add Product button fixed at bottom -->
            <div class="add-productbtn-fixed">
                <input class="add_statusbtn" type="submit" name="" value="Update Status">
            </div>
        </form>
    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>

@endsection