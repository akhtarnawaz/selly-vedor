@extends('admin.layouts.master')

@php
    use App\Models\Admin\Orders;
    use App\Models\Admin\OrdersItems;
    use App\Models\Admin\Customers;
    use App\Models\Admin\Users;
    use App\Models\Admin\OrderShippingStatusTranslations;
    use App\Models\Admin\OrderPaymentStatusTranslations;

@endphp

@section('content')

    <div id="wrapper">

        <!-- MAIN -->
        <div class="main">
            <!-- MAIN CONTENT -->
            @include('admin.layouts.header')
            <div id="main">
                <div class="row">
                    <div class="col-md-12" >
                        <div>
                                <h3>{{$userData->name}}</h3>
                               <ul class="customer_detail">

                                   <li>
                                       <strong>Email</strong>
                                       <span>{{$userData->email}}</span>
                                   </li>
                                   <li>
                                       <strong>Phone No</strong>
                                       <span>{{$userData->cellno}}</span>
                                   </li>
                                   <li>
                                       <strong>Address</strong>
                                       <span>{{$userData->address}}</span>
                                   </li>
                                   <li>
                                       <strong>Member Since</strong>
                                       @php($firstOrder=$userOrder->first())
                                       <span>{{date('M d, Y',strtotime($firstOrder['created_at']))}}</span>
                                   </li>
                                   <li>
                                       <strong>Member Status</strong>
                                       <span>@if(count($userOrder) >49) Silver
                                           @elseif(count($userOrder) >99) Gold
                                           @elseif(count($userOrder) >149) Platinum
                                           @elseif(count($userOrder) >199)Titanium
                                           @else
                                                 New
                                           @endif</span>
                                   </li>

                               </ul>
                            <table class="table table-hover" style="width: 40%">
                                <thead>
                                <tr>
                                    <th>Total Orders</th>
                                    <th><i class="fa fa-television television-icon"></i></th>
                                    <th><i class="fa fa-mobile mobile-icon"></i></th>
                                    <th><i class="fa fa-phone mobile-icon"></i></th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{count($userOrder)}}</td>
                                    @php
                                        $web= $userOrder->where('ordersource',1)->count();
                                        $app= $userOrder->where('ordersource',0)->count();
                                        $call= $userOrder->where('ordersource',2)->count();

                                    @endphp
                                    <td>{{$web}}</td>
                                    <td>{{$app}}</td>
                                    <td>{{$call}}</td>
                                </tr>
                                </tbody>
                            </table>
                            {{--<p><strong>Email</strong>:  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>--}}
                            {{--<p><strong>Phone No</strong>:&nbsp; {{$userData->cellno}}</p>--}}
                            {{--<p><strong>Address</strong>:&nbsp; {{$userData->address}}</p>--}}
                            {{--<p><strong></p>--}}
                            {{--<p><strong>Member Status</strong>:&nbsp; {{date('M d, Y',strtotime($lastOrderDate))}}</p>--}}
                                        {{--<p><strong>Total Orders</strong> &nbsp;<strong>Mobile order</strong> &nbsp; <strong>Web order</strong> &nbsp; <strong>Call order</strong></p>--}}
                            <br>

                            <h3 class="mt-35 font-normal mb-20 display-ib"> Order Count</h3>
                            <div class="table-responsive">
                                <table class="table order_table" style="width: 80%">
                                    <thead>
                                    <tr>
                                        <th>Sr#</th>
                                        <th>Order#</th>
                                        <th>Source</th>
                                        <th>Total Items</th>
                                        <th>Date</th>
                                        <th>Fulfilment Status</th>
                                        <th>Payment Status</th>
                                        <th>Amount</th>
                                        <th>Processed by</th>
                                        <th>Delivered by</th>

                                    </tr>
                                    </thead>
                                    <tbody>


                                    @php
                                        $counterloo=0;
                                        $order_count=0;
                                        $totalAmount=array();
                                        $totalCommission=array();
                                    @endphp
                                    @foreach($userOrder as $data)

                                        @php

                                            $counterloo++;
                                             $paymentStatusName=   OrderPaymentStatusTranslations::where('id',$data->payment_status_id)->pluck('name')->first();
                                           $shippingStatusName=OrderShippingStatusTranslations::where('id',$data->shipping_status_id)->pluck('name')->first();
                                              $proccessedBy = Users::where('id',$data->processed_by)->pluck('username')->first();
                                              $deliverdBy = Users::where('id',$data->delivered_by)->pluck('username')->first();

                                            $totalItems = OrdersItems::getOrderItemsCount($data['id']);

                                            $totalCommission[$counterloo] = $data['commission'];
                                            $totalAmount[$counterloo] = isset($data['total'])?$data['total']:'';

                                            if($data->user_id != '')
                                            {


                                                $cur_user_id = $data->user_id;

                                                $userObj = new Users();

                                                $userInfo = $userObj->getUserInfo($cur_user_id);
                                                $orderbyname = isset($userInfo[0]->username)?$userInfo[0]->username:'';
                                                $userId = isset($userInfo[0]->id)?$userInfo[0]->id:'';
                                                $orderbylastname = isset($userInfo[0]->last_name)?$userInfo[0]->last_name:'';
                                                $orderbyemail = isset($userInfo[0]->email)?$userInfo[0]->email:'';

                                                $order_count = Orders::getOrdersCountByUID($cur_user_id);
                                            }
                                            else
                                            {
                                                $cur_customer_id = $data->customer_id;

                                                $customerObj = new Customers();
                                                $customerInfo = $customerObj->getCustomerInfo($cur_customer_id);

                                                $orderbyname = isset($customerInfo[0]->username)?$customerInfo[0]->username:'';
                                                $orderbyemail = isset($customerInfo[0]->email)?$customerInfo[0]->email:'';
                                                $userId = isset($customerInfo[0]->id)?$customerInfo[0]->id:'';
                                                $order_count = Orders::getOrdersCountByCID($cur_customer_id);
                                            }
                                       if($data->user_id == ''){
                                       $id=$data->customer_id;
                                        }
                                       else{
                                       $id=$data->user_id;
                                       }


                                        @endphp
                                        <tr @if($paymentStatusName=='Declined') style="color: red" @endif>

                                            <td>{{$counterloo}}.</td>
                                            <td>
                                                <a href="{{url('/admin/orders-management/OrdersDetail',[$data['id']])}}">{{$data['orderNumber']}}</a>
                                            </td>
                                            <td>
                                                @php
                                                    $source=isset($data->ordersource)?isset($data->ordersource):'';
                                                @endphp
                                                @if($data->ordersource==1)

                                                    <i class="fa fa-television television-icon" aria-hidden="true"></i>

                                                @elseif($data->ordersource==0)
                                                    <i class="fa fa-mobile mobile-icon" aria-hidden="true"></i>

                                                @elseif($data->ordersource==2)
                                                    <i class="fa fa-phone mobile-icon" aria-hidden="true"></i>

                                                @endif
                                            </td>
                                            <td>{{$totalItems}}</td>
                                            <td class="cell-date">
                                                <span class="date">{{date('M d, Y',strtotime($data['created_at']))}} </span>
                                                <span class="time">{{date('g:i A',strtotime($data['created_at']))}} </span>
                                            </td>


                                            {{--<td><a href="#">{{$orderbyname}}</a>--}}
                                                {{--<small class="display_block">{{$orderbyemail}}</small>--}}
                                            {{--</td>--}}
                                            <td>
                                                {{$shippingStatusName}}
                                            </td>
                                            <td>
                                                    {{$paymentStatusName}}
                                            </td>
                                            <td>
                                                @php
                                                    $totalAmount[$counterloo] = isset($data['total'])?$data['total']:'';
                                                @endphp
                                                <span>RS.{{number_format((float)$data['total'], 2, '.', '')}}</span>
                                            </td>


                                            <td>
                                                <span>{{$proccessedBy}}</span>

                                            </td>
                                            <td>
                                                <span>{{$deliverdBy}}</span>

                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN -->
        <div class="clearfix"></div>
    </div>
    </div>
@endsection
@section('footer')

    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>

@endsection