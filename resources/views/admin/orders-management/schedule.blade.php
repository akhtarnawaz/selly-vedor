@extends('admin.layouts.master')

@php
    use App\Models\Admin\Orders;
    use App\Models\Admin\OrdersItems;
    use App\Models\Admin\Customers;
    use App\Models\Admin\Users;
    use App\Models\Admin\Log;
@endphp

@section('content')

    <div id="wrapper">

        <!-- MAIN -->
        <div class="main">
            <!-- MAIN CONTENT -->
            @include('admin.layouts.header')
            <div id="main">
                <div class="row">
                    <div class="col-md-12">
                        <div>
                            <h3 class="mt-35 font-normal mb-20 display-ib">Schedule Orders</h3>
                            {{--<div class="filters">--}}
                                {{--<ul>--}}
                                    {{--@php--}}
                                        {{--$order='orders';--}}
                                        {{--$paidOrder='paidorders';--}}
                                        {{--$unPaidOrder='unpaidorders';--}}
                                    {{--@endphp--}}
                                    {{--<li><a href="{{url('/admin/orders-management/orders',[$order])}}">All Orders</a></li>--}}
                                    {{--<li><a href="{{url('/admin/dashboard')}}">Awaiting processing --}}{{--(3)--}}{{--</a></li>--}}
                                    {{--<li><a href="{{url('/admin/orders-management/unpaid-orders',[$unPaidOrder])}}">Unpaid</a></li>--}}
                                    {{--<li><a href="{{url('/admin/orders-management/paid-orders',[$paidOrder])}}">Paid but not--}}
                                            {{--Shipped</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        </div>
                        {{--<form name="frmSearch" id="frmSearch" role="form" method="post"--}}
                              {{--action="{{ url($searchUrl,$title) }}">--}}
                            {{--{{csrf_field()}}--}}
                            <div class="search-conditions-box clearfix">
                                <ul>
                                    <li class="col-md-3">
                                        <input class="width_100 input_padding border_blue border_radius3" type="text"
                                               name="search_keyword" id="search_keyword"
                                               value="@if(isset($search_keyword)){{$search_keyword}} @endif"
                                               placeholder="OrderID or email, ID1-ID2 for range">
                                    </li>
                                    <li class="col-md-3">
                                        <div class="form-group mb-0">
                                            <div class="width_100 border_blue border_radius3">
                                                <select name="shipping_status[]" id="dates-field2"
                                                        class="multiselect-ui form-control" multiple="multiple">
                                                    @foreach ($fulfilmentStatus as $fStatus)
                                                        <option value="{{$fStatus['id']}}" @if(isset($shipping_status) && $shipping_status==$fStatus['id']){{"selected"}} @endif>{{$fStatus['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="form-group mb-0">
                                            <div class="width_100 border_blue border_radius3">
                                                <select name="payment_status[]" id="dates-field2"
                                                        class="multiselect-ui form-control" multiple="multiple">
                                                    @foreach ($orderPaymentStatusesValues as $pStatus)
                                                        <option value="{{$pStatus['id']}}" @if(isset($payment_status) && $payment_status==$pStatus['id']){{"selected"}} @endif>{{$pStatus['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-md-2">

                                        <i class="ti-calendar"></i>
                                        <input name="order_date" id="daterange"
                                               value="@if(isset($order_date)){{$order_date}} @endif"
                                               class="relative_position width_100 input_padding border_blue border_radius3"
                                               type="text" placeholder="Enter Date Range">
                                    </li>

                                    <li class="col-md-1">
                                        <button class="border_blue search_btn border_radius3">Search</button>
                                        @php
                                            $search=isset($clearsearch)?$clearsearch:'empty';

                                        @endphp
                                        @if($search == 'clearsearch')
                                            <br>
                                            <br>
                                            <a href="{{url('/admin/orders-management/orders',[$order])}}" class="border_blue search_btn border_radius3">Clear</a>
                                        @endif
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                                <div class="col-md-12 text-center">
                                    <button type="button" class="absolute_position angle_downbtn border_radius3"><i
                                                class="ti-angle-down"></i></button>
                                </div>
                            </div>

                            <div class="search-conditions-hidden search-conditions-box">
                                <ul class="mt-10">
                                    <li class="mr-20">
                                        <p>Customer</p>
                                    </li>
                                    <li class="mr-20">
                                        <input name="customer_name" type="text"
                                               value="@if(isset($customer_name)){{$customer_name}} @endif"
                                               placeholder="Customer Name">
                                    </li>
                                    <li class="mr-20">
                                        <p>Telephone</p>
                                    </li>
                                    <li class="mr-20">
                                        <input type="text" name="tel" id="tel" value="@if(isset($tel)){{$tel}} @endif"
                                               placeholder="Telephone no">
                                    </li>
                                </ul>
                            </div>
                        </form>
                        <form method="post" action="{{url('/admin/orders-management/deleteOrExportorders')}}">
                            {{--      <form method="post" action="{{url('/delete/vehicles')}}" >--}}
                            {{csrf_field()}}

                            <script type="text/javascript">
                                function selectAll(status) {
                                    $('.multiplerow').each(function () {
                                        $(this).prop('checked', status);
                                    });

                                }
                            </script>

                            <div class="table-responsive JStableOuter">
                                <table class="table order_table table_125">
                                    <thead>
                                    <tr>
                                        {{--<th>
                                            <input type="checkbox">

                                        </th>--}}
                                        <th><input type="checkbox" onclick='selectAll(this.checked)' value="Select All"
                                                   style="width: 20px"/></th>
                                        <th>Sr#</th>
                                        <th>Order#</th>
                                        <th>Source</th>
                                        <th>Total Items</th>
                                        <th>Payment Type</th>
                                        <th>Date</th>
                                        <th>Schedule Date</th>
                                        <th>Customer</th>
                                        {{--<th>Move to</th>--}}
                                        <th>Order Count</th>
                                        <th>Fulfilment Status</th>
                                        <th>Payment Status</th>
                                        <th>Amount</th>
                                        <th>FOC</th>
                                        @can('Discount')
                                            <th>Discount on Order</th>
                                        @endcan
                                        <th>Processed by</th>
                                        <th>Delivered by</th>
                                        <th>Location</th>

                                        <th>Delivery Time(HH:MM)</th>
                                        <th>Processing Time(HH:MM)</th>
                                        {{--<th width="8%">Commission</th>--}}
                                        {{--<th>Delivery Status</th>--}}
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $counterloo=0;
                                        $order_count=0;
                                        $totalAmount=array();
                                        $totalCommission=array();

                                    @endphp

                                    @foreach($scheduleOrder as $data)

                                        @php

                                                    $counterloo++;

                                                    $totalItems = OrdersItems::getOrderItemsCount($data['id']);

                                                    $totalCommission[$counterloo] = $data['commission'];
                                                    $totalAmount[$counterloo] = isset($data['total'])?$data['total']:'';

                                                    if($data->user_id != '')
                                                    {


                                                        $cur_user_id = $data->user_id;

                                                        $userObj = new Users();

                                                        $userInfo = $userObj->getUserInfo($cur_user_id);
                                                        $orderbyname = ($userInfo)?$userInfo->username:'';
                                                        $userId = ($userInfo)?$userInfo->id:'';
                                                        $orderbylastname = ($userInfo)?$userInfo->last_name:'';
                                                        $orderbyemail = ($userInfo)?$userInfo->email:'';

                                                        $order_count = Orders::getOrdersCountByUID($cur_user_id);
                                                    }
                                                    else
                                                    {
                                                        $cur_customer_id = $data->customer_id;

                                                        $customerObj = new Customers();
                                                        $customerInfo = $customerObj->getCustomerInfo($cur_customer_id);

                                                        $orderbyname = ($customerInfo)?$customerInfo->username:'';
                                                        $orderbyemail = ($customerInfo)?$customerInfo->email:'';
                                                        $userId = ($customerInfo)?$customerInfo->id:'';
                                                        $order_count = Orders::getOrdersCountByCID($cur_customer_id);
                                                    }
                                               if($data->user_id == ''){
                                               $id=$data->customer_id;
                                                }
                                               else{
                                               $id=$data->user_id;
                                               }
                                                $currentDate= date('Y-m-d',  strtotime(\Carbon\Carbon::now()));

                                        $subTwoDays= date('Y-m-d', strtotime('-2 day', strtotime($data['schedule_order_date'])));
                                        $subOneDays= date('Y-m-d', strtotime('-1 day', strtotime($data['schedule_order_date'])));

                                        @endphp

                                            <tr>

                                                {{--<td class="right_border_dotted text-center"><input type="checkbox"></td>--}}
                                                <td><input type="checkbox" class="multiplerow" id="multiplerow"
                                                           name="delete[]" value="{{$data['id']}}"></td>
                                                <td>{{$counterloo}}.</td>
                                                <td>
                                                    <a href="{{url('/admin/orders-management/OrdersDetail',[$data['id']])}}">
                                                        @if($subTwoDays == $currentDate )
                                                            <sup class="order-blink"></sup>
                                                            @elseif($subOneDays == $currentDate )
                                                            <sup class="order-blink"></sup>
                                                         @endif
                                                            {{$data['orderNumber']}}</a>
                                                </td>
                                                <td>
                                                    @php
                                                        $source=isset($data->ordersource)?isset($data->ordersource):'';
                                                    @endphp
                                                    @if($data->ordersource==1)

                                                        <i class="fa fa-television television-icon" aria-hidden="true"></i>

                                                    @elseif($data->ordersource==0)
                                                        <i class="fa fa-mobile mobile-icon" aria-hidden="true"></i>

                                                    @elseif($data->ordersource==2)
                                                        <i class="fa fa-phone mobile-icon" aria-hidden="true"></i>

                                                    @endif
                                                </td>
                                                <td><a href="#">{{$totalItems}}</a></td>
                                                <td><span>{{$data['order_payment_method']}}</span></td>
                                                <td class="cell-date">
                                                    <span class="date">{{date('M d, Y',strtotime($data['created_at']))}} {{--Nov 21, 2017.--}}</span>
                                                    <span class="time">{{date('g:i A',strtotime($data['created_at']))}} {{--11:57 AM--}}</span>
                                                </td>
                                                <td class="cell-date">
                                                   <a> <span class="date">{{date('M d, Y',strtotime($data['schedule_order_date']))}} {{--Nov 21, 2017.--}}</span>
                                                    <span class="time">{{date('g:i A',strtotime($data['schedule_order_date']))}} {{--11:57 AM--}}</span></a>
                                                </td>

                                                <td><a href="{{url('/admin/orders-management/users-orders',[$data['id']])}}">{{$orderbyname}}</a>
                                                    <small class="display_block">{{$orderbyemail}}</small>
                                                </td>
                                                <td>
                                                    <a href="{{url('/admin/orders-management/users-orders',[$data['id']])}}">{{$order_count}}</a>
                                                </td>
                                                <?php
                                                $hide = [];
                                                ?>
                                                @cannot('edit_orders_master')
                                                    <?php


                                                    if ($data['shipping_status_id'] == 1) {
                                                        $hide = [3, 4, 5, 6, 7];
                                                    }
                                                    if ($data['shipping_status_id'] == 3) {
                                                        $hide = [2];
                                                    }

                                                    if ($data['shipping_status_id'] == 4) {
                                                        $hide = [2, 3, 5, 6, 7];
                                                    }

                                                    if ($data['shipping_status_id'] == 5) {
                                                        $hide = [2, 3, 4, 6, 7];
                                                    }
                                                    if ($data['shipping_status_id'] == 6) {
                                                        $hide = [2, 3, 4, 5, 7];
                                                    }
                                                    if ($data['shipping_status_id'] == 7) {
                                                        $hide = [2, 3, 4, 6, 5];
                                                    }

                                                    if ($data['shipping_status_id'] > 1) {
                                                        array_push($hide, 1);
                                                    }

                                                    ?>
                                                @endcannot
                                                <td>
                                                    <select style="{{isset($data['order_sub_shipment_status'])?'margin-top: 33px;':''}}"
                                                            name="fulfil_status" id="fulfil_status_{{$data["id"]}}"
                                                            @cannot('order_shipping_status')
                                                            disabled
                                                            @endcannot
                                                            onchange="updateFullfilmentStatus(this.value,'{{$data["id"]}}','{{$data["created_at"]}}')">
                                                        <option value="" disabled>---Select One---</option>
                                                        @foreach ($fulfilmentStatus as $fStatus)
                                                            @if(!in_array($fStatus['id'],$hide))
                                                                <option value="{{$fStatus['id']}}"
                                                                        @cannot('order_shipping_status_'.$fStatus['code'])
                                                                        disabled
                                                                        @endcannot
                                                                        @if($data['shipping_status_id'] == $fStatus['id']) selected @endif>{{$fStatus['name']}}
                                                                </option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    <p id="shipping_sub_status_message_{{$data["id"]}}"
                                                       class="text-danger mt-7">{{isset($data['order_sub_shipment_status'])?$data['order_sub_shipment_status']:''}}</p>
                                                </td>

                                                <?php
                                                $hide = [];
                                                ?>
                                                @cannot('edit_orders_master')
                                                    <?php


                                                    if ($data['payment_status_id'] == 4) {
                                                        $hide = [5, 7, 10, 11];
                                                    }

                                                    if ($data['payment_status_id'] == 5) {
                                                        $hide = [4, 7, 10, 11];
                                                    }
                                                    if ($data['payment_status_id'] == 7) {
                                                        $hide = [5, 4, 10, 11];
                                                    }
                                                    if ($data['payment_status_id'] == 10) {
                                                        $hide = [5, 7, 4, 11];
                                                    }
                                                    if ($data['payment_status_id'] == 11) {
                                                        $hide = [5, 7, 4, 10];
                                                    }

                                                    if ($data['payment_status_id'] > 1) {
                                                        array_push($hide, 1);
                                                    }

                                                    ?>
                                                @endcannot

                                                <td>
                                                    <select name="payment_status" id="payment_status"
                                                            @cannot('order_payment_status')
                                                            disabled
                                                            @endcannot
                                                            onchange="updatePaymentStatus(this.value,'{{$data["id"]}}')">

                                                        <option value="" disabled>---Select One---</option>
                                                        @foreach ($orderPaymentStatusesValues as $pStatus)
                                                            @if(!in_array($pStatus['id'],$hide))
                                                                <option value="{{$pStatus['id']}}"
                                                                        @if($data['payment_status_id'] == $pStatus['id']) selected @endif>{{$pStatus['name']}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td id="totalAmount">
                                                    @php
                                                        $totalAmount[$counterloo] = isset($data['total'])?$data['total']:'';
                                                    @endphp
                                                    <span>RS.{{number_format((float)$data['total'], 2, '.', '')}}</span>
                                                </td>
                                                <td>
                                                <span>
                                                    @if($data['is_foc']==1)
                                                        <i class="fa fa-check" style="color:#2caae1;"></i>
                                                    @else<i class="fa fa-times" style="color:red;"></i>@endif</span>
                                                </td>
                                                @can('Discount')
                                                    <td>
                                                        <input type="text" placeholder="%" name="discount" value="{{$data['discount']}}" id="discount_{{$data['id']}}" onchange="discountFunction({{$data['id']}})">
                                                    </td>
                                                @endcan
                                                <td>
                                                    <select disabled="" name="processed_by"
                                                            id="processed_by_{{$data["id"]}}"
                                                            onchange="updateProcessedBy(this.value,'{{$data["id"]}}')">
                                                        <option value="" selected>---Select One---</option>
                                                        @foreach ($csrusers as $user)
                                                            <option value="{{$user['id']}}"
                                                                    @if($data['processed_by'] == $user['id']) selected @endif>{{$user['username']}}</option>
                                                        @endforeach
                                                        {{--@foreach($csrusers as $newUser)--}}

                                                        {{--@php--}}

                                                        {{--$userLog=Log::where('user_id','!=',$newUser->id)->where('order_id',$data["id"])->get();--}}
                                                        {{--@endphp--}}
                                                        {{--@foreach ($userLog as $userLogID)--}}
                                                        {{--@php $userName= Users::where('id',$userLogID->user_id)->pluck('username')->first();--}}
                                                        {{--@endphp--}}
                                                        {{--{{$userName}}--}}
                                                        {{--@endforeach--}}
                                                        {{--@endforeach--}}
                                                    </select>

                                                </td>
                                                <td>
                                                    <select name="delivered_by" id="delivered_by"
                                                            onchange="updateDeliveredBy(this.value,'{{$data["id"]}}')">
                                                        <option value="" selected>---Select One---</option>
                                                        @php

                                                                @endphp
                                                        @foreach ($riderusers as $user)
                                                            <option value="{{$user['id']}}"
                                                                    @if($data['delivered_by'] == $user['id']) selected @endif>{{$user['username']}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>

                                                <td>@php
                                                        $storeName=$location->where('id','=',$data->location_id)->pluck('store_name')->first();
                                                    @endphp
                                                    {{$storeName}}
                                                </td>
                                                <td>
                                                    <span>{{$data['delivery_time']}}</span>
                                                </td>
                                                <td>
                                                    <span>{{$data['processing_time']}}</span>
                                                </td>
                                                @can('OrderDelete')
                                                    <td class="left_border_dotted text-center"
                                                        onclick="return confirm('Are You sure to delete order?');">
                                                        <a href="{{url('/admin/orders-management/delete',[$data['id']])}}"><i
                                                                    class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                    </td>
                                                @endcan
                                            </tr>



                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="table_pager clearfix mt-20">
                                <div class="text-right col-lg-12 export-as-btn p-0">
                                    <div class="col-md-8 text-left pl-0">
                                        <div class="display-ib mr-10 select-store">
                                            <div class="dropdown">
                                                <select name="locationId" class="form-control">
                                                    <option value="">Select Location</option>
                                                    @foreach($location as $storelocation)
                                                        <option value="{{$storelocation->id}}">{{$storelocation->store_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="display-ib">
                                            <input type="submit" value="Update" id="update"
                                                   class="btn add_inputbtn btn-primary mr-10" name="submitbutton">
                                            @can('AccountReport')
                                                <button value="excel" class="btn btn-primary dropdown-toggle mr-10"
                                                        type="submit" name="submitbutton" aria-expanded="false">CSV
                                                    Account
                                                </button>
                                            @endcan
                                            @can('CSRSupervioserReport')
                                                <button value="csrReport" class="btn btn-primary dropdown-toggle mr-10"
                                                        type="submit" name="submitbutton" aria-expanded="false">CSV CSR
                                                </button>
                                            @endcan
                                            @can('OrderDelete')
                                                <button class="btn btn-danger mr-10" type="submit" name="submitbutton"
                                                        id="delete-order" value="delete">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </button>
                                            @endcan
                                        </div>
                                    </div>
                                    <div class="col-md-4 pr-0">
                                        <div class="pull-right">
                                            {{$links}}
                                        </div>
                                    </div>
                                </div>
                                <div class="tex-right col-md-12 text-right pr-0">
                                    <ul class="mt-20">
                                        <li>
                                            <span class="mr-5">Orders Total:</span><strong>RS.{{array_sum($totalAmount)}}</strong>
                                        </li>
                                        <li>
                                            <span class="mr-5">Grand Total:</span><strong>RS.{{$total or array_sum($totalAmount)}}</strong>
                                        </li>
                                        <li>
                                            <span class="mr-5">Commission Total:</span><strong>RS.{{array_sum($totalCommission)}}</strong>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END MAIN CONTENT -->
            </div>
            <!-- END MAIN -->
            <div class="clearfix"></div>
        </div>
    </div>
@endsection
@section('footer')



    <script>


        $(document).ready(function() {
            $('.JStableOuter table').scroll(function(e) {

                $('.JStableOuter thead').css("left", -$(".JStableOuter tbody").scrollLeft());
                $('.JStableOuter thead th:nth-child(1)').css("left", $(".JStableOuter table").scrollLeft() -0 );
                $('.JStableOuter tbody td:nth-child(1)').css("left", $(".JStableOuter table").scrollLeft());

                $('.JStableOuter thead').css("top", -$(".JStableOuter tbody").scrollTop());
                $('.JStableOuter thead tr th').css("top", $(".JStableOuter table").scrollTop());

            });
        });



    </script>
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>

    <script language="JavaScript">
        function updateProcessedBy(value, order_id) {
            $.ajax({
                url: "{{ url('/admin/orders-management/order/setProcessor') }}",
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {'orderId': order_id, 'value': value},
                success: function (data) {
                    if (data != "Error") {
                        /*var message_contaner = $('#direct-chat-messages_' + id);
                        message_contaner.append(data);*/
                    }
                },
            });
        }

        function updateDeliveredBy(value, order_id) {

            $.ajax({
                url: "{{ url('/admin/orders-management/order/setDeliverBoy') }}",
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {'orderId': order_id, 'value': value},
                success: function (data) {
                    if (data != "Error") {
                        /*var message_contaner = $('#direct-chat-messages_' + id);
                        message_contaner.append(data);*/

                    }
                },
            });
        }

        function updateCommission(value, order_id) {
            $.ajax({
                url: "{{ url('/admin/orders-management/order/addCommission') }}",
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {'orderId': order_id, 'value': value},
                success: function (data) {
                    if (data != "Error") {
                        /*var message_contaner = $('#direct-chat-messages_' + id);
                        message_contaner.append(data);*/
                    }
                },
            });
        }

        function searchProducts() {
            var search_keyword = $('#search_keyword').val();
            var shipping_status = $('#shipping_status').val();
            var payment_status = $('#payment_status').val();
            var order_date = $('#order_date').val();
            var customer_name = $('#customer_name').val();
            var tel = $('#tel').val();

            $.ajax({
                type: 'get',
                url: '{{url("admin/orders-management/searchAllOrders")}}',
                data: {
                    'search_keyword': search_keyword,
                    'shipping_status': shipping_status,
                    'payment_status': payment_status,
                    'order_date': order_date,
                    'customer_name': customer_name,
                    'tel': tel
                },
                success: function (data) {
                    console.log(data);
                    $(' tbody').html(data);
                },
                error: function () {
                },
            });
        }


    </script>
    <script>
        function updateOrderLocation(orderId, val) {

            var location = val.options[val.selectedIndex].value;
//        alert(location);
            $.ajax({
                url: "{{url('/admin/updateOrderLocation')}}", //Cart\CartController@placeorder
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {location: location, orderId: orderId},
                success: function (data) {
                    if (data) {
                        window.location.reload();
                    }
                }
            });
        }
        function discountFunction(val) {
            var discount=$('#discount_'+val).val();

            $.ajax({
                url: "{{url('/admin/orders-management/addDiscount')}}", //Cart\CartController@placeorder
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {discount: discount, orderId: val},
                success: function (data) {

                }
            });

        }
    </script>
@endsection