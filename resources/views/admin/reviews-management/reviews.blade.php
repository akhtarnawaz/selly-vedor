@extends('admin.layouts.master')

@section('content')
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

   <div class="main">
       <!-- MAIN CONTENT -->
       @include('admin.layouts.header')
       <div id="main">
           <div class="row">
               <div class="col-md-12">
                   <div>
                       <h3 class="mt-35 font-normal mb-20 display-ib">Products reviews</h3>
                   </div>
                   {{--<div class="search-conditions-box clearfix products_box">--}}
                       {{--<div class="col-md-6">--}}
                           {{--<input type="text" placeholder="Search Keywords">--}}
                       {{--</div>--}}
                       {{--<div class="col-md-2">--}}
                           {{--<select>--}}
                               {{--<option>Any Category</option>--}}
                               {{--<option>No Catogory Assigned</option>--}}
                               {{--<option>Vagetables</option>--}}
                               {{--<option>Fruits</option>--}}
                           {{--</select>--}}
                       {{--</div>--}}
                       {{--<div class="col-md-2">--}}
                           {{--<select>--}}
                               {{--<option>Any Stock Status</option>--}}
                               {{--<option>In Stock</option>--}}
                               {{--<option>Low Stock</option>--}}
                               {{--<option>Out Of Stock</option>--}}
                           {{--</select>--}}
                       {{--</div>--}}
                       {{--<div class="col-md-2">--}}
                           {{--<button class="border_blue search_btn border_radius3 btn-block">Search</button>--}}
                       {{--</div>--}}
                   {{--</div>--}}
                   <br>
                   <form   method="post" action="{{url('/admin/reviews-management/deleteMutilpleReviews')}}" onsubmit="return confirm('Are you sure to delete these records?');">
                       {{csrf_field()}}

                       <a href="{{url('/admin/reviews-management/add-reviews')}}" class="btn add_statusbtn">Add Review</a>

                       <input type="submit" name="submit" id="delete-order" class="btn add_inputbtn"     value="Delete Selected Value" >
                   <br>
                   <br>

                   <script type="text/javascript">
                       $(function() {
                           $("#hideaftermoment").delay(2000).fadeOut(0);
                       });
                   </script>
                   @if (session('status'))
                       <div id="hideaftermoment" class="alert alert-success">
                           {{ session('status') }}
                       </div>
                   @endif
                   <script type="text/javascript">
                       $(function() {
                           $("#testdiv").delay(2000).fadeOut(0);
                       });
                   </script>
                       <script type="text/javascript">
                           function selectAll(status){
                               $('.multiplerow').each(function(){
                                   $(this).prop('checked',status);
                               });

                           }
                       </script>
                   @if($errors->any())
                       <h4 id="testdiv" class="alert alert-danger">{{$errors->first()}}</h4>
                   @endif
                       <table class="table order_table"  id="myTable">
                       <thead>
                       <tr>

                           <th>
                               <input type="checkbox" onclick='selectAll(this.checked)' value="Select All"/>
                           </th>
                           <th>Product</th>
                           <th>Reviewer</th>
                           <th>Review</th>
                           <th>Rating</th>
                           <th>Status</th>
                           <th>Date</th>
                           <th></th>

                       </tr>
                       </thead>
                       <tbody>
                       @foreach($reviews as $reviewsInfo)
                       <tr>
                           <td class="right_border_dotted"><input type="checkbox" class="multiplerow" id="multiplerow" name="delete[]" value="{{$reviewsInfo->id}}"></td>

                           <td>
                               @foreach($products as $prod)
                               @if($reviewsInfo->product_id == $prod->id)
                           {{$prod->product_title}}

                               @endif
                           @endforeach
                           </td>
                           <td><a href="{{url('/admin/reviews-management/updreviewlisting',[$reviewsInfo->id])}}">{{$reviewsInfo->reviewerName}}</a></td>
                           <td>{{strip_tags($reviewsInfo->review)}}</td>
                           <td>{{$reviewsInfo->rating}}</td>
                           <td title="Enabled" class=" text-center">
                           @if($reviewsInfo->status=='Published')
                               <a href="{{url('/admin/reviews-management/disableStatus',[$reviewsInfo->id])}}"><i class="fa fa-power-off" aria-hidden="true"></i></a>
                           @else
                               <a href="{{url('/admin/reviews-management/enableStatus',[$reviewsInfo->id])}}"><i class="fa fa-power-off" style="color:red" aria-hidden="true"></i></a>
                               @endif
                           </td>
                           <td>{{$reviewsInfo->additionDate}}</td>
                           <td class="left_border_dotted text-center" onclick="return confirm('Are You sure to delete this record?');"><a href="{{url('/admin/reviews-management/delete',[$reviewsInfo->id])}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                       </tr>
                       @endforeach
                       </tbody>
                   </table>


                   <div class="table_pager">
                       <div class="col-lg-4 col-md-4 text-left">

                       </div>
                       <div class="col-lg-8 col-md-8 text-right">
                           <div class="items-per-page-wrapper" style="float: right">
                               {{$reviews->links()}}
                           </div>
                       </div>
                       <div class="clearfix"></div>
                   </div>
                   </form>
               </div>
               @include('admin.layouts.sidebar-right')
           </div>
       </div>
       <!-- END MAIN CONTENT -->
   </div>

@endsection
@section('footer')
<script>

    $(document).ready(function() {
        $('#myTable').DataTable();
    } );
</script>
    @endsection
