@extends('admin.layouts.master')

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<style>
    @if($errors->has('review'))
        #cke_1_contents{
        border: 1px solid red;
    }
    @endif
        @if($errors->has('response'))
           #cke_2_contents{
        border: 1px solid red;
    }
    @endif
    @if($errors->has('product'))
           #product_listing{
        border: 1px solid red;
    }
    @endif

</style>
@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <form name="frmProducts" id="frmProducts" method="post" action="{{ url('/admin/reviews-management/addreviews') }}">
            {{ csrf_field() }}
            <div id="main">

                <!-- Add Product section Start-->
                <div class="add-product-sec">
                    <div class="row">
                        <div class="col-md-9">
                            <div>
                                <h3 class="mt-35 font-normal mb-20 display-ib">Add review</h3>
                            </div>
                            <div class="add-product-box">
                                <div class="form-horizontal">
                                    <fieldset>
                                        <!-- Product name Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_id">Product *</label>
                                            <div class="col-md-4" id="product_listing">
                                                {{--<input id="product_id" name="product_id" class="form-control input-md" required type="text">--}}
                                             <select multiple class="form-control plugin_select form-control" name="product[]">
                                                 <option >Select Product</option>
                                                 @foreach($productsData as $product)
                                                 <option value="{{$product->id}}">{{$product->product_title}}</option>
                                                                                    @endforeach


                                             </select>
                                            </div>
                                        </div>
                                        {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('product') }}</span>--}}
                                        <!-- SKU Text input-->
                                        {{--<div class="form-group{{ $errors->has('rating') ? 'has-error' : '' }}">--}}
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="rating">Rating</label>
                                            <div class="col-md-4">
                                                <input id="rating" name="rating" class="form-control input-md"  type="text" value="{{ old('rating') }}" @if($errors->has('rating'))style=" border-color:red ;" @endif>
                                            </div>
                                        </div>
                                        {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('rating') }}</span>--}}

                                        <!-- UPC/ISBN Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="reviewerName">Reviewer name *</label>
                                            <div class="col-md-4">
                                                <input id="reviewerName" name="reviewerName" class="form-control input-md"  type="text" value="{{ old('reviewerName') }}" @if($errors->has('reviewerName'))style=" border-color:red ;" @endif>
                                            </div>
                                        </div>
{{--                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('reviewerName') }}</span>--}}
                                        <!-- Mnf#/Vendor# Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_weight">Profile</label>
                                            <div class="col-md-4">
                                                <input id="mnf_vendor" name="mnf_vendor" class="form-control input-md"  type="text" value="{{ old('mnf_vendor') }}" @if($errors->has('mnf_vendor'))style=" border-color:red ;" @endif>
                                            </div>
                                        </div>
                                        {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('mnf_vendor') }}</span>--}}
                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="review">Text of review</label>
                                            <div class="col-md-8">
                                                <textarea name="review" id="review" class='ckeditor span12' rows="5" >{{ old('review') }}</textarea>
                                            </div>
                                        </div>
                                        {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('review') }}</span>--}}
                                            <!-- Description Text area-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="response">Text of response</label>
                                            <div class="col-md-8">
                                                <div action="#" method="POST" class='form-wysiwyg'>
                                                    <textarea name="response" id="response" class='ckeditor span12' rows="5" > {{ old('response') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('response') }}</span>--}}

                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Add Product section End-->
            </div>
            <!-- END MAIN CONTENT -->

            <!-- Add Product button fixed at bottom -->
            <div class="add-productbtn-fixed">
                <input class="add_statusbtn" type="submit" name="" value="Create">
            </div>
        </form>
    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/jquery.selectric.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>

@endsection