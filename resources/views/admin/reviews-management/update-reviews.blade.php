@extends('admin.layouts.master')

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<style>
    @if($errors->has('review'))
        #cke_1_contents{
        border: 1px solid red;
    }
    @endif
        @if($errors->has('response'))
           #cke_2_contents{
        border: 1px solid red;
    }
    @endif
    @if($errors->has('product'))
           #product_listing{
        border: 1px solid red;
    }
    @endif

</style>
@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        
        @include('admin.layouts.header')
        <form name="frmProducts" id="frmProducts" method="post" action="{{ url('/admin/reviews-management/updateReviews') }}">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$reviews->id}}">



                    <div id="main">

                        <!-- Add Product section Start-->
                        <div class="add-product-sec">
                            <div class="row">
                                <div class="col-md-9">
                                    <div>
                                        <h3 class="mt-35 font-normal mb-20 display-ib">Add review</h3>
                                    </div>
                                    <div class="add-product-box">
                                        <div class="form-horizontal">
                                            <fieldset>
                                                <!-- Product name Text input-->
                                                {{--<div class="form-group{{ $errors->has('product_id') ? 'has-error' : '' }}">--}}
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" for="product_id">Product *</label>
                                                        <div class="col-md-4">
                                                            {{--@if($productname == '')--}}
                                                                {{--<select name="product_id" id="product_id" class="form-control input-md">--}}
                                                                    {{--<option> Select Product</option>--}}
                                                                    {{--@foreach($allProducts as $product)--}}
                                                                        {{--<option value="{{$product->id}}">{{$product->product_title}}</option>--}}
                                                                    {{--@endforeach--}}
                                                                {{--</select>--}}
                                                            {{--@else--}}
                                                                <input id="product_id"  value="{{$productname->id}}" name="product" class="form-control input-md"  type="hidden">
                                                                <input id="product" disabled value="{{$productname->product_title}}"  class="form-control input-md"  type="text">

                                                            {{--@endif--}}

                                                        </div>
                                                    </div>
                                                {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('product_id') }}</span>--}}
                                                <!-- SKU Text input-->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="rating">Rating</label>
                                                    <div class="col-md-4">
                                                        <input id="rating" name="rating" class="form-control input-md"  type="text" value="{{$reviews->rating}}" @if($errors->has('rating'))style=" border-color:red ;" @endif>
                                                    </div>
                                                </div>
                                                {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('rating') }}</span>--}}

                                                <!-- UPC/ISBN Text input-->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="reviewerName">Reviewer name *</label>
                                                    <div class="col-md-4">
                                                        <input id="reviewerName" name="reviewerName" class="form-control input-md"  type="text"  value="{{$reviews->reviewerName}}" @if($errors->has('reviewerName'))style=" border-color:red ;" @endif>
                                                    </div>
                                                </div>
{{--                                                <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('reviewerName') }}</span>--}}
                                                <!-- Mnf#/Vendor# Text input-->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="product_weight">Profile</label>
                                                    <div class="col-md-4">
                                                        <input id="mnf_vendor" name="mnf_vendor" class="form-control input-md"  type="text"  value="{{$reviews->reviewerName}}">
                                                    </div>
                                                </div>
{{--                                                <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('mnf_vendor') }}</span>--}}
                                                <!-- Text input-->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="review">Text of review</label>
                                                    <div class="col-md-8">
                                                        <textarea name="review" id="review" class='ckeditor span12' rows="5">{{$reviews->review}}</textarea>
                                                    </div>
                                                </div>
{{--                                                <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('review') }}</span>--}}
                                                <!-- Description Text area-->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="response">Text of response</label>
                                                    <div class="col-md-8">
                                                        <div action="#" method="POST" class='form-wysiwyg'>
                                                            <textarea name="response" id="response" class='ckeditor span12' rows="5"> {{$reviews->response}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
{{--                                                <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('response') }}</span>--}}
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" for="user_status">Status</label>
                                                        <div class="col-md-8">
                                                            <div class="checkbox">
                                                                <label>

                                                                    <input type="checkbox" data-toggle="toggle" name="status"  id="user_status" value="Published" @if($reviews->status == 'Published') checked @endif >
                                                                    

                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Add Product section End-->
                    </div>
                    <!-- END MAIN CONTENT -->

                    <!-- Add Product button fixed at bottom -->


            <!-- END MAIN CONTENT -->

            <!-- Add Product button fixed at bottom -->
            <div class="add-productbtn-fixed">
                <input class="add_statusbtn" type="submit" name="" value="Update">
            </div>
        </form>
    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/jquery.selectric.min.js') }}"></script>

    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>

@endsection