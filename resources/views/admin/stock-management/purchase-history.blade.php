@extends('admin.layouts.master')

@section('content')
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/daterangepicker.css') }}">
    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main">
            <div class="row">
                <div class="col-md-9">
                    <div>
                        <h3 class="mt-35 font-normal mb-20 display-ib">Purchase history</h3>
                    </div>
                    {{--@if($purchaseName=='oldReport')--}}
                        {{--@php($name='newReport') <a href="{{ url('/admin/stock-management/purchase-history',[$name]) }}" class="btn add_statusbtn">New Purchase Loss Report</a>--}}
                    {{--@else--}}
                        {{--@php($name='oldReport')<a href="{{ url('/admin/stock-management/purchase-history',[$name]) }}" class="btn add_statusbtn">Old Purchase and Loss</a>--}}
                    {{--@endif--}}
                    <br>
                    <form   method="post" action="{{url('/admin/stock-management/StockExcel')}}" >
                        {{csrf_field()}}

                        <table class="table order_table" id="datatable1">
                        <thead>
                        <tr>
                            <script type="text/javascript">
                                function selectAll(status){
                                    $('.multiplerow').each(function(){
                                        $(this).prop('checked',status);
                                    });

                                }
                            </script>

                            <th>
                                <input type="checkbox" onclick='selectAll(this.checked)' value="Select All"/>
                            </th>
                            <th>Date</th>
                            <th>Product</th>
                            <th>Invoice</th>
                            <th>Quantity</th>
                            <th>Unit</th>
                            <th>Cost Price</th>
                            <th>Total</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                            {{--@if($purchaseName=='oldReport')--}}
                        {{--<tbody>--}}
                        {{--@php--}}

                        {{--@endphp--}}
                        {{--@foreach($purchaseHistory as $purchaseInfo)--}}
                            {{--@php--}}
                                {{--$total = $purchaseInfo->quantity*$purchaseInfo->unit_price;--}}

                            {{--@endphp--}}
                            {{--<tr>--}}
                                {{--<td class="right_border_dotted"><input type="checkbox" class="multiplerow" id="multiplerow" name="excel[]" value="{{$purchaseInfo->id}}"></td>--}}

                                {{--<td>{{$purchaseInfo->purchase_date}}</td>--}}
                                {{--<td><a class="ml-20" href="{{url('/admin/reports/stock-report',[$purchaseInfo->id])}}" target="_blank">{{$purchaseInfo->product_title}}</a></td>--}}
                                {{--<td>{{$purchaseInfo->invoice_no}}</td>--}}
                                {{--<td>{{$purchaseInfo->quantity}}</td>--}}
                                {{--<td>{{$purchaseInfo->unit}}</td>--}}
                                {{--<td>{{$purchaseInfo->unit_price}}</td>--}}
                                {{--<td>{{$total}}</td>--}}
                                {{--<td>--}}{{--<a class="ml-20" href="#">Edit</a>--}}{{--</td>--}}
                            {{--</tr>--}}
                        {{--@endforeach--}}
                        {{--</tbody>--}}
                                {{--@else--}}
                                <tbody>
                                @php

                                        @endphp
                                @foreach($purchaseHistory as $purchaseInfo)
                                    @php
                                        $total = $purchaseInfo->quantity*$purchaseInfo->unit_price;
                                        $productinfo=\App\Models\Admin\Products::where('id','=',$purchaseInfo->product_id)->pluck('product_title')->first();
                                    @endphp
                                    <tr>
                                        <td class="right_border_dotted"><input type="checkbox" class="multiplerow" id="multiplerow" name="excel[]" value="{{$purchaseInfo->id}}"></td>

                                        <td>{{$purchaseInfo->created_at}}</td>
                                        <td><a class="ml-20" href="{{url('/admin/reports/stock-report',[$purchaseInfo->id])}}" target="_blank">{{$productinfo}}</a></td>
                                        <td>{{$purchaseInfo->invoice}}</td>
                                        <td>{{$purchaseInfo->quantity}}</td>
                                        <td>{{$purchaseInfo->unit}}</td>
                                        <td>{{$purchaseInfo->unit_price}}</td>
                                        <td>{{$total}}</td>
                                        <td>{{--<a class="ml-20" href="#">Edit</a>--}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            {{--@endif--}}
                    </table>
                        <div class="table_pager">
                        <div class="col-lg-6 col-md-6 text-left"></div>
                        <div class="col-lg-6 col-md-6 text-right">
                            <div class="items-per-page-wrapper">

                                <button  value="excel" class="btn btn-primary dropdown-toggle mr-10" type="submit"  name="submitbutton" aria-expanded="false">Export As CSV
                                </button>

                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    </form>

                </div>
                @include('admin.layouts.sidebar-right')
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

@endsection

@section('footer')

       <script>

        $(document).ready(function() {
            $('#datatable1').DataTable();
            $('#datatable2').DataTable();
        } );

        function searchProducts(){
            var keyword =  $('#searchkeyword').val();
            var category =  $('#category').val();
            var stock =  $('#stock').val();

            $.ajax({
                type:'get',
                url:'{{url("admin/products-management/searchProducts")}}',
                data:{'keyword':keyword,'category':category,'stock':stock},
                success:function(data) {
                    $(' tbody').html(data);


                },
                error:function(){

                },
            });

        }

      function  confirmdelete(a){

          var txt=a;
          if (!confirm("Are You sure to delete this record?")) {
              return false;

          } else {
              window.location = "{{url('/admin/products-management/delProducts')}}/"+txt+"!";

          }

      }

    </script>


@endsection