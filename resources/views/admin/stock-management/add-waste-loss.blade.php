@extends('admin.layouts.master')

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main" class="stock-management-data">
            <div class="row">
                <form name="frmPurchase" id="frmPurchase" role="form" method="post" action="{{ url('/admin/stock-management/addWasteLossStock') }}">
                    {{csrf_field()}}
                    <div class="col-md-12">
                        <h3 class="mt-35 font-normal mb-20">Wast, Expire, Loss Management</h3>
                        <div class="">
                            @if (session('status'))
                                <div class="alert alert-{{ session('statusClass') }}">
                                    {{ session('status') }}
                                </div>
                            @endif
                        </div>
                        <div class="search-conditions-box">
                            {{--<div class="col-md-2 pl-0">
                                <label>Product catagories</label>
                                <select class="form-control">
                                    <option>Fruits</option>
                                </select>
                            </div>--}}
                            <div class="col-md-2 pl-0 stock-arrival-date">
                                <label>Stock Arrival Date</label>
                                <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                                    <input name="purchase_date" id="purchase_date" value="" type="text" class="form-control" required>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2 pl-0 stock-arrival-date">
                                <label>Invoice#</label>
                                <input name="invoice_no" id="invoice_no" value="" type="text" class="form-control" required>
                            </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-2 pl-0">
                                <button class="border_blue search_btn border_radius3 mr-12 add-record" id="addWastage"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add Wastage</button>
                            </div>
                            <div class="col-md-2 pl-0">
                                <button type="submit" class="border_blue search_btn border_radius3 mr-12" id="saveWastage">Save</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table class="table" id="tbl_products">
                                <thead>
                                <tr>
                                    {{--<th width="3%" class="right_border_dotted text-center">Sr</th>--}}
                                    <th width="10%">Product</th>
                                    <th width="5%">Units</th>
                                    <th width="5%">Available Stock</th>
                                    <th width="5%">Unit Price</th>
                                    <th width="5%">Waste / Damage</th>
                                    <th width="5%">Total Price</th>
                                    <th width="5%">Carry Forward</th>
                                    <th width="3%">Action</th>
                                </tr>
                                </thead>
                                <tbody id="tbl_products_body">
                                <tr>
                                    {{--<td class="right_border_dotted text-center">1</td>--}}
                                    <td>
                                        <select class="e1 form-control" name="products[]" id="product_1" onchange="getProductInfo(this.value,'1');" required>
                                            <option value="">Select Product</option>
                                            @foreach($productsData as $product)
                                                <option value="{{$product->id}}">{{$product->product_title}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="" id="unit1">

                                    </td>
                                    <td><input readonly style="background: #ddd;" type="text" name="openingStocks[]" id="opening_stock_1" class="disabledControl"></td>
                                    <td><input readonly style="background: #ddd;" type="text" name="unitprice[]" id="setunitprice_1" class="disabledControl"></td>
                                    <td><input type="text" name="quantities[]" id="quantitiy_1" onchange="updateTotalStock(this.value,'1');" required></td>

                                    <td><input readonly style="background: #ddd;" type="text" name="totalprice[]" id="total_price_1" class="disabledControl"></td>
                                    <td><input readonly style="background: #ddd;" type="text" name="totalStocks[]" id="total_stock_1"></td>
                                    <td width="50" class="left_border_dotted text-center">{{--<a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>--}}</td>
                                </tr>
                                </tbody>
                            </table>
                            <input type="hidden" name="total_products" id="total_products" value="1">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

@endsection

@section('footer')
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>

    <script type="text/javascript">
        function updateTotalStock(val,divid)
        {
            var totalStock;
            var openingStock = parseInt($('#opening_stock_'+divid).val());
            var newQuantity = parseInt(val);
            var opning_price =$('#setunitprice_'+divid).val();
        if(parseInt($('#quantitiy_'+divid).val()) > parseInt($('#opening_stock_'+divid).val())  ){
            alert('Please select available quantity')
            $('#addWastage').hide()
            $('#saveWastage').hide()

        }else {
            $('#addWastage').show()
            $('#saveWastage').show()
        }
            if(newQuantity !='' && !isNaN(newQuantity))
            {
                totalStock = openingStock - newQuantity;
                $('#total_stock_'+divid).val(totalStock);
                $('#total_price_'+divid).val(val * opning_price);
            }
        }

        function getSalePrice(divid)
        {
            var productSaleprice;
            var costPrice = parseInt($('#cost_price_'+divid).val());
            var profitPercentage = parseInt($('#profit_'+divid).val());
            var profitAmount = (profitPercentage/100) * costPrice;

            productSaleprice = costPrice + profitAmount;

            if(costPrice !='' && !isNaN(costPrice) && profitPercentage !='' && !isNaN(profitPercentage))
            {
                $('#sale_price_'+divid).val(productSaleprice);
            }
        }

        function getProductInfo(val,divid)
        {
            var productid = val;

            $.ajax({
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{url('/admin/stock-management/getProductInfo')}}',
                data: 'pid=' + productid + '&divid=' + divid,
                dataType: 'json',
                success: function(data)
                {
                    if(data.status==="success")
                    {
                        $('#unit'+divid).html(data.unitData);
                    }
                    else if(data.status==="error")
                    {
                        //
                    }
                },
                error:function (error)
                {
                    //
                }
            });
        }

        $(document).ready(function(){

            jQuery(document).delegate('button.add-record', 'click', function(e) {

                e.preventDefault();

                var content = jQuery('#sample_table tr'),
                    size = jQuery('#tbl_products >tbody >tr').length + 1,
                    element = null,
                    element = content.clone();

                $('#total_products').val(size);

                /*element.attr('id', 'rec-'+size);
                element.find('.delete-record').attr('data-id', size);
                element.appendTo('#tbl_products_body');
                element.find('.sn').html(size);*/

                var i = size;

                //$('#tbl_products_body').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="name[]" placeholder="Enter your Name" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
                $('#tbl_products_body').append('<tr id="rec-'+i+'">\n' +
                    '                <td>\n' +
                    '                    <select required class="e1 form-control" name="products[]" id="product_'+i+'" onchange="getProductInfo(this.value,'+i+');">\n' +
                    '                        <option value="">Select Product</option>\n' +
                    '                        @foreach($productsData as $product)\n' +
                    '                            <option value="{{$product->id}}">{{$product->product_title}}</option>\n' +
                    '                        @endforeach\n' +
                    '                    </select>\n' +
                    '                </td>\n' +
                    '                 <td class="" id="unit'+i+'">\n' +
                    '                </td>\n' +
                    '                <td><input readonly style="background: #ddd;" type="text" name="openingStocks[]" id="opening_stock_'+i+'"></td>\n' +
                    '                <td><input required style="background: #ddd;" type="text" name="unitprice[]" id="setunitprice_'+i+'" ></td>\n' +
                    '                <td><input required type="text" name="quantities[]" id="quantitiy_'+i+'" onchange="updateTotalStock(this.value,'+i+');"></td>\n' +
                    '                <td><input required style="background: #ddd;" type="text" name="totalprice[]" id="total_price_'+i+'" ></td>\n' +
                    '                <td><input readonly style="background: #ddd;" type="text" name="totalStocks[]" id="total_stock_'+i+'"></td>\n' +
                    '                <td width="50" class="left_border_dotted text-center"><a class="btn btn-xs delete-record" data-id="'+i+'"><i class="glyphicon glyphicon-trash"></i></a></td>\n' +
                    '            </tr>');
            });

            jQuery(document).delegate('a.delete-record', 'click', function(e) {

                e.preventDefault();
                var didConfirm = confirm("Are you sure You want to delete");
                if (didConfirm == true)
                {
                    var id = jQuery(this).attr('data-id');
                    var targetDiv = jQuery(this).attr('targetDiv');
                    jQuery('#rec-' + id).remove();

                    size = jQuery('#tbl_products >tbody >tr').length;
                    $('#total_products').val(size);

                    //regnerate index number on table
                    $('#tbl_products_body tr').each(function(index){
                        $(this).find('span.sn').html(index+1);
                    });

                    return true;
                }
                else
                {
                    return false;
                }
            });

        });
        function setunitPrice(divid , productid) {
            var unit = $('#productUnits_'+divid).val();
            $.ajax({
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{url('/admin/stock-management/getProductUnitPrice')}}',
                data: 'pid=' + productid + '&divid=' + divid + '&unit=' + unit,
                dataType: 'json',
                success: function(data)
                {
                    if(data.status==="success")
                    {
                        $('#opening_stock_'+divid).val(data.openingStock);
                        $('#total_stock_'+divid).val(data.openingStock);
                        $('#setunitprice_'+divid).val(data.product_price);
                        $('#unit'+divid).html(data.unitData);

                    }
                    else if(data.status==="error")

                    {
                        //
                    }
                },
                error:function (error)
                {
                    //
                }
            });
        }

    </script>

@endsection