@extends('admin.layouts.master')

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">

@section('content')

        <!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="content-heading clearfix">
            <div class="pull-left">
                <div class="heading-left">
                    <button type="button" class="btn-toggle-minified" title="Toggle Minified Menu"><i class="ti-menu"></i></button>
                    <div class="dropdown">
                        <a href="#" class="dropdown-toggle quick-menu display-ib ml-30 mt-7" data-toggle="dropdown"> <i class="ti-bell mt-10 font-size-20"></i></a>
                        <div class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>

                            <li><a href="#">Separated link</a></li>
                            <li class="divider"></li>
                            <li><a href="#">One more separated link</a> </li>
                        </div>
                    </div>
                    <div class="dropdown">
                        <a href="#" class="dropdown-toggle quick-menu display-ib ml-30 mt-7" data-toggle="dropdown"> <i class="ti-calendar mt-10 font-size-20"></i></a>
                        <div class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                            <li class="divider"></li>
                            <li><a href="#">One more separated link</a> </li>
                        </div>
                    </div>
                    <div class="dropdown">
                        <a href="#" class="dropdown-toggle quick-menu display-ib ml-30 mt-7 gray_text" data-toggle="dropdown">Quick Menu</a>
                        <div class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                            <li class="divider"></li>
                            <li><a href="#">One more separated link</a> </li>
                        </div>
                    </div>
                </div>
                <div class="input-group display-ib">
                    <button type="button" class="btn btn-default dropdown-toggle ml-30 search_in_btn" data-toggle="dropdown" aria-expanded="true">Search in <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#" data-code="product" data-placeholder="Products - p: key">Products</a></li>
                        <li><a href="#" data-code="profile" data-placeholder="Users - u: key">Users</a></li>
                        <li><a href="#" data-code="order" data-placeholder="Orders - o: key">Orders</a></li>
                    </ul>
                    <input type="text" class="form-control display-ib products_input" name="substring" value="" placeholder="Products">
                </div>
            </div>
            <div class="pull-right">
                <ul class="breadcrumb">
                    <li><a href="shop" class="gray_text"><label class="switch">
                                <input type="checkbox" checked>
                                <span class="slider round"></span>
                            </label> View Storefornt</a>
                    </li>
                    <div class="dropdown display-ib">
                        <a href="#" class="dropdown-toggle quick-menu display-ib ml-30 mt-7" data-toggle="dropdown"> <i class="ti-user mt-10 font-size-20"></i></a>
                        <div class="dropdown-menu right0">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                            <li class="divider"></li>
                            <li><a href="#">One more separated link</a> </li>
                        </div>
                    </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="main" class="stock-management-data">
        <div class="row">
            <div class="col-md-12">
                <h3 class="mt-35 font-normal mb-20">Stock Management</h3>
                <div class="">
                </div>
                <div class="search-conditions-box">
                    <div class="col-md-2 pl-0">
                        <label>Product catagories</label>
                        <select class="form-control">
                            <option>Fruits</option>
                            <option>Vegetables</option>
                            <option>Electorincs</option>
                            <option>Medicines</option>
                            <option>Furnitures</option>
                            <option>Dairy Products</option>
                        </select>
                    </div>
                    <div class="col-md-2 pl-0 stock-arrival-date">
                        <label>Stock Arrival Date</label>
                        <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                            <input  type="text" class="form-control">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                    <div class="col-md-2 pl-0">
                        <label>Search stock by</label>
                        <select class="form-control">
                            <option>Product</option>
                            <option>Quantity</option>
                            <option>Sold</option>
                            <option>Left</option>
                            <option>Waste</option>
                            <option>Forward</option>
                        </select>
                    </div>
                    <div class="col-md-2 pl-0">
                        <button class="border_blue search_btn border_radius3 mr-12">Search</button>
                        <button class="border_blue search_btn border_radius3">Clear</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <br>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th width="3%" class="right_border_dotted text-center">Sr</th>
                            <th width="10%">Product</th>
                            <th width="5%">Opening Stock</th>
                            <th width="5%">Units</th>
                            <th width="5%">New Quantity</th>
                            <th width="5%">Units</th>
                            <th width="5%">Cost Price</th>
                            <th width="5%">Profit %</th>
                            <th width="5%">Sale Price</th>
                            <th width="3%">FOC</th>
                            <th width="5%">Products Sold</th>
                            <th width="5%">Units</th>
                            <th width="5%">Products Left</th>
                            <th width="5%">Units</th>
                            <th width="5%">Waste / Damage</th>
                            <th width="5%">Units</th>
                            <th width="5%">Carry Forward</th>
                            <th width="5%">Units</th>
                            <th width="3%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="right_border_dotted text-center">1</td>
                            <td>
                                <select class="e1 form-control">
                                    <option>Select Product</option>
                                    <option>Vegetables</option>
                                    <option>Fruits</option>
                                    <option>Medicines</option>
                                    <option>Cars</option>
                                </select>
                            </td>
                            <td><input disabled type="text"></td>
                            <td class="disabled_select_units">
                                <select disabled class="e1 form-control">
                                    <option>Item</option>
                                    <option>Piece</option>
                                    <option>Dozen</option>
                                    <option>Bunch</option>
                                    <option>Gms</option>
                                    <option>Kg</option>
                                    <option>Lbs</option>
                                    <option>ML</option>
                                    <option>Liter</option>
                                    <option>Yard</option>
                                </select>
                            </td>
                            <td>
                                <input type="text">
                            </td>
                            <td>
                                <select class="e1 form-control">
                                    <option>Item</option>
                                    <option>Piece</option>
                                    <option>Dozen</option>
                                    <option>Bunch</option>
                                    <option>Gms</option>
                                    <option>Kg</option>
                                    <option>Lbs</option>
                                    <option>ML</option>
                                    <option>Liter</option>
                                    <option>Yard</option>
                                </select>
                            </td>
                            <td><input type="text"></td>
                            <td><input type="text" placeholder="0.00 %"></td>
                            <td><input type="text"></td>
                            <td><input type="checkbox"></td>
                            <td><input disabled type="text"></td>
                            <td class="disabled_select_units">
                                <select disabled class="e1 form-control">
                                    <option>Item</option>
                                    <option>Piece</option>
                                    <option>Dozen</option>
                                    <option>Bunch</option>
                                    <option>Gms</option>
                                    <option>Kg</option>
                                    <option>Lbs</option>
                                    <option>ML</option>
                                    <option>Liter</option>
                                    <option>Yard</option>
                                </select>
                            </td>
                            <td><input disabled type="text"></td>
                            <td class="disabled_select_units">
                                <select disabled class="e1 form-control">
                                    <option>Item</option>
                                    <option>Piece</option>
                                    <option>Dozen</option>
                                    <option>Bunch</option>
                                    <option>Gms</option>
                                    <option>Kg</option>
                                    <option>Lbs</option>
                                    <option>ML</option>
                                    <option>Liter</option>
                                    <option>Yard</option>
                                </select>
                            </td>
                            <td><input type="text"></td>
                            <td>
                                <select class="e1 form-control">
                                    <option>Item</option>
                                    <option>Piece</option>
                                    <option>Dozen</option>
                                    <option>Bunch</option>
                                    <option>Gms</option>
                                    <option>Kg</option>
                                    <option>Lbs</option>
                                    <option>ML</option>
                                    <option>Liter</option>
                                    <option>Yard</option>
                                </select>
                            </td>
                            <td><input disabled type="text"></td>
                            <td class="disabled_select_units">
                                <select disabled class="e1 form-control">
                                    <option>Item</option>
                                    <option>Piece</option>
                                    <option>Dozen</option>
                                    <option>Bunch</option>
                                    <option>Gms</option>
                                    <option>Kg</option>
                                    <option>Lbs</option>
                                    <option>ML</option>
                                    <option>Liter</option>
                                    <option>Yard</option>
                                </select>
                            </td>
                            <td width="50" class="left_border_dotted text-center"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                        </tr>
                        <tr>
                            <td class="right_border_dotted text-center">1</td>
                            <td>
                                <select class="e1 form-control">
                                    <option>Select Product</option>
                                    <option>Vegetables</option>
                                    <option>Fruits</option>
                                    <option>Medicines</option>
                                    <option>Cars</option>
                                </select>
                            </td>
                            <td><input disabled type="text"></td>
                            <td class="disabled_select_units">
                                <select disabled class="e1 form-control">
                                    <option>Item</option>
                                    <option>Piece</option>
                                    <option>Dozen</option>
                                    <option>Bunch</option>
                                    <option>Gms</option>
                                    <option>Kg</option>
                                    <option>Lbs</option>
                                    <option>ML</option>
                                    <option>Liter</option>
                                    <option>Yard</option>
                                </select>
                            </td>
                            <td>

                                <input type="text">
                            </td>
                            <td>
                                <select class="e1 form-control">
                                    <option>Item</option>
                                    <option>Piece</option>
                                    <option>Dozen</option>
                                    <option>Bunch</option>
                                    <option>Gms</option>
                                    <option>Kg</option>
                                    <option>Lbs</option>
                                    <option>ML</option>
                                    <option>Liter</option>
                                    <option>Yard</option>
                                </select>
                            </td>
                            <td><input type="text"></td>
                            <td><input type="text" placeholder="0.00 %"></td>
                            <td><input type="text"></td>
                            <td><input type="checkbox"></td>
                            <td><input disabled type="text"></td>
                            <td class="disabled_select_units">
                                <select disabled class="e1 form-control">
                                    <option>Item</option>
                                    <option>Piece</option>
                                    <option>Dozen</option>
                                    <option>Bunch</option>
                                    <option>Gms</option>
                                    <option>Kg</option>
                                    <option>Lbs</option>
                                    <option>ML</option>
                                    <option>Liter</option>
                                    <option>Yard</option>
                                </select>
                            </td>
                            <td><input disabled type="text"></td>
                            <td class="disabled_select_units">
                                <select disabled class="e1 form-control">
                                    <option>Item</option>
                                    <option>Piece</option>
                                    <option>Dozen</option>
                                    <option>Bunch</option>
                                    <option>Gms</option>
                                    <option>Kg</option>
                                    <option>Lbs</option>
                                    <option>ML</option>
                                    <option>Liter</option>
                                    <option>Yard</option>
                                </select>
                            </td>
                            <td><input type="text"></td>
                            <td>
                                <select class="e1 form-control">
                                    <option>Item</option>
                                    <option>Piece</option>
                                    <option>Dozen</option>
                                    <option>Bunch</option>
                                    <option>Gms</option>
                                    <option>Kg</option>
                                    <option>Lbs</option>
                                    <option>ML</option>
                                    <option>Liter</option>
                                    <option>Yard</option>
                                </select>
                            </td>
                            <td><input disabled type="text"></td>
                            <td class="disabled_select_units">
                                <select disabled class="e1 form-control">
                                    <option>Item</option>
                                    <option>Piece</option>
                                    <option>Dozen</option>
                                    <option>Bunch</option>
                                    <option>Gms</option>
                                    <option>Kg</option>
                                    <option>Lbs</option>
                                    <option>ML</option>
                                    <option>Liter</option>
                                    <option>Yard</option>
                                </select>
                            </td>
                            <td width="50" class="left_border_dotted text-center"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                        </tr>
                        <tr>
                            <td class="right_border_dotted text-center">1</td>
                            <td>
                                <select class="e1 form-control">
                                    <option>Select Product</option>
                                    <option>Vegetables</option>
                                    <option>Fruits</option>
                                    <option>Medicines</option>
                                    <option>Cars</option>
                                </select>
                            </td>
                            <td><input disabled type="text"></td>
                            <td class="disabled_select_units">
                                <select disabled class="e1 form-control">
                                    <option>Item</option>
                                    <option>Piece</option>
                                    <option>Dozen</option>
                                    <option>Bunch</option>
                                    <option>Gms</option>
                                    <option>Kg</option>
                                    <option>Lbs</option>
                                    <option>ML</option>
                                    <option>Liter</option>
                                    <option>Yard</option>
                                </select>
                            </td>
                            <td>
                                <input type="text">
                            </td>
                            <td>
                                <select class="e1 form-control">
                                    <option>Item</option>
                                    <option>Piece</option>
                                    <option>Dozen</option>
                                    <option>Bunch</option>
                                    <option>Gms</option>
                                    <option>Kg</option>
                                    <option>Lbs</option>
                                    <option>ML</option>
                                    <option>Liter</option>
                                    <option>Yard</option>
                                </select>
                            </td>
                            <td><input type="text"></td>
                            <td><input type="text" placeholder="0.00 %"></td>
                            <td><input type="text"></td>
                            <td><input type="checkbox"></td>
                            <td><input disabled type="text"></td>
                            <td class="disabled_select_units">
                                <select disabled class="e1 form-control">
                                    <option>Item</option>
                                    <option>Piece</option>
                                    <option>Dozen</option>
                                    <option>Bunch</option>
                                    <option>Gms</option>
                                    <option>Kg</option>
                                    <option>Lbs</option>
                                    <option>ML</option>
                                    <option>Liter</option>
                                    <option>Yard</option>
                                </select>
                            </td>
                            <td><input disabled type="text"></td>
                            <td class="disabled_select_units">
                                <select disabled class="e1 form-control">
                                    <option>Item</option>
                                    <option>Piece</option>
                                    <option>Dozen</option>
                                    <option>Bunch</option>
                                    <option>Gms</option>
                                    <option>Kg</option>
                                    <option>Lbs</option>
                                    <option>ML</option>
                                    <option>Liter</option>
                                    <option>Yard</option>
                                </select>
                            </td>
                            <td><input type="text"></td>
                            <td>
                                <select class="e1 form-control">
                                    <option>Item</option>
                                    <option>Piece</option>
                                    <option>Dozen</option>
                                    <option>Bunch</option>
                                    <option>Gms</option>
                                    <option>Kg</option>
                                    <option>Lbs</option>
                                    <option>ML</option>
                                    <option>Liter</option>
                                    <option>Yard</option>
                                </select>
                            </td>
                            <td><input disabled type="text"></td>
                            <td class="disabled_select_units">
                                <select disabled class="e1 form-control">
                                    <option>Item</option>
                                    <option>Piece</option>
                                    <option>Dozen</option>
                                    <option>Bunch</option>
                                    <option>Gms</option>
                                    <option>Kg</option>
                                    <option>Lbs</option>
                                    <option>ML</option>
                                    <option>Liter</option>
                                    <option>Yard</option>
                                </select>
                            </td>
                            <td width="50" class="left_border_dotted text-center"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="mt-35">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#home">Low inventory products (0)</a></li>
                        <li><a data-toggle="tab" href="#menu1">Best sellers</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">
                            <p>All the products are in sufficient quantities</p>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>SKU (Stock Keeping Unit ID)</th>
                                    <th>Name</th>
                                    <th>Sold</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>10000000045</td>
                                    <td><a href="#">Ornage</a></td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>10000000043</td>
                                    <td><a href="#">Apple</a></td>
                                    <td>2</td>
                                </tr>
                                <tr>
                                    <td>10000000047</td>
                                    <td><a href="#">Brocolli</a></td>
                                    <td>4</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
    <!-- RIGHT SIDEBAR -->
    <div id="sidebar-right" class="right-sidebar">
        <div class="sidebar-widget">
            <h4 class="widget-heading"><i class="fa fa-calendar"></i> TODAY</h4>
            <p class="date">Wednesday, 22 December</p>
            <div class="row margin-top-30">
                <div class="col-xs-4">
                    <a href="#">
                        <div class="icon-transparent-area custom-color-blue first"> <i class="fa fa-tasks"></i> <span>Tasks</span> <span class="badge">5</span> </div>
                    </a>
                </div>
                <div class="col-xs-4">
                    <a href="#">
                        <div class="icon-transparent-area custom-color-green"> <i class="fa fa-envelope"></i> <span>Mail</span> <span class="badge">12</span> </div>
                    </a>
                </div>
                <div class="col-xs-4">
                    <a href="#">
                        <div class="icon-transparent-area custom-color-orange last"> <i class="fa fa-user-plus"></i> <span>Users</span> <span class="badge">24</span> </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="sidebar-widget">
            <div class="widget-header">
                <h4 class="widget-heading">YOUR APPS</h4>
                <a href="#" class="show-all">Show all</a>
            </div>
            <div class="row">
                <div class="col-xs-3"> <a href="#" class="icon-app" title="Dropbox" data-toggle="tooltip" data-placement="top"> <i class="fa fa-dropbox dropbox-color"></i> </a> </div>
                <div class="col-xs-3"> <a href="#" class="icon-app" title="WordPress" data-toggle="tooltip" data-placement="top"> <i class="fa fa-wordpress wordpress-color"></i> </a> </div>
                <div class="col-xs-3"> <a href="#" class="icon-app" title="Drupal" data-toggle="tooltip" data-placement="top"> <i class="fa fa-drupal drupal-color"></i> </a> </div>
                <div class="col-xs-3"> <a href="#" class="icon-app" title="Github" data-toggle="tooltip" data-placement="top"> <i class="fa fa-github github-color"></i> </a> </div>
            </div>
        </div>
        <div class="sidebar-widget">
            <div class="widget-header">
                <h4 class="widget-heading">MY PROJECTS</h4>
                <a href="#" class="show-all">Show all</a>
            </div>
            <ul class="list-unstyled list-project-progress">
                <li>
                    <a href="#" class="project-name">Project XY</a>
                    <div class="progress progress-xs progress-transparent custom-color-orange">
                        <div class="progress-bar" role="progressbar" aria-valuenow="67" aria-valuemin="0" aria-valuemax="100" style="width:67%"></div>
                    </div>
                    <span class="percentage">67%</span>
                </li>
                <li>
                    <a href="#" class="project-name">Growth Campaign</a>
                    <div class="progress progress-xs progress-transparent custom-color-blue">
                        <div class="progress-bar" role="progressbar" aria-valuenow="23" aria-valuemin="0" aria-valuemax="100" style="width:23%"></div>
                    </div>
                    <span class="percentage">23%</span>
                </li>
                <li>
                    <a href="#" class="project-name">Website Redesign</a>
                    <div class="progress progress-xs progress-transparent custom-color-green">
                        <div class="progress-bar" role="progressbar" aria-valuenow="87" aria-valuemin="0" aria-valuemax="100" style="width:87%"></div>
                    </div>
                    <span class="percentage">87%</span>
                </li>
            </ul>
        </div>
    </div>
    <!-- END RIGHT SIDEBAR -->
</div>
<!-- END MAIN -->

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>

@endsection