@extends('admin.layouts.master')

@section('content')
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main">
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <h3 class="mt-35 font-normal mb-20 display-ib">Categories</h3>
                    </div>
                    <br>
                    <a href="{{url('/admin/categories-management/add-categories')}}" class="btn add_statusbtn">Add Category</a>
                    <br>
                    <br>

                    <div class="search-conditions-hidden search-conditions-box products-hidden-box clearfix">
                        <div class="col-md-3">
                            <div>
                                <p>Search In:</p>
                                <ul>
                                    <li>
                                        <label>
                                            <input type="checkbox" name="" value="">
                                            Name</label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" name="" value="">
                                            SKU</label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" name="" value="">
                                            Description</label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" name="" value="">
                                            Tag</label>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-9">
                            <div>
                                <span>Availability</span>
                                <select>
                                    <option>any availability status</option>
                                    <option>Only Enabled</option>
                                    <option>Only Disabled</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <form   method="post" action="{{url('/admin/categories-management/delMultipleCategory')}}" onsubmit="return confirm('Are you sure to delete these records?');">
                        {{csrf_field()}}

                        <script type="text/javascript">
                            $(function() {
                                $("#hideaftermoment").delay(2000).fadeOut(0);
                            });
                        </script>
                        @if (session('status'))
                            <div id="hideaftermoment" class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <script type="text/javascript">
                            $(function() {
                                $("#testdiv").delay(2000).fadeOut(0);
                            });
                        </script>
                        @if($errors->any())
                            <h4 id="testdiv" class="alert alert-danger">{{$errors->first()}}</h4>
                        @endif
                        <table class="table order_table" >
                            <thead>
                            <tr>
                                <th width="5%"></th>
                                <th width="5%"></th>
                                <th width="5%"></th>
                                <th width="30%">Category</th>
                                <th width="30%">Parent Category</th>
                                <th width="25%"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categoriesData as $categoryInfo)
                                <tr>
                                    <td class="right_border_dotted text-center"><i class="ti-move"></i></td>
                                    @if($categoryInfo->status == '1')
                                        <td title="Enabled" class="right_border_dotted text-center"><a href="{{url('/admin/categories-management/statusupdate',[$categoryInfo->id])}}"><i class="fa fa-power-off" aria-hidden="true"></i></a></td>
                                    @else
                                        <td title="Disabled" class="right_border_dotted text-center"><a href="{{url('/admin/categories-management/updatestatusone',[$categoryInfo->id])}}"><i class="fa fa-power-off" style="color:red" aria-hidden="true"></i></a></td>
                                    @endif

                                    <td>
                                        <div class="file-uploader">
                                            <img src="{{ URL::asset('public/images/category') }}/{{$categoryInfo->category_icon}}" alt="" class="img-responsive">
                                        </div>

                                    <td>
                                        <a href="{{url('/admin/categories-management/update', [$categoryInfo->id])}}" class="ml-20">{{$categoryInfo->category_title}}</a>
                                    </td>
                                    <td>
                                        @if($categoryInfo->parent_id !='')
                                        @php
                                            $parentCategory=$categoryInfo->where('id','=',$categoryInfo->parent_id)->pluck('category_title')->first()
                                            @endphp
                                        {{$parentCategory}}
                                        @endif
                                    </td>
                                    {{--<td><a href="">subcat under this category counter</a></td>--}}
                                    <td><a href="">{{--products under this category--}}</a></td>
                                    <td id="delete-category" onclick="return confirm('Are You sure to delete this record?');" class="left_border_dotted text-center"><a href="{{url('/admin/categories-management/delCategory' ,[$categoryInfo->id]) }}"> <i class="fa fa-trash-o" aria-hidden="true"></i></a></td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="table_pager">
                            <div class="col-lg-6 col-md-6 text-left">

                                {{--<ul class="pagination">--}}
                                {{--<li><a href="#">&laquo;</a></li>--}}
                                {{--<li><a href="#">1</a></li>--}}
                                {{--<li><a href="#">2</a></li>--}}
                                {{--<li><a href="#">3</a></li>--}}
                                {{--<li><a href="#">4</a></li>--}}
                                {{--<li><a href="#">5</a></li>--}}
                                {{--<li><a href="#">&raquo;</a></li>--}}
                                {{--</ul>--}}

                            </div>
                            <div class="col-lg-6 col-md-6 text-right">
                                <div class="items-per-page-wrapper" style="float: right">
                                    {{  $categoriesData->links()}}

                                    {{--<span><b>{{  $categoriesData->count()}}</b> records</span>--}}
                                    {{--<input type="submit" name="submit" value="Delete Selected Value" >--}}
                                    {{--<span>found</span>--}}
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
                @include('admin.layouts.sidebar-right')
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-progressbar/js/bootstrap-progressbar.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.time.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/flot.tooltip/jquery.flot.tooltip.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/x-editable/bootstrap3-editable/js/bootstrap-editable.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-sparkline/js/jquery.sparkline.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-tour/js/bootstrap-tour.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widget.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/data.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/scroll-parent.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/disable-selection.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widgets/mouse.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widgets/sortable.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/datatables/js-main/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/datatables/js-bootstrap/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-appear/jquery.appear.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jqvmap/jquery.vmap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jqvmap/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>

@endsection