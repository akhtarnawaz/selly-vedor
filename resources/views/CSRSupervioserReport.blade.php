<tr>
    <th>OrderNumber</th>
    <th>Customer Name</th>
    <th>Customer Email</th>
    <th>Customer Address</th>
    <th>Customer Number</th>
    <th>Order Total</th>
    <th>Order Discount</th>
    <th>Order Discount Total</th>
    <th>shipping status</th>
    <th>payment status</th>
    <th>payment method</th>
    <th>orders date</th>
    <th>Staff note</th>
    <th>Process By</th>
    <th>Delivered By</th>
</tr>
<br>
@php
    use App\Models\Admin\Customers;
    use App\Models\Admin\Users;
    use App\Models\Admin\OrderShippingStatusTranslations;
    use App\Models\Admin\OrderPaymentStatusTranslations;

    $grandTatal=0;
@endphp
@foreach($data as $order)

    @foreach($order as $orderitem)
        <tr>
            <th>{{$orderitem->orderNumber}}</th>
            @php

                $user=  Users::all();
                $username=$user->where('id','=',$orderitem->user_id)->pluck('username')->first();
                $email=$user->where('id','=',$orderitem->user_id)->pluck('email')->first();
                $address=$user->where('id','=',$orderitem->user_id)->pluck('address')->first();
                $cell_no=$user->where('id','=',$orderitem->user_id)->pluck('cell_no')->first();

                  $customer= Customers::all();

                $customerusername=$customer->where('id','=',$orderitem->customer_id)->pluck('username')->first();
                $customeremail=$customer->where('id','=',$orderitem->customer_id)->pluck('email')->first();
                $customeraddress=$customer->where('id','=',$orderitem->customer_id)->pluck('address')->first();
                $customerphoneno=$customer->where('id','=',$orderitem->customer_id)->pluck('phoneno')->first();

                    $deliverdBy= $user->where('id','=',$orderitem->delivered_by)->pluck('username')->first();
                    $processBy= $user->where('id','=',$orderitem->processed_by)->pluck('username')->first();

             $shippingStatus=   OrderShippingStatusTranslations::where('id','=',$orderitem->shipping_status_id)->pluck('name')->first();

            $paymentStatus=    OrderPaymentStatusTranslations::where('id','=',$orderitem->payment_status_id)->pluck('name')->first();


            @endphp
            @if($customerusername != null)
            <th>{{$customerusername}}</th>
            <th>{{$customeremail}}</th>
            <th>{{$customeraddress}}</th>
            <th>{{$customerphoneno}}</th>
          @else
            <th>{{$username}}</th>
            <th>{{$email}}</th>
            <th>{{$address}}</th>
            <th>{{$cell_no}}</th>
            @endif
            <th>{{$orderitem->total}}</th>
            <th>{{$orderitem->discount}}@if($orderitem->discount > 0)%@endif</th>
            <th>{{$orderitem->discountedAmount}}</th>
            <th>{{$shippingStatus}}</th>
            <th>{{$paymentStatus}}</th>
            <th>COD</th>
            <th>{{$orderitem->created_at}}</th>
            <th>{{$orderitem->staff_note}}</th>



            <th>{{$processBy}}</th>
            <th>{{$deliverdBy}}</th>
        </tr>
    @endforeach
@endforeach
