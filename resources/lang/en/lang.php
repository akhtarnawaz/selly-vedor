<?php
/**
 * Created by PhpStorm.
 * Users: P3Care
 * Date: 1/11/2018
 * Time: 10:58 AM
 */
return [

    /*
    |--------------------------------------------------------------------------
    | Site Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on different Pages of Site
    |
    */

    'site_title' => 'Selly', //Site Title
    /**
     * General
     */

    'all_categories' => 'All Categories',
    'fruits' => 'Fruits',
    'vegetables' => 'vegetables',
    'search_products' => 'Search Products Here',
    'searchs' => 'SEARCH',
    'info_services' => 'Information Service',
    'info_services_num' => '03041117355',
    'categories' => 'Categories',
    'about_us' => 'About Us',
    'home' => 'Home',
    'my_account' => 'Login / Register',
    'register' => 'Register',
    'login' => 'Login',
    'checkout' => 'Checkout',
    'my_cart' => 'My Cart',
    'my_cart_empty' => 'Your shopping cart is empty!',


    /**
     * End General
     */

    /*
     * Top Bar Strings
     */
    'home_top_bar_wishlist' => 'Wish List',
    'home_top_bar_checkout' => 'Checkout',
    'home_top_bar_usd' => '$ USD',
    'home_top_bar_english' => 'English',
    /*
     * End Top Bar Strings
     */

    /**
     * Banner
     */
    'service_free_shipping' => 'FREE SHIPPING WORLDWIDE',
    'customer_service' => '24/7 CUSTOMER SERVICE',
    'money_back' => 'money back guarantee',
    /**
     *End Banner
     */




];