<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CategoriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'categoryTitle' => 'sometimes|required',
            'category_description' => 'sometimes|required',
//            'category_icon' => 'category_icon',
            'category_page_title' => 'sometimes|required',
//            'meta_descriptions' => 'sometimes|required',
//            'meta_keywords' => 'sometimes|required',
            'product_page_title' => 'sometimes|required',
            'cleanURL' => 'sometimes|required',
        ];

//        return $validator;
    }
}
