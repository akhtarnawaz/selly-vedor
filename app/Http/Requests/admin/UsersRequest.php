<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return[
            'first_name' => 'sometimes|required',
            'email' => [
                'sometimes',
                'required',
                'unique:users,email,' . $this->getSegmentFromEnd().',id'
            ],
            'user_name' => [
                'sometimes',
                'required',
                'unique:users,username,'.$this->getSegmentFromEnd().',id',
            ],

//            'email' => 'sometimes|required|email|unique:users,email' .$this->getSegmentFromEnd().',id',
//            'user_name' => 'sometimes|required|unique:users,username'.$this->getSegmentFromEnd().',id',
            'password' => 'sometimes|required|min:8',
            'confirm_password' => 'sometimes|required|min:8|same:password',

        ];
    }
        private function getSegmentFromEnd($position_from_end = 1) {
        $segments =$this->segments();
        return $segments[sizeof($segments) - $position_from_end];
    }

}
