<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return[
            'productTitle' => 'sometimes|required|max:255',
//            'sku' => 'sometimes|required',
//            'upc_isbn' => 'sometimes|required',
//            'mnf_vendor' => 'sometimes|required',
            'category_id' => 'sometimes|required',
            'product_description' => 'sometimes|required',
//            'product_detail' => 'sometimes|required',
            // 'marketPrice' => 'sometimes|required|numeric',
            'arrivalDate' => 'sometimes|required',
            'expireDate' => 'sometimes|required',
//            'quantity_in_stock' => 'sometimes|required|numeric',
//            'product_length_fractional_part' => 'sometimes|required',
            'unit' => 'sometimes|required',
//            'weight' => 'sometimes|required|numeric',
//            'freight' => 'sometimes|numeric',
//            'meta_descriptions' => 'sometimes|required',
//            'meta_keywords' => 'sometimes|required',
//            'product_page_title' => 'sometimes|required',
            'cleanURL' => 'sometimes|required',

        ];

//        return $validator;
    }

}
