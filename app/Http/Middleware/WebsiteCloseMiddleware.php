<?php

namespace App\Http\Middleware;

use App\Models\WebSiteOnOff;
use Closure;
use Illuminate\Support\Facades\Redirect;

class WebsiteCloseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

   $web =WebSiteOnOff::orderBy('id','asc')->first();
   if($web->status == 'On'){
        return $next($request);
    }
    else{
       return redirect('/Storeview');
    }
    }
}
