<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;
class ClearanceMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$permission)
    {
        if(in_array($permission,Auth::user()->getAllPermissions()->pluck('name')->toArray()))
        {
            return $next($request);
        }
        else{
            abort('401');
//            return view('admin/users-management/users');
        }
        return true;

    }
}
