<?php

namespace App\Http\Controllers;

use App\Helpers\General\UtilityHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */

    protected $categories;

    public function __construct()
    {
        $this->categories = UtilityHelper::getCategories();
        View::share('categories' , $this->categories);

//        $prducts=UtilityHelper::getProducts();
//        return view('layouts.home.products-block')->with(['prducts'=>$prducts]);
    }
}
