<?php

namespace App\Http\Controllers\admin;

use App\Models\Admin\Location;
use App\Models\Admin\Users;
use App\Models\Shops;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Spatie\Permission\Models\Role;

class LocationController extends Controller
{

    public function index()
    {
        $location = Location::all();

        return view('admin/storeLocation-management/store-location', ['location' => $location]);
    }

    public function StoreLocation()
    {
        $userRole = Role::all();
//        dd($userRole);
        return view('admin/storeLocation-management/addstore-location');

    }

    public function addStoreLocation(Request $request)
    {
        $values = array(
            'store_name' => $request->storeName,
            'location_name' => $request->storeLocation,
            'timefrom' => $request->shortTimefrom,
            'timeto' => $request->shortTimeto,
        );
        $location = new Location();
        $lastid = $location->addLocation($values);
        return Redirect::to('/admin/store-management/Store');

    }

    public function updatStoreLocation(Redirect $redirect, $id)
    {

        $updateLocation = Location::find($id);
        return view('admin/storeLocation-management/updateStore', ['location' => $updateLocation]);

    }

    public function updatStore(Request $request)
    {
        $id = $request->id;
        $values = array(
            'store_name' => $request->storeName,
            'location_name' => $request->storeLocation,
            'timefrom' => $request->shortTimefrom,
            'timeto' => $request->shortTimeto,
        );

        $updat = Location::where('id', '=', $id)->update($values);
        return Redirect::to('/admin/store-management/Store');

    }

    public function delete(Request $request, $id)
    {
        Location::where('id', '=', $id)->delete();
        return Redirect::to('/admin/store-management/Store');

    }

    public function shops()
    {
        $shops = Shops::with('owners')->get();
        return view('admin/shops/shops', ['shops' => $shops]);
    }

    public function shopAddView()
    {
        $users = Users::where('user_status', '=', '1')->get();
        return view('admin/shops/add-shops', compact('users'));
    }
    public function addShop(Request $request){

        Shops::create( [
            'name'=>$request->name,
            'owner'=>$request->owner,
        ]);
        return Redirect::to('/admin/shops')->with(['success'=>'Successfully Added']);

    }
    public function deleteShop($id){
        Shops::where('id','=',$id)->delete();
        return Redirect::to('/admin/shops')->with(['success'=>'Successfully Deleted']);
    }

}
