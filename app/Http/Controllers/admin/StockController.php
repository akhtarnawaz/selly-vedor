<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\ProductsRequest;
use App\Models\Admin\InventoryRecord;
use App\Models\Admin\Orders;


use App\Models\Admin\StockLocation;
use App\Models\Admin\StockLog;
use App\Models\Admin\Units;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Models\Admin\Categories;
use App\Models\Admin\Products;
use App\Models\Admin\Inventory;
use App\Models\Admin\InventoryItems;
use App\Models\Admin\InventoryWastage;
use App\Models\Admin\InventoryWastageItems;
use App\Models\Admin\OrdersItems;
use Auth;

use Maatwebsite\Excel\Facades\Excel;

class StockController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function stockListings()
    {
        $productsData = Products::get();
        //$OrderItems = new OrdersItems;

        return view('admin/stock-management/stock', compact('productsData'));
    }

    public function purchaseHistory($name)
    {
        if ($name == 'oldReport') {
            $purchaseName = 'oldReport';
            $purchaseHistory = inventory
                ::join('inventory_items', 'inventory.id', '=', 'inventory_items.inventory_id')
                ->join('products', 'inventory_items.product_id', '=', 'products.id')
                ->select('products.id', 'products.product_title', 'inventory.purchase_date', 'inventory.invoice_no',
                    'inventory_items.quantity', 'inventory_items.unit', 'inventory_items.unit_price')
                ->get();
        } else {
            $purchaseHistory = InventoryRecord::where('record_type', '=', 'p')->get();
            $purchaseName = 'newReport';
        }
        return view('admin/stock-management/purchase-history', compact('purchaseHistory', 'purchaseName'));
    }

    public function addPurchaseHistory()
    {
        $productsData = Products::all();

        return view('admin/stock-management/add-purchase', compact('productsData'));
    }

    public function getProductInfo(Request $request)
    {
        $product_id = $request->pid;
        $divid = $request->divid;

        try {
            $productsData = Products::where(['id' => $product_id])->get();
            $product_quantity = $productsData[0]->quantity_in_stock;

            $units = Units::where(['product_id' => $product_id])->get();
            $unitData = '';
            $unitData .= '<select class="e1 form-control" name="units[]" id="productUnits_' . $divid . '" onchange="setunitPrice(' . $divid . ',' . $product_id . ')" id="unit_' . $divid . '" required>';


            $unitData .= '<option >Select Unit</option>';
            foreach ($units as $unit) {
                $unitData .= '<option value="' . $unit->unit . '">' . $unit->unit . '</option>';


            }
            $unitData .= '</select>';


            return response()->json(['status' => 'success', 'openingStock' => $product_quantity, 'unitData' => $unitData]);
        } catch (QueryException $ex) {
            return response()->json(['status' => 'error', 'message' => 'Error Occurred. Data not retrieved.']);
        }
    }

    public function getProductUnitPrice(Request $request)
    {
        $product_id = $request->pid;
        $divid = $request->divid;
        $unit = $request->unit;

        try {
            $productsData = Units::where('unit', '=', $unit)->where('product_id', '=', $product_id)->first();
            $inventry=InventoryRecord::where('unit', '=', $unit)->where('record_type', '=', 'p')->where('product_id', '=', $product_id)->orderBy('created_at', 'DESC')->first();

            $product_quantity = $productsData->product_quantity;
            if($inventry != null){
            $product_price = $inventry->unit_price;
            }else{
                $product_price ='';
            }
            return response()->json(['status' => 'success', 'openingStock' => $product_quantity, 'product_price' => $product_price]);
        } catch (QueryException $ex) {
            return response()->json(['status' => 'error', 'message' => 'Error Occurred. Data not retrieved.']);
        }
    }

    public function addPurchaseEntry(Request $request)
    {

        $inventoryDate_array = explode("/", $request->purchase_date);//03/22/2018
        $inventoryDate = $inventoryDate_array[2] . "-" . $inventoryDate_array[0] . "-" . $inventoryDate_array[1];
        $invoiceNo = $request->invoice_no;

        $products_array = $request->products;

//        $inventoryValues = array(
//            'purchase_date' => $inventoryDate,
//            'invoice_no' => $invoiceNo,
//            'added_by' => Auth::User()->id,
//            'added_on' => date('Y-m-d H:i:s')
//        );
//
//        $inventory = new Inventory();
//        $lastInventoryId = $inventory->addInventory($inventoryValues);
//
//        if($lastInventoryId)
//        {
        foreach ($products_array as $pkey => $pval) {
            $product = isset($_POST['products'][$pkey]) ? $_POST['products'][$pkey] : '';
            $unit = isset($_POST['units'][$pkey]) ? $_POST['units'][$pkey] : '';
            $openingStock = isset($_POST['openingStocks'][$pkey]) ? $_POST['openingStocks'][$pkey] : '';
            $quantity = isset($_POST['quantities'][$pkey]) ? $_POST['quantities'][$pkey] : '';
            $totalStock = isset($_POST['totalStocks'][$pkey]) ? $_POST['totalStocks'][$pkey] : '';
            $costPrice = isset($_POST['costPrices'][$pkey]) ? $_POST['costPrices'][$pkey] : '';
            $profit = isset($_POST['profits'][$pkey]) ? $_POST['profits'][$pkey] : '';
            $salePrice = isset($_POST['salePrices'][$pkey]) ? $_POST['salePrices'][$pkey] : '';
            $foc = isset($_POST['focs'][$pkey]) ? $_POST['focs'][$pkey] : '';
            $inventoryItemsValues = array(
                'added_by' => \Illuminate\Support\Facades\Auth::user()->id,
                'product_id' => $product,
                'invoice' => $invoiceNo,
                'unit' => $unit,
                'opening_stock' => $openingStock,
                'quantity' => $quantity,
                'total_stock' => $totalStock,
                'unit_price' => $costPrice,
                'profit' => $profit,
                'sale_price' => $salePrice,
                'record_type' => 'p',
                'record_date' => $inventoryDate,
                'created_at' => date('Y-m-d H:i:s')


            );

            $inventoryItems = new InventoryRecord();
            $lastInventoryItemsId = $inventoryItems->addInventoryRecords($inventoryItemsValues);

            if ($lastInventoryItemsId) {
                //---------- Update Product Values ----------------
                $productsValues = array(
                    'product_quantity' => $totalStock,
                    'product_price' => $salePrice
                );

                $products = Units::where('product_id', '=', $product)->where('unit', '=', $unit)->update($productsValues);
//                    $products = new Products();
//                    $updateProduct = $products->updateProduct($product, $productsValues);

                $message = 'Stock Added Source : ' . 'New Purchase Added' . ' - Opening Stock : ' . $openingStock . ' Change : ' . $quantity . ' New Quantity : ' . $totalStock . ' Unit : ' . $unit;
                StockLog::addLog($message, null, $product, null);
                //-------------------------------------------------
            }
        }
//        }

        $productsData = Products::all();
        return redirect('admin/stock-management/add-purchase')->with(['statusClass' => 'info', 'status' => 'Purchase has been added successfully.'], compact('productsData'));
        exit;
    }

    public function addWasteLoss()
    {
        $productsData = Products::all();
        return view('admin/stock-management/add-waste-loss', compact('productsData'));
    }

    public function addWasteLossEntry(Request $request)
    {
        $inventoryDate_array = explode("/", $request->purchase_date);//03/22/2018
        $inventoryDate = $inventoryDate_array[2] . "-" . $inventoryDate_array[0] . "-" . $inventoryDate_array[1];
        $invoiceNo = $request->invoice_no;

        $products_array = $request->products;
        $units_array = $request->units;
        $openingStocks_array = $request->openingStocks;
        $quantities_array = $request->quantities;
        $totalStocks_array = $request->totalStocks;

        foreach ($products_array as $pkey => $pval) {
            $product = isset($_POST['products'][$pkey]) ? $_POST['products'][$pkey] : '';
            $unit = isset($_POST['units'][$pkey]) ? $_POST['units'][$pkey] : '';
            $openingStock = isset($_POST['openingStocks'][$pkey]) ? $_POST['openingStocks'][$pkey] : '';
            $quantity = isset($_POST['quantities'][$pkey]) ? $_POST['quantities'][$pkey] : '';
            $totalStock = isset($_POST['totalStocks'][$pkey]) ? $_POST['totalStocks'][$pkey] : '';
            $unitprice = isset($_POST['unitprice'][$pkey]) ? $_POST['unitprice'][$pkey] : '';
            $totalprice = isset($_POST['totalprice'][$pkey]) ? $_POST['totalprice'][$pkey] : '';

            $inventoryItemsValues = array(
                'added_by' => \Illuminate\Support\Facades\Auth::user()->id,
                'product_id' => $product,
                'invoice' => $invoiceNo,
                'unit' => $unit,
                'opening_stock' => $openingStock,
                'waste_opening_price' => $unitprice,
                'quantity' => $quantity,
                'total_stock' => $totalStock,
                'total_waste_price' => $totalprice,
                'unit_price' => 0,
                'profit' => 0,
                'sale_price' => 0,
                'record_type' => 'w',
                'record_date' => $inventoryDate,
                'created_at' => date('Y-m-d H:i:s'),


            );

            $inventoryItems = new InventoryRecord();
            $lastInventoryItemsId = $inventoryItems->addInventoryRecords($inventoryItemsValues);

            if ($lastInventoryItemsId) {
                //---------- Update Product Values ----------------
                $productsValues = array(
                    'product_quantity' => $totalStock
                );

                $products = Units::where('product_id', '=', $product)->where('unit', '=', $unit)->update($productsValues);
//                    $products = new Products();
//                    $products->updateProduct($product,$productsValues);
                //-------------------------------------------------
                $message = 'Stock Wasted Source : ' . 'Opening Stock' . ' - New Waste Added  : ' . $openingStock . ' Change : ' . $quantity . ' New Quantity : ' . $totalStock . ' Unit : ' . $unit;
                StockLog::addLog($message, null, $product, null);
            }
        }
//        }

        $productsData = Products::all();
        return redirect('admin/stock-management/waste-loss-stock')->with(['statusClass' => 'info', 'status' => 'Wastage has been added successfully.'], compact('productsData'));
    }

    public function StockExcel(Request $request)
    {
        $excel = $request->input('excel');

        if (is_array($excel)) {
            $orderItem = array();
            foreach ($excel as $id) {
                $orderItem[] = InventoryRecord::join('products', 'inventory_record.product_id', '=', 'products.id')
                    ->select('products.id', 'products.product_title', 'inventory_record.created_at', 'inventory_record.invoice', 'inventory_record.id',
                        'inventory_record.opening_stock', 'inventory_record.quantity', 'inventory_record.total_stock', 'inventory_record.unit', 'inventory_record.sale_price', 'inventory_record.unit_price')
                    ->where('record_type', '=', 'p')
                    ->where('inventory_record.id', '=', $id)->get();

            }

            $orderItemArray = array();

            foreach ($orderItem as $key => $order) {

                $parser = $order->toArray();

                if ($key == 0) {
                    $orderItemArray[] = array_keys($parser[0]);
                }
                $orderItemArray[] = $parser[0];

            }

            Excel::create('Purchase History', function ($excel) use ($orderItemArray) {

                $excel->setTitle('Inventry Detail');
                $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
                $excel->setDescription('Inventry Detail file');

                $excel->sheet('sheet1', function ($sheet) use ($orderItemArray) {
                    $sheet->fromArray($orderItemArray, null, 'A1', false, false);
                });

            })->download('csv');

        }
    }


    public function stockReport(Request $request)
    {
        $excel = $request->input('excel');

        if (is_array($excel)) {
            $orderItem = array();
            foreach ($excel as $id) {
//                        dd($del);
                $orderItem[] = Products::where('id', '=', $id)->get();

            }


            Excel::create('OrdersDetail', function ($excel) use ($orderItem) {

                $excel->setTitle('stock Detail');
                $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
                $excel->setDescription('Stck Detail file');


                $excel->sheet('sheet1', function ($sheet) use ($orderItem) {
                    $sheet->loadView('stockDetailExcel')->with('productsData', $orderItem);
                });

            })->download('csv');

        }
    }


    public function wasteHistory($name)
    {

        if ($name == 'oldReport') {
            $wasteHistory = InventoryWastageItems::all();
            $product = Products::all();
            $invetryWastage = InventoryWastage::all();
            $wasteName = 'oldReport';
        } else {
            $wasteHistory = InventoryRecord::where('record_type', '=', 'w')->get();
            $product = Products::all();
            $wasteName = 'newReport';
        }
        return view('admin/stock-management/waste-and-lose-history', compact('wasteHistory', 'product', 'invetryWastage', 'wasteName'));

    }

    public function WasteExcel(Request $request)
    {
        $excel = $request->input('excel');

        if (is_array($excel)) {
            $orderItem = array();
            foreach ($excel as $id) {
                $orderItem[] = InventoryRecord::join('products', 'inventory_record.product_id', '=', 'products.id')
                    ->select('products.id', 'products.product_title', 'inventory_record.created_at', 'inventory_record.invoice', 'inventory_record.id',
                        'inventory_record.opening_stock', 'inventory_record.quantity', 'inventory_record.total_stock', 'inventory_record.unit', 'inventory_record.sale_price')
                    ->where('record_type', '=', 'w')->where('inventory_record.id', '=', $id)->get();
            }

            $orderItemArray = array();
            foreach ($orderItem as $key => $order) {
                $parser = $order->toArray();
                if ($key == 0) {
                    $orderItemArray[] = array_keys($parser[0]);
                }
                $orderItemArray[] = $parser[0];
            }
            Excel::create('Purchase History', function ($excel) use ($orderItemArray) {

                $excel->setTitle('Inventry Detail');
                $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
                $excel->setDescription('Inventry Detail file');

                $excel->sheet('sheet1', function ($sheet) use ($orderItemArray) {
                    $sheet->fromArray($orderItemArray, null, 'A1', false, false);
                });

            })->download('csv');

        }
    }

    public function wastByDate(Request $request)
    {
        $dates = $request->date;
        $date = explode(' - ', $dates);
        $firstDate = $date[0];
        $lastDate = $date[1];
        $single = explode('/', $firstDate);
        $first_date = $single[2] . '-' . $single[0] . '-' . $single[1];
        $last = explode('/', $lastDate);
        $last_date = $last[2] . '-' . $last[0] . '-' . $last[1];
        if ($firstDate != $lastDate) {
            $data = InventoryRecord::whereBetween('created_at', array($first_date, $last_date))->where('record_type', '=', 'w')->get();
        } else {
            $data = InventoryRecord::where('record_type', '=', 'w')->where('record_date', 'LIKE', '%' . $first_date . '%')->get();
        }

        return view('admin/stock-management/import/wasteRecord', ['data' => $data]);
    }


}