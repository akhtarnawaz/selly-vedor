<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Mail\forgotpassword;
use App\Models\Admin\Location;
use Illuminate\Http\Request;
use App\Models\Admin\Orders;
use App\Models\Admin\Products;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        //$awaitingorders = Orders::where('payment_status_id','=','1' )->where('shipping_status_id','=','1' )->get();

        /*$awaitingorders = Orders
            ::join('users', 'orders.user_id', '=', 'users.id')
            ->leftJoin('customers', 'orders.customer_id', '=', 'customers.id')
            ->join('order_payment_status_translations', 'orders.payment_status_id', '=', 'order_payment_status_translations.id')
            ->join('order_shipping_status_translations', 'orders.shipping_status_id', '=', 'order_shipping_status_translations.id')
            ->select('orders.*', 'users.first_name','users.last_name','users.email','customers.full_name', 'order_payment_status_translations.name AS order_payment_status','order_shipping_status_translations.name AS order_shipment_status')
            ->where('orders.payment_status_id','=','1' )
            ->where('orders.shipping_status_id','=','1' )
            ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid Users objects.
            ->get();*/

        $awaitingorders = Orders
            ::join('order_payment_status_translations', 'orders.payment_status_id', '=', 'order_payment_status_translations.id')
            ->join('order_shipping_status_translations', 'orders.shipping_status_id', '=', 'order_shipping_status_translations.id')
            ->leftjoin('payment_methods', 'orders.payment_method', '=', 'payment_methods.id')
            ->select('orders.*', 'order_payment_status_translations.name AS order_payment_status','order_shipping_status_translations.name AS order_shipment_status','payment_methods.title AS order_payment_method')
            ->where('orders.payment_status_id','=','1' )
            ->where('orders.shipping_status_id','=','1' )
            ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid Users objects.
            ->orderBy('orders.created_at','DESC')
            ->paginate(10);
$location=Location::all();
//dd($awaitingorders);
        $lowinvtproducts = Products::where('quantity_in_stock','<','1' )->get();
        $lowinvtproductsCount = Products::where('quantity_in_stock','<','1' )->count();
        $bestsellersproducts = Products::where('best_sellers','=','1' )->get();

        return view('admin/dashboard',['location'=>$location,'awaitingorders'=>$awaitingorders,'lowinvproducts'=>$lowinvtproducts,'lowinvtproductsCount'=>$lowinvtproductsCount,'bestsellersproducts'=>$bestsellersproducts]);
    }

    public function updateOrderLocation(Request $request){
        $locationId=$request->location;
        $orderId=$request->orderId;

        $data=Orders::where('id','=',$orderId)->update(['location_id'=>$locationId]);
        return response()->json($data);
    }


    public function updatlocation(Request $request){
        $orderid=$request->update;
        $locationId=$request->locationId;
        if($locationId){
        if($orderid){

            foreach ($orderid as $id){
            $data=Orders::where('id','=',$id)->update(['location_id'=>$locationId]);
            }
        }
        return redirect()->back();

        }
        else{
            return redirect()->back();
        }
    }
}