<?php

namespace App\Http\Controllers\admin;

use App\Helpers\General\UtilityHelper;
use App\Http\Controllers\Controller;

use App\Models\Admin\Location;
use App\Models\Admin\Log;
use App\Models\Admin\OrderPaymentStatusTranslations;
use App\Models\Admin\OrderShippingStatusTranslations;
use App\Models\Admin\Categories;
use App\Models\Admin\OrdersItems;
use App\Models\Admin\OrderSubShippingStatusTranslations;
use App\Models\Admin\Products;
use App\Models\Admin\StockLog;
use App\Models\Admin\Units;
use App\Models\Admin\Users;
use App\Models\OrderItems;
use Carbon\Carbon;
use function GuzzleHttp\Promise\promise_for;
use Illuminate\Http\Request;
//use Request;
use App\Models\Admin\Orders;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Redirect;
use App\Models\Admin\Customers;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Permission\Models\Role;

class OrdersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request, $order)
    {
        $currentDate=Carbon::now();

        if ($order == 'orders') {
            $result = Orders::with('orderShippingStatus','orderPaymentStatus','orderPaymentMethods','orderUser','orderCustomer','orderItems')
                ->orderBy('orders.created_at', 'DESC')
                ->paginate(25);

            $title = 'Orders';
            $searchUrl = '/admin/orders-management/order/searchAllOrders';
        } elseif ($order == 'paidorders') {
            $result =Orders::with('orderShippingStatus','orderPaymentStatus','orderPaymentMethods','orderUser','orderCustomer','orderItems')
                ->where('payment_status_id', '=', '4')
                ->paginate(25);
            $title = 'Paid Orders';
            $searchUrl = '/admin/orders-management/order/searchPaidOrders';
        } elseif ($order == 'unpaidorders') {
            $result = Orders::with('orderShippingStatus','orderPaymentStatus','orderPaymentMethods','orderUser','orderCustomer','orderItems')
                ->where('payment_status_id', '=', '1')
                ->paginate(25);
            $title = 'Unpaid Orders';
            $searchUrl = '/admin/orders-management/order/searchUnPaidOrders';
        };


        $riderrole = Role::where('name', '=', 'Rider')->first();
        $riderusers = Users::join('user_has_roles', 'users.id', '=', 'user_has_roles.users_id')
            ->leftJoin('roles', 'user_has_roles.role_id', '=', 'roles.id')->where('roles.id', '=', $riderrole->id)
            ->select('users.username', 'users.id')->get();

        $location = Location::all();
        $csrrole = Role::where('name', '=', 'CSR')->first();
        $csrusers = Users::join('user_has_roles', 'users.id', '=', 'user_has_roles.users_id')
            ->leftJoin('roles', 'user_has_roles.role_id', '=', 'roles.id')->where('roles.id', '=', $csrrole->id)
            ->select('users.username', 'users.id')->get();

        $location = Location::all();
        $users = Users::all();
        $orderPaymentStatusesValues = OrderPaymentStatusTranslations::all();
        $fulfilmentStatus = OrderShippingStatusTranslations::all();
        $subFulfilmentStatus = OrderSubShippingStatusTranslations::all();
//        $result=$orederList->paginate(25);

        $links = $result->links();
        $grandTotal = Orders::select(DB::raw("SUM(total) as count"))->first()->count;

        return view('admin/orders-management/order', ['csrusers' => $csrusers,
            'riderusers' => $riderusers,
            'location' => $location,
            'result' => $result,
            'links' => $links,
            'orderPaymentStatusesValues' => $orderPaymentStatusesValues,
            'fulfilmentStatus' => $fulfilmentStatus,
            'subFulfilmentStatus' => $subFulfilmentStatus,
            'users' => $users,
            'total' => $grandTotal,
            'title' => $title,
            'searchUrl' => $searchUrl,
        ]);
    }

    public function OrdersDetail(Request $request, $id)
    {

            $userDetails = Users::leftjoin('orders', 'users.id', '=', 'orders.user_id')
            ->leftjoin('order_shipping_sub_status_translations', 'orders.sub_shipping_status_id', '=', 'order_shipping_sub_status_translations.status_number')
            ->where('orders.id', '=', $id)
            ->select('users.cell_no as phoneno', 'orders.payment_status_id as payment_status_id', 'orders.shipping_status_id as shipping_status_id',
                'order_shipping_sub_status_translations.name AS order_sub_shipment_status',
                'orders.id as id','orders.username as orderUsername','orders.address as orderAddress','orders.phoneno as orderPhoneno',
                'orders.discountedAmount as discountedAmount','orders.schedule_order_date as schedule_order_date', 'orders.discount as discount', 'orders.orderNumber as orderNumber',
                'orders.staff_note as staff_note', 'orders.notes as notes', 'orders.total as total', 'orders.user_id as user_id',
                'orders.created_at as created_at', 'users.username as username', 'users.email as email', 'users.address as address')
            ->first();


        if ($userDetails != null) {
            $userDetail = $userDetails;
            $userId = $userDetail->user_id;
        } else {
            $userDetail = Customers::leftjoin('orders', 'customers.id', '=', 'orders.customer_id')->where('orders.id', '=', $id)
                ->leftjoin('order_shipping_sub_status_translations', 'orders.sub_shipping_status_id', '=', 'order_shipping_sub_status_translations.status_number')
                ->select('customers.phoneno  as phoneno', 'orders.id as id','orders.username as orderUsername','orders.address as orderAddress','orders.phoneno as orderPhoneno',
                    'orders.discountedAmount as discountedAmount', 'orders.discount as discount', 'orders.orderNumber as orderNumber',
                    'orders.staff_note as staff_note', 'orders.schedule_order_date as schedule_order_date', 'orders.notes as notes', 'orders.payment_status_id as payment_status_id', 'orders.total as total',
                    'order_shipping_sub_status_translations.name AS order_sub_shipment_status',
                    'orders.shipping_status_id as shipping_status_id', 'orders.customer_id as customer_id', 'orders.created_at as created_at',
                    'customers.username as username', 'customers.email as email', 'customers.address as address')
                ->first();
            $userId = $userDetail->customer_id;

        }
        $orderDetail = Orders::leftjoin('order_items', 'orders.id', '=', 'order_items.order_id')
            ->leftJoin('products', 'order_items.object_id', '=', 'products.id')
            ->where('orders.id', '=', $id)->select('order_items.price as price',
                'order_items.total as total','order_items.shop_id as shop_id', 'order_items.quantity_unit_id as 	quantity_unit', 'order_items.item_quantity as item_quantity','order_items.object_id as object_id',
                'order_items.id as id',
                'order_items.discount as discount', 'order_items.discountedSubtotal as discountedSubtotal', 'order_items.order_id as order_id',
                'products.product_title as product_title', 'products.unit as unit')->get();


        $orderPaymentStatusesValues = OrderPaymentStatusTranslations::all();
        $fulfilmentStatus = OrderShippingStatusTranslations::all();
        $subFulfilmentStatus = OrderSubShippingStatusTranslations::all();
        $productsData = Products::all();
      return view('admin/orders-management/order-detail', ['userId' => $userId,
            'productsData' => $productsData,
            'orderDetail' => $orderDetail,
            'usersDetails' => $userDetail,
            'orderPaymentStatusesValues' => $orderPaymentStatusesValues,
            'fulfilmentStatus' => $fulfilmentStatus,
            'subFulfilmentStatus' => $subFulfilmentStatus
        ]);

    }

    public function searchAllOrders(Request $request, $title)
    {
        if($request->search_keyword){
        $search_keyword = isset($request->search_keyword) ? $request->search_keyword : '';
        }else{
            $search_keyword = isset($request->customer_name) ? $request->customer_name : '';

        }
        $shipping_status = isset($request->shipping_status) ? $request->shipping_status : '';
        $payment_status = isset($request->payment_status) ? $request->payment_status : '';
        $order_date = isset($request->order_date) ? $request->order_date : '';

        $tel = isset($request->tel) ? $request->tel : '';
        if ($title == 'Orders') {
            $searchQuery = Orders::with('orderShippingStatus','orderPaymentStatus','orderPaymentMethods','orderUser','orderCustomer','orderItems');
             $searchUrl = '/admin/orders-management/order/searchAllOrders';
        }
        elseif ($title == 'Paid Orders') {
            $searchQuery = Orders::with('orderShippingStatus','orderPaymentStatus','orderPaymentMethods','orderUser','orderCustomer','orderItems');
            $searchUrl = '/admin/orders-management/order/searchPaidOrders';
        } //unpaid orders
        elseif ($title == 'Unpaid Orders') {
            $searchQuery =Orders::with('orderShippingStatus','orderPaymentStatus','orderPaymentMethods','orderUser','orderCustomer','orderItems');
            $searchUrl = '/admin/orders-management/order/searchUnPaidOrders';
        }
        $AllValue = $request->all();
//        dd($searchQuery->get());

        if ($search_keyword != '') {

            $searchQuery->whereHas('orderUser', function ($query) use ($search_keyword){
                $query->where('first_name', 'like', '%'.$search_keyword.'%')
                        ->orWhere('last_name', 'LIKE', '%' . $search_keyword . '%')
                        ->orWhere('email', 'LIKE', '%' . $search_keyword . '%');
            })->orWhereHas('orderCustomer', function ($query) use ($search_keyword){
                $query->Where('email', 'LIKE', '%' . $search_keyword . '%');
            })->orWhere('orders.orderNumber', 'LIKE', '%' . $search_keyword . '%')
                ->with(['orderUser' => function($query) use ($search_keyword){
                    $query->where('first_name', 'like', '%'.$search_keyword.'%')
                        ->orWhere('last_name', 'LIKE', '%' . $search_keyword . '%')
                        ->orWhere('email', 'LIKE', '%' . $search_keyword . '%');
                }]);
        }

        if ($order_date != '') {
            $dates = explode(' - ', $order_date);
            $start_date = $dates[0];

            $end_date = $dates[1];
            if ($start_date == $end_date) {

                $single = explode('/', $start_date);
                $single_date = $single[2] . '-' . $single[0] . '-' . $single[1];

                $searchQuery->orwhere('orders.created_at', 'LIKE', '%' . $single_date . '%');


            } else {
                $datesnew = explode(' - ', $order_date);
                $first_date = Carbon::parse($datesnew[0]);
                $last_date = Carbon::parse($datesnew[1]);
                $forward_date  =$last_date->addDay(1);
                $searchQuery->whereBetween('orders.created_at', array($first_date, $last_date));
            }
        }


        if (!empty($AllValue['shipping_status'])) {

            $searchQuery->wherein('orders.shipping_status_id', $AllValue['shipping_status']);

        }
        if (!empty($AllValue['payment_status'])) {
            $searchQuery->wherein('orders.payment_status_id', $AllValue['payment_status']);
        }

//        if ($customer_name != '') {
//            if ($searchQuery->orWhere('customers.username', 'LIKE', '%' . $customer_name . '%') == '') {
//                $searchQuery->orWhere('users.username', 'LIKE', '%' . $customer_name . '%')
//                    ->orWhere('users.last_name', 'LIKE', '%' . $customer_name . '%');
//            }
//        }

        if ($tel != '') {
            $searchQuery->orWhere('customers.phoneno', 'LIKE', '%' . $tel . '%');
        }

        $searchResults = $searchQuery->orderBy('orders.created_at', 'DESC')->get();
        $location = Location::all();
        $riderrole = Role::where('name', '=', 'Rider')->first();
        $riderusers = Users::join('user_has_roles', 'users.id', '=', 'user_has_roles.users_id')
            ->leftJoin('roles', 'user_has_roles.role_id', '=', 'roles.id')->where('roles.id', '=', $riderrole->id)
            ->select('users.username', 'users.id')->get();

        $location = Location::all();
        $csrrole = Role::where('name', '=', 'CSR')->first();
        $csrusers = Users::join('user_has_roles', 'users.id', '=', 'user_has_roles.users_id')
            ->leftJoin('roles', 'user_has_roles.role_id', '=', 'roles.id')->where('roles.id', '=', $csrrole->id)
            ->select('users.username', 'users.id')->get();

        $users = Users::all();
        $orderPaymentStatusesValues = OrderPaymentStatusTranslations::all();
        $fulfilmentStatus = OrderShippingStatusTranslations::all();
        $links = '';


        //order stats
        $now = Carbon::now();
        $weekStart = Carbon::now()->subDays($now->dayOfWeek)->setTime(0, 0);
        $yearStart = Carbon::now()->startOfYear();
        $monthStart = Carbon::now()->startOfMonth();

        $itemquantity = OrdersItems::where('created_at', '>', $yearStart)
            ->select(array('object_id', DB::raw('COUNT(item_quantity) AS count')))->groupBy('object_id')->get();
        $ordersweek = Orders::where('created_at', '>', $weekStart)->get();
        $orderemonthly = Orders::where('created_at', '>', $monthStart)->get();
        $ordersyearly = Orders::where('created_at', '>', $yearStart)->get();
        $ordersday = Orders::where('created_at', 'like', '%' . date('Y-m-d', strtotime($now)) . '%')->get();
        $dayordernew = Orders::where('created_at', 'like', '%' . date('Y-m-d', strtotime($now)) . '%')->get();
        $weekorders = Orders::where('created_at', '>', $weekStart)->get();
        $monthlyOrders = Orders::where('created_at', '>', $monthStart)->get();
        $yearlyOrders = Orders::where('created_at', '>', $yearStart)->get();
//end order stats
        return view('admin/orders-management/order', ['csrusers' => $csrusers,
            'riderusers' => $riderusers, 'result' => $searchResults, 'links' => $links,
            'orderPaymentStatusesValues' => $orderPaymentStatusesValues,
            'fulfilmentStatus' => $fulfilmentStatus, 'users' => $users,
            'search_keyword' => $search_keyword, 'order_date' => $order_date,
            'shipping_status' => $shipping_status, 'payment_status' => $payment_status,
            'tel' => $tel, 'location' => $location,
            'title' => $title,
            'searchUrl' => $searchUrl,
            'orders' => $ordersday,
            'ordersweek' => $ordersweek,
            'orderemonthly' => $orderemonthly,
            'ordersyearly' => $ordersyearly,
            'dayordernew' => $dayordernew,
            'weekorders' => $weekorders,
            'monthlyOrders' => $monthlyOrders,
            'yearlyOrders' => $yearlyOrders,
            'clearsearch' => 'clearsearch',
        ]);
    }

    public function singledelete($id)
    {

        Products::addStockBack($id,'Delete Order');
        DB::table('orders')->where('id', '=', $id)->delete();
        $msg = "Record has been deleted successfully";
        return Redirect::back()->withErrors([$msg, 'this message']);
    }

    public function multipledelete(Request $request)
    {
        $delete = $request->input('delete');
        if ($delete) {
            foreach ($delete as $del) {
                DB::table('orders')->where('id', '=', $del)->delete();
                $msg = "Records deleted ";
                return Redirect::back()->withErrors([$msg, 'this message']);
            }
        } else {
            $msg = "Please select at least one record ";
            return Redirect::back()->withErrors([$msg, 'this message']);
        }

        return redirect()->back();
    }

    public static function getDeliveryboyName($d)
    {
        $d = (int)$d;
        $deliveryboy = DB::table('users')->where('id', '=', $d)->get();
        $deliveryboy = json_decode($deliveryboy, true);
        $result[0] = $deliveryboy[0]['first_name'];
        $result[1] = $deliveryboy[0]['last_name'];

        return $result;
    }

    public function getItemsQuantityOfOrder($order_id)
    {
        $quantity = DB::table('order_items')->where('order_id', '=', $order_id)->count();
        return $quantity;
    }

    public function getOrdersCount($user_id)
    {
        $orders_count = DB::table('orders')->where('user_id', '=', $user_id)->count();
        return $orders_count;
    }

    public static function addCommission(Request $request)
    {
        $addCommission = DB::table('orders')->where('id', '=', $request->orderId)->update(['commission' => $request->value]);
        if ($addCommission) {
            // return response()->json([$request->orderId,$request->value]);
            return response()->json($addCommission);
        } else {
            return false;
        }
    }

    public function setPayStatus(Request $request)
    {
        $addPayStatus = Orders::where('id', '=', $request->orderId)->update(['payment_status_id' => $request->value]);

        if ($addPayStatus) {
            return response()->json($addPayStatus);
        } else {
            return false;
        }
    }

    public function setFulfilStatus(Request $request)
    {

        $shipping_status = $request->value;

        $addFulfilStatus = Orders::where('id', '=', $request->orderId)->update(['shipping_status_id' => $shipping_status]);
        if ($addFulfilStatus) {
            if ($shipping_status != '1') {
                if ($shipping_status != '2') {
                    $order_created_date = $request->order_date;
                    $current_date = date('Y-m-d H:i:s');

                    $datetime1 = strtotime($order_created_date);
                    $datetime2 = strtotime($current_date);
                    $interval = abs($datetime2 - $datetime1);
                    $minutess = round($interval / 60);

                    $format = '%02d:%02d';
                    $hours = floor($minutess / 60);
                    $minutes = ($minutess % 60);
                    $deliveryTime = sprintf($format, $hours, $minutes);

                    $updateDeliveryTime = Orders::where('id', '=', $request->orderId)->update(['delivery_time' => $deliveryTime]);
                }
            }
            return response()->json($addFulfilStatus);
        } else {
            return false;
        }
    }

    public function setProcessedBy(Request $request)
    {
        $addProcessedBy = Orders::where('id', '=', $request->orderId)->update(['processed_by' => $request->value]);
        if ($addProcessedBy) {
            return response()->json($addProcessedBy);
        } else {
            return false;
        }
    }

    public function setDeliveredBy(Request $request)
    {
        $addDeliveredBy = Orders::where('id', '=', $request->orderId)->update(['delivered_by' => $request->value]);
        if ($addDeliveredBy) {
            return response()->json($addDeliveredBy);
        } else {
            return false;
        }
    }

    public function getAllUsers()
    {
        $result = DB::select("SELECT * from users");
        $result = json_decode(json_encode($result), True);
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }


    public function addNewOrders(Request $request, $ordernumber)
    {

        switch ($request->submitbutton) {
            case 'save':
                $orderid = Orders::where('orderNumber', '=', $ordernumber)->pluck('id')->first();
                $products_array = $request->products;

                if ($orderid) {
                    foreach ($products_array as $pkey => $pval) {

                        $product = isset($_POST['products'][$pkey]) ? $_POST['products'][$pkey] : '';
                        $unit = isset($_POST['units'][$pkey]) ? $_POST['units'][$pkey] : '';
                        $openingStock = isset($_POST['openingStocks'][$pkey]) ? $_POST['openingStocks'][$pkey] : '';
                        $costPrice = isset($_POST['costPrices'][$pkey]) ? $_POST['costPrices'][$pkey] : '';
                        $newQuantityValue = isset($_POST['orderQuantity'][$pkey]) ? $_POST['orderQuantity'][$pkey] : '';
                        $totalPrices = isset($_POST['totalPrices'][$pkey]) ? $_POST['totalPrices'][$pkey] : '';


                        $products = new Products();
                        $productsData = $products->getProductInfo($product);
                        $productName = $productsData[0]->product_title;

                        $removeProduct = Units::where('unit','=',$unit)->where('product_id','=',$product)->pluck('product_quantity')->first();
                        $newQuantity = $removeProduct - $newQuantityValue;
                        Units::where('unit','=',$unit)->where('product_id','=',$product)->update(['product_quantity' => $newQuantity]);

                        $message = 'Stock Removed Source : '.'Add New Items'.' - Opening Stock : '.$removeProduct.' Change : '.$newQuantityValue.' New Quantity : '.$newQuantity;
                        StockLog::addLog($message,$orderid,$product,null);

                        $productData = OrdersItems::where('quantity_unit_id','=',$unit)->where('object_id', '=', $product)->where('order_id', '=', $orderid)->count();

                        if ($productData > 0) {

                            $productId = OrdersItems::where('quantity_unit_id','=',$unit)->where('order_id', '=', $orderid)->where('object_id', '=', $product)->get();

                            foreach ($productId as $prodId) {


                                $itemQuantity = $prodId->item_quantity;
                                $itemPrice = $prodId->total;

                                $orderItemsValues = array(
                                    'object_id' => $product,
                                    'name' => $productName,
                                    'price' => $costPrice,
                                    'quantity_unit_id' => $unit,
                                    'item_quantity' => $newQuantityValue + $itemQuantity,
                                    'total' => $totalPrices + $itemPrice
                                );


                                $total = Orders::where('id', '=', $orderid)->pluck('total')->first();
                                $updateTotal = $total + $totalPrices;
                                Orders::where('id', '=', $orderid)->update(['total' => $updateTotal]);

                                OrdersItems::where('quantity_unit_id', '=', $unit)->where('object_id', '=', $product)->where('order_id', '=', $orderid)->update($orderItemsValues);
                                Log::addLog($productName . ' is added to order (Quantity = ' . $newQuantityValue . ')', $orderid);
                            }
                        } else {

                            $orderItemsValues = array(
                                'order_id' => $orderid,
                                'object_id' => $product,
                                'name' => $productName,
                                'unit' => $unit,
                                'price' => $costPrice,
                                'quantity_unit_id' => $unit,
                                'item_quantity' => $newQuantityValue,
                                'total' => $totalPrices,
                                'updated_at' => date('Y-m-d H:i:s'),
                            );

                            $total = Orders::where('id', '=', $orderid)->pluck('total')->first();
                            $updateTotal = $total + $totalPrices;
                            Orders::where('id', '=', $orderid)->update(['total' => $updateTotal]);

                            $ordersItems = new OrdersItems();
                            $lastOrderItemsId = $ordersItems->addOrderItems($orderItemsValues);
                            Log::addLog($productName . ' is added to order (Quantity = ' . $newQuantityValue . ')', $orderid);

                        }
                    }


                }
                return redirect()->back();

                break;
            case 'Send Email':

                $orderNumber = $request->orderNumber;
                UtilityHelper::sendOrderMail($orderNumber);
                return redirect()->back();
                break;


        }


    }


    public function deleteOrders($id)
    {

        $orderItem = OrdersItems::where('id', '=', $id)->select('object_id', 'item_quantity', 'name', 'quantity_unit_id', 'total', 'order_id')->first();
        $order = Orders::where('id', '=', $orderItem->order_id)->select('total','shipping_status_id')->first();

        $ordertotal = $order->total - $orderItem->total;

        $orderupdate = Orders::where('id', '=', $orderItem->order_id)->update(['total' => $ordertotal]);
        $product = Units::where('unit', '=', $orderItem->quantity_unit_id)->where('product_id', '=', $orderItem->object_id)->first();
        $quantity = $product->product_quantity;
        if($order->shipping_status_id == 2 || $order->shipping_status_id == 4)
        {
            Units::where('unit', '=', $orderItem->quantity_unit_id)->where('product_id', '=', $orderItem->object_id)
                ->update(['product_quantity'=>$quantity + $orderItem->item_quantity]);

        }
        $delete = OrdersItems::where('id', '=', $id)->delete();
        Log::addLog($orderItem->name . ' is Deleted From Order', $orderItem->order_id);
        if($order->shipping_status_id == 2 || $order->shipping_status_id == 4)
        {
        $message = 'Stock Added Source : '.'Order Item Deleted'.' - Quantity : '.$quantity;
        StockLog::addLog($message,null,$orderItem->object_id,null);
        }
        $itemCount=OrdersItems::where('id', '=', $id)->count();

        if($itemCount > 0){

        return redirect()->back();

        }
    }

    public function setAddress(Request $request)
    {
        $id = $request->customerId;
        $orderId = $request->orderId;

        $address = $request->address;
        $name = $request->name;
        $phoneno = $request->phoneno;
        $customerEmail = $request->customerEmail;

        $user = Orders::where('id', '=', $orderId)->first();
//        ->pluck('user_id')
        if($user->address !='' && $user->phoneno !='' && $user->username != '' )
        $user = Orders::where('id', '=', $orderId)->update(['address' => $address, 'phoneno' => $phoneno, 'username' => $name]);

        elseif($user->user_id != '') {
            $users = Users::where('id', '=', $id)->update(['address' => $address, 'cell_no' => $phoneno]);
        } else {
            $customer = Customers::where('id', '=', $id)->update(['address' => $address, 'phoneno' => $phoneno, 'email' => $customerEmail, 'username' => $name]);

        }

        return redirect()->back();

    }

    public function customerDetail()
    {
        $orders = Orders::all();
        $customer = Customers::paginate(15);
        return view('admin/customer-management/customer-detail', ['usersData' => $customer, 'orders' => $orders]);
    }

    public function Deletecustomer($id)
    {
        Customers::where('id', '=', $id)->delete();

        return Redirect::back()->withErrors(['Record Deleted', 'this message']);

    }

    public function searchcustomer(Request $request)
    {

        $keyword = $request->keyword;
        $orders = Orders::all();

        if ($keyword != '') {
            $searchQuery = Customers::Where('username', 'LIKE', '%' . $keyword . '%')->orWhere('email', 'LIKE', '%' . $keyword . '%')
                ->orWhere('phoneno', 'LIKE', '%' . $keyword . '%')
                ->orWhere('address', 'LIKE', '%' . $keyword . '%')->paginate(15);
        } else {
            $searchQuery = Customers::paginate(15);
        }


        return view('admin/customer-management/customer-detail', ['usersData' => $searchQuery, 'orders' => $orders]);
    }

    public function staffNote(Request $request)
    {
        $stafNote = $request->stafNote;
        $id = $request->id;
        $upd = Orders::where('id', '=', $id)->update(['staff_note' => $stafNote]);

    }

    public function deleteOrExportorders(Request $request)
    {

        switch ($request->submitbutton) {


            case 'delete':

                $delete = $request->input('delete');

                if ($delete) {
                    foreach ($delete as $del) {
                        $del = Orders::where('id', '=', $del)->delete();


                    }

                } else {
                    $msg = "please select at least one record ";
                    return Redirect::back()->withErrors([$msg, 'this message']);
                }
                return redirect()->back();
                break;
            case 'excel':
                $excel = $request->input('delete');
                if (is_array($excel)) {
                    $orderItem = array();
                    foreach ($excel as $del) {
                        $orderItem[] = OrdersItems::join('orders', 'order_items.order_id', '=', 'orders.id')
                            ->leftjoin('customers', 'orders.customer_id', '=', 'customers.id')
                            ->join('order_shipping_status_translations', 'orders.shipping_status_id', 'order_shipping_status_translations.id')
                            ->join('order_payment_status_translations', 'orders.payment_status_id', 'order_payment_status_translations.id')
                            ->where('orders.id', '=', $del)->select('order_payment_status_translations.name AS payment_name',
                                'order_shipping_status_translations.name AS shipping_status_name',
                                'order_items.price AS item_price', 'order_items.name AS item_name', 'order_items.total AS order_item_total',
                                'order_items.discountedSubtotal AS order_item_discount_price','order_items.discount AS order_item_discount',
                                'order_items.quantity_unit_id AS quantity_unit',
                                'order_items.item_quantity AS item_quantity', 'customers.email AS customers_email',
                                'customers.username AS customers_username', 'customers.address AS customers_address', 'customers.phoneno AS phoneno',
                                'orders.user_id AS userID', 'orders.orderNumber AS orderNumber', 'orders.total AS ordertotal',
                                'orders.discount AS orderDiscount','orders.discountedAmount AS orderDiscountTotal',
                                'orders.created_at AS orderdate', 'orders.staff_note AS staff_note',
                                'orders.username as orderUsername','orders.address as orderAddress','orders.phoneno as orderPhoneno',
                                'orders.delivered_by AS delivered_by', 'orders.processed_by AS processed_by')
                            ->get();

                    }
                    Excel::create('OrdersDetail', function ($excel) use ($orderItem) {

                        $excel->setTitle('Order Detail');
                        $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
                        $excel->setDescription('Order Detail file');


                        $excel->sheet('sheet1', function ($sheet) use ($orderItem) {
                            $sheet->loadView('OrderDetailexcel')->with('data', $orderItem);
                        });

                    })->download('xls');

                } else {
                    return redirect()->back();
                }

                break;
                case 'csv':
                $excel = $request->input('delete');
                if (is_array($excel)) {
                    $orderItem = array();
                    foreach ($excel as $del) {
                        $orderItem[] = OrdersItems::join('orders', 'order_items.order_id', '=', 'orders.id')
                            ->leftjoin('customers', 'orders.customer_id', '=', 'customers.id')
                            ->join('order_shipping_status_translations', 'orders.shipping_status_id', 'order_shipping_status_translations.id')
                            ->join('order_payment_status_translations', 'orders.payment_status_id', 'order_payment_status_translations.id')
                            ->where('orders.id', '=', $del)->select('order_payment_status_translations.name AS payment_name',
                                'order_shipping_status_translations.name AS shipping_status_name',
                                'order_items.price AS item_price', 'order_items.name AS item_name', 'order_items.total AS order_item_total',
                                'order_items.discountedSubtotal AS order_item_discount_price','order_items.discount AS order_item_discount',
                                'order_items.quantity_unit_id AS quantity_unit',
                                'order_items.item_quantity AS item_quantity', 'customers.email AS customers_email',
                                'customers.username AS customers_username', 'customers.address AS customers_address', 'customers.phoneno AS phoneno',
                                'orders.user_id AS userID', 'orders.orderNumber AS orderNumber', 'orders.total AS ordertotal',
                                'orders.discount AS orderDiscount','orders.discountedAmount AS orderDiscountTotal',
                                'orders.created_at AS orderdate', 'orders.staff_note AS staff_note',
                                'orders.username as orderUsername','orders.address as orderAddress','orders.phoneno as orderPhoneno',
                                'orders.delivered_by AS delivered_by', 'orders.processed_by AS processed_by')
                            ->get();

                    }
                    Excel::create('OrdersDetail', function ($excel) use ($orderItem) {

                        $excel->setTitle('Order Detail');
                        $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
                        $excel->setDescription('Order Detail file');


                        $excel->sheet('sheet1', function ($sheet) use ($orderItem) {
                            $sheet->loadView('OrderDetailcsv')->with('data', $orderItem);
                        });

                    })->download('csv');

                } else {
                    return redirect()->back();
                }

                break;
            case 'Update':

                $orderid = $request->input('delete');

                $locationId = $request->locationId;

                if ($locationId) {
                    if ($orderid) {

                        foreach ($orderid as $id) {
                            $data = Orders::where('id', '=', $id)->update(['location_id' => $locationId]);
                        }
                    }
                    return redirect()->back();

                } else {
                    return redirect()->back();
                }
                break;

            case 'csrReport':
                $excel = $request->input('delete');
                if (is_array($excel)) {
                    $orderItem = array();
                    foreach ($excel as $id) {

//                        $orderItem[]=OrdersItems::where('order_id','=',$del)->get();


                        $orderItem[] = Orders::where('id', '=', $id)->get();

                    }
//                    dd($orderItem);

                    Excel::create('OrdersDetail', function ($excel) use ($orderItem) {

                        $excel->setTitle('Order Detail');
                        $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
                        $excel->setDescription('Order Detail file');


                        $excel->sheet('sheet1', function ($sheet) use ($orderItem) {
                            $sheet->loadView('CSRSupervioserReport')->with('data', $orderItem);
                        });

                    })->download('csv');

                } else {
                    return redirect()->back();
                }

                break;
        }

    }

    public function DeleteMultiplecustomer(Request $request)
    {

        $delete = $request->input('delete');
        if ($delete) {
            foreach ($delete as $del) {
                Customers::where('id', '=', $del)->delete();

            }
        } else {
            $msg = "please select at least one record ";
            return Redirect::back()->withErrors([$msg, 'this message']);
        }
        return Redirect::back()->withErrors(['Record Deleted', 'this message']);

    }

    public function userOrders(Request $request, $id)
    {
        $userId = Orders::where('id', '=', $id)->pluck('user_id')->first();

        if ($userId) {
            $userOrder = Orders::where('user_id', $userId)->get();
            $customerFirstOrder = Orders::where('user_id', $userId)->select('created_at')->first();
            $customerLastOrder = Orders::where('user_id', $userId)->orderBy('id','DESC')->select('created_at')->first();
            $userData = Users::where('id',$userId)->select('username AS name','email AS email','cell_no AS cellno','address AS address')->first();
        } else {
            $userId = Orders::where('id', '=', $id)->pluck('customer_id')->first();
            $userOrder = Orders::where('customer_id', $userId)->get();
            $customerFirstOrder = Orders::where('customer_id', $userId)->select('created_at')->first();
            $customerLastOrder = Orders::where('user_id', $userId)->orderBy('id','DESC')->select('created_at')->first();
            $userData = Customers::where('id',$userId)->select('username AS name','email AS email','phoneno AS cellno','address AS address')->first();
        }
        return view('admin/orders-management/users-orders', ['userOrder' => $userOrder,'FirstOrderDate'=>$customerFirstOrder,'lastOrderDate'=>$customerLastOrder,'userData'=>$userData]);
    }

    public function addDiscount(Request $request)
    {
        $orderId = $request->orderId;
        $discount = $request->discount;

        $totalAmount = Orders::where('id', $orderId)->pluck('total')->first();
        $toalDiscount = $totalAmount * $discount / 100;
        $discountedAmount = $totalAmount - $toalDiscount;
        $data = Orders::where('id', $orderId)->update(['discount' => $discount, 'discountedAmount' => $discountedAmount]);
        Log::addLog($discount.'% Discount on order is added', $orderId);
        return response($data);
    }

    public function productdiscount(Request $request)
    {
        $orderItemId = $request->order_item;

        $discount = $request->discount;

        $totalAmount = OrderItems::where('id', $orderItemId)->first();
       $productName= $totalAmount->name;
        $toalDiscount = $totalAmount->total * $discount / 100;

        $discountedAmount = $totalAmount->total - $toalDiscount;

        $update = OrderItems::where('id', $orderItemId)->update(['discount' => $discount, 'discountedSubtotal' => $discountedAmount]);
        $data = OrderItems::where('id', $orderItemId)->get();
        if ($update) {
            $orders = OrderItems::where('order_id', $request->orderId)->get();
            $totalAmount = 0;
            foreach ($orders as $order) {
                if ($order->discountedSubtotal == null) {
                    $totalAmount += $order->total;
                } else {
                    $totalAmount += $order->discountedSubtotal;
                }
            }

        }

        Orders::where('id', $request->orderId)->update(['discountedAmount' => $totalAmount]);
        Log::addLog($discount.'% Discount is added on '.$productName, $request->orderId);
        return response($data);
    }
}