<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Admin\Users;
class PermissionController extends Controller
{
    public function permissions(Request $request){
        $roles=Role::all();
        $permission=Permission::all();
        return view('admin/users-access-module/permissions',['roles' => $roles,'permissionData' => $permission,]);
    }
    public  function addPermissions(Request $request){

        $roles=$request->role;

        $create=Permission::create([
            'name'=>$request->Name,
            'title'=>$request->Title,
            'description'=>$request->Description
        ]);

        $permission_id=$create->id;
//        foreach ($role as $role_id){
//            $rolePermission=RolePermission::create([
//                'permission_id'=>$permission_id,
//                'role_id'=>$role_id,
//            ]);
//
//        }
        if (!empty($request['role'])) {
            foreach ($roles as $role) {
                $r = Role::where('id', '=', $role)->firstOrFail(); //Match input role to db record

                $permission = Permission::where('id', '=', $permission_id)->first();
                $r->givePermissionTo($permission);
            }
        }
        return Redirect::to('admin/users-access-module/permissions');
    }
    public function editPermissions(Request $request){
        $id=$request->id;
        $roles=Role::all();
        $rolesSelected=Role::Join('role_has_permissions','roles.id','=','role_has_permissions.role_id')
            ->where('role_has_permissions.permission_id',$id)->get();
        //SELECT * FROM `roles` LEFT join role_has_permissions on roles.id = role_has_permissions.role_id  WHERE role_has_permissions.permission_id = '1'

//echo $rolesSelected;

        $permission=Permission::find($id);




        return view('admin/users-access-module/editPermission',['permission'=>$permission,'rolesSelected'=>$rolesSelected,'roles'=>$roles,]);
    }
    public function updatePermissions(Request $request){
        $id=$request->Id;
//        dd($role);

        $create=Permission::where('id',$id)->update([
            'name'=>$request->Name,
            'title'=>$request->Title,
            'description'=>$request->Description
        ]);
        $permission=Permission::find($id);
        $roleId=$request->role;
        $roleAll=Role::all();
        $permissionName=$permission->name;

        $roleString="";
        foreach($roleAll as $currentRoleAll){///remove role to permission
            $roleAllSelected=Role::findByName($currentRoleAll->name);
            if($roleAllSelected->hasPermissionTo($permissionName)) {
                $roleAllSelected->revokePermissionTo($permissionName);
            }
        }
        if($roleId != ''){
        foreach($roleId as $roleList){//// assign role to permission
            $role=Role::find($roleList);
            $roleString.=$role->id."/";
            $role->givePermissionTo($permissionName);
        }
        }
        return Redirect::to('admin/users-access-module/permissions');
    }
    public function deletePermissions(Request $request,$id){
        $create=Permission::where('id',$id)->delete();
        return Redirect::to('admin/users-access-module/permissions');
    }
    public function getPermissions(Request $request){
        $all_permissions=Permission::all();
        $role=Role::join('role_has_permissions', 'roles.id','=','role_has_permissions.role_id')
            ->where('roles.id','=',$request->role_id)
            ->get();

        $roleName=Role::find($request->role_id);
        return view('admin.users-access-module.loadPermission',['all_permissions'=>$all_permissions,'role'=>$role,'roleName'=>$roleName]);
    }
    public function getUserPermissions(Request $request){
        $id=$request->user;
        $all_permissions=Permission::all();
        $users=Users::where('id',$id)
            ->first();
        $userPermissions = $users->getAllPermissions()->pluck('id');
        return view('admin/users-access-module/loadUserPermission',['all_permissions'=>$all_permissions,'userPermissions'=>$userPermissions->toArray(),'user'=>$users]);
    }
    public function assignPermissionsToRole(Request $request){

        $action=$request->checkbox;

        $role=$request->role_id;
        $permission_id=$request->permission_id;
        $rolePermission=Role::where('id','=',$role)->firstOrFail();
        $permission = Permission::where('id', '=', $permission_id)->first();

        if($action=='checked'){

            $rolePermission->givePermissionTo($permission);
        }
        elseif ($action=='unchecked'){
            $rolePermission->revokePermissionTo($permission);
        }
        echo $rolePermission;

    }
   public function assignPermissionsToUser(Request $request){

        $action=$request->checkbox;

        $user=$request->user_id;
        $permission_id=$request->permission_id;
        $userPermission=Users::where('id','=',$user)->firstOrFail();
        $permission = Permission::where('id', '=', $permission_id)->first();

        if($action=='checked'){

            $userPermission->givePermissionTo($permission);
        }
        elseif ($action=='unchecked'){
            $userPermission->revokePermissionTo($permission);
        }
        echo $userPermission;

    }

}
