<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;

use App\Models\Admin\OrderShippingStatusTranslations;
use App\Models\Admin\OrderPaymentStatusTranslations;

class OrdersStatusesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $orderpaymenttatuses = OrderPaymentStatusTranslations::all();
        $ordershipmentstatuses = OrderShippingStatusTranslations::all();

        return view('admin/orders-management/orders-statuses',['orderpaymentstatuses'=>$orderpaymenttatuses,'ordershipmentstatuses'=>$ordershipmentstatuses]);
    }

    public function add()
    {
        return view('admin/orders-management/add-order-status');
    }

    public function update($id)
    {
        $ordersatus=OrderPaymentStatusTranslations::find($id);


        return view('admin/orders-management/update-order-payment-status',['orderpaymentstatuses'=>$ordersatus]);
    }
    public function shipmentupdate($id)
    {
        $ordershipment=OrderShippingStatusTranslations::find($id);


        return view('admin/orders-management/update-order-shipment-status',['ordershipmentstatuses'=>$ordershipment]);
    }

    public function addStatus(Request $request)
    {
        $statusType = $request->type;

        $statusValues = array(
            'name' => $request->name
        );

        if($statusType == 'P')
        {
            $ordersPaymentStatus = new OrderPaymentStatusTranslations();

            if($lastInsertId = $ordersPaymentStatus->addStatus($statusValues))
            {
                $msg = 'Payment Status added successfully.';

                return Redirect::to('/admin/orders-management/order-statuses')->with('status',$msg);
                exit();
            }
            else
            {
                $msg = 'Error: Payment Status not added.';

                return Redirect::to('/admin/orders-management/add-order-status')->with('status',$msg);
                exit();
            }
        }
        else
        {
            $ordersShipmentStatus = new OrderShippingStatusTranslations();

            if($lastInsertId = $ordersShipmentStatus->addStatus($statusValues))
            {
                $msg = 'Shipping Status added successfully.';

                return Redirect::to('/admin/orders-management/order-statuses')->with('status',$msg);
                exit();
            }
            else
            {
                $msg = 'Error: Shipping Status not added.';

                return Redirect::to('/admin/orders-management/add-order-status')->with('status',$msg);
                exit();
            }
        }
    }

    public function updateOrdershipmentStatus(Request $request , $id){
        $statusType = $request->type;

        $statusValues = array(
            'name' => $request->name
        );

            $ordersShipmentStatus = OrderShippingStatusTranslations::where('id', $id)->update($statusValues);

            if($ordersShipmentStatus)
            {
                $msg = 'Shipping Status updated successfully.';

                return Redirect::to('/admin/orders-management/order-statuses')->with('status',$msg);
                exit();
            }
            else
            {
                $msg = 'Error: Shipping Status not updated.';

                return Redirect::to('/admin/orders-management/order-statuses')->with('status',$msg);
                exit();
            }
        }

    public function updatePaymentStatus(Request $request , $id)
    {
        $statusType = $request->type;

        $statusValues = array(
            'name' => $request->name
        );



            $ordersPaymentStatus =OrderPaymentStatusTranslations::where('id',$id)->update($statusValues);
//            $ordersPaymentStatus = new OrderPaymentStatusTranslations();

            if( $ordersPaymentStatus)
            {
                $msg = 'Payment Status updated successfully.';

                return Redirect::to('/admin/orders-management/order-statuses')->with('status',$msg);
                exit();
            }
            else
            {
                $msg = 'Error: Payment Status not updated.';

                return Redirect::to('/admin/orders-management/order-statuses')->with('status',$msg);
                exit();
            }
        }


    public function deletepayment($id)
    {
        OrderPaymentStatusTranslations::where('id',$id)->delete();
        $msg="Record is deleted";
        return Redirect::back()->withErrors([$msg,'this message']);
    }
    public function deleteshipment($id)
    {
        OrderShippingStatusTranslations::where('id',$id)->delete();
        $msg="Record is deleted";
        return Redirect::back()->withErrors([$msg,'this message']);
    }

    public static function  getPaymentStatusesValues(){
        $result =   OrderPaymentStatusTranslations::select("*");
        $result = json_decode(json_encode($result), True);
        if($result){
            return $result;
        }
        else{
            return false;
        }
    }

    public static function  getFulfilmentStatusesValues(){
        $result =   OrderShippingStatusTranslations::select("*");
        $result = json_decode(json_encode($result), True);
        if($result){
            return $result;
        }
        else{
            return false;
        }
    }
}