<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\ProductsRequest;
use App\Models\Admin\MarketingNewLaunchSlider;
use App\Models\Categories;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Models\Admin\MarketingCategoryBanners;
use App\Models\Admin\MarketingNewLaunchImages;
use Auth;

class MarketingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function newLaunch()
    {
//        $newLaunchBanners = MarketingNewLaunchSliders::all();
        $newLaunchSlider = MarketingNewLaunchSlider::all();
        $newLaunchImages = MarketingNewLaunchImages::all();

        return view('/admin/marketing/new-launch', ['newLaunchSlider' => $newLaunchSlider, 'newLaunchImages' => $newLaunchImages]);
    }

    public function addSlider()
    {
        return view('admin/marketing/add-new-launch-slider');
    }

    public function addSliderEntry(Request $request)
    {
        $this->validate($request,[
            'ImageTitle' => 'required',
            'slider_image' => 'required',
            ]);
        $file = $request->file('slider_image');

        $name = time() . $file->getClientOriginalName();
        $destinationPath = public_path('images/SliderImages');
        $file->move($destinationPath, $name);
        $sliderValues = array(

            'title' => $request->ImageTitle,
            'image' => $name,
            'date_added' => date('Y-m-d H:i:s'),
            'status' => $request->status,
        );
        $addslider = new MarketingNewLaunchSlider();
        $lastid = $addslider->addSlider($sliderValues);
        $msg = 'Record added successfully.';
        return Redirect::to('/admin/marketing/new-launch-premier')->with('status',$msg);;
    }

    public function updateSliderEntry(Request $request, $id)
    {
        $sliderData = MarketingNewLaunchSlider::where('id', $id)->first();

        return view('admin/marketing/update-new-launch-slider', compact('sliderData'));
    }

    public function updateSlider(Request $request)
    {

        $this->validate($request,[
            'ImageTitle' => 'required',
        ]);
        $id = $request->sliderId;
        $file = $request->file('slider_image');
        if ($file != '') {
            $name = time() . $file->getClientOriginalName();
            $destinationPath = public_path('images/SliderImages');
            $file->move($destinationPath, $name);
            $sliderValues = array(

                'title' => $request->ImageTitle,
                'image' => $name,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $request->status,
            );
        } else {
            $sliderValues = array(

                'title' => $request->ImageTitle,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $request->status,
            );
        }
        $uodate = MarketingNewLaunchSlider::where('id', $id)->update($sliderValues);


        return Redirect::to('/admin/marketing/new-launch-premier')->with('status','Record updated successfully.');
    }

    public function deleteSliderEntry(Request $request, $id)
    {

        $upd = MarketingNewLaunchSlider::where('id', $id)->delete();

        $msg = "Record Deleted";
        return Redirect::back()->withErrors([$msg, 'this message']);

    }

    public function statusEnable(Request $request, $id)
    {

        $upd = MarketingNewLaunchSlider::where('id', $id)->update(array('status' => '1'));
        return redirect()->back();
    }

    public function statusDisable(Request $request, $id)
    {

        $upd = MarketingNewLaunchSlider::where('id', $id)->update(array('status' => '0'));
        return redirect()->back();

    }


    public function categoryBanners()
    {
        $categoryBanners = MarketingCategoryBanners::all();
        $categories = Categories::all();

        return view('/admin/marketing/category-banners', ['categoryBanners' => $categoryBanners, 'categories' => $categories]);
    }

    public function addBanners()
    {
        $categories = Categories::all();

        return view('admin/marketing/add-banners', ['categories' => $categories]);
    }

//    public function addBannersEntry(Request $request)
//    {
//        $inventoryDate_array = explode("/", $request->purchase_date);//03/22/2018
//        $inventoryDate = $inventoryDate_array[2] . "-" . $inventoryDate_array[0] . "-" . $inventoryDate_array[1];
//        $invoiceNo = $request->invoice_no;
//
//        $products_array = $request->products;
//
//        $inventoryValues = array(
//            'purchase_date' => $inventoryDate,
//            'invoice_no' => $invoiceNo,
//            'added_by' => Auth::User()->id,
//            'added_on' => date('Y-m-d H:i:s')
//        );
//
//        $inventory = new Inventory();
//        $lastInventoryId = $inventory->addInventory($inventoryValues);
//
//        if ($lastInventoryId) {
//            foreach ($products_array as $pkey => $pval) {
//                $product = isset($_POST['products'][$pkey]) ? $_POST['products'][$pkey] : '';
//                $unit = isset($_POST['units'][$pkey]) ? $_POST['units'][$pkey] : '';
//                $openingStock = isset($_POST['openingStocks'][$pkey]) ? $_POST['openingStocks'][$pkey] : '';
//                $quantity = isset($_POST['quantities'][$pkey]) ? $_POST['quantities'][$pkey] : '';
//                $totalStock = isset($_POST['totalStocks'][$pkey]) ? $_POST['totalStocks'][$pkey] : '';
//                $costPrice = isset($_POST['costPrices'][$pkey]) ? $_POST['costPrices'][$pkey] : '';
//                $profit = isset($_POST['profits'][$pkey]) ? $_POST['profits'][$pkey] : '';
//                $salePrice = isset($_POST['salePrices'][$pkey]) ? $_POST['salePrices'][$pkey] : '';
//                $foc = isset($_POST['focs'][$pkey]) ? $_POST['focs'][$pkey] : '';
//
//                $inventoryItemsValues = array(
//                    'inventory_id' => $lastInventoryId,
//                    'product_id' => $product,
//                    'unit' => $unit,
//                    'opening_stock' => $openingStock,
//                    'quantity' => $quantity,
//                    'total_stock' => $totalStock,
//                    'unit_price' => $costPrice,
//                    'profit' => $profit,
//                    'sale_price' => $salePrice,
//                    'foc' => $foc
//                );
//
//                $inventoryItems = new InventoryItems();
//                $lastInventoryItemsId = $inventoryItems->addInventoryItems($inventoryItemsValues);
//
//                if ($lastInventoryItemsId) {
//                    //---------- Update Product Values ----------------
//                    $productsValues = array(
//                        'quantity_in_stock' => $totalStock,
//                        'marketPrice' => $salePrice
//                    );
//
//                    $products = new Products();
//                    $updateProduct = $products->updateProduct($product, $productsValues);
//                    //-------------------------------------------------
//                }
//            }
//        }
//
//        $productsData = Products::all();
//        return redirect('admin/stock-management/add-purchase')->with(['statusClass' => 'info', 'status' => 'Purchase has been added successfully.'], compact('productsData'));
//        exit;
//    }

    public function addBannersEntryNew(Request $request)
    {
        $this->validate($request,[
            'horizontal_banner' => 'required',
            'vertical_banner' => 'required',
            'category_id' => 'required',
            'vertical_banner_position' => 'required',
        ]);
        $file = $request->file('horizontal_banner');
        $horizontal_banner = time() . $file->getClientOriginalName();
        $destinationPath = public_path('images/categoryBanner');
        $file->move($destinationPath, $horizontal_banner);

        $file = $request->file('vertical_banner');
        $vertical_banner = time() . $file->getClientOriginalName();
        $destinationPath = public_path('images/categoryBanner');
        $file->move($destinationPath, $vertical_banner);

        $bannerValue = array(
            'category_id' => $request->category_id,
            'horizontal_banner' => $horizontal_banner,
            'horizontal_banner_status' => $request->horizontal_banner_status,
            'vertical_banner' => $vertical_banner,
            'vertical_banner_status' => $request->vertical_banner_status,
            'added_on' => date('Y-m-d H:i:s'),
            'vertical_banner_position' => $request->vertical_banner_position,

        );
        $banner = new MarketingCategoryBanners();
        $lastinsertid = $banner->addBanners($bannerValue);
        return Redirect::to('admin/marketing/category-banners')->with('status','Record added successfully.');;
    }

    public function horizentalStatusupdateEnable(Request $request, $id)
    {

        $upd = MarketingCategoryBanners::where('id', $id)->update(array('horizontal_banner_status' => '1'));
        return redirect()->back();
    }

    public function horizentalStatusupdateDisable(Request $request, $id)
    {

        $upd = MarketingCategoryBanners::where('id', $id)->update(array('horizontal_banner_status' => '0'));
        return redirect()->back();

    }

    public function verticleStatusupdateEnable(Request $request, $id)
    {

        $upd = MarketingCategoryBanners::where('id', $id)->update(array('vertical_banner_status' => '1'));
        return redirect()->back();
    }

    public function verticleStatusupdateDisable(Request $request, $id)
    {

        $upd = MarketingCategoryBanners::where('id', $id)->update(array('vertical_banner_status' => '0'));
        return redirect()->back();

    }

    public function DeleteBanners(Request $request, $id)
    {

        $upd = MarketingCategoryBanners::where('id', $id)->delete();

        $msg = "Record Deleted";
        return Redirect::back()->withErrors([$msg, 'this message']);

    }

    public function UpdateBanners(Request $request, $id)
    {
        $categorybanner = MarketingCategoryBanners::where('id', $id)->first();
        $categories = Categories::all();
        return view('/admin/marketing/update_category_banner', ['categorybanner' => $categorybanner, 'categories' => $categories]);

    }

    public function UpdateCategoryBanners(Request $request)
    {

        $id = $request->bannerid;


        $horizontalfile = $request->file('horizontal_banner');
        if ($horizontalfile != '') {
            $horizontal_banner = time() . $horizontalfile->getClientOriginalName();
            $destinationPath = public_path('images/categoryBanner');
            $horizontalfile->move($destinationPath, $horizontal_banner);
//            $bannerValue =['horizontal_banner' => $horizontal_banner,];
        } else {
            $horizontal_banner = MarketingCategoryBanners::where('id', $id)->pluck('horizontal_banner')->first();

        }
        $file = $request->file('vertical_banner');
        if ($file != '') {
            $vertical_banner = time() . $file->getClientOriginalName();
            $destinationPath = public_path('images/categoryBanner');
            $file->move($destinationPath, $vertical_banner);

//            $bannerValue =[];

        } else {
            $vertical_banner = MarketingCategoryBanners::where('id', $id)->pluck('vertical_banner')->first();

        }
        $bannerValue = array(
            'category_id' => $request->category_id,
            'horizontal_banner' => $horizontal_banner,
            'horizontal_banner_status' => $request->horizontal_banner_status,
            'vertical_banner' => $vertical_banner,
            'vertical_banner_status' => $request->vertical_banner_status,
            'vertical_banner_position' => $request->vertical_banner_position,
            'updated_on' => date('Y-m-d H:i:s'),

        );

        $upd = MarketingCategoryBanners::where('id', $id)->update($bannerValue);

        return Redirect::to('admin/marketing/category-banners')->with('status','Record updated successfully.');;

    }


    public function addImages()
    {
        return view('admin/marketing/add-new-launch-premier-images');
    }

    public function addImagesEntry(Request $request)
    {
        $this->validate($request,[
            'image1' => 'required',
            'image2' => 'required',
            'image3' => 'required',
            'image4' => 'required',
        ]);
        $file1 = $request->file('image1');
        $name1 = time() . $file1->getClientOriginalName();
        $destinationPath = public_path('images/PremierImages');
        $file1->move($destinationPath, $name1);


        $file2 = $request->file('image2');
        $name2 = time() . $file2->getClientOriginalName();
        $destinationPath = public_path('images/PremierImages');
        $file2->move($destinationPath, $name2);

        $file3 = $request->file('image3');
        $name3 = time() . $file3->getClientOriginalName();
        $destinationPath = public_path('images/PremierImages');
        $file3->move($destinationPath, $name3);

        $file4 = $request->file('image4');
        $name4 = time() . $file4->getClientOriginalName();
        $destinationPath = public_path('images/PremierImages');
        $file4->move($destinationPath, $name4);


        $imageValues = array(

            'image_1' => $name1,
            'image_1_status' => $request->status1,
            'image_2' => $name2,
            'image_2_status' => $request->status2,
            'image_3' => $name3,
            'image_3_status' => $request->status3,
            'image_4' => $name4,
            'image_4_status' => $request->status4,
            'added_on' => date('Y-m-d H:i:s'),
        );
        $images = new MarketingNewLaunchImages();
        $lastid = $images->addImages($imageValues);
        return Redirect::to('/admin/marketing/new-launch-premier')->with('status','Record added successfully.');;
    }

    public function updateImages(Request $request, $id)
    {
        $imagesData = MarketingNewLaunchImages::where('id', $id)->first();

        return view('admin/marketing/update-premier-images', compact('imagesData'));
    }

    public function updateImagesPremier(Request $request)
    {
        $id = $request->imagesId;

        $file1 = $request->file('image1');
        if ($file1 != '') {
            $name1 = time() . $file1->getClientOriginalName();
            $destinationPath = public_path('images/PremierImages');
            $file1->move($destinationPath, $name1);
        } else {
            $name1 = MarketingNewLaunchImages::where('id', $id)->pluck('image_1')->first();
        }

        $file2 = $request->file('image2');
        if ($file2 != '') {
            $name2 = time() . $file2->getClientOriginalName();
            $destinationPath = public_path('images/PremierImages');
            $file2->move($destinationPath, $name2);
        } else {
            $name2 = MarketingNewLaunchImages::where('id', $id)->pluck('image_2')->first();
        }
        $file3 = $request->file('image3');
        if ($file3 != '') {
            $name3 = time() . $file3->getClientOriginalName();
            $destinationPath = public_path('images/PremierImages');
            $file3->move($destinationPath, $name3);
        } else {
            $name3 = MarketingNewLaunchImages::where('id', $id)->pluck('image_3')->first();
        }

        $file4 = $request->file('image4');
        if ($file4 != '') {
            $name4 = time() . $file4->getClientOriginalName();
            $destinationPath = public_path('images/PremierImages');
            $file4->move($destinationPath, $name4);
        } else {
            $name4 = MarketingNewLaunchImages::where('id', $id)->pluck('image_4')->first();
        }
        $imageValues = array(

            'image_1' => $name1,
            'image_1_status' => $request->status1,
            'image_2' => $name2,
            'image_2_status' => $request->status2,
            'image_3' => $name3,
            'image_3_status' => $request->status3,
            'image_4' => $name4,
            'image_4_status' => $request->status4,
            'updated_on' => date('Y-m-d H:i:s'),
        );
        $update = MarketingNewLaunchImages::where('id', $id)->update($imageValues);
        return Redirect::to('/admin/marketing/new-launch-premier')->with('status','Record updated successfully.');;
    }

    public function deleteImages(Request $request, $id)
    {
        MarketingNewLaunchImages::where('id', $id)->delete();
        $msg = "Record Deleted";
        return Redirect::back()->withErrors([$msg, 'this message']);
    }

    public function statusEnableImage1(Request $request, $id)
    {
        $upd = MarketingNewLaunchImages::where('id', $id)->update(array('image_1_status' => '1'));
        return redirect()->back();
    }

    public function statusDisableImage1(Request $request, $id)
    {
        $upd = MarketingNewLaunchImages::where('id', $id)->update(array('image_1_status' => '0'));
        return redirect()->back();
    }

        public function statusEnableImage2(Request $request ,$id){
            $upd = MarketingNewLaunchImages::where('id', $id)->update(array('image_2_status' => '1'));
            return redirect()->back();
    }
    public function statusDisableImage2(Request $request ,$id){

        $upd = MarketingNewLaunchImages::where('id', $id)->update(array('image_2_status' => '0'));
        return redirect()->back();
    }
    public function statusEnableImage3(Request $request ,$id){
        $upd = MarketingNewLaunchImages::where('id', $id)->update(array('image_3_status' => '1'));
        return redirect()->back();
    }
    public function statusDisableImage3(Request $request ,$id){

        $upd = MarketingNewLaunchImages::where('id', $id)->update(array('image_3_status' => '0'));
        return redirect()->back();
    }
    public function statusEnableImage4(Request $request ,$id){
        $upd = MarketingNewLaunchImages::where('id', $id)->update(array('image_4_status' => '1'));
        return redirect()->back();
    }
    public function statusDisableImage4(Request $request ,$id){

        $upd = MarketingNewLaunchImages::where('id', $id)->update(array('image_4_status' => '0'));
        return redirect()->back();
    }

}