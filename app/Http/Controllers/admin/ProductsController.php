<?php

namespace App\Http\Controllers\admin;

use App\Helpers\General\UtilityHelper;
use App\Http\Controllers\Controller;

use App\Http\Requests\admin\ProductsRequest;

use App\Mail\forgotpassword;
use App\Models\Admin\Orders;

use App\Models\Admin\ProductsImages;
use App\Models\Admin\StockLog;
use App\Models\Admin\Units;
use App\Models\Shops;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Models\Admin\Categories;
use App\Models\Admin\Products;
use PhpParser\Node\Expr\New_;

class ProductsController extends Controller
{


    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function listings()
    {

        if(UtilityHelper::getUserRole() == 'Vendor'){
           $shop =  Shops::where('owner','=',Auth::id())->first();
            $productsData = Products::where('shop_id','=',$shop->id)->paginate(40);
            $category = Categories::all();
            $order = Orders::all();
            $units=Units::all();
            $mytime = Carbon::now();
            $link=$productsData->links();

        }else{
            $productsData = Products::paginate(40);
            $category = Categories::all();
            $order = Orders::all();
            $units=Units::all();
            $mytime = Carbon::now();
            $link=$productsData->links();

        }

        return view('admin/products-management/product', compact('productsData', 'category', 'order', 'mytime','units','link'));
    }

    public function index()
    {
        $categories = Categories::where('status', '=', '1')->get();
        if(UtilityHelper::getUserRole() == 'Vendor'){
            $shops = Shops::where('owner','=',Auth::id())->get();
        }else{
            $shops = Shops::all();
        }
        return view('admin/products-management/add-products', ['categories' => $categories,'shops' => $shops]);
    }

    public function addProducts(ProductsRequest $request)
    {


        $this->validate($request, [
            'product_image' => 'required',
        ]);
        $multipleimages = $request->file('multiple_images');

        $file = $request->file('product_image');
        $name = time() . $file->getClientOriginalName();
        $destinationPath = public_path('images/products');
        $file->move($destinationPath, $name);


        $date = $request->arrivalDate;


        $arrivaldate_array = explode("/", $date);//01/23/2018
        $arrival_date = $arrivaldate_array[2] . '-' . $arrivaldate_array[0] . '-' . $arrivaldate_array[1];
        $expireDate_array = explode("/", $request->expireDate);//01/23/2018
        $expire_date = $expireDate_array[2] . '-' . $expireDate_array[0] . '-' . $expireDate_array[1];


        $productValues = array(
            'product_title' => $request->productTitle,
            'sku' => $request->sku,
            'upc_isbn' => $request->upc_isbn,
            'mnf_vendor' => $request->mnf_vendor,
            'product_image' => $name,
            'category_id' => $request->category_id,
            'product_description' => $request->product_description,
            'health_benifits' => $request->product_benefits,
            'product_detail' => $request->product_detail,
            'available_for_sales' => $request->available_for_sales,
            'premiumProducts' => $request->premiumProducts,
            'popular_items' => $request->popular_items,
            'best_sellers' => $request->best_sellers,
            'featured_product' => $request->featured_product,
            'arrivalDate' => $arrival_date,
            'expire_date' => $expire_date,
            'inventoryEnabled' => $request->inventoryEnabled,
            'product_length_fractional_part' => $request->product_length_fractional_part,
            'weight' => $request->weight,
            'require_shipping' => $request->require_shipping,
            'free_shipping' => $request->free_shipping,
            'freight' => $request->freight,
            'meta_descriptions' => $request->meta_descriptions,
            'meta_keywords' => $request->meta_keywords,
            'product_page_title' => $request->product_page_title,
            'unit' => $request->rows[0]['unit'],
            'status' => $request->status,
            'cleanURL' => $request->cleanURL,
            'shop_id' => $request->shop_id,
            'CreatedDate' => date('Y-m-d H:i:s')
        );


        $products = new Products();
        $lastInsertId = $products->addProduct($productValues);
        $units = $request->input('rows');

        if ($lastInsertId) {
            $this->addProductUnits($units, $lastInsertId);
            $message = 'Stock Added Source : ' . 'Products is Added' . ' - Opening Stock : ' . '0' . ' Change : ' . $request->quantity_in_stock . ' New Quantity : ' . $request->quantity_in_stock;
            StockLog::addLog($message, null, $lastInsertId, null);
            if($multipleimages){
            $this->addProductImages($multipleimages, $lastInsertId);
            }
            $msg = 'Product added successfully.';
            return Redirect::to('/admin/products-management/products')->with('status', $msg);
        } else {
            $msg = 'Error: Product not added.';
            return Redirect::to('/admin/products-management/add-products')->withErrors([$msg, 'this message']);

        }
    }

    public function listingProducts($id)
    {
        $prod = Products::all();
        $units = Units::where('product_id', '=', $id)->get();
        $products = $prod->find($id);
        $categoryId = $products->category_id;
        $cat = Categories::all();

        $categories = Categories::where('status', '=', '1')->get();
        if ($products->arrivalDate != NULL) {
            $arrivaldate_array = explode("-", $products->arrivalDate);//2018-02-23
            $arrival_date = $arrivaldate_array[1] . '/' . $arrivaldate_array[2] . '/' . $arrivaldate_array[0];
        } else {
            $arrival_date = '';
        }
        if ($products->expire_date != NULL) {
            $expireDate_array = explode("-", $products->expire_date);//01/23/2018
            $expire_date = $expireDate_array[1] . '/' . $expireDate_array[2] . '/' . $expireDate_array[0];
        } else {
            $expire_date = '';
        }
        if(UtilityHelper::getUserRole() == 'Vendor'){
            $shops = Shops::where('owner','=',Auth::id())->get();
        }else{
            $shops = Shops::all();
        }
        return view('admin/products-management/update-products', compact('units','shops', 'categories', 'products', 'arrival_date', 'expire_date'));
    }


    public function updateProducts(ProductsRequest $request, $id)
    {

        $rows = $request->input('rows');
        $units = $request->input('rows');
        if($units ==''){
            $msg = 'Error: Please select a valid unit.';
            return Redirect::to('/admin/products-management/updateProducts')->withErrors([$msg, 'this message']);
        }
        $productId = $request->id;
        $date = $request->arrivalDate;
        $arrivaldate_array = explode("/", $date);//01/23/2018
        $arrival_date = $arrivaldate_array[2] . '-' . $arrivaldate_array[0] . '-' . $arrivaldate_array[1];
        $expireDate_array = explode("/", $request->expireDate);//01/23/2018
        $expire_date = $expireDate_array[2] . '-' . $expireDate_array[0] . '-' . $expireDate_array[1];

        $file = $request->file('product_image');
        if ($file != '') {
            $name = time() . $file->getClientOriginalName();
            $destinationPath = public_path('images/products');
            $file->move($destinationPath, $name);
        } else {
            $name = Products::where('id', $productId)->pluck('product_image')->first();


        }
        $productValues = array(
            'product_title' => $request->productTitle,
            'sku' => $request->sku,
            'upc_isbn' => $request->upc_isbn,
            'mnf_vendor' => $request->mnf_vendor,
            'product_image' => $name,
            'category_id' => $request->category_id,
            'product_description' => $request->product_description,
            'health_benifits' => $request->product_benefits,
            'product_detail' => $request->product_detail,
            'available_for_sales' => $request->available_for_sales,
            'premiumProducts' => $request->premiumProducts,
            'popular_items' => $request->popular_items,
            'best_sellers' => $request->best_sellers,
            'featured_product' => $request->featured_product,
            'salePriceValue' => $request->salePrice,
            'arrivalDate' => $arrival_date,
            'expire_date' => $expire_date,
            'inventoryEnabled' => $request->inventoryEnabled,
            'product_length_fractional_part' => $request->product_length_fractional_part,
            'weight' => $request->weight,
            'require_shipping' => $request->require_shipping,
            'free_shipping' => $request->free_shipping,
            'freight' => $request->freight,
            'meta_descriptions' => $request->meta_descriptions,
            'meta_keywords' => $request->meta_keywords,
            'product_page_title' => $request->product_page_title,
            'status' => $request->status,
            'cleanURL' => $request->cleanURL,
            'shop_id' => $request->shop_id,
            'ModifiedDate' => date('Y-m-d H:i:s')
        );

        $products = Products::where('id', $productId)->update($productValues);

        if ($products) {

            $multipleimages = $request->file('multiple_images');
            if($multipleimages){
            $this->updateProductImages($multipleimages, $productId);
            }
            $this->updateProductUnits($units, $productId);



            $msg = 'Product updated successfully.';

            return Redirect::to('/admin/products-management/products')->with(['status'=>$msg]);
            //            return $this->searchProducts($request, $request->category_id);


        } else {
            $msg = 'Error: Product not updated.';
            return Redirect::to('/admin/products-management/add-products')->withErrors([$msg, 'this message']);

        }
    }


    public function updatestatus(Request $request, $id)
    {
        $units=Units::all();
        $upd = Products::where('id', $id)->update(array('status' => '0'));
        $categoryId = Products::where('id', $id)->select('category_id')->first()->category_id;
        $message = 'Status Update : ' . 'Products is Updated' . ' Old Status : ' . 'On' . ' Change : ' . 'Off';
        StockLog::addLog($message, null, $id, null);

        return $this->searchProducts($request, $categoryId);

    }

    public function updatestatusone(Request $request, $id)
    {
        $units=Units::all();
        $upd = Products::where('id', $id)->update(array('status' => '1'));
        $categoryId = Products::where('id', $id)->select('category_id')->first()->category_id;
        $message = 'Status Update : ' . 'Products is Updated' . ' Old Status : ' . 'off' . ' Change : ' . 'on';
        StockLog::addLog($message, null, $id, null);
        return $this->searchProducts($request, $categoryId);
    }

    public function deleteCategories($id)
    {
        DB::table('categories')->where('id', '=', $id)->delete();

        return redirect()->back();


    }

    public function updatestock($id)
    {
        $products = Products::find($id);
        return view('admin/products-management/addstock', compact('products'));
    }

    public function deleteProducts($id)
    {

       $productName= Products::where('id',$id)->pluck('product_title')->first();
        $message = 'Status Update : ' . 'Products is Delete' ;
        StockLog::addLogDelete($message, null, $id, null, $productName, null);
        DB::table('products')->where('id', '=', $id)->delete();
        $msg = "Record deleted";


        return Redirect::back()->withErrors([$msg, 'this message']);


    }

    public function deletemultipleProducts(Request $request)
    {
        $delete = $request->input('delete');
        if ($delete) {
            foreach ($delete as $del) {
                $productName = Products::where('id',$del)->pluck('product_title')->first();
                $message = 'Status Update : ' . 'Products is Delete' ;
                StockLog::addLogDelete($message, null, $del, null,$productName, null);
                DB::table('products')->where('id', '=', $del)->delete();

            }
        } else {
            $msg = "please select at least one record ";
            return Redirect::back()->withErrors([$msg, 'this message']);
        }
        return redirect()->back();
    }

    //stock management------
    public function stock()
    {

        $productstock = Products::all();

        $categories = Categories::all();


        return view('admin/stock-management/stock', ['stockData' => $productstock, 'category' => $categories]);
    }

    public function productItem(ProductsRequest $request)
    {
        $order_item = DB::table('order_items')->where('object_id', '=', $request->id)->get();

        if (count($order_item) == 0) {
            $data = Products::where('id', '=', $request->id)->get();
            return response($data);

        } else {
            $data = DB::table('products')->join('order_items', 'products.id', '=', 'order_items.object_id')->where('id', '=', $request->id)->get();
            return response($data);
        }

    }

    public function productorders(Request $request)
    {

        $order = DB::table('order_items')->leftJoin('products', 'orders.product_id', '=', 'orders.product_id')->get();

        return response()->json($order);

    }

    public function updatestocks(Request $request)
    {
        $productid = $request->productselected;
        $updateValue = array('productselected' => '', 'quantity_in_stock' => '', 'price' => '', 'sale_price' => '');
        foreach ($updateValue as $key => $value) {
            $updateValue[$key] = $request->input($key);

        }
        for ($counter = 0; $counter < count($updateValue['price']); $counter++) {

            $dbUpdate = array();
            foreach ($updateValue as $key => $value) {
                if ($key == 'productselected') {
                    $productData = Products::find($updateValue[$key][$counter]);
                    $quantityInStock = $productData->quantity_in_stock;


                } else if ($key == 'quantity_in_stock') {
                    $dbUpdate['quantity_in_stock'] = $updateValue[$key][$counter] + $quantityInStock;

                    $message = 'Stock Added Source : ' . 'Update Stocks' . ' - Opening Stock : ' . $quantityInStock . ' Change : ' . $updateValue[$key][$counter] . ' New Quantity : ' . $dbUpdate['quantity_in_stock'];
                    StockLog::addLog($message, null, $updateValue['productselected'][$counter], null);

                } else if ($key == 'price') {
                    $dbUpdate['marketPrice'] = $updateValue[$key][$counter];
                } else if ($key == 'sale_price') {
                    $dbUpdate['salePriceValue'] = $updateValue[$key][$counter];
                } else {
                    $dbUpdate[$key] = $updateValue[$key][$counter];
                }
            }

            $update = Products::where('id', '=', $updateValue['productselected'][$counter])->update($dbUpdate);
            echo $update;
        }
    }


    public function setCategory(Request $request)
    {
        $id = $request->id;

        $productstock = DB::table('categories')
            ->join('products', 'categories.id', '=', 'products.category_id')
            ->where('category_id', '=', $id)->get();

        $total_item = DB::table('products')
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->join('order_items', 'products.id', '=', 'order_items.object_id')
            ->where('category_id', '=', $id)->sum('item_quantity');


        $allProduct = $productstock->count();
        $product_counter = 1;
        $Data = '';
        foreach ($productstock as $productdata) {


            $order_item = DB::table('products')
                ->join('order_items', 'products.id', '=', 'order_items.object_id')
                ->where('object_id', '=', $productdata->id)->sum('item_quantity');


            $mytime = Carbon::now();
//

            $tomorrow = $mytime->addDay();
            if ($tomorrow <= $productdata->expire_date) {
                $forword = $productdata->quantity_in_stock - $order_item;
            } elseif ($productdata->expire_date == '') {
                $forword = $productdata->quantity_in_stock - $order_item;
            } else {
                $forword = '';
            }

            if ($productdata->expire_date == '') {
                $remaining_quantity = '';
            } else if ($mytime >= $productdata->expire_date) {
                $remaining_quantity = $productdata->quantity_in_stock - $order_item;
            } else {
                $remaining_quantity = '';
            }


            if ($productdata->expire_date == '') {
                $quantity = $productdata->quantity_in_stock - $order_item;
            } elseif ($mytime >= $productdata->expire_date) {

                $quantity = '';

            } else {
                $quantity = $productdata->quantity_in_stock - $order_item;
            }

            $Data[$product_counter] = '<tr id="rowshow' . $product_counter . '"><td class="right_border_dotted text-center">' . $product_counter . '</td>
                <td ><input type="hidden" name="productselected[]" value="' . $productdata->id . '">
                <input disabled id="productselected' . $product_counter . '" name="user_product[]" value="' . $productdata->product_title . '"></td>
                <td ><input disabled id="prouct_unit' . $product_counter . '" value="' . $productdata->quantity_in_stock . '" ></td>
                <td class="disabled_select_units" ><input disabled type="text" id="marketprice' . $product_counter . '" class="marketprice" name="marketprice[]" style="width:60px" value="' . $productdata->unit . '"></td>
                <td><input type="text" name="quantity_in_stock[]" value="   "></td>
                <td><input type="text" name="price[]" id="price' . $product_counter . '"  style="width: 100px" class="price"> </td>
                <td><input name="percentage" type="text" onkeyup="myFunction()" style="width: 100px" id="percentage' . $product_counter . '"  placeholder="0.00 %"></td>
                <td><input type="text" name="sale_price[]" id="profit' . $product_counter . '" style="width: 100px"></td>
                <td><input type="checkbox"></td>
                <td><input disabled type="text" id="product_sold" value="' . $order_item . '"></td>
                <td><input disabled type="text" value="' . $quantity . '" id="product_left" ></td>
                <td><input disabled type="text" value="' . $remaining_quantity . '" name="waste" class="waste"></td>
                <td><input disabled id="forword" value="' . $forword . '"  type="text"></td>
                <td width="50" class="ibtnDel left_border_dotted text-center"><a onclick="deleteRow(this)" ><i class="fa fa-trash-o" aria-hidden="true" ></i></a></td>
                </tr>
                ';


            $product_counter++;
        }

        return response($Data);

    }

    public function categoryValue(Request $request)
    {
        $id = $request->id;
        $counter = $request->counter;


        $productData = DB::table('categories')
            ->join('products', 'categories.id', '=', 'products.category_id')
            ->where('category_id', '=', $id)->get();

        $data = '';


        $data .= '
               <tr id="rowshow' . $counter . '">
                <td class="right_border_dotted text-center">' . $counter . '</td>
                <td ><select class="e1 form-control productselected" onchange="productItemFunction(this.value,' . $counter . ')"id="productselected' . $counter . '"  name="productselected[]" style="width: 150px">
                <option >Select Product</option>';
        foreach ($productData as $product) {
            $productTitle = $product->product_title;
            $productId = $product->id;

            $data .= '<option value="' . $productId . '">' . $productTitle . '</option>';
        }

        $data .= '</select></td>
                <td ><input readonly id="prouct_unit' . $counter . '" ></td>
                <td class="disabled_select_units" ><input disabled type="text" id="marketprice' . $counter . '" class="marketprice" name="marketprice[]"></td>
                <td><input type="text" name="quantity_in_stock[]" id="quantity_in_stock' . $counter . '"></td>
                <td><input type="text" name="price[]" id="price' . $counter . '" style="width: 100px" class="price"> </td>
                <td><input name="percentage" type="text" style="width: 100px" id="percentage' . $counter . '" onkeyup="myFunction()" placeholder="0.00 %"></td>
                <td><input type="text" name="sale_price[]" id="profit' . $counter . '" style="width: 100px"></td><td><input type="checkbox"></td>
                <td><input disabled type="text" id="product_sold' . $counter . '"></td>
                <td><input disabled type="text" id="product_left' . $counter . '" ></td>
                <td><input disabled type="text" name="waste' . $counter . '" class="waste"></td>
                <td><input disabled id="forword' . $counter . '"  type="text"></td>
                <td class=" left_border_dotted text-center "><a  class="ibtnDel" ><i class="fa fa-trash-o" style="margin-left:15px" aria-hidden="true"></i></a>
                </td>
                </tr>
                ';
        return response($data);
    }

    function searchProducts(Request $request, $category = null)
    {
        $keyword = $request->keyword;
        $stock = $request->stock;


        $categoryId = $category ? $category : $request->category;
        $catgory = Categories::all();

        $data = '';
        $searchProducts = '';
        if ($keyword != '') {
            $searchProducts = Products::where('product_title', 'like', '%' . $keyword . '%')
                ->orWhere('sku', 'like', '%' . $keyword . '%');

        }
        if ($stock != '') {

            if ($stock == 'lowstock') {

                $searchProducts = Products::where(function ($query) use ($keyword) {
                    $query
                        ->orWhere('sku', 'like', '%' . $keyword . '%')
                        ->orWhere('product_title', 'like', '%' . $keyword . '%');
                })->where('quantity_in_stock', '<', '6')->where('quantity_in_stock', '>', '0');


            } elseif ($stock == 'outofstock') {
                $searchProducts = Products::where(function ($query) use ($keyword) {
                    $query
                        ->orWhere('sku', 'like', '%' . $keyword . '%')
                        ->orWhere('product_title', 'like', '%' . $keyword . '%');
                })->where('quantity_in_stock', '==', '0');
            }
        }
        if ($categoryId != '') {
            $searchProducts = Products::where('category_id', '=', $categoryId)->where(function ($query) use ($keyword) {
                $query
                    ->orWhere('sku', 'like', '%' . $keyword . '%')
                    ->orWhere('product_title', 'like', '%' . $keyword . '%');
            });
            if ($stock != '') {
                if ($stock == 'lowstock') {

                    $searchProducts->where('quantity_in_stock', '<', '6')->where('quantity_in_stock', '>', '0');
                } elseif ($stock == 'outofstock') {
                    $searchProducts->where('quantity_in_stock', '==', '0');
                }
            }
        }
        if ($searchProducts != '') {
            $productsData = $searchProducts->paginate('40');
        } else {
            $productsData = Products::paginate('40');
        }
        $category = Categories::all();
        $order = Orders::all();

        $mytime = Carbon::now();
        $clearsearch = 'clearsearch';
        $units=Units::all();
        $link='';
        return view('admin/products-management/product', compact('productsData', 'category', 'order', 'mytime','clearsearch','units','link'));

    }

    public function addProductImages($multipleimages, $productid)
    {

        foreach ($multipleimages as $images) {
            $imagename = time() . $images->getClientOriginalName();
            $path = public_path('images/multipleProductImages');
            $images->move($path, $imagename);

            $multipleImages[] = [
                'product_id' => $productid,
                'images' => $images,
                'created_at' => date('Y-m-d H:i:s')
            ];
        }
        $productsImages = ProductsImages::insert($multipleImages);
    }

    public function updateProductImages($multipleimages, $productid)
    {
        $del = ProductsImages::where('product_id', $productid)->delete();
        foreach ($multipleimages as $images) {
            $imagename = time() . $images->getClientOriginalName();
            $path = public_path('images/multipleProductImages');
            $images->move($path, $imagename);
            $multipleImages[] = [
                'product_id' => $productid,
                'images' => $images,
                'updated_at' => date('Y-m-d H:i:s')
            ];
        }
        ProductsImages::insert($multipleImages);
    }

    public function addProductUnits($units, $productid)
    {


        foreach ($units as $unit) {
            $charges[] = [
                'unit' => $unit['unit'],
                'product_id' => $productid,
                'product_price' => $unit['salePrice'],
                'product_price_discount' => $unit['discountPrice'],
                'product_quantity' => $unit['quantity_in_stock'],
            ];
        }
        $unit = Units::insert($charges);
    }

    public function updateProductUnits($units, $productid)
    {
     $unit=   Units::where('product_id', '=', $productid)->delete();


        foreach ($units as $row) {
            $charges[] = [
                'unit' => $row['unit'],
                'product_id' => $productid,
                'product_price' => $row['salePrice'],
                'product_price_discount' => $row['discountPrice'],
                'product_quantity' => $row['quantity_in_stock'],
            ];
            $message = 'Stock Updated Source : ' . 'Products is Updated' . ' - Opening Stock : ' . '0' . ' Change : ' . $row['quantity_in_stock'] . ' New Quantity : ' . $row['quantity_in_stock'];
            StockLog::addLog($message, null, $productid, null);
        }
        Units::insert($charges);
    }
}

