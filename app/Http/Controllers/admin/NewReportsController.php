<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\Models\Admin\Categories;
use App\Models\Admin\CategoryLog;
use App\Models\Admin\Log;
use App\Models\Admin\Orders;
use App\Models\Admin\StockLog;
use App\Models\Products;
use Illuminate\Http\Request;



class NewReportsController extends Controller
{

    /**
     * NewReportsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getLogReport()
    {
        return view('admin/reports/logReport');
    }

    public function getLogData(Request $request)
    {
        $orderID = Orders::where('orderNumber', 'LIKE', '%'.$request->orderNumber.'%')->first();
        if($orderID)
        {
            $orderID = $orderID->id;
        }
        else{
            return "Order Not Found";
        }
        $data = Log::where('order_id', $orderID)->get();
        $content = view('admin/reports/imports/logReportTable')->with('data',$data);
        return $content;
    }

    public function getStockReport()
    {
        return view('admin/reports/stockReport');
    }

    public function getStockData(Request $request)
    {
        $productIdArray = Products::where('product_title', 'LIKE', '%'.$request->productName.'%')->pluck('id');
        if(count($productIdArray))
        {
            $productID = $productIdArray;

        }
        else{
            $stockArray=StockLog::where('product_name', 'LIKE', '%'.$request->productName.'%')->pluck('product_id');
            if($stockArray){
                $productID = $stockArray;

            }
            else{

                return "Order Not Found";

        }

        }
        $data = StockLog::wherein('product_id', $productID)->get();
        $content = view('admin/reports/imports/stockReportTable')->with('data',$data);
        return $content;

    }

 public function categoryLogReport()
    {
        return view('admin/reports/category-log');
    }

    public function getcategoryLogData(Request $request)
    {
        $data = CategoryLog::where('category_name',  'LIKE', '%'.$request->categoryName.'%')->get();
        $content = view('admin/reports/imports/categoryReportTable')->with('data',$data);
        return $content;

    }
}