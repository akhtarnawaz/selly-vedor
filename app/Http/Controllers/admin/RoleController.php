<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Auth;
use App\Models\Admin\Users;
class RoleController extends Controller
{
    public function roles(Request $request){
        $roles=Role::all();
        return view('admin/users-access-module/roles',['roles'=>$roles]);
    }
    public  function addRoles(Request $request){

        $name=$request->Name;
        $create=Role::create(['name'=>$name]);
        return Redirect::to('admin/users-access-module/roles');
    }
    public  function editRoles(Request $request){

        $id=$request->id;
        $userDetail=Users::join('user_has_roles','users.id','=','user_has_roles.users_id')->where('role_id','=',$id)->get();

        return view('admin/users-access-module/roleHasUser',['userdata'=>$userDetail]);

    }
    public  function updateRoles(Request $request){

        $id=$request->Id;
        $name=$request->Name;
        $update=Role::where('id',$id)->update(['name'=>$name]);
        return Redirect::to('admin/users-access-module/roles');
    }
    public  function deleteRoles(Request $request , $id ){

        $update=Role::where('id',$id)->delete();
        return Redirect::to('admin/users-access-module/roles');
    }
}
