<?php

namespace App\Http\Controllers\Product;

use App\Helpers\Products\ProductsHelper;
use App\Models\Admin\Units;
use App\Models\Categories;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class ProductsController extends BaseController
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *Get Products Page
     */
    public function index($inputCategory=null){
        $search = Input::get('search');
        $slug = Input::get('category');

        $category = $inputCategory==null?$category = $slug:$category= $inputCategory;

        $categoryId  = Categories::where('cleanURL',$category)->pluck('id')->first();
        $parentId  = Categories::where(['parent_id'=>$categoryId,'status'=>1])->first();
        $parentCategory  = Categories::where(['parent_id'=>$categoryId,'status'=>1])->get();

    if($parentId == null || $search){
        $category  = Categories::where('cleanURL',$category)->first();
        if(!$category && $search==null)
        {
            return Redirect::back();
        }
        $products = self::getProducts($slug,$category,$search);
        $productsOutOfStock = self::getProductsOutOfStock($slug,$category,$search);
        $category_title = isset($category->category_title)?$category->category_title:'Products';
        $content  = self::getProductHtml($products,$productsOutOfStock,$category_title);
        $title = isset($category->category_page_title)?$category->category_page_title:'Products';
        $meta_descriptions = isset($category->meta_descriptions)?$category->meta_descriptions:'Products';
        $meta_keyword = isset($category->meta_keywords)?$category->meta_keywords:'Products';

        return view('templates.products')->with(['content'=>$content , 'title' => $title,  'category_title' => $category_title, 'meta_keyword' => $meta_keyword, 'meta_descriptions' => $meta_descriptions]);
    }
    else{
//        $childCategory = self::getChildCategory($slug,$category,$search);
        $category  = Categories::where('cleanURL',$category)->first();
        $category_title = isset($category->category_title)?$category->category_title:'Products';
        $category_title = isset($category->category_title)?$category->category_title:'Products';
        $title = isset($category->category_page_title)?$category->category_page_title:'Products';
        $meta_descriptions = isset($category->meta_descriptions)?$category->meta_descriptions:'Products';
        $meta_keyword = isset($category->meta_keywords)?$category->meta_keywords:'Products';
        $content  = self::getChildCategoryHtml($parentCategory);

        return view('templates.products')->with(['content'=>$content , 'title' => $title,  'category_title' => $category_title, 'meta_keyword' => $meta_keyword, 'meta_descriptions' => $meta_descriptions]);
    }
    }

    /**
     * get Products Content
     */
    public function getProductHtml($data,$outOfStockProduct,$category_title){

        return view('layouts.products.grid-product')->with(['products'=>$data,'outOfStockProduct'=>$outOfStockProduct,'category_title'=>$category_title]);
    }
    public function getChildCategoryHtml($data){

        return view('layouts.childCategory.categories')->with('categories',$data);
    }

    /**
     * @param $slug
     * @param $category
     * @return $this|\Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection|static[]
     */

    public function getProducts($slug,$category,$search = null){

        $params =[
                'select' => [
                    'id',
                    'product_title',
                    'product_description',
                    'product_image',
                    'marketPrice',
                    'salePriceValue',
                    'on_sales',
                    'quantity_in_stock',
                    'cleanURL',
                    'category_id'
                ],
                'where' => [['product_title','like', '%' . $search . '%'],['status','=',1]],
                'order_by' => 'product_title',
                'order' => 'asc',
                'paginate' => '60',
            ];

        return ProductsHelper::getProductsBySlug($params,$slug,$category);
    }
    public function getProductsOutOfStock($slug,$category,$search = null){

        $params =[
                'select' => [
                    'id',
                    'product_title',
                    'product_description',
                    'product_image',
                    'marketPrice',
                    'salePriceValue',
                    'on_sales',
                    'quantity_in_stock',
                    'cleanURL',
                    'category_id'
                ],
                'where' => [['product_title','like', '%' . $search . '%'],['status','=',1]],
                'order_by' => 'id',
                'order' => 'desc',

            ];

        return ProductsHelper::getProductsBySlug($params,$slug,$category);
    }
}
