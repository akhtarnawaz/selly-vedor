<?php

namespace App\Http\Controllers;

use App\Models\WebSiteOnOff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function siteOnOff(Request $request){
        $Id=WebSiteOnOff::orderBy('id','asc')->first();
        $status=$request->status;

       $update= WebSiteOnOff::where('id',$Id->id)->update(['status'=>$status,'updated_at'=>date('Y-m-d H:i:s')]);


    }
}
