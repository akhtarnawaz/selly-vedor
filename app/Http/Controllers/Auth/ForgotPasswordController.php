<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Users;
use App\Mail\forgotpassword;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    function forgotPasword(Request $request){
        $email=$request->email;
   $user=Users::where('email','=',$email)->first();
//   dd($user);
   if($user){
$message="set your pasword";
      $userid= $user->id;
        Mail::to($email)->send(new forgotpassword($message,$userid));
       return redirect()->back()->withErrors(['You Will Shortly receive an email', 'this message']);

   }
   else
       {
           return response()->json(['error'=>'Something Wrong']);

   }
    }
}
