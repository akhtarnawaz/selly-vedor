<?php

namespace App\Http\Controllers\Misc;

use App\Helpers\Products\ProductsHelper;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Pages;


class Misc extends BaseController
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getQuickView(Request $request)
    {
        $item = ProductsHelper::getSingleProduct($request->slug);
        return view('layouts.products.quick-view')->with('item',$item);
    }

    public function getAboutUs()
    {

        $aboutus=Pages::where('name','=','About Us')->first();
        $title=$aboutus->metaTitle;
        $meta_keyword=$aboutus->metaKeywords;
        $meta_descriptions=$aboutus->metaDescriptions;
        return view('layouts.inner-pages.about-us',['aboutus'=> $aboutus,'title'=>$title,'meta_keyword'=>$meta_keyword,'meta_descriptions'=>$meta_descriptions]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPrivacyPolicy()
    {

        $PrivacyPolicy=Pages::where('name','=','Privacy Policy')->first();
        $title=$PrivacyPolicy->metaTitle;
        $meta_keyword=$PrivacyPolicy->metaKeywords;
        $meta_descriptions=$PrivacyPolicy->metaDescriptions;
        return view('layouts.inner-pages.privacy-policy',['PrivacyPolicy'=> $PrivacyPolicy,'title'=>$title,'meta_keyword'=>$meta_keyword,'meta_descriptions'=>$meta_descriptions]);
        }
    public function getReturnPolicy()
    {

        $ReturnPolicy=Pages::where('name','=','Return Policy')->first();
        $title=$ReturnPolicy->metaTitle;
        $meta_keyword=$ReturnPolicy->metaKeywords;
        $meta_descriptions=$ReturnPolicy->metaDescriptions;
        return view('layouts.inner-pages.return-policy',['ReturnPolicy'=> $ReturnPolicy,'title'=>$title,'meta_keyword'=>$meta_keyword,'meta_descriptions'=>$meta_descriptions]);
       }
    public function getTermsConditions()
    {

        $Termsandconditions=Pages::where('name','=','Terms and conditions')->first();
        $title=$Termsandconditions->metaTitle;
        $meta_keyword=$Termsandconditions->metaKeywords;
        $meta_descriptions=$Termsandconditions->metaDescriptions;
        return view('layouts.inner-pages.terms-conditions',['Termsandconditions'=> $Termsandconditions,'title'=>$title,'meta_keyword'=>$meta_keyword,'meta_descriptions'=>$meta_descriptions]);
    }
}
