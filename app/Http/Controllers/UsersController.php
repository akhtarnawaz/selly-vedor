<?php

namespace App\Http\Controllers;

use App\Models\Admin\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/users-listings');
    }
    public function resetuserPasword(Request $request)
    {
        $id = Auth::user()->id;
        $pass = Users::where('id', $id)->pluck('password')->first();

        if (Hash::check($request->oldpassword, $pass)) {
        if($request->newpassword == $request->confirmpassword){
            $passChange = Hash::make($request->newpassword);
            $upd = Users::where('id', $id)->update(['password' => $passChange]);

            if($upd){
                return response()->json(['success'=>'Password updated']);
            }
            else{
                return response()->json(['error'=>'Something Wrong']);
            }
        }
        else{
            return response()->json(['error'=>'The confirm password and password must match.']);
        }
        }
        else {
            return response()->json(['error'=>'Old Password is not correct']);
        }
    }
}
