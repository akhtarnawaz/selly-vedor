<?php

namespace App\Http\Controllers;
use Exception;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\Users;
use Illuminate\Http\Request;

class SocialAuthFacebookController extends Controller
{


    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }


    public function callback()
    {
//        try {


            $googleUser = Socialite::driver('facebook')->user();
            $existUser = Users::where('email',$googleUser->email)->first();


            if($existUser) {
                Auth::loginUsingId($existUser->id);
            }
            else {
                $user = new Users;
                $user->username = $googleUser->name;
                $user->email = $googleUser->email;
                $user->facebook_id = $googleUser->id;
                $user->password = md5(rand(1,10000));
                $user->save();
                Auth::loginUsingId($user->id);
            }
            return redirect()->to('/');
//        }
//        catch (Exception $e) {
//            return 'error';
//        }
    }

}
