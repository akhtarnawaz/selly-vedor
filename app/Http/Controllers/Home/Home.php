<?php

namespace App\Http\Controllers\Home;

use App\Helpers\Products\ProductsHelper;
use App\Http\Controllers\BaseController;
use App\Models\Admin\OrderPaymentStatusTranslations;
use App\Models\Admin\OrderShippingStatusTranslations;
use App\Models\Admin\Units;
use App\Models\Categories;
use App\Models\Orders;
use App\Models\Products;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class Home extends BaseController
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */


    public function __construct()
    {
        parent::__construct();
//
    }


    /*
     *Load Index Page
     *
     * @return View
     */

    public function index()
    {
        $content = $this->getHomePageView();
        return view('templates.home', ['content' => $content]);
    }


    /**
     * Get Home Page View
     *
     * return View
     */

    private function getHomePageView()
    {
        $content = "";
        $content .= view('templates.imports.home.clearfix');
        $content .= view('layouts.home.categories-tab');
        $content .= view('templates.imports.home.clearfix');
//        $content .= view('templates.imports.home.new-launch-feature');
        $content .= view('templates.imports.home.clearfix');
        $content .= view('layouts.home.products-block');
        $content .= view('templates.imports.home.product_banner');
//        $content .= view('templates.imports.home.brands');
        return $content;
    }

    /**
     * Best Sellers
     */

    private function getSpecialProductBox()
    {
        $params =
            [
                'select' => [
                    'id',
                    'product_title',
                    'product_image',
                    'marketPrice',
                    'salePriceValue',
                    'on_sales',
                    'cleanURL'
                ],
                'where' => [['best_sellers', '1']],
                'order_by' => 'id',
                'order' => 'desc',
                'limit' => '20',
                'paginate' => '',
            ];
        $products = ProductsHelper::getProductsBySlug($params);
        return view('layouts.home.special-products-tab')->with('products',$products);
    }

    /**
     * Get Products Content
     *
     * return View
     */

    private function getProductsView()
    {
        $content = "";
        foreach (Config::get('constants.categories') as $item) {
            $content .= $this->getProductsTab($item);
            $content .= view('templates.imports.home.clearfix');
        };
        return $content;
    }


    /**
     * Get Products Tab
     * @param Products
     * @return View
     */

    private function getProductsTab($category)
    {

        $html_container = view('layouts.home._home_products_container')->with('title', $category);
        return $html_container;
    }

    public function orderlist($id){
        $orderlist=Orders::where('user_id','=',$id)->orderBy('id','DESC')->get();
        $paymentStatus=OrderPaymentStatusTranslations::all();
        $fulfilmentStatus=OrderShippingStatusTranslations::all();

        return view('templates.orderList',['orderlist'=>$orderlist,'paymentStatus'=>$paymentStatus,'fulfilmentStatus'=>$fulfilmentStatus,]);
    }
    public function orderItems(Request $request){
//        dd($request->orderId);
        $orderlist=Orders::join('order_items','orders.id','=','order_items.order_id')
            ->where('order_items.order_id','=',$request->orderId)->get();

        return view('layouts.home.orderItemList')->with('orderlist',$orderlist);
    }

    public function setprice(Request $request){

           $unit=$request->unit;
        $productId=$request->pid;
        $data=Units::where('unit','=',$unit)->where('product_id','=',$productId)->select('product_price','unit','product_quantity')->first();
//$data.=$unit->product_price.'/'.$unit->unit;
//        $data.=$unit->product_quantity;
    return response()->json($data);
    }

}