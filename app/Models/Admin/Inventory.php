<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table = 'inventory';

    public $timestamps = false;

    protected $fillable = [
        'purchase_date',
        'invoice_no',
        'supplier_id',
        'added_by',
        'added_on',
        'updated_by',
        'updated_on'
    ];

    function addInventory($Values)
    {
        $created = Inventory::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }

    function updateInventory($iid,$Values)
    {
        $updated = Inventory::update($Values);
    }
}