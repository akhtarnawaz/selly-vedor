<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Log extends Model
{
   protected $table = 'log';

   protected $fillable =[

       'users_id',
       'message',
       'order_id',
       'created_at'
       ];

   public static function addLog($message,$orderId,$userId = null)
   {
       if($userId == null)
       {
           $userId = isset(Auth::user()->id)&&Auth::user()->id!=null?Auth::user()->id:null;
       }
       $log = new Log();
       $log->message = $message;
       $log->order_id = $orderId;
       $log->user_id = $userId;
       $log->save();
   }
}
