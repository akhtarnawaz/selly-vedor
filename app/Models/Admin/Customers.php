<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Customers extends Model
{
    public $timestamps = false;
    use SoftDeletes;
    protected $softDelete = true;
    protected $fillable = [
        'id',
        'full_name',        
        'email',
        'phoneno',
        'address',
        'username',
        'house_no',
        'street',
        'town',
        'block',
        'city',
        'state',
        'zip',
        'country_code',        
        'created_at',
        'updated_at'
    ];

    function addCustomer($Values)
    {
        $created = Customers::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }

    function updateCustomers($pid,$Values)
    {
        $created = Customers::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }


    function getCustomerDetails($id)
    {

        $user = Customers::where('email','=',$id)->get();

        if ($user)
        {
            return $user[0]['id'];
        }
        else
        {
            return false;
        }
    }

    function getCustomerInfo($customerId){
        $customerInfo = Customers::where('id','=',$customerId)->first();
        return $customerInfo;
    }
}