<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class OrderPaymentStatusTranslations extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    function addStatus($Values)
    {
        $created = OrderPaymentStatusTranslations::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }

    function updateStatus($sid,$Values)
    {
        $created = OrderPaymentStatusTranslations::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }
}