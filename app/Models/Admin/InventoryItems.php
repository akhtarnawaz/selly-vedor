<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class InventoryItems extends Model
{
    protected $table = 'inventory_items';

    public $timestamps = false;

    protected $fillable = [
        'inventory_id',
        'product_id',
        'unit',
        'opening_stock',
        'quantity',
        'total_stock',
        'unit_price',
        'profit',
        'sale_price',
        'foc'
    ];

    function addInventoryItems($Values)
    {
        $created = InventoryItems::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }

    function updateInventoryItems($iid,$Values)
    {
        $updated = InventoryItems::update($Values);
    }

    public function sumItemsPurchased($pid)
    {
        $noofItemsSold =  InventoryItems::where('product_id','=',$pid)->sum('quantity');

        return $noofItemsSold;
    }

    public function ExpenseOnPurchase($pid)
    {
        $totalExpense =  InventoryItems::where('product_id','=',$pid)->sum('unit_price');

        return $totalExpense;
    }
}