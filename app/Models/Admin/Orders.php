<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'customer_id',
        'delivered_by',
        'processed_by',
        'calltype',
        'commission',
        'delivery_time',
        'payment_status_id',
        'shipping_status_id',
        'currency_id',
        'shipping_id',
        'shipping_method_name',
        'payment_method',
        'tracking',
        'date',
        'lastRenewDate',
        'notes',
        'adminNotes',
        'orderNumber',
        'ordersource',
        'location_id',
        'recent',
        'total',
        'subtotal',
        'is_foc',
        'username',
        'address',
        'phoneno',
        'is_order',
        'isPosOrder',
        'created_at'
    ];

    public static function getOrdersCountByUID($uid)
    {
        $itemsquantity = Orders::where('user_id', '=', $uid)->count();
        return $itemsquantity;
    }

    public static function getOrdersCountByCID($oid)
    {
        $itemsquantity = Orders::where('customer_id', '=', $oid)->count();
        return $itemsquantity;
    }

    function addOrder($Values)
    {
        $created = Orders::create($Values);
        $lastInsertId = $created->id;

        if ($created) {
            return $lastInsertId;
        } else {
            return false;
        }
    }

    public function orderShippingStatus()
    {
        return $this->hasOne('App\Models\Admin\OrderShippingStatusTranslations', 'id', 'shipping_status_id');
    }
    public function orderPaymentMethods()
    {
        return $this->hasOne('App\Models\Admin\PaymentMethods', 'id', 'shipping_status_id');
    }
    public function orderPaymentStatus()
    {
        return $this->hasOne('App\Models\Admin\OrderPaymentStatusTranslations', 'id', 'payment_status_id');
    }
    public function ordersprocess()
    {
        return $this->hasOne('App\Models\Admin\Users', 'id', 'processed_by');
    }
    public function orderDelieverdBy()
    {
        return $this->hasOne('App\Models\Admin\Users', 'id', 'delivered_by');
    }
    public function orderUser()
    {
        return $this->hasOne('App\Models\Admin\Users', 'id', 'user_id');
    }
    public function orderCustomer()
    {
        return $this->hasOne('App\Models\Admin\Customers', 'id', 'customer_id');
    }
  public function orderItems()
    {
        return $this->hasMany('App\Models\Admin\OrdersItems', 'order_id', 'id');
    }

    public static function addStockBack($orderId, $source)
    {
        $stockIn = Orders::where('id', $orderId)->first()->stock_in;
        $stockOut = Orders::where('id', $orderId)->first()->stock_out;
        if ($stockIn == 0 && $stockOut == 1) {
            $orderItems = OrdersItems::where('order_id', $orderId)->get();
            foreach ($orderItems as $item) {
                $oldQuantity = Units::where('unit', $item->quantity_unit_id)->where('product_id', $item->object_id)->pluck('product_quantity')->first();
                $newQuantityValue = $item->item_quantity;
                $newQuantity = $oldQuantity + $newQuantityValue;
                Units::where('unit', $item->quantity_unit_id)->where('product_id', $item->object_id)->update(['product_quantity' => $newQuantity]);
                $message = 'Stock Added Source : ' . $source . ' - Opening Stock : ' . $oldQuantity . ' Change : ' . $newQuantityValue . ' New Quantity : ' . $newQuantity;
                StockLog::addLog($message, $orderId, $item->object_id, null);
            }
            Orders::where('id', $orderId)->update(['stock_in' => '1']);
        }

        return true;
    }

    /**
     * @param $orderId
     * @param $source
     * @return bool
     */

    public static function removeStock($orderId, $source)
    {

        $stockOut = Orders::where('id', $orderId)->first()->stock_out;
        if ($stockOut == 0) {
            $orderItems = OrdersItems::where('order_id', $orderId)->get();
            foreach ($orderItems as $item) {
                $removeProduct = Units::where('unit', $item->quantity_unit_id)->where('product_id', $item->object_id)->pluck('product_quantity')->first();
                $newQuantityValue = $item->item_quantity;
//                dd($newQuantityValue);
                $newQuantity = $removeProduct - $newQuantityValue;
                Units::where('unit', $item->quantity_unit_id)->where('product_id', $item->object_id)->update(['product_quantity' => $newQuantity]);
                Orders::where('id', $orderId)->update(['stock_out' => '1']);
                $message = 'Stock Removed Source : ' . $source . ' - Opening Stock : ' . $removeProduct . ' Change : ' . $newQuantityValue . ' New Quantity : ' . $newQuantity;
                StockLog::addLog($message, $orderId, $item->object_id, null);
            }
        }
        return true;
    }
}
