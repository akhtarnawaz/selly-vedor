<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CategoryLog extends Model
{
    public $timestamps = false;
    protected $table = 'category_log';
     protected $fillable = [
         'user_id',
         'category_id',
         'message',
         'category_name',
     ];
    public static function addLog($categoryId,$categoryTitle,$userId = null,$parentid)
    {
        if($userId == null)
        {
            $userId = isset(Auth::user()->id)&&Auth::user()->id!=null?Auth::user()->id:null;
            $username = isset(Auth::user()->username)&&Auth::user()->username!=null?Auth::user()->username:null;
        }
        $message= 'Category: '.$categoryTitle.', added By '.$username.', parent category '.$parentid.'';
        $log = new CategoryLog();
        $log->message = $message;
        $log->category_id = $categoryId;
        $log->category_name = $categoryTitle;
        $log->user_id = $userId;
        $log->created_at = date('Y-m-d H:i:s');
        $log->save();
    }
    public static function updateLog($categoryId,$categoryTitle,$userId = null,$parentcat)
    {
        if($userId == null)
        {
            $userId = isset(Auth::user()->id)&&Auth::user()->id!=null?Auth::user()->id:null;
            $username = isset(Auth::user()->username)&&Auth::user()->username!=null?Auth::user()->username:null;
        }
        $message= 'Category: '.$categoryTitle.', updated By '.$username.', parent category '.$parentcat.'';
        $log = new CategoryLog();
        $log->message = $message;
        $log->category_id = $categoryId;
        $log->category_name = $categoryTitle;
        $log->user_id = $userId;
        $log->updated_at = date('Y-m-d H:i:s');
        $log->save();
    }
    public static function disablestatus($categoryId,$categoryTitle,$userId = null,$parentcat)
    {
        if($userId == null)
        {
            $userId = isset(Auth::user()->id)&&Auth::user()->id!=null?Auth::user()->id:null;
            $username = isset(Auth::user()->username)&&Auth::user()->username!=null?Auth::user()->username:null;
        }
        $message= 'Category: '.$categoryTitle.',status updated By '.$username.', parent category '.$parentcat.' status = 0';
        $log = new CategoryLog();
        $log->message = $message;
        $log->category_id = $categoryId;
        $log->category_name = $categoryTitle;
        $log->user_id = $userId;
        $log->updated_at = date('Y-m-d H:i:s');
        $log->save();
    }
    public static function enablestatus($categoryId,$categoryTitle,$userId = null,$parentcat)
    {
        if($userId == null)
        {
            $userId = isset(Auth::user()->id)&&Auth::user()->id!=null?Auth::user()->id:null;
            $username = isset(Auth::user()->username)&&Auth::user()->username!=null?Auth::user()->username:null;
        }
        $message= 'Category: '.$categoryTitle.',status updated By '.$username.', parent category '.$parentcat.' status = 1';
        $log = new CategoryLog();
        $log->message = $message;
        $log->category_id = $categoryId;
        $log->category_name = $categoryTitle;
        $log->user_id = $userId;
        $log->updated_at = date('Y-m-d H:i:s');
        $log->save();
    }
    public static function deleteCategory($categoryId,$categoryTitle,$userId = null,$parentcat)
    {
        if($userId == null)
        {
            $userId = isset(Auth::user()->id)&&Auth::user()->id!=null?Auth::user()->id:null;
            $username = isset(Auth::user()->username)&&Auth::user()->username!=null?Auth::user()->username:null;
        }
        $message= 'Category: '.$categoryTitle.',deleted By '.$username.', parent category '.$parentcat.'';
        $log = new CategoryLog();
        $log->message = $message;
        $log->category_id = $categoryId;
        $log->category_name = $categoryTitle;
        $log->user_id = $userId;
        $log->updated_at = date('Y-m-d H:i:s');
        $log->save();
    }

}
