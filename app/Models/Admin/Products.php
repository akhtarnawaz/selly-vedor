<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'product_title',
        'sku',
        'upc_isbn',
        'mnf_vendor',
        'product_image',
        'category_id',
        'product_description',
        'health_benifits',
        'product_detail',
        'available_for_sales',
        'new_arrival',
        'popular_items',
        'best_sellers',
        'featured_product',
        'marketPrice',
        'on_sales',
        'salePriceValue',
        'arrivalDate',
        'expire_date',
        'inventoryEnabled',
        'quantity_in_stock',
        'product_length_fractional_part',
        'unit',
        'weight',
        'require_shipping',
        'free_shipping',
        'freight',
        'meta_descriptions',
        'meta_keywords',
        'product_page_title',
        'cleanURL',
        'status',
        'shop_id',
        'CreatedDate',
        'ModifiedDate'
    ];

    function addProduct($Values)
    {
        $created = Products::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }

    function updateProduct($pid,$Values)
    {

        Products::where('id',$pid)->update($Values);
        //        $lastInsertId = $created->id;
        //
        //        if ($created)
        //        {
        //            return $lastInsertId;
        //        }
        //        else
        //        {
        //            return false;
        //        }
    }

    function getProductInfo($pId)
    {
        $productInfo = Products::where('id','=',$pId)->get();
        return $productInfo;
    }
    function productUnits()
    {
        return $this->hasMany('App\Models\Admin\Units','product_id', 'id');
    }
}