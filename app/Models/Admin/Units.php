<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Units extends Model
{
    protected $table = 'products_unit';
    public $timestamps =false;

    protected $fillable = [
        'unit',
        'value',
        'product_id'
 
   ];

    function addUnit($Values)
    {
        $created = Units::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
        }
}
