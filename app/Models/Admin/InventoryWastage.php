<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class InventoryWastage extends Model
{
    protected $table = 'inventory_wastage';

    public $timestamps = false;

    protected $fillable = [
        'purchase_date',
        'invoice_no',
        'added_by',
        'added_on',
        'updated_by',
        'updated_on'
    ];

    function addWastageInventory($Values)
    {
        $created = InventoryWastage::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }

    function updateWastageInventory($iid,$Values)
    {
        $updated = InventoryWastage::where('id',$iid)->update($Values);
    }
}