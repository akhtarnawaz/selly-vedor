<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class MarketingNewLaunchImages extends Model
{
    protected $table = 'marketing_new_launch_images';

    public $timestamps = false;

    protected $fillable = [
        'image_1',
        'image_2',
        'image_3',
        'image_4',
        'title',
        'added_on',
        'updated_on'
    ];

    function addImages($Values)
    {
        $created = MarketingNewLaunchImages::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }

    function updateImages($iid,$Values)
    {
        $updated = MarketingNewLaunchImages::update($Values);
    }
}