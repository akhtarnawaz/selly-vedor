<?php

namespace App\Models;


use App\Helpers\General\UtilityHelper;
use Illuminate\Support\Facades\Config;

class Orders extends Model
{


    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable(Config::get('selly.table_names.orders'));
    }

    public function orderCustomer()
    {
        return $this->hasOne('App\Models\Customers');
    }

    public function placeuserOrder($customer_id,$cartTotal,$ordernote,$storeLocation,$request)
    {
        $orderId=Orders::orderBy('id','DESC')->pluck('id')->first();
            $orderNumber=$orderId + 1;
        $this->user_id = $customer_id;
        $this->total = $cartTotal['total'];
        $this->subtotal = $cartTotal['total'];
        $this->payment_status_id = 1;
        $this->shipping_status_id = 1;
        $this->notes = $ordernote;
        $this->ordersource = 1;
        $this->location_id = $storeLocation;
        $this->orderNumber = 'S0'.$orderNumber;
        $this->username = $request->full_name;
        $this->phoneno = $request->phone;
        $this->address = $request->address;
        $this->save();
        return $this;
    }
    public function placeOrder($customer_id , $cartTotal,$ordernote,$storeLocation, $request)
    {

        $orderId=Orders::orderBy('id','DESC')->pluck('id')->first();
        $orderNumber=$orderId + 1;


//        dd($orderNumber);
        $this->customer_id = $customer_id;
        $this->total = $cartTotal['total'];
        $this->subtotal = $cartTotal['total'];
        $this->payment_status_id = 1;
        $this->shipping_status_id = 1;
        $this->notes = $ordernote;
        $this->ordersource = 1;
        $this->location_id = $storeLocation;
        $this->orderNumber = 'S0'.$orderNumber;
        $this->save();
        return $this;
    }
    public function apiplaceOrder($customer_id, $cart_total,$cart_sub_total,$storeLocation, $address, $cnumber, $fullname){
        $orderId=Orders::orderBy('id','DESC')->pluck('id')->first();
        $orderNumber=$orderId + 1;
        $orderNumber =  'S0'.$orderNumber;
        $this -> user_id = $customer_id;
        $this -> total = $cart_total;
        $this -> subtotal = $cart_sub_total;
        $this -> payment_status_id = 1;
        $this -> shipping_status_id = 1;
        $this->ordersource = 0;
        $this->location_id = $storeLocation;
        $this -> orderNumber =   $orderNumber;
        $this -> address =   $address;
        $this -> phoneno =   $cnumber;
        $this -> username =   $fullname;
        $this ->save();


        $item_details = Orders::select('id')->where('orderNumber', $orderNumber)->first();
        if ($item_details){
//            UtilityHelper::sendOrderMail($order_num);
            //return response()->json(['success'=>$item_details ,  $orderNumber]);
return $item_details->id." ".$orderNumber;
        }


        else
            return response()->json(['error'=>'somethingWrong']);
    }

    public function ordersprocess()
    {
        return $this->hasOne('App\Models\Admin\Users','id','processed_by');
    }
}
