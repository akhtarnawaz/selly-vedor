<?php

namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Shops extends Model
{
    use SoftDeletes;
    protected $table = 'shops';
    public $timestamps = false;
    protected $fillable = [
        'name',
        'owner',
        'shop_detail',
        ];
    public function owners(){
        return $this->belongsTo('App\Models\Admin\Users','owner','id');
    }
}
