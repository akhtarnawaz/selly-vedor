<?php

namespace App\Models;

use Illuminate\Support\Facades\Config;

class FeaturedProducts extends Model
{
    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable(Config::get('selly.table_names.FeaturedProducts'));
    }

    /**
     * Get the Featured Products record.
     */
    public function productDetails()
    {
        return $this->hasOne(Config::get('selly.models.products'), 'id');
    }
}
