<?php

namespace App\Models;


use Illuminate\Support\Facades\Config;

class Categories extends Model
{

    public function getRouteKeyName()
    {
        return 'cleanURL';
    }

    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable(Config::get('selly.table_names.categories'));
    }

    /**
     * Get the Image record associated with the Categories.
     */
    public function categoryImage()
    {
        return $this->hasOne(Config::get('selly.models.CategoryImages'), 'category_id');
    }

    /**
     * The products that belong to the category.
     */
    public function products()
    {
        return $this->hasMany(Config::get('selly.models.products'), 'category_id');
    }

    /**
     * Find Category By Name
     *
     * @param string
     *
     * @return Categories
     */

    public static function findBySlug($name)
    {
        $category = Categories::where('cleanURL', $name)->first();

        if (!$category) {
            return response()->json('Error', 'Category Not Exist');
        }

        return $category;
    }
}
